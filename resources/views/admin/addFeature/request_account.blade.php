@extends('layout/control_panel')

@section('title')
Manage Account Request
@endsection

@section('middle')
<div class="container-fluid w3-margin">
  <div class="row">

    <div class="col-lg-4 col-md-4 col-sm-12">
      <h3>
        <a href="{{route('admin.request.account')}}" class="w3-text-black">
          <i class="fa fa-paper-plane fa-fw"></i> <strong>Manage Account Request</strong>
        </a>
      </h3>
    </div>

    <div class="col-lg-5 col-md-5"></div>

    <div class="col-lg-3 col-md-3 col-sm-12 w3-margin-top">
      <form action="{{url('admin/request-account/search')}}" method="GET">
        <div class="w3-display-container">
          <input type="text" class="form-control" placeholder="Search" name="q">
          <div class="w3-display-right">
            <button class="btn btn-default w3-light-gray" type="submit">
              <i class="glyphicon glyphicon-search"></i>
            </button>
          </div>
        </div>
      </form>
    </div>

  </div>

  <div class="row w3-margin-top">
    <!-- TABLE -->

    <table class="table table-hover">
      <thead class="w3-teal">
        <tr>
         <th class="w3-center">First Name</th>
         <th class="w3-center">Last Name</th>
         <th class="w3-center">Avatar</th>
         <th class="w3-center">Section</th>
         <th class="w3-center">Year</th>
         <th class="w3-center">Username</th>
         <th class="w3-center">Email</th>
         <th class="w3-center">Mobile No</th>
         <th class="w3-center">Status</th>
         <th class="w3-center">Action</th>
        </tr>
      </thead>
      <tbody class="w3-text-gray">

        @foreach($members as $member)


        <tr>
          <td class="w3-center">{{$member->first_name}}</td>
          <td class="w3-center">{{$member->last_name}}</td>
          <td class="w3-center"><img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) :asset('images/'.$member->avatar)}}" style="width: 64px; height: 64px"></td>
          <td class="w3-center">{{$member->section_code}}</td>
          <td class="w3-center">{{$member->year_desc}}</td>
          <td class="w3-center">{{$member->username}}</td>
          <td class="w3-center">{{$member->email}}</td>
          <td class="w3-center">{{$member->mobile_no}}</td>
          <td class="w3-center">{{$member->status}}</td>
          <td class="w3-center">

            <button class="btn btn-default w3-blue w3-text-white" data-toggle="modal" data-target="#view-{{$member->id}}" title="Review"><i class="fa fa-search"></i> Review</button>
            <button class="btn btn-default w3-green w3-text-white" data-toggle="modal" data-target="#confirm-{{$member->id}}" title="Confirm"><i class="fa fa-thumbs-up"></i> Confirm</button>
            <button class="btn btn-default w3-red" data-toggle="modal" data-target="#reject-{{$member->id}}" title="Reject"><i class="fa fa-thumbs-down"></i> Reject</button>
            <button class="btn btn-default w3-yellow w3-text-black" data-toggle="modal" data-target="#info-{{$member->id}}" title="View"><i class="fa fa-info-circle"></i> Info</button>

          </td>
        </tr>

        <!-- CONFIRMED REQUEST-->

        <div class="modal fade" id="confirm-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title w3-text-gray" id="myModalLabel">Confirm Request</h4>
                      </div>
                    <form action="{{ url('admin/request-account/'.$member->id.'/confirm') }}" method="POST">
                      <div class="modal-body w3-text-black">

                        <p>Are you sure you want to Confirm {{$member->first_name}} {{$member->last_name}}?</p>

                        <div class="modal-footer">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-thumbs-up"></i> Confirm</button>
                        </div>

                      </div>
                    </form>
                    </div>
                  </div>
                </div>

        <!-- END CONFIRM -->

        <!-- REJECT REQUEST-->

        <div class="modal fade" id="reject-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title w3-text-gray" id="myModalLabel">Reject Request</h4>
                      </div>
                    <form action="{{ url('admin/request-account/'.$member->id.'/reject') }}" method="POST">
                      <div class="modal-body w3-text-black">
                     <p>Let's us know why do you want to REJECT '{{$member->first_name}} {{$member->last_name}}'?</p>

                          <div class="form-group">
                            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" required placeholder="Reason" style="border:0px;" rows="3"></textarea>
                          </div>


                        <div class="modal-footer">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <button type="submit" class="btn btn-default w3-red w3-text-white"><i class="fa fa-thumbs-down"></i> Reject</button>
                        </div>

                      </div>
                    </form>
                    </div>
                  </div>
                </div>

        <!-- END REJECT-->

        <!-- VIEW/EDIT MEMBER -->
        <div class="modal fade" id="view-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">List of accounts with the same name</h4>
        </div>
        <div class="modal-body w3-center">
          @foreach($reviews as $review)
          @if(strtolower($review->first_name) == strtolower($member->first_name) and $review->id != $member->id)
          <div class="w3-container">
          <div class="row">
            <img src="{{url('images/'.$review->avatar)}}" style="width: 25%">
            <br>
            <br>
            <h4><strong>Name: {{$review->first_name}} {{$review->last_name}}</strong></h4>
            <p>Section: {{$review->section_code}}</p>
            <p>Status: {{$review->status}}</p>
          </div>
          </div>
          @if(!$loop->last)
          <hr>
          @endif
          @endif
          @endforeach
        </div>
        <div class="modal-footer">

        </div>

        </div>
        </div>
        </div>

        <!-- END VIEW -->

        <div class="modal fade" id="info-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Request Confirmation</h4>
        </div>
        <div class="modal-body">
          <div class="w3-container">
            <div class="row">
            @foreach($confirmations as $confirmed)
            @if($confirmed->req_member_id == $member->id)
            @foreach($action_member as $admin)
              @if($confirmed->admin_member_id == $admin->member->id)
              <p><i class="fa fa-thumbs-o-up fa-fw"></i> {{$admin->member->first_name}} {{$admin->member->last_name}}</p>
              <p>{{$confirmed->created_at}}</p>
              @if(!$loop->last)
              <hr>
              @endif
              @break
              @endif
            @endforeach
            @endif
            @endforeach
            </div>
          </div>
        </div>
        <div class="modal-footer">
        </div>

        </div>
        </div>
        </div>

        <!-- VIEW REQUEST INFORMATION -->
        @endforeach
      </tbody>
    </table>

    <!-- END TABLE -->

  </div>
</div>
@endsection
