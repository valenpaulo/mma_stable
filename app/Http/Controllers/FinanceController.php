<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\File;
use App\Notifications\SystemNotification;
use App\Member;
use App\Section;
use App\Year;
use App\MticsfundPayment;
use App\MticsPurchaseList;
use App\MticsPurchaseEquipList;
use App\Mticsfund;
use App\Course;
use App\MoneyRequest;
use App\Task;
use App\Role;
use App\RoleMember;
use App\ReimburseRequest;
use App\InventoryCategory;
use App\Receipt;
use App\ReceiptMticsitem;
use App\ReceiptEventitem;
use App\MticsPurchaseReport;
use App\EventPurchaseReport;
use App\MticsEvent;
use App\EventAmountBreakdown;
use App\EventPayment;
use App\EventPurchaseList;
use App\EventPurchaseEquipList;
use App\MticsDeposit;
use App\MticsDepositReceipt;
use App\MticsDepositDeny;
use App\MticsDepositReport;
use App\MticsWithdrawal;
use App\MticsWithdrawalReceipt;
use App\MticsWithdrawalDeny;
use App\MticsWithdrawalReport;
use App\AmountValidation;
use App\StepValidation;
use App\FinanceStepValidation;
use App\MticsBudgetExpense;
use App\EventDeposit;
use App\EventDepositReceipt;
use App\EventDepositDeny;
use App\EventDepositReport;
use App\EventWithdrawal;
use App\EventExpense;
use App\EventOffer;
use App\EventMember;

use App\Events\SystemEvent;
use Illuminate\Support\Facades\Event;

use App\Penalty;
use App\Officerterm;

class FinanceController extends Controller
{

//PAYMENTS PAYMENTS PAYMENTS PAYMENTS PAYMENTS


//PAY VIEW //PAY VIEW //PAY VIEW //PAY VIEW //PAY VIEW //PAY VIEW
 	public function pay_view()
 	{

    if(!Gate::allows('ReceivePayment-only') and !Gate::allows('superadmin-only'))
        {
        return redirect('/');
        }

    $formula = new FormulaMoneyController();
    $mticsFundOnHand = $formula->CheckMticsFundPayment();

    $term = Officerterm::where('status','on-going')->first();

    $faculty_id = Section::getId('App\Section','section_code','Faculty');
 		$members = Member::with('year')->where('status','active')->where('section_id','!=',$faculty_id)->get();

    $mticspayments = MticsfundPayment::with('member')->with('admin')->where('updated_at','>=',date($term->created_at))->get();

    $mticsfunds = Mticsfund::with('year')->with('course')->get();
    $external_id = Role::getId('App\Role','name','external');
        
    $role_members = RoleMember::where('role_id',$external_id)->get();
    
    
    if(!$role_members->isEmpty())
    {
       foreach ($role_members as $role_member) {      
          $member_id = $role_member->member_id;
        }
    } 
    else{
      $member_id = '';
    }

    $tasks = Task::where('member_id',$member_id)->with('mticspurchaselist')->with('moneyrequest_all')->get();

    $events = MticsEvent::where('status','on-going')->where('amount_confirm','approved')->get();


    $eventPayments = EventPayment::where('updated_at','>=',date($term->created_at))->get();

    $penalties = Penalty::where('penalty_type','fee')->where('penalty_status','pending')->groupby('member_id')->selectRaw('member_id, sum(fee) as sum')->get();

    $eventoffers = EventOffer::where('offer_status','on-going')->get();

 		return view('officers/payment', compact('members','years','courses','mticsfunds', 'mticspayments','tasks','events', 'eventPayments','penalties','eventoffers', 'mticsFundOnHand'));
 	}

  public function mtics_ajax_call($id) {
    $member = Member::findOrFail($id);
    $fund = Mticsfund::where('year_id', $member->year_id)->where('course_id', $member->section->course_id)->first();

    // dd($fund->mticsfund_amt);
    return $fund->mticsfund_amt;
  }

  public function eventPayment_ajax_call($id) {
    $event = MticsEvent::findOrFail($id);
    dd($event);

    if($event->amount_confirm == 'approved') {
      $amount = $event->event_amount;
    }
    else {
      $amount = 'N/A';
    }
    return $amount;
  }

  public function manageFund_view(Request $request) {

    if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }

    $years = Year::where('year_code','!=','Alumni')->where('year_code','!=','Faculty')->get();
    $courses = Course::where('course_code','!=','Alumni')->where('course_code','!=','Faculty')->get();
    if ($request->q) {
        $yid = Year::where('year_code','LIKE',"%{$request->q}%")->orWhere('year_desc','LIKE',"%{$request->q}%")->first();
        $cid = Course::where('course_code','LIKE',"%{$request->q}%")->orWhere('course_name','LIKE',"%{$request->q}%")->first();

        if(!is_null($yid)){
            $mticsfunds = Mticsfund::where('mticsfund_amt','LIKE',"%{$request->q}%")->orWhere('year_id', $yid->id)->get();
          return view('officers/manage_fund', compact('years','courses','mticsfunds'));
        }

        elseif(!is_null($cid)){
          $mticsfunds = Mticsfund::where('mticsfund_amt','LIKE',"%{$request->q}%")->orWhere('course_id', $cid->id)->get();
          return view('officers/manage_fund', compact('years','courses','mticsfunds'));
        }

        else {
          $mticsfunds = Mticsfund::where('mticsfund_amt','LIKE',"%{$request->q}%")->get();
          return view('officers/manage_fund', compact('years','courses','mticsfunds'));
        }
    }

    $formula = new FormulaMoneyController();
    $cash = $formula->CheckMticsFundPayment();

    $mticsfunds = Mticsfund::all();
    return view('officers/manage_fund', compact('years','courses','mticsfunds', 'cash'));
  }




//PAY-STORE (MTICS FUND PAYMENT) //PAY-STORE (MTICS FUND PAYMENT) //PAY-STORE (MTICS FUND PAYMENT)

  public function pay_mtics_delete(Request $request, $id)
  {
    if(!MticsfundPayment::where('id',$id)->get()->isEmpty())
      {
        $mticspayment = MticsfundPayment::findOrFail($id);
        $mticspayment->delete();
      }
    return redirect()->back();
  }

  public function pay_event_delete(Request $request, $id)
  {
     if(!EventPayment::where('id',$id)->get()->isEmpty())
      {
          $payment = EventPayment::findOrFail($id);
          $payment->delete();
      }
    return redirect()->back();
  }

  public function pay_mtics_store(Request $request)
  {
    //dd($request);
   $this->validate($request, [
    'mtics_payment' => 'required',
    'member_id' => 'required',

    ]);

    $member = Member::find($request->member_id);
    $member_section = Member::find($request->member_id)->section;
    $course_id = $member_section->course_id;
    $year_id = $member->year_id;

      if(!Mticsfund::Where('year_id',$year_id)->Where('course_id',$course_id)->get()->isEmpty())
        {   //GET TOTAL AMOUNT OF MTICS FUND FOR THIS STUDENT
            $mticsfunds = Mticsfund::Where('year_id',$year_id)->Where('course_id',$course_id)->get();
            foreach ($mticsfunds as $mticsfund) {
              $mticsfund_id = $mticsfund->id;
              $member_mtics_fund = $mticsfund->mticsfund_amt;
            }

            //IF THE AMOUNT EXCEED, 0 or NEGATIVE
            if($request->mtics_payment > $member_mtics_fund or $request->mtics_payment < 1)
            {
              return redirect()->back()->withErrors('Mtics Fund for '.$member->first_name.' '.$member->last_name.' is '.$member_mtics_fund.', please check the amount');
            }


            $isStore = false;
            if(MticsfundPayment::where('member_id',$request->member_id)->where('mticsfund_id',$mticsfund_id)->get()->isEmpty())
              {
                //IF THERE IS NO DATA FOR THIS MEMBER WITH MTICS ID
                $isStore = true;
              }
            elseif(MticsfundPayment::where('member_id',$request->member_id)->where('paymticsfund_status','=','balance')->where('mticsfund_id',$mticsfund_id)->get()->isEmpty())
              {
                //IF THE MEMBER ALREADY PAID
                return redirect()->back()->withErrors($member->first_name.' '.$member->last_name.' is already paid');
              }
            elseif(!MticsfundPayment::where('member_id',$request->member_id)->where('paymticsfund_status','=','balance')->where('mticsfund_id',$mticsfund_id)->get()->isEmpty())
              {
                //IF THE MEMBER HAS BALANCE
                $isStore = false;
              }


                $mtics_payment = New MticsfundPayment;
                $mtics_payment->member_id = $request->member_id;
                $mtics_payment->admin_id = Auth::id();
                $mtics_payment->paymticsfund_remarks = $request->remarks;
                $mtics_payment->mticsfund_id = $mticsfund_id;
                $mtics_payment->paymticsfund_amt = $request->mtics_payment;

              if($isStore == true)
              { //TRUE IF THERE IS NO BALANCE FROM PREVIOUS PAYMENT
                $mtics_payment->paymticsfund_amt = $request->mtics_payment;
                if($request->mtics_payment != $member_mtics_fund)
                {
                  $mtics_payment->paymticsfund_status = 'balance';
                }
              }
              elseif($isStore == false)
              { //FALSE IF THERE ARE BALANCE FROM PREV. PAYMENT
                $balances = MticsfundPayment::where('member_id',$request->member_id)->where('paymticsfund_status','=','balance')->where('mticsfund_id',$mticsfund_id)->get();
                $total_balance = 0;
                foreach ($balances as $balance) {
                  //COMPUTING TOTAL BALANCE
                  $total_balance = $total_balance + $balance->paymticsfund_amt;
                 }

                 //COMPUTING TOTAL PAYMENT INCLUDING BALANCE
                 $total_payment = $request->mtics_payment + $total_balance;
                if($total_payment < $member_mtics_fund)
                { //IF THERE ARE STILL BALANCE
                  $mtics_payment->paymticsfund_status = 'balance';
                }
                elseif ($total_payment > $member_mtics_fund) {
                  //IF THE TOTAL PAYMENT IS GREATER THAN MTICS FUND PAYMENT
                  $bal = $member_mtics_fund - $total_balance;
                  return redirect()->back()->withErrors('Mtics Fund balance for '.$member->first_name.' '.$member->last_name.' is '.$bal.', please check the amount');
                }
                elseif($total_payment == $member_mtics_fund)
                { //IF THE TOTAL PAYMENT IS SAME WITH THE MTICS FUND PAYMENT, CHANGE STATUS TO PAID
                    foreach ($balances as $balance) {
                    MticsfundPayment::where('id', $balance->id)->update(array('paymticsfund_status' => 'paid'));
                    }
                }

              }

              $mtics_payment->save();

              $data = array(
                      'message' => "You're paid in the MTICS fund.",
                      'redirect' => "member",
                      'origin' => 'payment',
                      );

              $member->notify(new SystemNotification($data));

              Event::fire(new SystemEvent(auth::id(), 'Paymet Added from ' . $member->username . '.'));

              return redirect('admin/finance/payment')->with('success', 'Payment Added!');
      }
      else
      {
        //IF THE ADMIN DID NOT SET MTICS FUND PAYMENT FOR YEAR AND COURSE
        $year = Year::find($year_id);
        $course = Course::find($course_id);

        $errors[] = 'MTICS Amount for '.$year->year_desc.' '.$course->course_code. ' has not been set (Finance need to set MTICS Amount)';
        return redirect('admin/finance/payment')->withErrors($errors);

      }
  }



//PAY-EVENT-STORE (EVENT PAYMENT) //PAY-EVENT-STORE (EVENT PAYMENT) //PAY-EVENT-STORE (EVENT PAYMENT)

  public function pay_event_store(Request $request)
  { //dd($request);
     $this->validate($request, [
    'event_payment' => 'required',
    'member_id' => 'required',
    'event_id' => 'required',
    ]);

    $member = Member::find($request->member_id);
    $event = MticsEvent::find($request->event_id);
    //check if the amount for this event has set
    if($event->amount_confirm !== 'approved' or $event->event_amount == null)
    {
      return redirect()->back()->withErrors('Amount for this event has not been set');
    }

   //IF THE AMOUNT EXCEED, 0 or NEGATIVE
    if($request->event_payment > $event->event_amount or $request->event_payment < 1)
    {
      return redirect()->back()->withErrors('Amount for this Event is '.$event->event_amount.', please check the amount');
    }


     $isStore = false;
    if(EventPayment::where('member_id',$request->member_id)->where('event_id',$request->event_id)->get()->isEmpty())
      {
        //IF THERE IS NO DATA FOR THIS MEMBER WITH MTICS ID
        $isStore = true;
      }
    elseif(EventPayment::where('member_id',$request->member_id)->where('paymevent_status','=','balance')->where('event_id',$request->event_id)->get()->isEmpty())
      {
        //IF THE MEMBER ALREADY PAID
        return redirect()->back()->withErrors($member->first_name.' '.$member->last_name.' is already paid');
      }
    elseif(!EventPayment::where('member_id',$request->member_id)->where('paymevent_status','=','balance')->where('event_id',$request->event_id)->get()->isEmpty())
      {
        //IF THE MEMBER HAS BALANCE
        $isStore = false;
      }


      $event_payment = New EventPayment;
      $event_payment->member_id = $request->member_id;
      $event_payment->admin_id = Auth::id();
      $event_payment->paymevent_remark = $request->remarks;
      $event_payment->event_id = $request->event_id;
      $event_payment->payevent_amt = $request->event_payment;

        if($isStore == true)
        { //TRUE IF THERE IS NO BALANCE FROM PREVIOUS PAYMENT

          if($request->event_payment != $event->event_amount)
          {
            $event_payment->paymevent_status = 'balance';
          }

        }
        elseif($isStore == false)
        { //FALSE IF THERE ARE BALANCE FROM PREV. PAYMENT
          $balances = EventPayment::where('member_id',$request->member_id)->where('paymevent_status','=','balance')->where('event_id',$request->event_id)->get();
          $total_balance = 0;
          foreach ($balances as $balance) {
            //COMPUTING TOTAL BALANCE
            $total_balance = $total_balance + $balance->payevent_amt;
           }

           //COMPUTING TOTAL PAYMENT INCLUDING BALANCE
           $total_payment = $request->event_payment + $total_balance;
          if($total_payment < $event->event_amount)
          { //IF THERE ARE STILL BALANCE
            $event_payment->paymevent_status = 'balance';
          }
          elseif ($total_payment > $event->event_amount) {
            //IF THE TOTAL PAYMENT IS GREATER THAN EVENT FUND PAYMENT
            $bal = $event->event_amount - $total_balance;
            return redirect()->back()->withErrors('Event '.$event->event_title.' balance, for '.$member->first_name.' '.$member->last_name.' is '.$bal.', please check the amount');
          }
          elseif($total_payment == $event->event_amount)
          { //IF THE TOTAL PAYMENT IS SAME WITH THE MTICS FUND PAYMENT, CHANGE STATUS TO PAID
          // dd($total_payment);
              foreach ($balances as $balance) {
              EventPayment::where('id', $balance->id)->update(array('paymevent_status' => 'paid'));
              }
          }

        }

          $event_payment->save();

          $data = array(
                  'message' => "You're paid in the Event Payment.",
                  'redirect' => "member",
                  'origin' => 'payment',
                  );

          $member->notify(new SystemNotification($data));

          Event::fire(new SystemEvent(auth::id(), 'Payment Added from ' . $member->username . '.'));

          return redirect('admin/finance/payment');


  }








  // EXCESS MONEY FROM EXTERNAL // EXCESS MONEY FROM EXTERNAL // EXCESS MONEY FROM
  public function excess_money(Request $request)
  {
      $this->validate($request, [
          'task_id' => 'required',
          'amount' => 'required',
          ]);

    $task = Task::find($request->task_id);
    if($task->event_id == null)
    {
      $purchaselist_total = MticsPurchaseList::where('task_id',$request->task_id)->get()->sum('mtics_total_price');
    }
    else
    {
      $purchaselist_total = EventPurchaseList::where('task_id',$request->task_id)->get()->sum('event_total_price');
    }

    $moneyrequest = MoneyRequest::where('task_id',$request->task_id)->first();
    $expected_excess = $moneyrequest->amount - $purchaselist_total;

    //dd($expected_excess);
    if($moneyrequest->amount <= $purchaselist_total){
      return redirect()->back()->withErrors('There is no excess money for this task');
    }
    elseif($request->amount == $expected_excess){
      MoneyRequest::where('task_id', $request->task_id)->update(array('excess_amount' => $request->amount));
      MoneyRequest::where('task_id', $request->task_id)->update(array('admin_receive_id' => Auth::id()));
      MoneyRequest::where('task_id', $request->task_id)->update(array('mon_req_status' => 'for validation'));
    }
    else{
      return redirect()->back()->withErrors('The excess amount for this task is P'.$expected_excess.'.00, please return exact excess amount');
    }


    Event::fire(new SystemEvent(auth::id(), 'Received Excess Money.'));

    return redirect()->back()->with('success', 'Received!');

  }








//MANGE MTICS FUND PAYMENT //MANGE MTICS FUND PAYMENT //MANGE MTICS FUND PAYMENT //MANGE MTICS FUND

  public function mtics_store(Request $request)
  {
      $this->validate($request, [
          'year' => 'required',
          'course' => 'required',
          'mticsfund_amt' => 'required',
          ]);

      foreach ($request->course as $key => $value) {


   //IF YEAR IS INVALID WITH SELECTED SECTION
         $course = Course::find($value);
         $year =  Year::find($request->year);

         if($course->course_year_count < $year->year_num)
         {
             $errors[] = $year->year_desc. ' is not valid with '. $course->course_code;
             continue;
           // return redirect('admin/finance/payment')->withErrors($errors);
         }

      if(Mticsfund::where('year_id',$request->year)->where('course_id',$value)->get()->isEmpty())
        {

          Mticsfund::create([
                'year_id' => $request->year,
                'course_id' => $value,
                'mticsfund_amt' => $request->mticsfund_amt,
            ]);
        }
      else
      {
        $mticsfunds = Mticsfund::where('year_id',$request->year)->where('course_id',$value)->get();
        foreach ($mticsfunds as $mticsfund) {
          $id = $mticsfund->id;
        }

        $year = Year::find($request->year);
        $course = Course::find($value);

      $errors[] = 'Mtics Fund Payment for '.$year->year_desc.' '.$course->course_code.' has been added ';
      //return redirect('admin/finance/payment')->withErrors($errors);
      continue;
      }



      }


         if (isset($errors)){
            return redirect('admin/finance/manage-mtics-payment')->withErrors($errors);
        }


        Event::fire(new SystemEvent(auth::id(), 'Added Payment Information for ' . $course->course_name . '.'));

        return redirect()->back()->with('success', 'Payment Information Added!');
  }

  public function mtics_edit(Request $request,$id)
  {
    $this->validate($request, [
          'year' => 'required',
          'course' => 'required',
          'mticsfund_amt' => 'required',
          ]);

       //IF YEAR IS INVALID WITH SELECTED SECTION
         $course = Course::find($request->course);
         $year =  Year::find($request->year);

         if($course->course_year_count < $year->year_num)
         {
             $errors[] = $year->year_desc. ' is not valid with your selected section';
            return redirect('admin/finance/manage-mtics-payment')->withErrors($errors);
         }

    $mticsfund = Mticsfund::find($id);
    if(!MticsfundPayment::where('mticsfund_id',$id)->get()->isEmpty())
      {
      return redirect()->back()->withErrors('Some student already paid for this payment, Cannot Edit this MTICS Payment ');
      }

    $mticsfund->year_id = $request->year;
    $mticsfund->course_id = $request->course;
    $mticsfund->mticsfund_amt = $request->mticsfund_amt;
    $mticsfund->save();


    Event::fire(new SystemEvent(auth::id(), 'Edited Payment Information for ' . $course->course_name . '.'));

    return redirect()->back()->with('success', 'Payment Information Edited!');
  }


 public function mtics_delete(Request $request,$id)
  {

    $mticsfund = Mticsfund::findOrFail($id);
    if(!MticsfundPayment::where('mticsfund_id',$id)->get()->isEmpty())
      {
      return redirect()->back()->withErrors('Some student already paid for this payment, Cannot Delete this MTICS Payment ');
      }

    Event::fire(new SystemEvent(auth::id(), 'Deleted Payment Information for ' . $mticsfund->course->course_name . '.'));
    $mticsfund->delete();

    return redirect()->back()->with('success', 'Payment Information Deleted!');

  }







//REQUESTED-MONEY //REQUESTED-MONEY //REQUESTED-MONEY //REQUESTED-MONEY //REQUESTED-MONEY //REQUESTED-MONEY

   public function req_money_view()
  {
    if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }
    $moneyrequests = MoneyRequest::where('mon_req_status','pending')->whereHas('task', function($q) {
      $q->where('event_id', null);
    })->get();

    // validation for request


    $requestValidation = MoneyRequest::where(function($q) {
      $q->where('mon_req_status', 'ongoing')->orWhere('mon_req_status', 'for validation');
    })->whereHas('task', function($q) {
      $q->where('event_id', null);
    })->whereHas('task', function($q) {
      $q->where('task_status', 'done')->orwhere('task_status', 'failed');
    })->get();

    $term = Officerterm::where('status','on-going')->first();

    // transaction
    $transactions = MoneyRequest::where('mon_req_status', '!=', 'pending')->whereHas('task', function($q) use($term) {
      $q->where('event_id', null)->where('updated_at','>=',date($term->created_at));
    })->get();

    // mtics budget
    $budget = new FormulaMoneyController;
    $mticsBudget = $budget->CheckMticsFundBudget();

    // external's budget
    $externalBudget = MoneyRequest::where('mon_req_status', 'ongoing')->sum('amount');

    return view('officers/requested_money',compact('moneyrequests', 'mticsBudget', 'externalBudget', 'requestValidation', 'transactions'));
  }


//ADD FARE //ADD FARE //ADD FARE //ADD FARE
   public function addfare(Request $request, $id)
  {//dd($request);
    $this->validate($request, [
          'est_fare' => 'required|integer',
          ]);

     if($request->est_fare < 1)
    {
        return redirect()->back()->withErrors('estimated price can not be 0 or negative');

    }

    $task = Task::find($id);
    if($task->event_id == null)
    {
        if(!MticsPurchaseList::where('task_id',$id)->where('mtics_itemname','fare')->get()->isEmpty())
          {
            return redirect()->back()->withErrors('You already add fare for this request');
          }
        $mticspurchaselist = New MticsPurchaseList;
        $mticspurchaselist->task_id = $id;
        $mticspurchaselist->mtics_itemname = 'fare';
        $mticspurchaselist->mtics_orig_quan = 1;
        $mticspurchaselist->mtics_est_price = $request->est_fare;
        $mticspurchaselist->mtics_inv_status = 'unable to move';
        $mticspurchaselist->save();

    }
    else
    {
        if(!EventPurchaseList::where('task_id',$id)->where('event_itemname','fare')->get()->isEmpty())
          {
            return redirect()->back()->withErrors('You already add fare for this request');
          }
        $eventpurchaselist = New EventPurchaseList;
        $eventpurchaselist->task_id = $id;
        $eventpurchaselist->event_itemname = 'fare';
        $eventpurchaselist->event_orig_quan = 1;
        $eventpurchaselist->event_est_price = $request->est_fare;
        $eventpurchaselist->event_inv_status = 'unable to move';
        $eventpurchaselist->save();

    }



    $moneyrequest = MoneyRequest::where('task_id',$id)->first();
    $amount = $moneyrequest->amount;
    $total_amt = $amount + $request->est_fare;
    $moneyrequest->amount = $total_amt;
    $moneyrequest->save();

    Event::fire(new SystemEvent(auth::id(), 'Miscellaneous Added.'));

    return redirect()->back()->with('success', 'Miscellaneous Added!');
  }

//ADD MEAL //ADD MEAL //ADD MEAL //ADD MEAL //ADD MEAL
  public function addmeal(Request $request, $id)
  {//dd($request);
    $this->validate($request, [
          'est_meal' => 'required|integer',
          ]);

     if($request->est_meal < 1)
    {
        return redirect()->back()->withErrors('estimated price can not be 0 or negative');

    }

    $task = Task::find($id);
    if($task->event_id == null)
    {
        if(!MticsPurchaseList::where('task_id',$id)->where('mtics_itemname','meal')->get()->isEmpty())
          {
            return redirect()->back()->withErrors('You already add meal for this request');
          }

        $mticspurchaselist = New MticsPurchaseList;
        $mticspurchaselist->task_id = $id;
        $mticspurchaselist->mtics_itemname = 'meal';
        $mticspurchaselist->mtics_orig_quan = 1;
        $mticspurchaselist->mtics_est_price = $request->est_meal;
        $mticspurchaselist->mtics_act_quan = 1;
        $mticspurchaselist->mtics_act_price = $request->est_meal;
        $mticspurchaselist->mtics_inv_status = 'unable to move';
        $mticspurchaselist->save();
    }
    else
    {
         if(!EventPurchaseList::where('task_id',$id)->where('event_itemname','meal')->get()->isEmpty())
          {
            return redirect()->back()->withErrors('You already add meal for this request');
          }

        $eventpurchaselist = New EventPurchaseList;
        $eventpurchaselist->task_id = $id;
        $eventpurchaselist->event_itemname = 'meal';
        $eventpurchaselist->event_orig_quan = 1;
        $eventpurchaselist->event_est_price = $request->est_meal;
        $eventpurchaselist->event_act_quan = 1;
        $eventpurchaselist->event_act_price = $request->est_meal;
        $eventpurchaselist->event_inv_status = 'unable to move';
        $eventpurchaselist->save();
    }



    $moneyrequest = MoneyRequest::where('task_id',$id)->first();
    $amount = $moneyrequest->amount;
    $total_amt = $amount + $request->est_meal;
    $moneyrequest->amount = $total_amt;
    $moneyrequest->save();

    Event::fire(new SystemEvent(auth::id(), 'Miscellaneous Added.'));

    return redirect()->back()->with('success', 'Miscellaneous Added!');
  }


   //EDIT ESTIMATED PRICE //EDIT ESTIMATED PRICE //EDIT ESTIMATED PRICE

  public function est_price_edit(Request $request, $id,$task_id)
  {

    $this->validate($request, [
          'est_price' => 'required|integer',
          ]);

    if($request->est_price < 1)
    {
        return redirect()->back()->withErrors('estimated price can not be 0 or negative');

    }

    $task = Task::find($task_id);
    if($task->event_id == null)
    {
      $mticspurchaselist = MticsPurchaseList::find($id);
      $moneyrequest = MoneyRequest::where('task_id',$mticspurchaselist->task_id)->first();
      $amount = $moneyrequest->amount;
      $prev_total = $mticspurchaselist->mtics_est_price * $mticspurchaselist->mtics_orig_quan;
      $total_amt = ($amount + ($request->est_price * $mticspurchaselist->mtics_orig_quan)) - $prev_total;
      $moneyrequest->amount = $total_amt;
      $moneyrequest->save();
      MticsPurchaseList::where('id', $id)->update(array('mtics_est_price' => $request->est_price));
    }
    else
    {
      $eventpurchaselist = EventPurchaseList::find($id);
      $moneyrequest = MoneyRequest::where('task_id',$eventpurchaselist->task_id)->first();
      $amount = $moneyrequest->amount;
      $prev_total = $eventpurchaselist->event_est_price * $eventpurchaselist->event_orig_quan;
      $total_amt = ($amount + ($request->est_price * $eventpurchaselist->event_orig_quan)) - $prev_total;
      $moneyrequest->amount = $total_amt;
      $moneyrequest->save();
      EventPurchaseList::where('id', $id)->update(array('event_est_price' => $request->est_price));
    }

    Event::fire(new SystemEvent(auth::id(), 'Indicated an Estimate Price.'));

    return redirect()->back()->with('success', 'Done!');
  }


//ACCEPT //ACCEPT //ACCEPT //ACCEPT //ACCEPT //ACCEPT
  public function accept(Request $request, $id)
  {
    $moneyrequest = MoneyRequest::where('task_id', $id)->first();
    $task = Task::find($id);

    if($moneyrequest->amount < 1){
        return redirect()->back()->withErrors('Request cannot be negative or 0, please input estimated price');
    }


    if($task->event_id == null)
    {
        $budget = new FormulaMoneyController;
        $mtics_fund = $budget->CheckMticsFundBudget();
        $total = $mtics_fund - $moneyrequest->amount;

    }
    else
    {   $eventbudget = New FormulaMoneyController;
        $event_fund = $eventbudget->CheckEventFundBudget($task->event_id);
        $total = $event_fund - $moneyrequest->amount;
    }

        if($total < 0){

          return redirect()->back()->withErrors('Request denied, Budget Fund will be negative');
        }
        elseif($moneyrequest->amount < 0){
          return redirect()->back()->withErrors('Check the amount of request, cannot be negative');
        }
        else{
        MoneyRequest::where('task_id', $id)->update(array('mon_req_status' => 'ongoing'));
        MoneyRequest::where('task_id', $id)->update(array('admin_affirm_id' => Auth::id()));
        Task::where('id', $id)->update(array('task_status' => 'on-going'));

        }
    if($task->event_id != NULL){
      $data = array(
        'message' => "Finance accepted your request",
        'redirect' => array('role' => 'external', 'event_id' => $task->event_id),
        'origin' => 'finance',
        );

      $role_id = Role::where('name', 'external')->first()->id;
      $member = Member::whereHas('role', function($q) use ($role_id) {
          $q->where('role_id', $role_id);
      })->first();
      $member->notify(new SystemNotification($data));
    } else {
      $data = array(
        'message' => "Approved money request.",
        'redirect' => "admin/external/task/" . $id,
        'origin' => 'approve-money-request',
        );

      $role_id = Role::where('name', 'external')->first()->id;
      $member = Member::whereHas('role', function($q) use ($role_id) {
          $q->where('role_id', $role_id);
      })->first();
      $member->notify(new SystemNotification($data));
    }
    Event::fire(new SystemEvent(auth::id(), 'Approved Money Request.'));

    return redirect()->back()->with('success', 'Approved!');
  }

//DENY //DENY //DENY //DENY //DENY //DENY //DENY //DENY //DENY
  public function deny(Request $request, $id)
  {
    $this->validate($request, [
    'deny_reason' => 'required',
    ]);
    MoneyRequest::where('task_id', $id)->update(array('mon_req_status' => 'denied'));
    MoneyRequest::where('task_id', $id)->update(array('admin_confirm_id' => Auth::id()));
    MoneyRequest::where('task_id', $id)->update(array('denied_reason' => $request->deny_reason));
    Task::where('id', $id)->update(array('task_status' => 'cancel'));


      $data = array(
        'message' => "Denied money request.",
        'redirect' => "admin/external/task/" . $id,
        'origin' => 'approve-money-request',
        );

      $role_id = Role::where('name', 'external')->first()->id;
      $member = Member::whereHas('role', function($q) use ($role_id) {
          $q->where('role_id', $role_id);
      })->first();
      $member->notify(new SystemNotification($data));

    Event::fire(new SystemEvent(auth::id(), 'Denied Money Request.'));


    return redirect('admin/finance/requested-money')->with('success', 'Denied!');
  }






//MTICS-FUND //MTICS-FUND //MTICS-FUND //MTICS-FUND //MTICS-FUND //MTICS-FUND

  public function mticsfund_view()
  {
    $payments = MticsfundPayment::with('member')->with('admin')->get();
    $formula = new FormulaMoneyController();
    $mticsfund_money = $formula->CheckMticsFundPayment();
    return view('officers/mtics_fund', compact('payments','mticsfund_money'));
  }

  //PURCHASED-ITEMS //PURCHASED-ITEMS //PURCHASED-ITEMS //PURCHASED-ITEMS //PURCHASED-ITEMS

 /* public function purchased_item_view()
  {
    //EXTERNAL MEMBER_ID
    $external_id = Role::getId('App\Role','name','external');
    $role_members = RoleMember::where('role_id',$external_id)->get();
    foreach ($role_members as $role_member) {
      $member_id = $role_member->member_id;
    }

    $receipts = Receipt::with('mticspurchaselist')->get();
    $tasks = Task::with('moneyrequest_ongoing')->with('mticspurchaselist')->with('eventpurchaselist')->where('member_id',$member_id)->where('task_status','done')->orwhere('task_status','for validation')->orwhere('task_status','failed')->get();
    $mtics_purchases = MticsPurchaseList::with('mticspurchaseequiplist')->where('mtics_itemstatus','purchased')->get();
    return view('officers/finance_purchased_items',compact('tasks','mtics_purchases','receipts'));
  }*/

  public  function purchased_item_upload_receipt(Request $request,$id)
  {

      $task = Task::find($id);
      if($task->event_id == null)
      {
          $this->validate($request, [
            'receipt' => 'image|mimes:jpeg,jpg,png|max:2000', //image only
            'mticspurchaselist_id' => 'required',
          ]);

            $mtics_item_id = null;
            $receipt_array = null;
            foreach ($request->mticspurchaselist_id as $key => $value) {
              if(!ReceiptMticsitem::where('mtics_purchase_list_id',$value)->get()->isEmpty())
                {
                  //delete receipt if exist
                  $receitmtics = ReceiptMticsitem::where('mtics_purchase_list_id',$value)->first();
                  $receipt = Receipt::findOrFail($receitmtics->receipt_id);
                  $receitmtics->delete();
                  $receipt_array[] = $receipt->id;
                }
                  $mtics_item_id[]=$value;
            }

            if($receipt_array !== null){
              foreach ($receipt_array as $key => $value) {

                  if(ReceiptMticsitem::where('receipt_id',$value)->get()->isEmpty())
                    {
                      if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                      File::delete('public/images/' . $receipt->receipt_image);
                      }
                      else{
                      File::delete('images/' . $receipt->receipt_image);
                      }
                      Receipt::where('id',$value)->delete();
                    }
              }
            }

            if($mtics_item_id)
            {
              // image upload in public/images/receipts folder.
                $name = "";
                foreach ($request->mticspurchaselist_id as $key => $value) {
                  $name = $name . $value;
                }
                $date = date('Y-m-d');
                $time = time();
                $file = $request->file('receipt');
                if($request->hasFile('receipt')){

                  if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                  $file->move('public/images/receipts', $task->id.'_'.$name.'_'.$time.'_'.$date.'_receipt.jpg');
                  }
                  else{
                  $file->move('images/receipts', $task->id.'_'.$name.'_'.$time.'_'.$date.'_receipt.jpg');
                  }

                  $imgname =  "receipts/".$task->id.'_'.$name.'_'.$time.'_'.$date.'_receipt.jpg';


                  $receipt = New Receipt;
                  $receipt->receipt_image = $imgname;
                  $receipt->admin_id = Auth::id();
                  $receipt->receipt_remarks = $request->remarks;
                  $receipt->save();
                  foreach ($mtics_item_id as $key => $value) {
                    $receiptmticsitem = New ReceiptMticsitem;
                    $receiptmticsitem->receipt_id = $receipt->id;
                    $receiptmticsitem->mtics_purchase_list_id = $value;
                    $receiptmticsitem->save();
                  }
                }
            }
      }
      else
      {
        $this->validate($request, [
            'receipt' => 'image|mimes:jpeg,jpg,png|max:2000', //image only
            'eventpurchaselist_id' => 'required',
          ]);

        $event_item_id = null;
            foreach ($request->eventpurchaselist_id as $key => $value) {
              if(!ReceiptEventitem::where('event_purchase_list_id',$value)->get()->isEmpty())
                {
                  //delete receipt if exist
                  $receitevent = ReceiptEventitem::where('event_purchase_list_id',$value)->first();
                  $receipt = Receipt::findOrFail($receitevent->receipt_id);
                  if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                  File::delete('public/images/' . $receipt->receipt_image);
                  }
                  else{
                  File::delete('images/' . $receipt->receipt_image);
                  }
                  $receipt->delete();
                  $receitevent->delete();

                }


                  $event_item_id[]=$value;
            }


            if($event_item_id)
            {
              // image upload in public/images/receipts folder.
                $name = "";
                foreach ($request->eventpurchaselist_id as $key => $value) {
                  $name = $name . $value;
                }
                $date = date('Y-m-d');
                $time = time();
                $file = $request->file('receipt');
                if($request->hasFile('receipt')){



                  if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                  $file->move('public/images/receipts', $task->id.'_'.$name.'_'.$time.'_'.$date.'_receipt.jpg');
                  }
                  else{
                  $file->move('images/receipts', $task->id.'_'.$name.'_'.$time.'_'.$date.'_receipt.jpg');
                  }

                  $imgname =  "receipts/".$task->id.'_'.$name.'_'.$time.'_'.$date.'_receipt.jpg';


                  $receipt = New Receipt;
                  $receipt->receipt_image = $imgname;
                  $receipt->admin_id = Auth::id();
                  $receipt->receipt_remarks = $request->remarks;
                  $receipt->save();
                  foreach ($event_item_id as $key => $value) {
                    $receipteventitem = New ReceiptEventitem;
                    $receipteventitem->receipt_id = $receipt->id;
                    $receipteventitem->event_purchase_list_id = $value;
                    $receipteventitem->save();
                  }
                }
            }

      }

     if (isset($errors)){
        return redirect()->back()->withErrors($errors);
    }

    Event::fire(new SystemEvent(auth::id(), 'Uploaded Receipt.'));

    return redirect()->back()->with('success', 'Receipt Uploaded!');
  }



  public function purchased_done(Request $request,$id)
  {

    $task = Task::with('mticspurchaselist')->with('eventpurchaselist')->where('id',$id)->first();

    if($task->event_id == null)
    {
        $mticspurchaselists = MticsPurchaseList::where('task_id',$id)->get();
        foreach($mticspurchaselists as $mticspurchaselist){
          if(ReceiptMticsitem::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->isEmpty())
            {
            return redirect()->back()->withErrors('You cannot clear this request, please upload receipt for other item');
            }
          if(!MticsPurchaseReport::where('mtics_purchase_list_id',$mticspurchaselist->id)->where('report_category','critical')->where('solution_fr_critical',null)->get()->isEmpty())
          {
          return redirect()->back()->withErrors('You cannot clear this request, has Critical Report');
          }
        }

        //check if good to clear this request
        $moneyrequest = MoneyRequest::where('task_id',$id)->first();
        $reimburse = ReimburseRequest::where('money_request_id',$moneyrequest->id)->first();
        $isMoneyzero = false;
        if($moneyrequest->excess_amount == null)
        { //If there is still pending reimbursement request
          if($reimburse !== null)
          {
            if($reimburse->reimburse_status == 'pending')
            {
              return redirect('/admin/finance/requested-money/validate/'.$id.'/step-3')->withErrors('Has pending reimbursement request, check the reimbursement request');
            }
            elseif($reimburse->reimburse_status == 'denied')
            {
              $isMoneyzero = true;
            }
            elseif($reimburse->reimburse_status == 'accepted')
            {
              $isMoneyzero = true;
            }
          }
          else
          { //Check if the money bucket is 0 amount already
            //Compute External Moneybucket
            $moneyrequest = MoneyRequest::where('task_id',$id)->first();
            $sum_equip_price = 0;
            $sum_price = 0;
            foreach ($task->mticspurchaselist as $mticspurchaselist) {
              if(!MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->isEmpty()){
                $sum_equip_price = $sum_equip_price + MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->sum('mtics_equip_price');
              }
              else{
                $sum_price = $sum_price + $mticspurchaselist->mtics_total_price;
              }
            }
            $total_deduction = $sum_price + $sum_equip_price;
            //COMPUTING TOTAL MONEY BUCKET
            $total_money_bucket = $moneyrequest->amount - $total_deduction;
            if($total_money_bucket == 0)
            {
              $isMoneyzero = true;
            }
            elseif($total_money_bucket < 0){
              return redirect()->back()->withErrors('Money Bucket is negative, external needs to send a reimbursement request');
            }else{
              return redirect()->back()->withErrors('There is Excess Money; External needs to return excess money first');
            }
          }

        }
        else
        {
          $isMoneyzero = true;
        }

        if($isMoneyzero == true)
        {
          if($task->task_status !== 'for validation')
          {
            MoneyRequest::where('task_id', $id)->update(array('mon_req_status' => 'cleared'));
            MoneyRequest::where('task_id', $id)->update(array('admin_confirm_id' => Auth::id()));

            $validation = AmountValidation::where('money_request_id', $moneyrequest->id)->where('status', 'ongoing')->first();

            $step = StepValidation::where('validation_id', $validation->id)->where('status', 'current')->first();

            $step->status = 'done';
            $validation->status = 'successful';

            $validation->save();
            $step->save();

            Event::fire(new SystemEvent(auth::id(), 'Money Request Cleared.'));

            return redirect('admin/finance/requested-money')->with('success', 'Cleared!');

          }
          else
          {
            return redirect()->back()->withErrors('Items are for validation by Logistics');
          }
        }
    }
    else
    {
        $eventpurchaselists = EventPurchaseList::where('task_id',$id)->get();
        foreach($eventpurchaselists as $eventpurchaselist){
          if(ReceiptEventitem::where('event_purchase_list_id',$eventpurchaselist->id)->get()->isEmpty())
            {
            return redirect()->back()->withErrors('You cannot clear this request, please upload receipt for other item');
            }
          if(!EventPurchaseReport::where('event_purchase_list_id',$eventpurchaselist->id)->where('report_category','critical')->where('solution_fr_critical',null)->get()->isEmpty())
            {
            return redirect()->back()->withErrors('You cannot clear this request, has Critical Report');
            }

        }

        //check if good to clear this request
        $moneyrequest = MoneyRequest::where('task_id',$id)->first();
        $reimburse = ReimburseRequest::where('money_request_id',$moneyrequest->id)->first();
        $isMoneyzero = false;
        if($moneyrequest->excess_amount == null)
        { //If there is still pending reimbursement request
          if($reimburse !== null)
          {
            if($reimburse->reimburse_status == 'pending')
            {
              return redirect()->back()->withErrors('Has pending reimbursement request, check the reimbursement request');
            }
            elseif($reimburse->reimburse_status == 'denied')
            {
              $isMoneyzero = true;
            }
            elseif($reimburse->reimburse_status == 'accepted')
            {
              $isMoneyzero = true;
            }
          }
          else
          { //Check if the money bucket is 0 amount already
            //Compute External Moneybucket
            $moneyrequest = MoneyRequest::where('task_id',$id)->first();
            $sum_equip_price = 0;
            $sum_price = 0;
            foreach ($task->eventpurchaselist as $eventpurchaselist) {
              if(!EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get()->isEmpty()){
                $sum_equip_price = $sum_equip_price + EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get()->sum('event_equip_price');
              }
              else{
                $sum_price = $sum_price + $eventpurchaselist->event_total_price;
              }
            }

            $total_deduction = $sum_price + $sum_equip_price;
            //COMPUTING TOTAL MONEY BUCKET
            $total_money_bucket = $moneyrequest->amount - $total_deduction;
            if($total_money_bucket == 0)
            {
              $isMoneyzero = true;
            }
            elseif($total_money_bucket < 0){
              return redirect()->back()->withErrors('Money Bucket is negative, external needs to send a reimbursement request');
            }else{
              return redirect()->back()->withErrors('There is Excess Money; External needs to return excess money first');
            }
          }

        }
         else
        {
          $isMoneyzero = true;
        }

        if($isMoneyzero == true)
        {
          if($task->task_status !== 'for validation')
          {
            MoneyRequest::where('task_id', $id)->update(array('mon_req_status' => 'cleared'));
            MoneyRequest::where('task_id', $id)->update(array('admin_confirm_id' => Auth::id()));

            $validation = AmountValidation::where('money_request_id', $moneyrequest->id)->where('status', 'ongoing')->first();

            $step = StepValidation::where('validation_id', $validation->id)->where('status', 'current')->first();
            $step->status = 'done';
            $validation->status = 'successful';

            $validation->save();
            $step->save();
          }
          else
          {
            return redirect()->back()->withErrors('Items are for validation by Logistics');
          }
        }




    }

    Event::fire(new SystemEvent(auth::id(), 'Money Request Cleared.'));


   return redirect('admin/finance/manage-event/' . $task->event_id)->with('success', 'Cleared!');

  }



  public function purchased_change(Request $request,$id)
  {

          // image upload in public/images/receipts folder.
          $file = $request->file('receipt');

          if($file == null)
          {
            return redirect()->back()->withErrors('Select an image');
          }

          foreach ($file as $key => $value) {
            if($value['image'])
            {
              $image = $value['image'];
              foreach ($request->receipt as $key1 => $valuereceipt) {
                if($key == $key1)
                {


                  if($image->getClientSize() == 0)
                  {
                    return redirect()->back()->withErrors('Maximum size of image is 2000kb only');
                  }

                  $task = Task::find($id);
                  $date = date('Y-m-d');
                  $time = time();

                   if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                      $image->move('public/images/receipts', $task->id.'_'.$valuereceipt['item_id'].'_'.$time.'_'.$date.'_receipt.jpg');
                    }
                    else{
                      $image->move('images/receipts', $task->id.'_'.$valuereceipt['item_id'].'_'.$time.'_'.$date.'_receipt.jpg');
                    }

                  $imgname =  "receipts/".$task->id.'_'.$valuereceipt['item_id'].'_'.$time.'_'.$date.'_receipt.jpg';

                  $receipt = New Receipt;
                  $receipt->receipt_image = $imgname;
                  $receipt->admin_id = Auth::id();
                  $receipt->save();
                  if($task->event_id == null)
                  {
                      ReceiptMticsitem::create([
                          'receipt_id' => $receipt->id,
                          'mtics_purchase_list_id' => $valuereceipt['item_id'],
                      ]);

                      $receiptmticsitem = ReceiptMticsitem::where('receipt_id',$valuereceipt['orig_receipt'])->where('mtics_purchase_list_id',$valuereceipt['item_id'])->first();
                      $receiptmticsitem->delete();
                  }
                  else
                  {
                       ReceiptEventitem::create([
                          'receipt_id' => $receipt->id,
                          'event_purchase_list_id' => $valuereceipt['item_id'],
                      ]);

                      $receipteventitem = ReceiptEventitem::where('receipt_id',$valuereceipt['orig_receipt'])->where('event_purchase_list_id',$valuereceipt['item_id'])->first();
                      $receipteventitem->delete();
                  }


                  $this->CheckReceipt($valuereceipt['orig_receipt'],$id);
                }
             }
          }

        }
   return redirect('admin/finance/purchased-items');

  }



  private function CheckReceipt($receipt_id,$task_id)
  {
    $task = Task::find($task_id);
    if($task->event_id == null)
    {
      if(ReceiptMticsitem::where('receipt_id',$receipt_id)->get()->isEmpty())
        {
          $receipt = Receipt::find($receipt_id);
          if(url('/') == 'http://mtics-ma.tuptaguig.com'){
          File::delete('public/images/' . $receipt->receipt_image);
          }
          else{
          File::delete('images/' . $receipt->receipt_image);
          }
          $receipt->delete();
        }
    }
    else
    {
      if(ReceiptEventitem::where('receipt_id',$receipt_id)->get()->isEmpty())
        {
          $receipt = Receipt::find($receipt_id);
          if(url('/') == 'http://mtics-ma.tuptaguig.com'){
          File::delete('public/images/' . $receipt->receipt_image);
          }
          else{
          File::delete('images/' . $receipt->receipt_image);
          }
          $receipt->delete();
        }
    }
  }


   public function purchased_report(Request $request,$task_id,$id)
  {

    $task = Task::find($task_id);
    $this->validate($request, [
        'report_name' => 'required',
        'report_reason' => 'required',
        'external_explain' => 'required',
        ]);

    if($task->event_id == null)
    {

        if(!MticsPurchaseReport::where('reportedby','finance')->where('mtics_purchase_list_id',$id)->get()->isEmpty()){
          return redirect()->back()->withErrors('you already reported this item');
        }
        $mticspurchaselist = MticsPurchaseList::find($id);
        $logisticsmticsreport = New MticsPurchaseReport;
        $logisticsmticsreport->admin_id = Auth::id();
        $logisticsmticsreport->reportedby = 'finance';
        $logisticsmticsreport->mtics_purchase_list_id = $id;
        $logisticsmticsreport->report_name = $request->report_name;
        $logisticsmticsreport->report_reason = $request->report_reason;
        $logisticsmticsreport->external_explanation = $request->external_explain;
        $logisticsmticsreport->report_category = 'critical';
        $logisticsmticsreport->save();

        if($task->task_status !== 'for validation')
        {
          Task::where('id', $task_id)->update(array('task_status' => 'for validation'));
        }

    }
    else
    {

        if(!EventPurchaseReport::where('reportedby','finance')->where('event_purchase_list_id',$id)->get()->isEmpty()){
          return redirect()->back()->withErrors('you already reported this item');
        }
        $eventpurchaselist = EventPurchaseList::find($id);
        $logisticseventreport = New EventPurchaseReport;
        $logisticseventreport->admin_id = Auth::id();
        $logisticseventreport->reportedby = 'finance';
        $logisticseventreport->event_purchase_list_id = $id;
        $logisticseventreport->report_name = $request->report_name;
        $logisticseventreport->report_reason = $request->report_reason;
        $logisticseventreport->external_explanation = $request->external_explain;
        $logisticseventreport->report_category = 'critical';
        $logisticseventreport->save();

        if($task->task_status !== 'for validation')
        {
          Task::where('id', $task_id)->update(array('task_status' => 'for validation'));
        }
    }

    $data = array(
    'message' => "Executive for Financial Affairs raised a critical issue upon purchased item.",
    'redirect' => 'admin/president/purchased-item-reported',
    'origin' => 'purchased-item-validation',
    );

    $role_id = Role::where('name', 'president')->first()->id;
    $member = Member::whereHas('role', function($q) use ($role_id) {
        $q->where('role_id', $role_id);
    })->first();
    $member->notify(new SystemNotification($data));

    Event::fire(new SystemEvent(auth::id(), 'Reported an Item.'));

    return redirect()->back()->with('success', 'Reported!');


  }





//EVENT-FUND //EVENT-FUND //EVENT-FUND //EVENT-FUND //EVENT-FUND //EVENT-FUND //EVENT-FUND


   public function eventfund_view()
  {
    $events = MticsEvent::where('status','!=','pending')->get();
    return view('officers/events_fund', compact('events'));
  }

   public function eventfund_details($id)
  {
    $event = MticsEvent::with('breakdown')->where('id',$id)->first();
     if($event == null)
    {
      return redirect('admin/finance/event-fund')->withErrors('Event id is invalid');
    }

    if($event->status == 'pending')
    {
      return redirect('admin/finance/event-fund')->withErrors('This Event is still pending');
    }
    $payments = EventPayment::with('member')->with('admin')->where('event_id',$id)->get();
    $eventpayment = new FormulaMoneyController();
    $eventbucket = $eventpayment->CheckEventFundPayment($id);
    return view('officers/events_fund_view', compact('payments','eventbucket'));
  }

//manage-payment //manage-payment //manage-payment //manage-payment //manage-payment

 public function event_view($id)
  {

    if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }

    $event = MticsEvent::findOrFail($id);

    $breakdown = EventAmountBreakdown::where('event_id', $id)->whereHas('event', function($q) {
      $q->where('amount_confirm', 'approved');
    })->sum('breakdown_amt');

    $payments = EventPayment::where('event_id', $id)->get();
    $totalPayment = 0;

    foreach ($payments as $payment) {
      $totalPayment += $payment->payevent_amt;
    }

    $noPaid = count($payments->groupby('member_id'));

    $bank = new BankTransactionController();

    $eventbucket = $bank->CheckEventFundBucketPerEvent($id);

    $eventbudget = New FormulaMoneyController;
    $event_budget = $eventbudget->CheckEventFundBudget($id);

    $eventPayments = EventPayment::where('event_id', $id)->get();
    $moneyRequests = MoneyRequest::whereHas('task', function($q) use($event) {
      $q->where('event_id', $event->id)->where('mon_req_status', 'pending');
    })->get();
    $moneyRequestTransactions = MoneyRequest::whereHas('task', function($q) use($event) {
      $q->where('event_id', $event->id);
    })->where(function($q) {
        $q->where('mon_req_status', 'ongoing')->orWhere('mon_req_status', 'cleared');
    })->get();

    $moneyRequestsTaskDone = MoneyRequest::where(function($q) {
        $q->where('mon_req_status', 'ongoing')->orWhere('mon_req_status', 'for validation');
    })->whereHas('task', function($w) use($event) {
        $w->where('event_id', $event->id);
    })->whereHas('task', function($w) use($event) {
        $w->where('task_status', 'done')->orwhere('task_status', 'failed')->orwhere('task_status', 'for validation');
    })->get();

    //dd($moneyRequestsTaskDone);
    $requestsForReimbursement = ReimburseRequest::where('reimburse_status', 'pending')->whereHas('moneyrequest', function($q) use($id) {
        $q->whereHas('task', function($q) use($id) {
            $q->where('event_id', $id);
        });
    })->get();

    $receipts = Receipt::with('mticspurchaselist')->get();

    // event offers
    $event_offers = EventOffer::where('event_id',$id)->get();

    //finance task
     $finance_tasks = Task::where('event_id', $id)->where(function($q) {
                $q->where('task_status', 'pending')->orWhere('task_status', 'on-going')->orWhere('task_status', 'for validation')->orWhere('task_status', 'disapproved');
            })->whereHas('role', function($q){
                $q->where('name', 'finance');
            })->get();


    //Expenses

     $roles = Role::where('name','=','internal')
            ->orwhere('name','=','external')
            ->orwhere('name','=','docu')
            ->orwhere('name','=','finance')
            ->orwhere('name','=','auditor')
            ->orwhere('name','=','info')
            ->orwhere('name','=','activity')
            ->orwhere('name','=','logistic')
            ->orwhere('name','=','president')
            ->orwhere('name','=','mtics_adviser')
            ->orwhere('name','=','vice_president')->get();
    $event_expenses = EventExpense::where('event_id',$id)->orderby('id','DESC')->get();
    $breakdowns = EventAmountBreakdown::where('event_id',$id)->get();

    return view('officers/event/finance',compact('event', 'eventbucket', 'eventPayments', 'moneyRequests', 'moneyRequestTransactions', 'moneyRequestsTaskDone', 'breakdown', 'totalPayment', 'noPaid', 'receipts', 'requestsForReimbursement', 'event_offers','event_budget','finance_tasks','roles','event_expenses','breakdowns'));
  }


    public function eventValidation($event_id, $request_id) {
      $validation = AmountValidation::where('money_request_id', $request_id)->where('status', 'ongoing')->first();

      if (is_null($validation)) {
        $validation = AmountValidation::create([
            'money_request_id' => $request_id,
            'status' => 'ongoing',
          ]);

        $step = FinanceStepValidation::where('step', 1)->first();

        StepValidation::create([
            'validation_id' => $validation->id,
            'step_id' => $step->id,
            'status' => 'current',
          ]);

        return redirect('admin/finance/manage-event/' . $event_id . '/validate/' . $request_id . '/' . $step->url);

      } else {
        $step = StepValidation::where('validation_id', $validation->id)->where('status', 'current')->first();

        return redirect('admin/finance/manage-event/' . $event_id . '/validate/' . $request_id . '/' . $step->step->url);
      }
    }

    public function step1($event_id, $request_id) {
       if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }
      $event = MticsEvent::findOrFail($event_id);
      $moneyRequestsTaskDone = MoneyRequest::findOrFail($request_id);

      return view('officers/event/validation/finance_step1', compact('event', 'moneyRequestsTaskDone'));
    }

    public function step2($event_id, $request_id) {
       if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }
      $event = MticsEvent::findOrFail($event_id);

      $moneyRequestsTaskDone = MoneyRequest::findOrFail($request_id);


      $withReceipts = EventPurchaseList::has('receipt')->where('task_id', $moneyRequestsTaskDone->task_id)->get();

      return view('officers/event/validation/finance_step2', compact('event', 'moneyRequestsTaskDone', 'withReceipts'));
    }

    public function step3($event_id, $request_id) {
       if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }
      $event = MticsEvent::findOrFail($event_id);

      $moneyRequestsTaskDone = MoneyRequest::findOrFail($request_id);

      // check for reimbursement or excess money
      $reimbursement = ReimburseRequest::where('money_request_id', $moneyRequestsTaskDone->id)->first();

      $excess = $moneyRequestsTaskDone->amount - $moneyRequestsTaskDone->task->eventpurchaselist->sum('event_total_price');

      $reportcount = EventPurchaseReport::where('report_category','critical')->where('solution_fr_critical', null)->whereHas('purchaselist', function($q) use($moneyRequestsTaskDone) {
        $q->where('task_id', $moneyRequestsTaskDone->task_id);
      })->count();

      if ($reimbursement or $excess < 0) {

        if ($excess < 0 and is_null($reimbursement)) {
          return view('officers/event/validation/finance_step3_reimburse', compact('moneyRequestsTaskDone', 'reimbursement', 'reportcount', 'event'));
        }
        elseif(count($reimbursement->confirm) > 0) {
          return redirect('admin/finance/manage-event/' . $event_id . '/validate/' . $request_id . '/step-3/wait-auditor-approval');
        }
        elseif ($reimbursement) {
          return view('officers/event/validation/finance_step3_reimburse', compact('event', 'moneyRequestsTaskDone', 'reimbursement', 'reportcount'));
        }

      }

      elseif ($excess > 0) {

        return view('officers/event/validation/finance_step3_excess', compact('event', 'moneyRequestsTaskDone', 'excess'));
      }

      else {
        return redirect('admin/finance/manage-event/' . $event->id . '/validate/' . $moneyRequestsTaskDone->id . '/next');
      }
    }

    public function step3WaitView($event_id, $request_id) {
       if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }
      $event = MticsEvent::findOrFail($event_id);

      $moneyRequestsTaskDone = MoneyRequest::findOrFail($request_id);

      return view('officers/event/validation/finance_step3_wait_auditors_approval', compact('event', 'moneyRequestsTaskDone'));
    }

    public function lastStep($event_id, $request_id) {
       if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }
      $event = MticsEvent::findOrFail($event_id);

      $moneyRequestsTaskDone = MoneyRequest::findOrFail($request_id);

      $reportcount = EventPurchaseReport::where('report_category','critical')->where('solution_fr_critical', null)->whereHas('purchaselist', function($q) use($moneyRequestsTaskDone) {
        $q->where('task_id', $moneyRequestsTaskDone->task_id);
      })->count();


      $withReceipts = EventPurchaseList::has('receipt')->where('task_id', $moneyRequestsTaskDone->task_id)->get();

      return view('officers/event/validation/finance_lastStep', compact('event', 'moneyRequestsTaskDone', 'withReceipts', 'reportcount'));
    }

    public function next($event_id, $request_id) {
       if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }
      $event = MticsEvent::findOrFail($event_id);

      $moneyRequestsTaskDone = MoneyRequest::findOrFail($request_id);

      $validation = AmountValidation::where('money_request_id', $request_id)->where('status', 'ongoing')->first();

      $step = StepValidation::where('validation_id', $validation->id)->where('status', 'current')->first();


      $nextStep = FinanceStepValidation::where('step', $step->step->step + 1)->first();

      if(is_null($nextStep)) {
        $nextStep = FinanceStepValidation::where('step', 0)->first();
      }

      $newStepValidation = StepValidation::create([
          'validation_id' => $validation->id,
          'step_id' => $nextStep->id,
          'status' => 'current',
        ]);

      $step->status = 'done';
      $step->save();

      return redirect('admin/finance/manage-event/' . $event_id . '/validate/' . $request_id . '/' . $newStepValidation->step->url);
    }




 public function event_add(Request $request,$id)
  {
    $event = MticsEvent::find($id);
    //dd($event);
    if($event->amount_confirm !== null and $event->amount_confirm !== 'denied' )
    {
        return redirect()->back()->withErrors('The request for Event Amount has been submitted, you cannot make any changes');
    }
    $this->validate($request, [
          'bname' => 'required',
          'bamt' => 'required',
          ]);
    if($request->bamt < 1)
    {
        return redirect()->back()->withErrors('Amount cannot be 0 or negative');
    }

    $breakdown = New EventAmountBreakdown;
    $breakdown->event_id = $id;
    $breakdown->breakdown_name = $request->bname;
    $breakdown->breakdown_desc = $request->bdesc;
    $breakdown->breakdown_amt = $request->bamt;
    $breakdown->save();

    $this->CheckEventAmount($id);

    return redirect('admin/finance/event-fund/'.$id.'/manage-payment');
  }

  public function event_edit(Request $request,$task_id, $id)
  {
    $breakdown = EventAmountBreakdown::find($id);
    $event = MticsEvent::find($breakdown->event_id);
    //dd($breakdown);
    if($event->amount_confirm !== null and $event->amount_confirm !== 'denied')
    {
        return redirect()->back()->withErrors('The request for Event Amount has been submitted, you cannot make any changes');
    }
    $this->validate($request, [
          'bname' => 'required',
          'bamt' => 'required',
          ]);

    if($request->bamt < 1)
    {
        return redirect()->back()->withErrors('Amount cannot be 0 or negative');
    }

    $breakdown->breakdown_name = $request->bname;
    $breakdown->breakdown_desc = $request->bdesc;
    $breakdown->breakdown_amt = $request->bamt;
    $breakdown->save();

    $this->CheckEventAmount($breakdown->event_id);

    return redirect()->back();
  }

public function event_submit($id)
  {
    $this->CheckEventAmount($id);
    $event = MticsEvent::find($id);
    if($event->amount_confirm == 'approved')
    {
        return redirect()->back()->withErrors('The request for Event Amount has been approved, you cannot make any changes');
    }
    MticsEvent::where('id', $id)->update(array('amount_confirm' => 'pending'));
    return redirect()->back();
  }



  private function CheckEventAmount($event_id)
  {
    $breakdowntotal = EventAmountBreakdown::where('event_id',$event_id)->get()->sum('breakdown_amt');
    MticsEvent::where('id', $event_id)->update(array('event_amount' => $breakdowntotal));
    return $breakdowntotal;
  }

}
