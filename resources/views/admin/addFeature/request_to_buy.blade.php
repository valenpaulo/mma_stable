@extends('layout/control_panel')

@section('title')
Purchase Request
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-shopping-basket fa-fw w3-xxlarge"></i>
      <strong>Purchase Request</strong>
    </h3>
  </div>

    <div class="row">
      <p><strong> Note</strong>: This Purchase Request is under MTICS Fund Transaction
    </div>

  <hr>

  <div class="row">
    <p><font color="red"><strong> Warning</strong></font>: If your  <b> Purchase Request </b> is under <a href="{{url('admin/manage-event')}}"><i class="fa fa-calendar-o fa-fw w3-xlarge"></i> <strong>Event</strong></a>, Please Speak with your President to give task to External   
  </div>


  <div class="row w3-margin-top">
    <h5>
      <a href="#add-task" class="w3-text-gray" data-toggle="modal" style="outline: 0">
        <i class="fa fa-plus-square-o fa-fw"></i>
        Add Request
      </a>
    </h5>
  </div>


  <!-- ADD MODAL -->
  <div class="modal fade" id="add-task" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Task</h4>
  </div>
  <form action="{{ url('admin/request-to-buy/store/') }}" method="POST" enctype="multipart/form-data">

  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

        <div class="form-group">
          <label>Due Date: </label>
          <input type="date" class="form-control" id="due_date" name="due_date">
        </div>

        <div class="form-group">
          <label>Task Name: </label>
          <input type="text" name="task_name" id="task_name" tabindex="1" class="form-control" placeholder="Task Name">
        </div>

        <!-- <div class="checkbox">
            <label style="display: none"><input type="checkbox" name="urgent[]" value="urgent">Urgent</label>
        </div>
 -->
        <br>

        <!-- EXTERNAL TASK -->
        <div id="external_task">

          <h4>
            <i class="fa fa-shopping-basket fa-fw"></i>
            Purchase List
            <a href="#" class="w3-text-gray w3-hover-text-green">
              <i class="fa fa-plus-square fa-fw" data-toggle="tooltip" id="add_item" title="Add Item"></i>
            </a>
          </h4>

          <div>

            <div class="form-group">
              <label>Item Name: </label>
              <input type="text" name="itemname[]" id="itemname" tabindex="1" class="form-control" placeholder="Item Name">
            </div>

            <div class="form-group">
              <label>Quantity: </label>
               <input type="number" name="itemquan[]" id="item_quan" tabindex="1" class="form-control" placeholder="Item Quantity">
            </div>

            <div class="form-group">
              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="itemdesc[]" placeholder="Item Description" rows="3"></textarea>
            </div>

          </div>

        </div>
        <!-- END EXTERNAL TASK -->

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
  </div>

  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->

  <div class="row w3-margin-top">
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="purchaseTable">
        <thead>
          <tr>
           <th class="w3-center">Task</th>
           <th class="w3-center">Items</th>
           <th class="w3-center">Order By</th>
           <th class="w3-center">Created At</th>
           <th class="w3-center">Due Date</th>
           <th class="w3-center">Status</th>
          </tr>
        </thead>
        <tbody class="w3-text-gray">
        @foreach($tasks as $task)
        <tr>
          <td class="w3-center">
            @if(Gate::allows('external-only'))
            <a href="{{url('admin/external/task/' . $task->id)}}" class="w3-text-gray">
              <i class="fa fa-exclamation-circle fa-fw"></i>
              {{$task->task_name}}
            </a>
            @else
              {{$task->task_name}}
            @endif
          </td>
          <td class="w3-center">
            @foreach($task->mticspurchaselist as $item)
              @if($loop->last)
              {{$item->mtics_itemname}}
              @else
              {{$item->mtics_itemname}},
              @endif
            @endforeach
          </td>
          <td class="w3-center">{{$task->admin->first_name}} {{$task->admin->last_name}}</td>
          <td class="w3-center">{{$task->created_at}}</td>
          <td class="w3-center">{{$task->due_date}}</td>
          <td class="w3-center">{{$task->task_status}}</td>
        </tr>
        @endforeach
       </tbody>
      </table>
    </div>
  </div>

</div>

<script>
  $(document).ready(function() {
    var wrapper         = $("#external_task"); //Fields wrapper
    var add_button      = $("#add_item"); //Add button ID

    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
          $(wrapper).append('\
            <div>\
            <br>\
            <div class="form-group">\
              <label>Item Name: </label>\
              <input type="text" name="itemname[]" id="itemname" tabindex="1" class="form-control" placeholder="Item Name">\
            </div>\
            <div class="form-group">\
              <label>Quantity: </label>\
               <input type="number" name="itemquan[]" id="item_quan" tabindex="1" class="form-control" placeholder="Item Quantity">\
            </div>\
            <div class="form-group">\
              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="itemdesc[]" placeholder="Item Description" rows="3"></textarea>\
            </div>\
              <a id="remove_item" href="" class="w3-text-red">\
            <p align="center">\
              <i class="fa fa-remove fa-fw"></i> Remove\
            </p>\
              </a>\
            </div>\
          '); //add input box
    });

    $(wrapper).on("click","#remove_item", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
  });
</script>

<script>
  // var input = document.getElementById( 'receipt' );

  // input.addEventListener('change', showFileName1);

  function showFileName1( event, id ) {

    // the change event gives us the input it occurred in
    var infoArea = document.getElementById( 'receipt-image-file-name-' + id );
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  };
</script>

<script>
  // var input = document.getElementById( 'receipt' );

  // input.addEventListener('change', showFileName1);

  function showWithdrawFileName( event, id ) {

    // the change event gives us the input it occurred in
    var infoArea = document.getElementById( 'w-receipt-image-file-name-' + id );
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  };
</script>

<script type="text/javascript">
$(document).ready( function () {
  $('#purchaseTable').DataTable();
} );
</script>

@endsection
