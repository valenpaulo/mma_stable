
@extends('layout/layout')
@section('title')

{{$post->title}}

@endsection

@section('content')
<style type="text/css">
  @media only screen and (max-width: 1365px) {
    .main {
      margin-top: 70px;
    }
  }

  @media (min-width: 1366px) {
    .main {
      margin-top: 105px;
    }
  }
</style>

<div class="container main">
  <div class="row">
    <div class="col-sm-8"></div>
     <div class="col-sm-4 w3-right">
    @if($errors->any())
      @foreach ($errors->all() as $error)

      <div class="alert alert-danger alert-dismissable fade in col-sm-12" id="success-alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>
      {{$error}}
      </strong>
      </div>
      @endforeach
    @endif
    </div>
  </div>

  <div class="row w3-padding">
    <br class="w3-hide-small w3-hide-medium">
    <span class="w3-margin">
      @if($author->username == Auth::user()->username)
      <div class="w3-dropdown-hover w3-left w3-white">
        <a href="javascript:void(0)" class="w3-text-gray"><i class="fa fa-sort-down fa-fw w3-large"></i></a>

        <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" style="left: 0">
          <a href="{{url('member/profile/post/'.$post->id.'/'.$post->slug.'/edit')}}" class="w3-text-gray w3-hover-teal"><i class="fa fa-pencil-square-o fa-fw w3-large"></i>Edit Post</a>

          <a href="javascript:void(0)" onclick="deletePost( {{$post->id}} )" class="w3-text-gray w3-hover-teal" style="outline: 0px"><i class="fa fa-trash-o fa-fw w3-large"></i>Delete Post</a>
        </div>

      </div>
      @else
      <div class="w3-dropdown-hover w3-left w3-white">
        <a href="javascript:void(0)" class="w3-text-gray"><i class="fa fa-sort-down fa-fw w3-large"></i></a>

        <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" style="left: 0">
          <a class="w3-text-gray w3-hover-teal" data-toggle="modal" href="#report" style="outline: 0px"><i class="fa fa-exclamation-triangle fa-fw w3-large"></i> Report Post</a>
        </div>

      </div>
      @endif
    </span>
    <span class="w3-text-gray w3-right">{{$post->created_at}}</span>
    <hr>
  </div>
  <div class="row w3-padding" align="center">
    <h1>{{$post->title}}</h1>
    <p>by {{$author->first_name}} {{$author->last_name}}</p>
  </div>

  <div class="row" align="center">
    <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$post->image) : asset('images/'.$post->image) }}" style="width: 50%">
  </div>

  <br>

  <div class="row w3-padding" align="justify">
    <p><?php echo($post->body);?></p>
  </div>

  <hr>

  <div class="row">
    <div class="w3-card-4 w3-white w3-padding w3-margin">
      <div class="w3-container">
        <div class="row w3-padding-small">
          <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.auth::user()->avatar) : asset('images/'.auth::user()->avatar) }}" alt=" Avatar" class="w3-left w3-circle w3-margin-right w3-hide-small" style="width: 80px; height: 80px">

          <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.auth::user()->avatar) : asset('images/'.auth::user()->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-large w3-hide-medium" style="width: 50px; height: 50px">

          <span class="w3-right w3-large">
            <button class="btn btn-default w3-green" type="submit" form="reply"><i class="fa fa-paper-plane-o fa-fw w3-large"></i>Send</button>
          </span>

          <span>
            <strong>{{auth::user()->username}}</strong>
            <br>
            {{auth::user()->id_num}}
          </span>
        </div>
        <br>
        <div class="row">
          <form action="{{ url('member/profile/post/'.$post->id.'/'.$post->slug.'/reply' ) }}" id="reply" method="POST">
            <div class="form-group">
              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reply_body" required placeholder="Join the discussion" rows="4"></textarea>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </form>
        </div>
      </div>
    </div>
  </div>
  <br>
  @foreach($replies as $reply)
  <div class="row w3-margin">
      <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$reply->member->avatar) :  asset('images/'.$reply->member->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-small" style="width: 80px; height: 80px">

      <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$reply->member->avatar) : asset('images/'.$reply->member->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-large w3-hide-medium" style="width: 50px; height: 50px">

      <span class="w3-margin w3-right">
        @if($reply->member->username == Auth::user()->username)
        <div class="w3-dropdown-hover w3-left w3-white">
          <a href="javascript:void(0)" class="w3-text-gray"><i class="fa fa-sort-down fa-fw w3-large"></i></a>

          <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" style="right: 0">
            <a href="javascript:void(0)" onclick="deleteReply( {{$reply->id}} )" class="w3-text-gray w3-hover-teal" style="outline: 0px"><i class="fa fa-trash-o fa-fw w3-large"></i>Delete Reply</a>
          </div>

        </div>

        <!-- DELETE REPLY MODAL -->
            <form action="{{ url('member/profile/post/comment/' . $reply->id . '/delete' ) }}" id="formReply-{{$reply->id}}" method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
        <!-- END DELETE REPLY MODAL -->
        @endif
      </span>

      <span>
        <strong>{{ $reply->member->first_name }} {{ $reply->member->last_name }}</strong>
        <br>
        {{$reply->created_at}}
        <br>
        <br>
        <i class="fa fa-comments-o fa-fw w3-large"></i>
        {{$reply->reply_body}}
      </span>
  </div>
  @if(!$loop->last)
  <hr>
  @endif
  @endforeach

  <br>
</div>

<!-- REPORT POST MODAL -->

<div class="modal fade" id="report" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Report Post</h4>
      </div>
    <form action="{{ url('member/profile/post/'.$post->id.'/'.$post->slug.'/report' ) }}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
        <p>Let's us know why do you want to REPORT '{{$post->title}}' Post?</p>

      <div class="form-group">
        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" required placeholder="Reason" rows="3"></textarea>
      </div>


        <div class="modal-footer">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-arrow-circle-o-right fa-fw w3-text-white"></i> Proceed</button>
        </div>

      </div>
    </form>
    </div>
  </div>
</div>

<!-- END REPORT POST MODAL -->

<!-- DELETE POST MODAL -->
    <form action="{{ url('member/profile/post/'.$post->id.'/delete' ) }}" id="formPost-{{$post->id}}" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>
<!-- END DELETE POST MODAL -->

<script type="text/javascript">
function deletePost( id ) {
  swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formPost-" + id).submit();
    }
  });
}
</script>

<script type="text/javascript">
function deleteReply( id ) {
  swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formReply-" + id).submit();
    }
  });
}
</script>

<script>
window.setTimeout(function() {
  $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
  });
}, 4000);

</script>

@endsection
