<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBanktransDenied extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtics_deposit_denies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mtics_deposit_id');
            $table->integer('faculty_id');
            $table->longtext('denied_reason');
            $table->timestamps();
        });

         Schema::create('mtics_withdrawal_denies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mtics_withdrawal_id');
            $table->integer('faculty_id');
            $table->longtext('denied_reason');
            $table->timestamps();
        });

         Schema::table('mtics_deposits', function ($table) {
             $table->dropColumn('denied_reason');
           
        });

          Schema::table('mtics_withdrawals', function ($table) {
             $table->dropColumn('denied_reason');
           
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mtics_deposit_denies');
         Schema::drop('mtics_withdrawal_denies');
    }
}
