@extends('layout/control_panel')

@section('title')
{{$event->event_title}}
@endsection

@section('middle')
<div class="w3-container w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-check-circle-o fa-fw w3-xxlarge"></i>
      <strong>{{$event->event_title}} <i class="fa fa-angle-double-right fa-fw"></i> {{$moneyRequestsTaskDone->task->task_name}} @if ($reportcount == 0) <a class="w3-text-green w3-xlarge" href="{{ url('admin/finance/purchased-items/'.$moneyRequestsTaskDone->task_id.'/done') }}" data-toggle="modal"><i class="fa fa-arrow-circle-right fa-fw" data-toggle="tooltip" data-placement="bottom" title="Finish"></i></a>@endif</strong>
    </h3>
  </div>

  <hr>

  @if ($reportcount == 0)
  <div class="row w3-margin-top">
      <p>Click the <i class="fa fa-arrow-circle-right w3-text-green"></i> to finish the validation.</p>

  </div>

  @else

   <div class="row w3-margin-top" align="center">
    <h3>
    <i class="fa fa-spinner fa-fw"></i>
    Waiting to Resolve the Issue
    </h3>
  </div>
  <br>
  @endif

  <div class="row w3-margin-top">

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-amber w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxxlarge fa-fw"></i>
              <span class="w3-right w3-margin-top">
                <strong>Requested Money</strong>
              </span>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$moneyRequestsTaskDone->amount}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>

    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-purple w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
            <i class="fa fa-shopping-bag w3-xxxlarge fa-fw"></i>
            <span class="w3-right w3-margin-top">
              <strong>Purchased Amount</strong>
            </span>
          </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$moneyRequestsTaskDone->task->eventpurchaselist->sum('event_total_price')}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-teal w3-text-black w3-padding">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-reply w3-xxxlarge fa-fw"></i>
              <span class="w3-right w3-margin-top">
                <strong>Reimbursement</strong>
              </span>
            </h4>
          </div>

          <hr style="border-color: black">

          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>@if($moneyRequestsTaskDone->reimburse)
                  {{$moneyRequestsTaskDone->reimburse->reimburse_amount}}
                @else
                  0
                @endif pesos</strong>
              </span>
            </h4>
          </div>

        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-blue w3-text-black w3-padding">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-mail-forward w3-xxxlarge fa-fw"></i>
              <span class="w3-right w3-margin-top">
                <strong>Excess Money</strong>
              </span>
            </h4>
          </div>

          <hr style="border-color: black">

          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                      <strong>@if($moneyRequestsTaskDone->excess_amount)
                {{$moneyRequestsTaskDone->excess_amount}}
              @else
                0
              @endif pesos</strong>
              </span>
            </h4>
          </div>

        </div>

      </div>
    </div>

  </div>

  <br>

  <div class="row w3-margin-top">
    <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
             <th class="w3-center">Task ID</th>
             <th class="w3-center">Item Name</th>
             <th class="w3-center">Quantity</th>
             <th class="w3-center">Amount</th>
             <th class="w3-center">Total</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($moneyRequestsTaskDone->task->eventpurchaselist as $item)
            <tr>
              <td class="w3-center">{{$item->task_id}}</td>
              <td class="w3-center">{{$item->event_itemname}}</td>
              <td class="w3-center">{{$item->event_act_quan}}</td>
              <td class="w3-center">{{$item->event_act_price}}</td>
              <td class="w3-center">{{$item->event_act_price * $item->event_act_quan}}</td>
            </tr>

          @endforeach
         </tbody>
        </table>
    </div>
  </div>

  <div class="row w3-margin-top">
    <h4>
      <i class="fa fa-file-text-o fa-fw"></i>
      <strong>Receipt</strong>
    </h4>

  </div>

  <div class="row w3-margin-top">
    @foreach($withReceipts as $item)
    <div class="col-sm-1 col-md-4 col-lg-4">
      <label>
        <i class="fa fa-shopping-bag fa-fw"></i>
        {{$item->event_itemname}}
      </label>
      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'. $item->receipt[0]->receipt_image) : asset('images/'. $item->receipt[0]->receipt_image)}}" style="width: 100%">
    </div>
    @endforeach
  </div>

</div>

@endsection
