<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Notifications\SystemNotification;
use App\MticsEvent;
use App\Meeting;
use App\Role;
use App\Member;
use App\Task;
use App\Document;
use App\Year;
use App\Course;
use App\EventMember;
use App\RoleMember;
use App\Officerterm;
use App\Penalty;

use App\Notifications\SystemBroadcastNotification;

use Illuminate\Support\Facades\Event;
use App\Events\SystemEvent;

class EventController extends Controller
{
    public function view() {

        $term = Officerterm::where('status','on-going')->first();
        $events = MticsEvent::where(function($q) {
            $q->where('status', 'pending')->orWhere('status', 'on-going')->orwhere('status', 'done');
        })->orWhere('status', 'done')->where('updated_at','>=',date($term->created_at))->get();
        //dd($events);
        return view('officers/event/event', compact('events'));
    }

    public function presidentView() {

        if(!Gate::allows('president-only'))
        {
        return redirect('/');
        }

        $term = Officerterm::where('status','on-going')->first();
        $events = MticsEvent::where('updated_at','>=',date($term->created_at))->get();

        $years = Year::where('year_code','!=','Alumni')->where('year_code','!=','Faculty')->get();
        $courses = Course::where('course_code','!=','Alumni')->where('course_code','!=','Faculty')->get();

        return view('officers/event', compact('events','years','courses'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'event_title' => 'required|max:255',
            'description' => 'max:255',
            'goal' => 'max:255',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        $event = MticsEvent::create([
            'event_title' => $request->event_title,
            'organizer' => Auth::id(),
            'description' => $request->description,
            'goal' => $request->goal,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ]);

        Event::fire(new SystemEvent(auth::id(), 'Added "' . $event->event_title . '" event.'));

        return redirect()->back()->with('success', 'Event Added!');
    }

    public function start($id) {

        $event = MticsEvent::findOrFail($id);
        if($event->status == 'done'){
        return redirect()->back()->withErrors('Event status is '.$event->status.' ,make changes are prohibited' );
        }

        $event->status = 'on-going';
        $event->update();

        $message = 'New MticsEvent: ' . $event->event_title;

        $members = Member::whereHas('role', function($q){
            $faculty = Role::where('name', 'faculty')->first()->id;
            $super_admin = Role::where('name', 'super_admin')->first()->id;
            $president = Role::where('name', 'president')->first()->id;

            $q->where('role_id', '!=', $faculty)->where('role_id', '!=', $super_admin)->where('role_id', '!=', $president);
        })->get();

        \Notification::send($members, new SystemBroadcastNotification($message, 'event'));

        Event::fire(new SystemEvent(auth::id(), 'Started "' . $event->event_title . '" event.'));

        return redirect()->back()->with('success', 'Event Started!');
    }


     public function done($id) {
        $event = MticsEvent::findOrFail($id);
        if(Task::where('event_id',$id)->where('task_status','pending')->orwhere('task_status','on-going')->orwhere('task_status','for validation')->get()->isEmpty())
        {
                $event->status = 'done';
                $event->update();
        }
        else
        {
        return redirect()->back()->withErrors('There is still pending task, You cannot set as done this Event');
        }
        return redirect()->back()->with('success', 'Cleared!');
    }


    /*public function edit(Request $request, $id) {
        $event = MticsEvent::findOrFail($id);

        if($request->event_title !== null and $request->description !== null and $request->goal !== null and $request->start_date !== null and $request->end_date !== null ){

            $event->event_title = $request->event_title;
            $event->description = $request->description;
            $event->goal = $request->goal;
            $event->start_date = $request->start_date;
            $event->end_date = $request->end_date;

            if($request->accomplishment !== null){
                if($event->status == 'done'){
                    $event->accomplishment = $request->accomplishment;
                }
                else
                {
                 return redirect()->back()->withErrors('Cannot set accomplishment field, event is not done yet');
                }
            }
            else{
                 if($event->status == 'done'){
                    return redirect()->back()->withErrors('Event is Done, Please input Accomplishment information');
                }
            }



                $event->update();
                Event::fire(new SystemEvent(auth::id(), 'Edited "' . $event->event_title . '" information.'));

                return redirect()->back()->with('success', 'Event Information Edited!');

        }
        else
        {
            return redirect()->back()->withErrors('Fields cannot be null, Please check the your event information');
        }

    }*/

     public function edit(Request $request, $id) {
        $event = MticsEvent::findOrFail($id);
        $flag = 0;

        if ($request->event_title != $event->event_title and $request->event_title != ""){
            $event->event_title = $request->event_title;
            $flag = 1;
        }

        if ($request->description != $event->description){
            $event->description = $request->description;
            $flag = 1;
        }

        if ($request->goal != $event->goal){
            $event->goal = $request->goal;
            $flag = 1;
        }

        if($request->accomplishment !== null)
        {
            if ($request->accomplishment != $event->accomplishment){
                if($event->status == 'done'){
                $event->accomplishment = $request->accomplishment;
                $flag = 1;
                }
                else{
                     return redirect()->back()->withErrors('Cannot set accomplishment field, event is not done yet');
                }

            }
        }

        if ($request->start_date != $event->start_date and $request->start_date != ""){
            $event->start_date = $request->start_date;
            $flag = 1;
        }

        if ($request->end_date != $event->end_date and $request->end_date != ""){
            $event->end_date = $request->end_date;
            $flag = 1;
        }

        if ($flag == 1) {
            $event->update();
        }


        Event::fire(new SystemEvent(auth::id(), 'Edited "' . $event->event_title . '" information.'));

        return redirect()->back()->with('success', 'Event Information Edited!');

    }

    public function cancel($id) {
        $event = MticsEvent::findOrFail($id);
        if($event->status == 'done'){
        return redirect()->back()->withErrors('Event status is '.$event->status.' ,make changes are prohibited' );
        }
        $event->status = 'cancelled';
        $event->update();


        $message = $event->event_title . ' has been cancelled';

        $members = Member::whereHas('role', function($q){
            $faculty = Role::where('name', 'faculty')->first()->id;
            $super_admin = Role::where('name', 'super_admin')->first()->id;
            $president = Role::where('name', 'president')->first()->id;

            $q->where('role_id', '!=', $faculty)->where('role_id', '!=', $super_admin)->where('role_id', '!=', $president);
        })->get();

        \Notification::send($members, new SystemBroadcastNotification($message, 'event_cancelled'));


        Event::fire(new SystemEvent(auth::id(), 'Cancelled "' . $event->event_title . '" event.'));

        return redirect()->back()->with('success', 'Event Cancelled!');
    }

    public function delete($id) {
        $event = MticsEvent::findOrFail($id);
        $event->delete();

        Event::fire(new SystemEvent(auth::id(), 'Deleted "' . $event->event_title . '" event.'));

        return redirect()->back()->with('success', 'Event Deleted!');
    }

    public function event_view($id) {
        if(!Gate::allows('president-only'))
        {
        return redirect('/');
        }

        $event = MticsEvent::findOrFail($id);
        $penalties = Penalty::whereHas('task', function($q) use($event) {
            $q->where('event_id', $event->id);
        })->count();

        // dd($penalties);
        $years = Year::where('year_code','!=','Alumni')->where('year_code','!=','Faculty')->get();
        $courses = Course::where('course_code','!=','Alumni')->where('course_code','!=','Faculty')->get();
        $participants = EventMember::where('event_id', $id)->get();
        $meetings = Meeting::where('event_id', $event->id)->get();
        $roles = Role::where('name','!=','faculty')->where('name','!=','super_admin')
                        ->where('name','!=','asst_internal')
                        ->where('name','!=','asst_external')
                        ->where('name','!=','asst_docu')
                        ->where('name','!=','asst_finance')
                        ->where('name','!=','asst_auditor')
                        ->where('name','!=','asst_info')
                        ->where('name','!=','asst_activity')
                        ->where('name','!=','asst_logistic')
                        ->where('name','!=','president')
                        ->where('name','!=','vice_president')
                        ->where('name','!=','mtics_adviser')
                        ->get();

        $external_id = Role::where('name', 'external')->first()->id;
        $external_tasks = Task::where('event_id', $id)->whereHas('role', function($q){
                $q->where('name', 'external');
            })->get();

        $externalCount = Task::where('event_id', $id)->where(function($q) {
                $q->where('task_status', 'pending')->orWhere('task_status', 'on-going');
            })->whereHas('role', function($q){
                $q->where('name', 'external');
            })->count();

        $finance_tasks = Task::where('event_id', $id)->where(function($q) {
                $q->where('task_status', 'pending')->orWhere('task_status', 'on-going')->orWhere('task_status', 'for validation')->orWhere('task_status', 'disapproved')->orWhere('task_status', 'done');
            })->whereHas('role', function($q){
                $q->where('name', 'finance');
            })->get();
        $finance_count = Task::where('event_id', $id)->where(function($q) {
                $q->Where('task_status', 'for validation');
            })->whereHas('role', function($q){
                $q->where('name', 'finance');
            })->count();

        $logistic_tasks = Task::where('event_id', $id)->where(function($q) {
                $q->where('task_status', 'pending')->orWhere('task_status', 'on-going')->orWhere('task_status', 'for validation')->orWhere('task_status', 'disapproved')->orWhere('task_status', 'done');
            })->whereHas('role', function($q){
                $q->where('name', 'logistic');
            })->get();
        $logistic_count = Task::where('event_id', $id)->where(function($q) {
                $q->Where('task_status', 'for validation');
            })->whereHas('role', function($q){
                $q->where('name', 'logistic');
            })->count();

        $auditor_tasks = Task::where('event_id', $id)->where(function($q) {
                $q->where('task_status', 'pending')->orWhere('task_status', 'on-going')->orWhere('task_status', 'for validation')->orWhere('task_status', 'disapproved')->orWhere('task_status', 'done');
            })->whereHas('role', function($q){
                $q->where('name', 'auditor');
            })->get();
        $auditor_count = Task::where('event_id', $id)->where(function($q) {
                $q->Where('task_status', 'for validation');
            })->whereHas('role', function($q){
                $q->where('name', 'auditor');
            })->count();

        $info_tasks = Task::where('event_id', $id)->where(function($q) {
                $q->where('task_status', 'pending')->orWhere('task_status', 'on-going')->orWhere('task_status', 'for validation')->orWhere('task_status', 'disapproved')->orWhere('task_status', 'done');
            })->whereHas('role', function($q){
                $q->where('name', 'info');
            })->get();
         $info_count = Task::where('event_id', $id)->where(function($q) {
                $q->Where('task_status', 'for validation');
            })->whereHas('role', function($q){
                $q->where('name', 'info');
            })->count();

        $activity_tasks = Task::where('event_id', $id)->where(function($q) {
                $q->where('task_status', 'pending')->orWhere('task_status', 'on-going')->orWhere('task_status', 'for validation')->orWhere('task_status', 'disapproved')->orWhere('task_status', 'done');
            })->whereHas('role', function($q){
                $q->where('name', 'activity');
            })->get();
         $activity_count = Task::where('event_id', $id)->where(function($q) {
                $q->Where('task_status', 'for validation');
            })->whereHas('role', function($q){
                $q->where('name', 'activity');
            })->count();

        // Internal
        $internal_tasks = Task::where('event_id', $id)->where(function($q) {
                $q->where('task_status', 'pending')->orWhere('task_status', 'on-going')->orWhere('task_status', 'done');
            })->whereHas('role', function($q){
                $q->where('name', 'internal');
            })->get();

        $documents = Document::whereHas('category', function($q) {
            $q->where('category_name', 'Letter');
        })->whereHas('task', function($q) use ($id) {
            $q->where('event_id', $id)->where('task_status', 'done');
        })->get();

        // docu
        $docu_tasks = Task::where('event_id', $id)->where(function($q) {
                $q->where('task_status', 'pending')->orWhere('task_status', 'on-going')->orWhere('task_status', 'for validation')->orWhere('task_status', 'disapproved')->orWhere('task_status', 'done');
            })->whereHas('role', function($q){
                $q->where('name', 'docu');
            })->get();
        $role_id = Role::where('name', 'docu')->first()->id;
        $tobe_validated_documents = Document::whereHas('task', function($q) use ($id, $role_id) {
            $q->where('event_id', $id)->where('task_status', 'for validation')->where('role_id', $role_id);
        })->get();


        return view('officers/event_view', compact('event', 'meetings', 'roles', 'tasks', 'external_tasks', 'finance_tasks', 'logistic_tasks', 'auditor_tasks','info_tasks', 'internal_tasks', 'docu_tasks', 'activity_tasks', 'external_id', 'documents', 'years', 'courses', 'participants', 'tobe_validated_documents', 'externalCount', 'penalties','finance_count','logistic_count','info_count','activity_count','auditor_count'));
    }

    //MANAGE PARTICIPANT //MANAGE PARTICIPANT //MANAGE PARTICIPANT //MANAGE PARTICIPANT

    public function participant(Request $request,$id){

        if(isset($request->all)){
            $this->participateAll($id);
        }
        else{

            $this->validate($request, [
              'year' => 'required',
              'course' => 'required',
              ]);


            foreach ($request->course as $key => $value) {
           //IF YEAR IS INVALID WITH SELECTED SECTION
                 $course = Course::find($value);
                 $year =  Year::find($request->year);

                 if($course->course_year_count < $year->year_num){
                     $errors[] = $year->year_desc. ' is not valid with '. $course->course_code;

                     continue;

                 }

                if(EventMember::where('year_id',$request->year)->where('course_id',$value)->get()->isEmpty()){

                  EventMember::create([
                        'year_id' => $request->year,
                        'event_id' => $id,
                        'course_id' => $value,
                    ]);
                }
                else{
                     $errors[] = $year->year_desc. ' '. $course->course_code.' already added';
            }

        }



        if (isset($errors)){
            return redirect()->back()->withErrors($errors);
        }
            Event::fire(new SystemEvent(auth::id(), 'Added participants in event.'));

        }
        return redirect()->back()->with('success', 'Participant Added!');
    }

    private function participateAll($event_id)
    {
        $courses = Course::where('course_code','!=','Alumni')->where('course_code','!=','Faculty')->get();

        foreach ($courses as $course) {
            $coursecount = $course->course_year_count;

            for ($i=1; $i <= $coursecount; $i++) {
                $year = Year::where('year_num',$i)->first();
                if(EventMember::where('event_id',$event_id)->where('year_id',$year->id)->where('course_id',$course->id)->get()->isEmpty()){

                  EventMember::create([
                        'year_id' => $year->id,
                        'event_id' => $event_id,
                        'course_id' => $course->id,
                    ]);
                }
            }
        }
        Event::fire(new SystemEvent(auth::id(), 'Added participants in event.'));

    }

    public function participant_delete($id){
        $event_member = EventMember::findOrFail($id);
        $event_member->delete();
        return redirect()->back();
    }


//MANAGE EVENT INFORMATION EDIT //MANAGE EVENT INFORMATION EDIT//MANAGE EVENT INFORMATION EDIT

    public function event_info_edit(Request $request,$id){
        $this->validate($request, [
              'title' => 'required',
              'body' => 'required',
              ]);

        $event = MticsEvent::find($id);
        $event->event_title = $request->title;
        $event->description = $request->body;
        $event->save();

        return redirect()->back();
    }




}
