<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSolforReportAdviserBankTrans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
        Schema::table('mtics_deposit_reports', function ($table) {
            $table->longtext('solution')->nullable();
        });

        Schema::table('mtics_withdrawal_reports', function ($table) {
            $table->longtext('solution')->nullable();
        });

         Schema::table('event_deposit_reports', function ($table) {
            $table->longtext('solution')->nullable();
        });

        Schema::table('event_withdrawal_reports', function ($table) {
            $table->longtext('solution')->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
