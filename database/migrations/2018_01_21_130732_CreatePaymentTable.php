<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id');
            $table->integer('admin_id');
            $table->integer('pay_totalamt');           
            $table->enum('pay_status',['paid','balance'])->nullable()->default('paid');
            $table->longtext('remarks')->nullable();
            $table->timestamps();                        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
