<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventProgram extends Model
{
    protected $table = 'event_programs';
     protected $fillable = [
        'admin_id','admin_role_id','event_id','title','body'
    ];

    public function admin(){
        return $this->belongsTo('App\Member', 'admin_id');
    }

    public function event(){
        return $this->belongsTo('App\MticsEvent', 'event_id');
    }

    public function role(){
        return $this->belongsTo('App\Role', 'admin_role_id');
    }

}
