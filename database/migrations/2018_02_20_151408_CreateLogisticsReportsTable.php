<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogisticsReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mtics_purchase_lists', function ($table) {
            $table->integer('logistics_id')->after('mtics_total_price')->nullable();
        });
        

        Schema::create('mtics_purchase_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->integer('mtics_purchase_list_id');
            $table->enum('reportedby',['finance','logistics']);
            $table->string('report_name');
            $table->longtext('report_reason');
            $table->longtext('external_explanation');
            $table->enum('report_category',['minor','critical'])->default('minor');
            $table->longtext('solution_fr_critical')->nullable();
            $table->timestamps();
        });    

        Schema::table('mtics_purchase_lists', function ($table) {
            $table->integer('external_id')->after('inventory_category_id')->nullable();
        });

        Schema::table('mtics_purchase_lists', function ($table) {
             $table->dropColumn('mtics_inv_status');
         
        });
        Schema::table('mtics_purchase_lists', function ($table) {
            $table->enum('mtics_inv_status',['pending','moved','unable to move'])->default('pending');
        });     

    }

    /*
     * Reverse the migrations.
     *
     * @return void*/
     
    public function down()
    {
        //
    }
}
