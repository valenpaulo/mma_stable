@extends('layout/control_panel')

@section('title')
Turn Over
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-rotate-right fa-fw w3-xxlarge"></i>
      <strong>Turn Over</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top w3-padding">
      <h4><i class="fa fa-file-text-o fa-fw w3-xlarge"></i> <strong>Accomplishment Report</strong></h4>
  </div>

  <hr>

  <div class="row w3-padding">
    <form action="{{ url('admin/turn-over/accomplishment-report') }}" method="POST">

        <div class="form-group form-inline" align="center">
            <input type="hidden" value="{{$year}}" name="year" placeholder="YYYY" min="2017" max="2100" class="form-control">
            <button type="submit" class="btn btn-default w3-teal w3-text-white"><i class="fa fa-eye fa-fw"></i> View</button>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <script>
          document.querySelector("input[type=number]")
          .oninput = e => console.log(new Date(e.target.valueAsNumber, 0, 1))
        </script>
    </form>
  </div>

  <div class="row w3-margin-top w3-padding">
      <h4><i class="fa fa-file-text-o fa-fw w3-xlarge"></i> <strong>MTICS Financial Report</strong></h4>
  </div>

  <hr>

  <div class="row w3-padding">
    <form action="{{ url('admin/turn-over/financial-report/mtics') }}" method="POST">
        <div class="form-group form-inline" align="center">
            <input type="hidden" value="{{$year}}" name="year" placeholder="YYYY" min="2017" max="2100" class="form-control">
            <button type="submit" class="btn btn-default w3-teal w3-text-white"><i class="fa fa-eye fa-fw"></i> View</button>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <script>
          document.querySelector("input[type=number]")
          .oninput = e => console.log(new Date(e.target.valueAsNumber, 0, 1))
        </script>
    </form>
  </div>

  <div class="row w3-margin-top w3-padding">
      <h4><i class="fa fa-file-text-o fa-fw w3-xlarge"></i> <strong>Event Financial Report</strong></h4>
  </div>

  <hr>

  <div class="row w3-margin-top w3-padding">
      <form action="{{ url('admin/turn-over/financial-report/event') }}" method="POST">
        <div class="form-group form-inline" align="center">
            <input type="hidden" value="{{$year}}" name="year" placeholder="YYYY" min="2017" max="2100" class="form-control">
            <button type="submit" class="btn btn-default w3-teal w3-text-white"><i class="fa fa-eye"></i> View</button>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <script>
          document.querySelector("input[type=number]")
          .oninput = e => console.log(new Date(e.target.valueAsNumber, 0, 1))
        </script>
    </form>
  </div>

@endsection
