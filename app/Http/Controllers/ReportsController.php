<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\File;
use DateTime;
use NumberToWords\NumberToWords;
use App\MticsBudget;
use App\MticsWithdrawal;
use App\MticsDeposit;
use App\MticsPurchaseList;
use App\Task;
use App\EventDeposit;
use App\MticsfundPayment;
use App\EventPayment;
use App\EventWithdrawal;
use App\MticsEvent;
use App\EventExpense;
use App\EventAmountBreakdown;
use App\Inventory;
use App\ArchivedInventory;
use App\BorrowerInventory;
use App\Borrower;
use App\EquipRepairReport;
use App\Role;
use App\RoleMember;
use App\AuditorFinancialApprove;
use App\Officerterm;
use App\Penalty;
















class ReportsController extends Controller
{

//FINANCE //FINANCE //FINANCE //FINANCE //FINANCE //FINANCE //FINANCE //FINANCE

//MTICS FINANCIAL REPORT //MTICS FINANCIAL REPORT //MTICS FINANCIAL REPORT //MTICS FINANCIAL
	private function CheckMticsFundBucket($month, $year)
    {
        //formula for total Mtics fund in bank (this is the virtual bank amount)
    $mticsfundwithdraw = MticsWithdrawal::where('withdrawal_status','cleared')->whereMonth('updated_at', '<=', $month)->whereYear('updated_at', $year)->get()->sum('withdrawal_amt');
	$mticsfunddeposit = MticsDeposit::where('deposit_status','cleared')->whereMonth('updated_at', '<=', $month)->whereYear('updated_at', $year)->get()->sum('deposit_amt');
	$mticsdeposittoevent = EventDeposit::where('deposit_source','MTICS Fund')->whereMonth('updated_at', '<=', $month)->whereYear('updated_at', $year)->get()->sum('deposit_amt');
	$mticsfundbucket = $mticsfunddeposit - $mticsfundwithdraw - $mticsdeposittoevent;
    return $mticsfundbucket;
    }


	public function finance_mtics_view(){

		if(!Gate::allows('auditor-only') and !Gate::allows('finance-only') and !Gate::allows('president-only') and !Gate::allows('mticsadviser-only') ) {
            return redirect('/');
        }

		$term = Officerterm::where('status','on-going')->first();
		$events = MticsEvent::where('start_date','>=',date($term->created_at))->get();
	
		return view('reports/finance/finance_mtics_view', compact('events'));
	}

	public function finance_mtics_view_month(Request $request){



		$this->validate($request, [
        'month' => 'required',
    	]);

		$dateObj = DateTime::createFromFormat('Y-m', $request->month);

    	$budgetmonth = $dateObj->format('F Y');
    	$month = $dateObj->format('m');
    	$year = $dateObj->format('Y');

    	//FETCHING OFFICERS
    	$day = $dateObj->format('Y-m-d');
		$officerterm = Officerterm::where('from_year',$year - 1)->where('to_year',$year)->first();

		if(!Officerterm::where('from_year',$year)->where('to_year',$year + 1)->get()->isEmpty()){

			$nextbatch = Officerterm::where('from_year',$year)->where('to_year',$year + 1)->first();
			
			$from = date($officerterm->created_at);
			$to = date($nextbatch->created_at);	

			if(($day >= $from)&&($day <= $to)){}
			else
			{
				$officerterm = $nextbatch;
			}
			
		}
    	
    	if($officerterm->status == 'on-going')
    	{
	    	$roles = Role::wherehas('role_member')->where('name','finance')->orwhere('name','asst_finance')->orwhere('name','auditor')->orwhere('name','asst_auditor')->orwhere('name','president')->orderby('id','DESC')->with('member')->get();
	    		
	    		
    	}
    	else
    	{
    		$roles = Role::wherehas('prev_officer',function($q) use($officerterm){
	            $q->where('officerterm_id',$officerterm->id);
	        })->where('name','finance')->orwhere('name','asst_finance')->orwhere('name','auditor')->orwhere('name','asst_auditor')->orwhere('name','president')->with('prev_member')->orderby('id','DESC')->get();
	    		
    	}




    	//MONTHLY BUDGET AND EXPENSES
		$monthly_budgets = MticsBudget::with('budget_breakdown.budget_expenses','budget_breakdown.budget_expenses_cat')->where('budget_month',$request->month)->where('budget_status','!=','denied')->orderby('updated_at','ASC')->get();

		//WITHDRAWAL FETCHING
		$withdrawals = MticsWithdrawal::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('withdrawal_status','cleared')->get();
		$totalbudget = 0;
		foreach ($monthly_budgets as $monthly_budget) {
			$totalbudget = $totalbudget + $monthly_budget->budget_amt;
		}
		
		$totalwithdraw = $withdrawals->sum('withdrawal_amt');

		$excess = '';
		if(!$monthly_budgets->isEmpty()){
		$excess = $monthly_budgets->first()->excess;
		}
		
		
		
		//DEPOSIT FETCING
    	$deposits = MticsDeposit::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('deposit_status','cleared')->get();
    	$totaldeposit = $deposits->sum('deposit_amt');

    	$externaltasks = Task::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('event_id',null)->has('mticspurchaselist')->where(function($q) {
    			$q->where('task_status','done')->orwhere('task_status','failed');
	    	})->get();
		// dd($externaltasks->mticspurchaselist);
//

		//CHECK THE TOTAL AMOUNT STATUS OF MTICS BANK FUND
		$mticsbankamount = $this->CheckMticsFundBucket($month, $year);

		//not yet used
		$eventborrows = EventDeposit::with('event')->where('deposit_source','MTICS Fund')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('deposit_status','cleared')->get();



		//FOR DETAILED INFO
    	$alldeposits = MticsDeposit::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();
		$allwithdrawals = MticsWithdrawal::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();
		$allmonthly_budgets = MticsBudget::with('withdraw.receipt')->with('budget_breakdown.budget_expenses','budget_breakdown.budget_expenses_cat','budget_breakdown.budget_expenses.receipt')->where('budget_month',$request->month)->get();

		$allexternaltasks = Task::with('moneyrequest_all')->with('moneyrequest_all.reimburse')->wherehas('mticspurchaselist')->with('mticspurchaselist.mticspurchaseequiplist')->with('mticspurchaselist')->with('mticspurchaselist.receipt')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where(function($q) {
				$q->where('task_status','done')->orwhere('task_status','failed');
			})->get();

		$mticspayments = MticsfundPayment::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();

		//dd($allexternaltasks);
		$requestmonth = $request->month;
		$auditapprove = AuditorFinancialApprove::where('month',$requestmonth)->first();
		//dd($auditapprove);
		//dd($externaltasks);
		$penalties = Penalty::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('fee','!=','null')->where('penalty_status','paid')->get();
		//dd($penaltyfee);

		return view('reports/finance/finance_mtics_view_month',compact('monthly_budgets','budgetmonth','totalbudget','totalwithdraw','withdrawals','externaltasks','eventborrows','mticsbankamount','deposits','totaldeposit','allwithdrawals','alldeposits','allmonthly_budgets','allexternaltasks','mticspayments','requestmonth','auditapprove','excess','officerterm','roles','penalties'));

	}



	public function finance_mtics_view_print(Request $request){

		//dd($request);
		$dateObj = DateTime::createFromFormat('Y-m', $request->month);

    	$budgetmonth = $dateObj->format('F Y');
    	$month = $dateObj->format('m');
    	$year = $dateObj->format('Y');


    	//MONTHLY BUDGET AND EXPENSES
		$monthly_budgets = MticsBudget::with('budget_breakdown.budget_expenses','budget_breakdown.budget_expenses_cat')->where('budget_month',$request->month)->where('budget_status','!=','denied')->orderby('updated_at','ASC')->get();

		//WITHDRAWAL FETCHING
		$withdrawals = MticsWithdrawal::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('withdrawal_status','cleared')->get();
		$totalbudget = 0;
		foreach ($monthly_budgets as $monthly_budget) {
			$totalbudget = $totalbudget + $monthly_budget->budget_amt;
		}
		
		$excess = '';	
		if(!$monthly_budgets->isEmpty())
		{
		$excess = $monthly_budgets->first()->excess;
		}
		$totalwithdraw = $withdrawals->sum('withdrawal_amt');

		

		//DEPOSIT FETCING
    	$deposits = MticsDeposit::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('deposit_status','cleared')->get();
    	$totaldeposit = $deposits->sum('deposit_amt');

		//REQUEST TO BUY EXTERNAL EXPENSES
		$externaltasks = Task::wherehas('mticspurchaselist')->with('mticspurchaselist')->where(function($q) {
			$q->where('task_status','done')->orwhere('task_status','failed');
		})->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();

		//CHECK THE TOTAL AMOUNT STATUS OF MTICS BANK FUND
		$mticsbankamount = $this->CheckMticsFundBucket($month, $year);

		//not yet used
		$eventborrows = EventDeposit::with('event')->where('deposit_source','MTICS Fund')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('deposit_status','cleared')->get();

		//$officerterm = Officerterm::where('status','on-going')->first();
		
		
    	$day = $dateObj->format('Y-m-d');

		$officerterm = Officerterm::where('from_year',$year - 1)->where('to_year',$year)->first();

		if(!Officerterm::where('from_year',$year)->where('to_year',$year + 1)->get()->isEmpty()){

			$nextbatch = Officerterm::where('from_year',$year)->where('to_year',$year + 1)->first();
			
			$from = date($officerterm->created_at);
			$to = date($nextbatch->created_at);	

			if(($day >= $from)&&($day <= $to)){}
			else
			{
				$officerterm = $nextbatch;
			}
			
		}
		

    	$fromyear = $officerterm->from_year;
    	$toyear = $officerterm->to_year;

    	if($officerterm->status == 'on-going')
    	{
	    	$roles = Role::where('name','finance')->orwhere('name','asst_finance')->orwhere('name','auditor')->orwhere('name','asst_auditor')->orwhere('name','president')->orderby('id','DESC')->with('member')->get();
	    		
	    		
    	}
    	else
    	{
    		$roles = Role::wherehas('prev_officer',function($q) use($officerterm){
	            $q->where('officerterm_id',$officerterm->id);
	        })->where('name','finance')->orwhere('name','asst_finance')->orwhere('name','auditor')->orwhere('name','asst_auditor')->orwhere('name','president')->with('prev_member')->orderby('id','DESC')->get();
	    		
    	}

    	//dd($roles);

    	$penalties = Penalty::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('fee','!=','null')->where('penalty_status','paid')->get()->sum('fee');
		return view('reports/finance/finance_mtics_view_print1',compact('monthly_budgets','budgetmonth','totalbudget','totalwithdraw','withdrawals','externaltasks','eventborrows','mticsbankamount','deposits','totaldeposit','excess','fromyear','toyear','roles','officerterm','penalties'));

	}


	public function auditor_mtics_verify(Request $request){
		//dd($request);

		$auditor = Role::with('member')->where('name','auditor')->first();
		$auditor_id = RoleMember::where('role_id',$auditor->id)->first()->member_id;

		if($auditor_id !== Auth::id()){
			return redirect('admin/finance/financial-reports')->withErrors('You are not Auditor');
		}

		if(!MticsBudget::where('budget_month',$request->month)->where('budget_status','on-going')->get()->isEmpty())
			{
			return redirect('admin/finance/financial-reports')->withErrors('You cannot validate this month, There is still an on-going Budget');
			}


		if(AuditorFinancialApprove::where('month',$request->month)->get()->isEmpty())
		{
			$approve = New AuditorFinancialApprove;
			$approve->month = $request->month;
			$approve->remarks = $request->remarks ;
			if($request->remarks == null)
			{
			$approve->remarks = 'has been validate';
			}
			$approve->admin_id = Auth::id();
			$approve->save();
		}
		else
		{
			return redirect('admin/finance/financial-reports')->withErrors('You already verify this report');
		}

		return redirect('admin/finance/financial-reports');
	}



//EVENT LIQUIDATION //EVENT LIQUIDATION //EVENT LIQUIDATION //EVENT LIQUIDATION //EVENT


	public function finance_event_reports($id){
		$event = MticsEvent::find($id);
		$payment = EventPayment::where('event_id',$id)->get()->sum('payevent_amt');
		$expenses = EventAmountBreakdown::wherehas('expenses')->with('expenses')->where('event_id',$id)->get();
		$fromMTICSfund = EventDeposit::where('event_id',$id)->where('deposit_source','MTICS Fund')->get()->sum('deposit_amt');
		$fromOthers = EventDeposit::where('event_id',$id)->where('deposit_source','other')->get();
		$eventwithdraws = EventWithdrawal::where('event_id',$id)->get();
		$externaltasks = Task::wherehas('eventpurchaselist')->with('eventpurchaselist')->where('task_status','done')->where('event_id',$id)->get();


		//detailed
		$eventdeposits = EventDeposit::where('event_id',$id)->get();
		$auditapprove = AuditorFinancialApprove::where('event_id',$id)->get();
		$eventpayments = EventPayment::where('event_id',$id)->get();

		
    	$day = date("Y-m-d", strtotime($event->start_date));
    	$month = date("m", strtotime($event->start_date));
    	$year =  date("Y", strtotime($event->start_date));

		$officerterm = Officerterm::where('from_year',$year - 1)->where('to_year',$year)->first();

		if(!Officerterm::where('from_year',$year)->where('to_year',$year + 1)->get()->isEmpty()){

			$nextbatch = Officerterm::where('from_year',$year)->where('to_year',$year + 1)->first();
			
			$from = date($officerterm->created_at);
			$to = date($nextbatch->created_at);	

			if(($day >= $from)&&($day <= $to)){}
			else
			{
				$officerterm = $nextbatch;
			}
			
		}
		

    	$fromyear = $officerterm->from_year;
    	$toyear = $officerterm->to_year;

    	if($officerterm->status == 'on-going')
    	{
	    	$roles = Role::where('name','finance')->orwhere('name','asst_finance')->orwhere('name','auditor')->orwhere('name','asst_auditor')->orwhere('name','president')->orderby('id','DESC')->with('member')->get();
	    		
	    		
    	}
    	else
    	{
    		$roles = Role::wherehas('prev_officer',function($q) use($officerterm){
	            $q->where('officerterm_id',$officerterm->id);
	        })->where('name','finance')->orwhere('name','asst_finance')->orwhere('name','auditor')->orwhere('name','asst_auditor')->orwhere('name','president')->with('prev_member')->orderby('id','DESC')->get();
	    		
    	}

		return view('reports/finance/finance_event_reports1', compact('event','payment','expenses','fromMTICSfund','externaltasks','fromOthers','eventwithdraws','eventdeposits','auditapprove','eventpayments','fromyear','toyear','officerterm','roles'));
	}


	public function finance_event_print($id){
		$event = MticsEvent::find($id);
		$auditapprove = AuditorFinancialApprove::where('event_id',$id)->get();
		if($auditapprove->isEmpty())
		{
			return redirect('admin/turn-over')->withErrors('Auditor needs an approval');
		}


    	$day = date("Y-m-d", strtotime($event->start_date));
    	$month = date("m", strtotime($event->start_date));
    	$year =  date("Y", strtotime($event->start_date));

		$officerterm = Officerterm::where('from_year',$year - 1)->where('to_year',$year)->first();

		if(!Officerterm::where('from_year',$year)->where('to_year',$year + 1)->get()->isEmpty()){

			$nextbatch = Officerterm::where('from_year',$year)->where('to_year',$year + 1)->first();
			
			$from = date($officerterm->created_at);
			$to = date($nextbatch->created_at);	

			if(($day >= $from)&&($day <= $to)){}
			else
			{
				$officerterm = $nextbatch;
			}
			
		}
		

    	$fromyear = $officerterm->from_year;
    	$toyear = $officerterm->to_year;

    	if($officerterm->status == 'on-going')
    	{
	    	$roles = Role::where('name','finance')->orwhere('name','asst_finance')->orwhere('name','auditor')->orwhere('name','asst_auditor')->orwhere('name','president')->orderby('id','DESC')->with('member')->get();
	    		
	    		
    	}
    	else
    	{
    		$roles = Role::wherehas('prev_officer',function($q) use($officerterm){
	            $q->where('officerterm_id',$officerterm->id);
	        })->where('name','finance')->orwhere('name','asst_finance')->orwhere('name','auditor')->orwhere('name','asst_auditor')->orwhere('name','president')->with('prev_member')->orderby('id','DESC')->get();
	    		
    	}



		$payment = EventPayment::where('event_id',$id)->get()->sum('payevent_amt');
		$expenses = EventAmountBreakdown::wherehas('expenses')->with('expenses')->where('event_id',$id)->get();
		$fromMTICSfund = EventDeposit::where('event_id',$id)->where('deposit_source','MTICS Fund')->get()->sum('deposit_amt');
		$fromOthers = EventDeposit::where('event_id',$id)->where('deposit_source','other')->get();
		$eventwithdraws = EventWithdrawal::where('event_id',$id)->get();
		$externaltasks = Task::wherehas('eventpurchaselist')->with('eventpurchaselist')->where('task_status','done')->where('event_id',$id)->get();

	
   

		return view('reports/finance/finance_event_print', compact('event','payment','expenses','fromMTICSfund','externaltasks','fromOthers','eventwithdraws','fromyear','toyear','roles','officerterm'));
	}


	public function auditor_event_verify(Request $request,$id){
		//dd($request);

		$auditor = Role::with('member')->where('name','auditor')->first();
		$auditor_id = RoleMember::where('role_id',$auditor->id)->first()->member_id;
		$event = MticsEvent::find($id);

		if($auditor_id !== Auth::id()){
			return redirect('admin/finance/financial-reports')->withErrors('You are not Auditor');
		}

		if($event->status !== 'done')
			{
			return redirect('admin/finance/financial-reports')->withErrors('You cannot validate this month, Event Status is '.$event->status);
			}

		if(AuditorFinancialApprove::where('event_id',$id)->get()->isEmpty())
		{
			$approve = New AuditorFinancialApprove;
			$approve->event_id = $id;
			$approve->remarks = $request->remarks ;
			if($request->remarks == null)
			{
			$approve->remarks = 'has been validate';
			}
			$approve->admin_id = Auth::id();
			$approve->save();
		}
		else
		{
			return redirect('admin/finance/financial-reports')->withErrors('You already verify this report');
		}

		return redirect('admin/finance/financial-reports');
	}



//RECEIPT //RECEIPT //RECEIPT //RECEIPT //RECEIPT //RECEIPT //RECEIPT //RECEIPT //RECEIPT
	public function finance_mtics_receipt($id){
		$payment = MticsfundPayment::find($id);
		$date = date('F d, Y');
		$numberToWords = new NumberToWords();
		$numberTransformer = $numberToWords->getNumberTransformer('en');
		$amt_in_words = $numberTransformer->toWords($payment->paymticsfund_amt);
		//dd($amt_in_words);
		return view('reports/finance/mtics_payment_receipt',compact('payment','date','amt_in_words'));
	}

	public function finance_event_receipt($id){
		$payment = EventPayment::find($id);
		$date = date('F d, Y');
		$numberToWords = new NumberToWords();
		$numberTransformer = $numberToWords->getNumberTransformer('en');
		$amt_in_words = $numberTransformer->toWords($payment->payevent_amt);
		//dd($amt_in_words);
		return view('reports/finance/event_payment_receipt',compact('payment','date','amt_in_words'));
	}

	public function finance_selected_event_receipt(Request $request){


		$payments = EventPayment::where('member_id',$request->member_id)->where('event_id',$request->event_id)->get();

		if($payments->isEmpty()){
			return redirect()->back()->withErrors('Student have not paid yet');
		}

		$totalpayment = $payments->sum('payevent_amt');
		$date = date('F d, Y');
		$numberToWords = new NumberToWords();
		$numberTransformer = $numberToWords->getNumberTransformer('en');
		$amt_in_words = $numberTransformer->toWords($totalpayment);
		//dd($amt_in_words);

		foreach ($payments as $payment) {
			$name = $payment->member->first_name.' '.$payment->member->last_name;
			$section = $payment->member->section->section_code;
			$eventtitle = $payment->event->event_title;
		}


		return view('reports/finance/selected_event_payment_receipt',compact('payments','date','amt_in_words','totalpayment','name','section','eventtitle'));


	}




//LOGISTICS //LOGISTICS //LOGISTICS //LOGISTICS //LOGISTICS //LOGISTICS //LOGISTICS //LOGISTICS

	public function logistics_monthly_reports(Request $request){
		//dd($request);

		$dateObj = DateTime::createFromFormat('Y-m', $request->month);
    	$selectedmonth = $dateObj->format('F Y');
    	$month = $dateObj->format('m');
    	$year = $dateObj->format('Y');

    	$addeds = Inventory::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();
    	$archiveds = ArchivedInventory::with('inventory')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();

    	/*$borrow_returns = Borrower::with('borrower_inv')->with('member')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('borrower_status','Returned')->get();
    	dd($borrow_returns);*/

    	$pending_borrows = Borrower::with('borrower_inv')->with('member')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('borrower_status','Borrowed')->get();

    	$repairreports = EquipRepairReport::with('equip_inv')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('status','damage')->get();
    	/*dd($repairreports);*/

		return view('reports/logistics/logistics_monthly_reports',compact('selectedmonth','addeds','archiveds','borrow_returns','pending_borrows','repairreports'));
	}




//AUDITOR //AUDITOR //AUDITOR //AUDITOR //AUDITOR //AUDITOR //AUDITOR //AUDITOR //AUDITOR //AUDITOR //AUDITOR

	public function auditor_mtics_view(){
		return view('reports/auditor/auditor_mtics_view');

	}

	public function auditor_mtics_view_month(Request $request){



		$this->validate($request, [
        'month' => 'required',
    	]);

		$dateObj = DateTime::createFromFormat('Y-m', $request->month);

    	$budgetmonth = $dateObj->format('F Y');
    	$month = $dateObj->format('m');
    	$year = $dateObj->format('Y');


    	//MONTHLY BUDGET AND EXPENSES
		$monthly_budgets = MticsBudget::with('budget_breakdown.budget_expenses','budget_breakdown.budget_expenses_cat')->where('budget_month',$request->month)->where('budget_status','!=','denied')->get();

		//WITHDRAWAL FETCHING
		$withdrawals = MticsWithdrawal::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('withdrawal_status','cleared')->get();
		$totalbudget = 0;
		foreach ($monthly_budgets as $monthly_budget) {
			$totalbudget = $totalbudget + $monthly_budget->budget_amt;
		}
		$totalwithdraw = $withdrawals->sum('withdrawal_amt');


		//DEPOSIT FETCING
    	$deposits = MticsDeposit::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('deposit_status','cleared')->get();
    	$totaldeposit = $deposits->sum('deposit_amt');

		//REQUEST TO BUY EXTERNAL EXPENSES
		$externaltasks = Task::wherehas('mticspurchaselist')->with('mticspurchaselist')->where('task_status','done')->orwhere('task_status','failed')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();

		//CHECK THE TOTAL AMOUNT STATUS OF MTICS BANK FUND
		$mticsbankamount = $this->CheckMticsFundBucket();

		//not yet used
		$eventborrows = EventDeposit::with('event')->where('deposit_source','MTICS Fund')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('deposit_status','cleared')->get();



		//FOR DETAILED INFO
    	$alldeposits = MticsDeposit::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();
		$allwithdrawals = MticsWithdrawal::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();
		$allmonthly_budgets = MticsBudget::with('withdraw.receipt')->with('budget_breakdown.budget_expenses','budget_breakdown.budget_expenses_cat')->where('budget_month',$request->month)->get();
		$allexternaltasks = Task::with('moneyrequest_all')->with('moneyrequest_all.reimburse')->wherehas('mticspurchaselist')->with('mticspurchaselist.mticspurchaseequiplist')->with('mticspurchaselist')->with('mticspurchaselist.receipt')->where('task_status','done')->orwhere('task_status','failed')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();


		$requestmonth = $request->month;

		//dd($monthly_budgets);
		return view('reports/auditor/auditor_mtics_view_month',compact('monthly_budgets','budgetmonth','totalbudget','totalwithdraw','withdrawals','externaltasks','eventborrows','mticsbankamount','deposits','totaldeposit','allwithdrawals','alldeposits','allmonthly_budgets','allexternaltasks','mticspayments','requestmonth','auditapprove'));

	}

	public function auditor_mtics_view_print(Request $request){



		$this->validate($request, [
        'month' => 'required',
    	]);

		$dateObj = DateTime::createFromFormat('Y-m', $request->month);

    	$budgetmonth = $dateObj->format('F Y');
    	$month = $dateObj->format('m');
    	$year = $dateObj->format('Y');


    	//MONTHLY BUDGET AND EXPENSES
		$monthly_budgets = MticsBudget::with('budget_breakdown.budget_expenses','budget_breakdown.budget_expenses_cat')->where('budget_month',$request->month)->where('budget_status','!=','denied')->get();

		//WITHDRAWAL FETCHING
		$withdrawals = MticsWithdrawal::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('withdrawal_status','cleared')->get();
		$totalbudget = 0;
		foreach ($monthly_budgets as $monthly_budget) {
			$totalbudget = $totalbudget + $monthly_budget->budget_amt;
		}
		$totalwithdraw = $withdrawals->sum('withdrawal_amt');


		//DEPOSIT FETCING
    	$deposits = MticsDeposit::whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('deposit_status','cleared')->get();
    	$totaldeposit = $deposits->sum('deposit_amt');

		//REQUEST TO BUY EXTERNAL EXPENSES
		$externaltasks = Task::wherehas('mticspurchaselist')->with('mticspurchaselist')->where('task_status','done')->orwhere('task_status','failed')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();

		//CHECK THE TOTAL AMOUNT STATUS OF MTICS BANK FUND
		$mticsbankamount = $this->CheckMticsFundBucket();

		//not yet used
		$eventborrows = EventDeposit::with('event')->where('deposit_source','MTICS Fund')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->where('deposit_status','cleared')->get();



		//FOR DETAILED INFO
		$allexternaltasks = Task::with('moneyrequest_all')->with('moneyrequest_all.reimburse')->wherehas('mticspurchaselist')->with('mticspurchaselist.mticspurchaseequiplist')->with('mticspurchaselist')->with('mticspurchaselist.receipt')->where('task_status','done')->orwhere('task_status','failed')->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();


		$requestmonth = $request->month;

		//dd($monthly_budgets);
		return view('reports/auditor/auditor_mtics_view_print',compact('monthly_budgets','budgetmonth','totalbudget','totalwithdraw','withdrawals','externaltasks','eventborrows','mticsbankamount','deposits','totaldeposit','allexternaltasks','mticspayments','requestmonth','auditapprove'));

	}


}
