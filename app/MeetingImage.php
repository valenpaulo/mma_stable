<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingImage extends Model
{
 protected $table = 'meeting_images';
	protected $fillable = [
    'meeting_id','admin_id','image_name'
    ];

     public function meeting()
    {
        return $this->belongsto(Meeting::class);
    }

     public function admin()
    {
        return $this->belongsto(Member::class,'admin_id');
    }

}
