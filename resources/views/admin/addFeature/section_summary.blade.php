@extends('admin/layout/addFeature')

@section('middle')




<div class="row">
<div class="col-sm-2"></div>
<div class="col-sm-8 w3-padding">
<div class="w3-card-6  w3-white">
	
    <div class="w3-container">
      <!-- Row -->
      <div class="row">

      <div class="col-sm-12">
      <h3>
          <i class="fa fa-group fa-fw"></i> <strong>Master List of {{$section->course->course_code}} - {{$year->year_code}} Year - {{$section->section_code}} Master List</strong>
      </h3>
      </div>

      </div>
    </div>

     <div class="tab-content">
    <div id="courseTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
        <table class="table table-bordered table-responsive">
          <thead class="w3-teal">
            <tr>
             <th class="w3-center">No</th>
             <th class="w3-center">Student ID</th>
             <th class="w3-center">Last Name</th>
             <th class="w3-center">First Name</th>
            </tr>
          </thead>
          <tbody class="w3-text-black">
          	<?php $count = 1; ?>
            @foreach($students as $student)
          	
            <tr>
              <td class="w3-center">{{$count++}}</td>
              <td class="w3-center">{{$student->id_num}}</td>
              <td class="w3-center">{{$student->last_name}}</td>
              <td class="w3-center">{{$student->first_name}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
    </div>

</div>
</div>
	

@if(!empty($NotInList))		
<div class="container">
  <!-- Row -->
  <div class="row">

  <div class="col-sm-12">
  <h3>
      <i class="fa fa-circle fa-fw w3-text-red"></i> Removed From Masterlist (Set as <strong>'Inactive'</strong>)
  </h3>
  </div>

  </div>
</div>

	<div class="tab-content">
    <div id="courseTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
    <table class="table table-bordered table-responsive">
      <thead class="w3-teal">
        <tr>
         <th class="w3-center">No.</th>
         <th class="w3-center">Student ID</th>
         <th class="w3-center">First Name</th>
         <th class="w3-center">Last Name</th>
        </tr>
      </thead>
      <tbody class="w3-text-black">
         <?php $count = 1; ?>
        @foreach($NotInList as $key => $value)
        <tr>
          <td class="w3-center">{{$count++}}</td>
          <td class="w3-center">{{$value['stud_id']}}</td>
          <td class="w3-center">{{$value['first_name']}}</td>
          <td class="w3-center">{{$value['last_name']}}</td>
        </tr>

        @endforeach
      </tbody>
    </table>
</div>
</div>

</div>
</div>
@endif


@if(!empty($NotInDB))		
<div class="container">
  <!-- Row -->
  <div class="row">

  <div class="col-sm-12">
  <h3>
      <i class="fa fa-circle fa-fw w3-text-green"></i> Not Registerred Student (Please <a href="{{ url('admin/manage-members')}}"> Register </a> the student)
  </h3>
  </div>

  </div>
</div>

	<div class="tab-content">
    <div id="courseTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
    <table class="table table-bordered table-responsive">
      <thead class="w3-teal">
        <tr>
         <th class="w3-center">No.</th>
         <th class="w3-center">Student ID</th>
         <th class="w3-center">First Name</th>
         <th class="w3-center">Last Name</th>
        </tr>
      </thead>
      <tbody class="w3-text-black">
         <?php $count = 1; ?>
        @foreach($NotInDB as $key => $value)
        <tr>
          <td class="w3-center">{{$count++}}</td>
          <td class="w3-center">{{$value['stud_id']}}</td>
          <td class="w3-center">{{$value['first_name']}}</td>
          <td class="w3-center">{{$value['last_name']}}</td>
        </tr>

        @endforeach
      </tbody>
    </table>
</div>
</div>

</div>
</div>
@endif



<br>
	<div class="row">
		<div class="col-sm-5"></div>
		<div class="col-sm-2">
		<a href="{{url('admin/manage-sections')}}"><button type="button" class="btn btn-primary w3-text-white"><i class="fa fa-arrow-circle-left"></i> Go Back</button></a>
			
		</div>
		<div class="col-sm-5"></div>

	</div>
</div>
</div>
</div>
    	



@endsection