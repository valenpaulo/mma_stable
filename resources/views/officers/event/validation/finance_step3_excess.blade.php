@extends('layout/control_panel')

@section('title')
{{$event->event_title}}
@endsection

@section('middle')
<div class="container-fluid w3-margin">
  <div class="row">
      <h3>
        <i class="fa fa-calendar-check-o fa-fw"></i>
        <strong>{{$event->event_title}} <i class="fa fa-angle-double-right fa-fw"></i> {{$moneyRequestsTaskDone->task->task_name}}</strong>
      </h3>

  </div>

  <div class="row w3-margin-top">

    <h4>
      <strong>Step 3: Collect Excess Money</strong>
      @if(!is_null($moneyRequestsTaskDone->excess_amount))
      <a class="w3-text-green" href="{{url('admin/finance/manage-event/' . $event->id . '/validate/' . $moneyRequestsTaskDone->id . '/next')}}"><i class="fa fa-arrow-circle-right fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Next Step"></i></a>
      @endif
    </h4>

  </div>

  @if(is_null($moneyRequestsTaskDone->excess_amount))
  <div class="row w3-margin-top" align="center">
    <a class="w3-text-green w3-large" href="#collectExcess" data-toggle="modal"><i class="fa fa-plus-square fa-fw" data-toggle="tooltip" data-placement="bottom" title="Accept Request"></i> Collect</a>
  </div>
  @endif

  <!-- COLLECT EXCESS MONEY MODAL -->
  <div class="modal fade" id="collectExcess" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Collect Excess Money</h4>
  </div>
  <form action="{{ url('admin/finance/payment/excess-money') }}" method="POST" enctype="multipart/form-data">
  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

      <div class="form-group">
        <label>Excess Money</label>
        <input type="number" name="amount" id="amount" tabindex="1" class="form-control" value="{{$excess}}" readonly>
      </div>

      <div class="form-group">
        <p>Do you want to proceed?</p>
      </div>

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="task_id" value="{{ $moneyRequestsTaskDone->task_id }}">
    <button type="submit" class="btn btn-default w3-green">Yes</button>
    <button type="button" data-dismiss="modal" class="btn btn-default w3-red">No</button>
  </div>

  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->

  <div class="row w3-margin-top">
  <p>Click the <i class="fa fa-arrow-circle-right fa-fw w3-text-green"></i> icon to proceed to the next step.</p>
  </div>

  <div class="row w3-margin-top" align="center">

    <div class="col-lg-3 col-md-3 col-sm-1">
      <br>
      <div class="w3-card-4 w3-green w3-text-black w3-padding w3-container">

        <i class="fa fa-money w3-xxxlarge"></i>

        <h3>
        <strong>{{$moneyRequestsTaskDone->amount}} pesos</strong>
        </h3>

        <label>Requested Amount</label>

      </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-1">
      <div class="w3-card-4 w3-yellow w3-text-black w3-padding w3-container">

        <br>
        <i class="fa fa-shopping-bag w3-xxxlarge"></i>
        <i class="fa fa-money w3-xxxlarge"></i>

        <h3>
        <strong>{{$moneyRequestsTaskDone->task->eventpurchaselist->sum('event_total_price')}} pesos</strong>
        </h3>

        <label>Purchased Amount</label>

        <br>
        <br>

      </div>
    </div>



    <div class="col-lg-3 col-md-3 col-sm-1">
      <br>
      <div class="w3-card-4 w3-purple w3-text-black w3-padding w3-container">

        <i class="fa fa-money w3-xxxlarge"></i>
        <i class="fa fa-reply w3-xxxlarge"></i>

        <h3>
        <strong>{{$excess}} pesos</strong>
        </h3>

        <label>Excess Money</label>

      </div>
    </div>

  </div>

  <div class="row w3-margin-top">
    <h4>
      <i class="fa fa-file-text-o"></i>
      <strong>Breakdown of Purchased Amount</strong>
    </h4>
  </div>

  <div class="row w3-margin-top">
    <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
             <th class="w3-center">Item Name</th>
             <th class="w3-center">Quantity</th>
             <th class="w3-center">Amount</th>
             <th class="w3-center">Total</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($moneyRequestsTaskDone->task->eventpurchaselist as $item)
            <tr>
              <td class="w3-center">{{$item->event_itemname}}</td>
              <td class="w3-center">{{$item->event_act_quan}}</td>
              <td class="w3-center">{{$item->event_act_price}}</td>
              <td class="w3-center">{{$item->event_act_price * $item->event_act_quan}}</td>
            </tr>

          @endforeach
         </tbody>
        </table>
    </div>
  </div>

</div>

@endsection
