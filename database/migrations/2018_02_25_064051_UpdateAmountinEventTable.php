<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAmountinEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {

        Schema::table('events', function ($table) {
             $table->integer('faculty_id')->nullable();
             $table->integer('event_amount')->nullable();
             $table->enum('amount_confirm',['pending','approved','denied'])->nullable();
        });

         Schema::create('event_amount_breakdowns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->string('breakdown_name');
            $table->string('breakdown_desc')->nullable();
            $table->longtext('breakdown_amt');          
            $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
