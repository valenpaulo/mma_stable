<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrower extends Model
{
     protected $table = 'borrowers';
	protected $fillable = [
        'borrower_status','returned_datetime','created_at', 'member_id'
    ];

     public function borrower_inv()
    {
        return $this->hasMany(BorrowerInventory::class);
    }

     public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

}
