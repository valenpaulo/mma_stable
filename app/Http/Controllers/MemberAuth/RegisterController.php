<?php

namespace App\Http\Controllers\MemberAuth;

use App\Member;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use App\Section;
use App\Year;
use App\Course;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Mail\EmailVerify;
use Mail;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/member/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('member.guest');
    }

     public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'type' => 'required',
            'mobile_no' => 'required|max:11',
            'guardian_mobile_no' => 'max:11',
            'email' => 'required|email|max:255|unique:members',
            'password' => 'required|min:8|confirmed',
            'username' => 'required|min:6|unique:members',

        ]);

         if ($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }

        if($request['type'] == 'Student') {
            $validator = Validator::make($request->all(), [
                'year' => 'required',
                'section' => 'required',
                'stud_type' => 'required',
                'id_num' => 'required|max:7|unique:members',
              ]);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

             $section = Section::find($request['section']);
             $course = Course::find($section->course_id);
             $year =  Year::find($request['year']);

             if($course->course_year_count < $year->year_num)
             {
                return redirect()->back()->withErrors(['year' => $year->year_desc. ' is  not valid with your selected section'])->withInput();
             }
        }

        // return $validator;

        event(new Registered($user = $this->create($request->all())));


        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Member
     */
    protected function create(array $data)
    {

       // dd($data);

        if($data['type'] == 'Alumni')
        {
             $section_id = Section::getId('App\Section','section_code','Alumni');
             $year_id = Year::getId('App\Year','year_code','Alumni');
        }
        elseif($data['type'] == 'Faculty')
        {
             $section_id = Section::getId('App\Section','section_code','Faculty');
             $year_id = Year::getId('App\Year','year_code','Faculty');

        }
        else
        {
            $section_id = $data['section'];
            $year_id = $data['year'];
        }



        //dd($year_id);
        $data['username'] = str_replace(' ', '', $data['username']);
        
       
        $member = Member::create([
            'email' => $data['email'],
            'username' => $data['username'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'age' => $data['age'],
            'birthday' => $data['birthday'],
            'address' => $data['address'],
            'work' => $data['work'],
            'section_id' => $section_id,
            'year_id' => $year_id,
            'type' => $data['stud_type'],
            'id_num' => $data['id_num'],
            'mobile_no' => $data['mobile_no'],
            'guardian_name' => $data['guardian_name'],
            'guardian_mobile_no' => $data['guardian_mobile_no'],
            'password' => bcrypt($data['password']),
        ]);

        $thisUser = Member::findOrFail($member->id);
        $this->sendEmail($thisUser);
        return $member;

    }

    public function sendEmail($thisUser){
        Mail::send(new EmailVerify($thisUser));

    }

    public function sendEmailDone($id){
        $member = Member::findOrFail($id);
        $member->status = 'pending';
        $member->save();
        return redirect('/member/login');
    }


    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $sections = DB::table('sections')
            ->join('courses', 'courses.id', '=', 'sections.course_id')
            ->where('sections.section_status','active')
            ->where('sections.section_code','!=','Alumni')
            ->where('sections.section_code','!=','Faculty')
            ->select('sections.*','courses.course_code')
            ->get();
        $years = Year::where('year_code','!=','Faculty')->where('year_code','!=','Alumni')->get();
        return view('member.auth.register', compact('sections','years'));
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('member');
    }
}
