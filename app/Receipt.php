<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
 	protected $table = 'receipts';
	protected $fillable = [
        'receipt_image','admin_id','receipt_remarks','created_at','updated_at'
    ];

    public function mticspurchaselist()
    {
        return $this->belongsToMany(MticsPurchaseList::class, 'receipt_mticsitems','receipt_id','mtics_purchase_list_id');
    }

    public function eventpurchaselist()
    {
        return $this->belongsToMany(EventPurchaseList::class, 'receipt_eventitems','receipt_id','event_purchase_list_id');
    }
}
