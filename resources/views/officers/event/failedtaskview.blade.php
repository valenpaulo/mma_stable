@extends('layout/control_panel')

@section('title')
Manage Penalty
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-balance-scale fa-fw w3-xxlarge"></i>
      <strong>Manage Penalty</strong>
    </h3>
  </div>

  <hr>

  <div class="row">
    <p>
      <a href="#add-penalty" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i> Add</a>
    </p>
  </div>

      <!-- ADD MODAL -->
    <div class="modal fade" id="add-penalty" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Penalty</h4>
    </div>
    <form action="{{ url('admin/president/manage-event/'.$id.'/failed-task/add') }}" method="POST" enctype="multipart/form-data">

    <div class="modal-body">
      <div class="w3-container">
        <div class="row">

          <div class="form-group">
            <label>Officers:</label>

            <select class="form-control" id="officer_id" name="officer_id" placeholder="Select officers">
             @foreach($member_officers as $officer)
              <option value='{{$officer->member_id}}'>{{$officer->member->first_name}} {{$officer->member->last_name}}</option>
             @endforeach
            </select>
          </div>


          <div class="form-group">
            <select name="penalty_type" class="form-control" onchange="showPenaltyAmount()" id="penaltyType">
                <option value="verbal" selected>verbal</option>
                <option value="written">written</option>
                <option value="fee">fee</option>
            </select>
          </div>

          <div class="form-group">
            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" placeholder="Reason" rows="3"></textarea>
          </div>

            <div class="form-group" id="penaltyAmount" style="display: none">
                <label>Amount:</label>
                <input type="number" name="fee_amount" id="fee_amount" tabindex="1" class="form-control" placeholder="Amount">
            </div>

          <input type="hidden" name="_token" value="{{ csrf_token() }}">

        </div>
      </div>
    </div>

    <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
    </div>

    </form>
    </div>
    </div>
    </div>
    <!-- END MODAL -->

    <div class="row">
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="penaltyTable">
        <thead>
          <tr>
           <th class="w3-center">Officer Name</th>
           <th class="w3-center">Officer Position</th>
           <th class="w3-center">Reason</th>
           <th class="w3-center">Penalty Type</th>
           <th class="w3-center">Penalty Fee (if any)</th>
           <th class="w3-center">Status</th>
           <th class="w3-center">Action</th>
          </tr>
        </thead>
        <tbody class="w3-text-gray">
          @foreach($failedtasks as $failedtask)
          <tr>
            <td class="w3-center">{{$failedtask->member->first_name}} {{$failedtask->member->last_name}}</td>
            <td class="w3-center">
                @foreach($failedtask->member->role as $role)
                @if($role->assistant == 'false')
                {{$role->officer_name}}
                @endif
                @endforeach
            </td>
            <td class="w3-center">{{$failedtask->reason}}</td>
            <td class="w3-center">{{$failedtask->penalty_type}}</td>
            <td class="w3-center">{{$failedtask->fee}}</td>
            <td class="w3-center">{{$failedtask->penalty_status}}</td>

            <td class="w3-center">

                @if($failedtask->penalty_status == 'paid' or $failedtask->penalty_status == 'done')
                <i class="fa fa-check-circle-o fa-fw w3-large w3-text-green" data-toggle="tooltip" data-placement="bottom" title="Done"></i>
                <a href="#view-{{$failedtask->id}}" data-toggle="modal" class="w3-text-blue" style="outline: 0"><i class="fa fa-eye fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="View Upload"></i></a>

                @else
                @if(is_null($failedtask->fee))
                <a href="#done-{{$failedtask->id}}" data-toggle="modal" class="w3-text-green" style="outline: 0"><i class="fa fa-thumbs-o-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Accomplished Penalty"></i></a>
                @endif
                <a href="#edit-{{$failedtask->id}}" class="w3-text-orange" data-toggle="modal" style="outline: 0"><i class="fa fa-pencil-square-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Edit Penalty"></i></a>
                @endif


            </td>

             <!-- View Meeting MODAL -->
            <div class="modal fade" id="view-{{$failedtask->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Penalty Images</h4>
            </div>
            <div class="modal-body">
            <div class="w3-container">
            <div class="row">


              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                    <?php $count = 0; ?>
                  @foreach($failedtask->image as $image)   
                  <?php $count++; ?>
                  @if($count == 1)               
                  <div class="item active">
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $image->image_name) : asset('images/' . $image->image_name)}}" alt="Los Angeles">
                  </div>
                  @else
                  <div class="item">
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $image->image_name) : asset('images/' . $image->image_name)}}" alt="Los Angeles">
                  </div>
                  @endif

                  @endforeach
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>


            </div>
            </div>
            </div>
            <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <!--  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-plus-square fa-fw"></i> Yes</button> -->
            </div>

            </form>
            </div>
            </div>
            </div>
            <!--view meeting END MODAL -->


            <!-- DONE PENALTY MODAL -->
            <div class="modal fade" id="done-{{$failedtask->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Accomplished Penalty</h4>
            </div>
            <form action="{{ url('admin/president/manage-event/'.$id.'/failed-task/done/'.$failedtask->id) }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
            <div class="w3-container">
            <div class="row">

                   <p>You are about to set as done this penalty, Do you to continue?</p>

                    <div class="form-group">
                        <label id="image-name-{{$failedtask->id}}" for="receipt-{{$failedtask->id}}" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-text fa-fw w3-text-green w3-large" ></i> Upload Image</label>
                        <input type="file" name="upload[]" id="receipt-{{$failedtask->id}}" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showuploadFileName(event, {{$failedtask->id}})" multiple />
                    </div>

            </div>
            </div>
            </div>

            <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default w3-green"> Yes</button>
            <button type="button" class="btn btn-default w3-red" data-dismiss="modal"> No</button>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!--DONE PENALTY END MODAL -->

            <!-- EDIT PENALTY MODAL -->
            <div class="modal fade" id="edit-{{$failedtask->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Penalty</h4>
            </div>
            <form action="{{ url('admin/president/manage-event/'.$id.'/failed-task/edit/'.$failedtask->id) }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
            <div class="w3-container">
            <div class="row">

                <div class="form-group">
                <select name="penalty_type" class="form-control" onchange="showPenaltyAmountEdit({{$failedtask->id}})" id="penaltyType-{{$failedtask->id}}">
                    @if($failedtask->penalty_type == 'verbal')
                      <option value="verbal" selected>verbal</option>
                      <option value="written">written</option>
                      <option value="fee">fee</option>
                     @endif
                     @if($failedtask->penalty_type == 'written')
                      <option value="written" selected>written</option>
                      <option value="verbal">verbal</option>
                      <option value="fee">fee</option>
                     @endif

                     @if($failedtask->penalty_type == 'fee')
                      <option value="fee" selected>fee</option>
                      <option value="verbal">verbal</option>
                      <option value="written">written</option>
                     @endif
                </select>
                </div>

                @if($failedtask->reason == 'overdue task')
                <div class="form-group">
                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" placeholder="Reason" rows="3" disabled> {{$failedtask->reason}} </textarea>
                </div>
                @else
                <div class="form-group">
                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" placeholder="Reason" rows="3"> {{$failedtask->reason}} </textarea>
                </div>
                @endif

                <div class="form-group" id="penaltyAmount-{{$failedtask->id}}" @if($failedtask->penalty_type != 'fee') style="display: none;" @endif>
                  <label>Amount:</label>
                  <input type="number" name="fee_amount" id="fee_amount" tabindex="1" class="form-control" placeholder="Amount" value="{{$failedtask->fee}}">
                </div>


            </div>
            </div>
            </div>

            <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!--EDIT PENALTY END MODAL -->
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    </div>
</div>

<script type="text/javascript">
function showPenaltyAmount() {
    var type = $("#penaltyType").val();

    if (type == 'verbal') {

        $("#penaltyAmount").hide()

    } else if (type == 'written') {
        $("#penaltyAmount").hide()

    } else {
        $("#penaltyAmount").show()
    }
}
</script>

<script type="text/javascript">
function showPenaltyAmountEdit(id) {
    var type = $("#penaltyType-" + id).val();

    if (type == 'verbal') {

        $("#penaltyAmount-" + id).hide()

    } else if (type == 'written') {
        $("#penaltyAmount-" + id).hide()

    } else {
        $("#penaltyAmount-" + id).show()
    }
}
</script>

<script type="text/javascript">
$(document).ready( function () {
  $('#penaltyTable').DataTable();
} );
</script>

<script>
  // var input = document.getElementById( 'receipt' );

  // input.addEventListener('change', showFileName1);

  function showuploadFileName( event, id ) {

    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    $("#image-name-" + id).html(fileName);
  };
</script>

@endsection
