<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsDeposit extends Model
{
    protected $table = 'mtics_deposits';
	protected $fillable = [
        'admin_id','faculty_id','deposit_desc','deposit_amt','deposit_status','deposit_source','event_id','remarks'
    ];

    public function faculty() {
        return $this->belongsTo(Member::class, 'faculty_id');
    }

    public function receipt() {
        return $this->hasOne(MticsDepositReceipt::class);
    }

     public function deny() {
        return $this->hasOne(MticsDepositDeny::class);
    }

    public function report() {
        return $this->hasOne(MticsDepositReport::class);
    }


}
