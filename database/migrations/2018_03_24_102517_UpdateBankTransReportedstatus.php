<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBankTransReportedstatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
        Schema::table('mtics_withdrawals', function ($table) {
            $table->dropColumn('withdrawal_status');
        });   

         Schema::table('mtics_withdrawals', function ($table) {
            $table->enum('withdrawal_status',['pending','approved','denied','cleared','reported'])->default('pending')->after('withdrawal_amt');
        });  

        Schema::table('mtics_deposits', function ($table) {
            $table->dropColumn('deposit_status');
        });   

         Schema::table('mtics_deposits', function ($table) {
            $table->enum('deposit_status',['pending','approved','denied','cleared','reported'])->default('pending')->after('event_id');
        });  


         Schema::table('event_deposits', function ($table) {
            $table->dropColumn('deposit_status');
        });   

         Schema::table('event_deposits', function ($table) {
            $table->enum('deposit_status',['pending','approved','denied','cleared','reported'])->default('pending')->after('remarks');
        }); 


         Schema::table('event_withdrawals', function ($table) {
            $table->dropColumn('withdrawal_status');
        });   

         Schema::table('event_withdrawals', function ($table) {
            $table->enum('withdrawal_status',['pending','approved','denied','cleared','reported'])->default('pending')->after('withdrawal_amt');
        });  



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
