@extends('admin/layout/admin_panel')

@section('middle')


<br><br><br>
<div class="container">
<div class="row">
  <div class="w3-margin w3-card-4 w3-white">
    <table class="table table-hover table-responsive">
      <thead class="w3-teal">
        <tr>
         <th class="w3-center">Title</th>
         <th class="w3-center">Start date</th>
         <th class="w3-center">End Date</th>
         <th class="w3-center">Status</th> 
         <th class="w3-center">Event Amount</th>
         <th class="w3-center">Amount Approved Status</th>   
         <th class="w3-center"></th>     
                       

        </tr>
      </thead>
      <tbody class="w3-text-gray">
        @foreach($events as $event)
       
        <tr>
          <td class="w3-center">{{$event->event_title}}</td>
          <td class="w3-center">{{$event->start_date}}</td>
          <td class="w3-center">{{$event->end_date}}</td>
          <td class="w3-center">{{$event->status}}</td>
          <td class="w3-center">{{$event->event_amount}}</td>          
          <td class="w3-center">{{$event->amount_confirm}}</td>   
		      <td>
            <a href="{{ url('admin/finance/event-fund/'.$event->id) }}"><button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#view-{{$event->id}}" title="Edit"><i class="fa fa-pencil"></i> VIEW</button></a>
		        <a href="{{ url('admin/finance/event-fund/'.$event->id.'/manage-payment') }}"><button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#managepayment-{{$event->id}}" title="Edit"><i class="fa fa-pencil"></i> Manage Payment</button></a>
            <a href="{{ url('admin/finance/event-fund/'.$event->id.'/expenses') }}"><button class="btn btn-default w3-orange w3-text-white" title="Edit"><i class="fa fa-pencil"></i>Expenses</button></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>

@endsection