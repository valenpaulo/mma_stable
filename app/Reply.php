<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
       protected $table = 'replies';
	protected $fillable = [
        'member_id','post_id','reply_body','remember_token'
    ];

     public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

     public function Post()
    {
        return $this->belongsto(Post::class);
    }
}
