<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Event;

use App\Notifications\SystemBroadcastNotification;

use App\Events\SystemEvent;

use App\Form;
use App\member;
use App\Question;
use App\Option;
use App\Respondent;

class FormController extends Controller
{
    //
    public function store(Request $request) {

        $msg = ['question.*.required' => 'The question field is required.'];

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'question.*' => 'required|max:255',
            'option.*.*.*' => 'required|max:255',
        ], [
             'question.*.required' => 'The question field is required.',
             'title.required' => 'The title field is required.',
             'option.*.*.*' => 'The option field is required.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $check_form = Form::where('title', $request->title)->get();

        if (count($check_form) != 0){
            return redirect()->back()->withErrors('Form title already existed.');
        }
        $form = New Form;
        $form->title = $request->title;
        $form->description = $request->desc;
        $form->member_id = Auth::user()->id;
        $form->save();

        $x = 0;
        foreach ($request->option as $options) {
            $check_question =  Question::where('form_id', $form->id)->where('question', $request->question[$x])->get();
            if(count($check_question) == 0 ) {
            $question = New Question;
            $question->question = $request->question[$x];
            $question->option_format = $request->format[$x];
            $question->form_id = $form->id;
            $question->save();


            foreach ($options as $option) {
                $check_option = Option::where('question_id', $question->id)->where('option', $option[0])->get();
                if(count($check_option) == 0) {

                $opt = New Option;
                $opt->option = $option[0];

                if (isset($option['image'])){
                    $checkFile = getimagesize($option['image'][0]);
                    if ($checkFile){
                        if(move_uploaded_file($option['image'][0], public_path().'/images/form/'.basename($option['image'][0]).$option['image'][0]->getClientOriginalName())){
                            $opt->image = 'form/'.basename($option['image'][0]).$option['image'][0]->getClientOriginalName();
                        }
                    }
                }

                $opt->question_id = $question->id;
                $opt->save();
            }
            }
            $x++;
            }
        }

        $message = 'Poll/Survey: ' . $request->title;

        $members = Member::all();
        \Notification::send($members, new SystemBroadcastNotification($message, 'form'));

        Event::fire(new SystemEvent(auth::id(), 'Added a form "' . $form->title . '".'));

        return redirect('admin/manage-form')->with('success', 'Form Added!');
    }

    public function view() {
        $forms = Form::with('member')->get();
        return view('admin/adminPanel/manage_form_view', compact('forms'));
        }

    public function answer_form_view($id) {
        $form = Form::findOrFail($id);
        $flag = 'answer';
        if (Respondent::where('form_id', $id)->where('member_id', auth::id())->first()) {
            return redirect()->back()->with('warning', 'You already answered this form!');
        }
        return view('admin/adminPanel/answer_form', compact('form' , 'flag'));
    }

    public function form_view($id) {
        $form = Form::findOrFail($id);
        $flag = 'view';
        return view('admin/adminPanel/answer_form', compact('form', 'flag'));
    }

    public function delete($id) {
        $form = Form::findOrFail($id);
        foreach ($form->question as $question) {
            foreach ($question->option as $option) {
                foreach ($option->answer as $answer) {
                    $answer->delete();
                }
                $option->delete();
            }
            $question->delete();
        }
        $form->delete();

        $respondent = Respondent::where('form_id', $id);
        $respondent->delete();

        Event::fire(new SystemEvent(auth::id(), 'Deleted a form"' . $form->title . '".'));

        return redirect()->back()->with('success', 'Form Deleted!');
    }

     public function done($id) {
        $form = Form::findOrFail($id);
        $form->form_status = 'done';
        $form->update();

        Event::fire(new SystemEvent(auth::id(), 'form"' . $form->title . '" is done.'));

        return redirect()->back()->with('success', 'Form is Done!');
    }



    public function edit_view($id) {
        $form = Form::findOrFail($id);
        $counter = array();

        return view('admin/adminPanel/manage_form_edit', compact('form', 'counter'));
    }

    public function edit(Request $request, $id) {

        if(!Respondent::where('form_id',$id)->get()->isEmpty()){
            return redirect()->back()->withErrors('This Form has respondent, make changes are prohibited');
        }
        $msg = ['question.*.required' => 'The question field is required.'];

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'question.*' => 'required|max:255',
            'option.*.*.*' => 'required|max:255',
        ], [
             'question.*.required' => 'The question field is required.',
             'title.required' => 'The title field is required.',
             'option.*.*.*' => 'The option field is required.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $form = Form::findOrFail($id);
        $form->title = $request->title;
        $form->description = $request->desc;
        $form->member_id = Auth::user()->id;
        $form->update();

        $questions = Question::where('form_id', $id)->get();

        foreach ($questions as $question) {
            Option::where('question_id', $question->id)->delete();
            $question->delete();
        }


        $x = 0;
        foreach ($request->option as $options) {
            $check_question =  Question::where('form_id', $form->id)->where('question', $request->question[$x])->get();
            if(count($check_question) == 0 ) {
            $question = New Question;
            $question->question = $request->question[$x];
            $question->option_format = $request->format[$x];
            $question->form_id = $form->id;
            $question->save();


            foreach ($options as $option) {
                $check_option = Option::where('question_id', $question->id)->where('option', $option[0])->get();
                if(count($check_option) == 0) {
                $opt = New Option;
                $opt->option = $option[0];

                if (isset($option['image'])){
                    $checkFile = getimagesize($option['image'][0]);
                    if ($checkFile){
                        if(move_uploaded_file($option['image'][0], public_path().'/images/form/'.basename($option['image'][0]).$option['image'][0]->getClientOriginalName())){
                            $opt->image = 'form/'.basename($option['image'][0]).$option['image'][0]->getClientOriginalName();
                        }
                    }
                }

                $opt->question_id = $question->id;
                $opt->save();
            }
            }
        }
            $x++;
        }

        Event::fire(new SystemEvent(auth::id(), 'Edited form"' . $form->title . '".'));

        return redirect('admin/manage-form')->with('success', 'Form Edited!');
    }
}
