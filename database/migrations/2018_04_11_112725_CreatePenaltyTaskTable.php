<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenaltyTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()
    {
        Schema::create('penalties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id')->nullable();
            $table->integer('member_id');
            $table->integer('validator_id');
            $table->longtext('reason');
            $table->enum('penalty_type',['verbal','written','fee'])->default('verbal');
            $table->enum('penalty_status',['pending','paid','done'])->default('pending');
            $table->integer('fee')->nullable();
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
