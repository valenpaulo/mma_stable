<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ReimburseRequest;
use App\ReimburseConfirm;
use App\Task;
use App\MticsPurchaseEquipList;
use App\MticsPurchaseList;
use App\EventPurchaseEquipList;
use App\InventoryCategory;
use App\MoneyRequest;
use App\MticsfundPayment;
use App\EventPayment;
use App\RoleMember;
use App\Role;
use App\MticsWithdrawal;
use App\MticsBudgetExpense;
use App\Member;
use App\Notifications\SystemNotification;
use Illuminate\Support\Facades\Event;
use App\Events\SystemEvent;



class ReimburseController extends Controller
{
    public function req_reimburse_view()
	  {
	    $reimburses = ReimburseRequest::with('moneyrequest')->whereHas('moneyrequest', function($q) {
            $q->whereHas('task', function($q) {
                $q->where('event_id', null);
            });
        })->get();

	    return view('officers/reimburse', compact('reimburses'));
	  }

    public function validateReimbursement($reimburse_id) {

        $reimburse = ReimburseRequest::findOrFail($reimburse_id);

        $withReceipts = MticsPurchaseList::has('receipt')->where('task_id', $reimburse->moneyrequest->task_id)->get();


       return view('officers/reimburse_view', compact('moneyRequestsTaskDone', 'reimburse', 'withReceipts'));
    }


	public function req_reimburse_accept(Request $request,$id)
	  {
	    $reimburse = ReimburseRequest::find($id);
	    $moneyrequest = MoneyRequest::find($reimburse->money_request_id);
	    $task = Task::find($moneyrequest->task_id);
	    if($reimburse == null){
	      return redirect()->back()->withErrors('Reimbursement id is invalid');
	    }
	    //CHECK THE ADMIN POSITION
	    $pres_id = Role::getId('App\Role','name','president');
	    $finance_id = Role::getId('App\Role','name','finance');
	    $auditor_id = Role::getId('App\Role','name','auditor');
	    if(!RoleMember::Where('role_id',$pres_id)->where('member_id',Auth::id())->get()->isEmpty()){
	    	$position = 'president';
	    }
	    elseif (!RoleMember::Where('role_id',$finance_id)->where('member_id',Auth::id())->get()->isEmpty()) {
	    	$position = 'finance';
	    }
	    elseif (!RoleMember::Where('role_id',$auditor_id)->where('member_id',Auth::id())->get()->isEmpty()) {
	    	$position = 'auditor';
	    }
	    else
	    {
	      return redirect()->back()->withErrors('Invalid Position');
	    }


	    //if position is auditor, needs an finance approval first
	    if($position == 'auditor')
	    {
		    if(ReimburseConfirm::where('reimburse_request_id',$id)->where('position','finance')->get()->isEmpty()){
		      return redirect()->back()->withErrors('Finance needs an approval');
		    }

	    }

	    if($task->event_id == null)
	    {
		    //Check if MTICSFUND, it cannot be negative
		    $budget = new FormulaMoneyController;
		    $mtics_fund = $budget->CheckMticsFundBudget();
		    $total = $mtics_fund - $reimburse->reimburse_amount;
		    if($total < 0){
		      return redirect()->back()->withErrors('Request denied, Mtics Fund will be negative');
		    }
	    }
	    else
	    {
	    	//Check if EVENTFUND, it cannot be negative
		    $event_fund = $this->CheckEventFund($task->event_id);
		    $total = $event_fund - $reimburse->reimburse_amount;
		    if($total < 0){
		      return redirect()->back()->withErrors('Request denied, Event Fund will be negative');
		    }
	    }


	    //Confirming Reimbursement Request
	    if(ReimburseConfirm::where('reimburse_request_id',$id)->where('admin_id',Auth::id())->where('position',$position)->get()->isEmpty()){
		    ReimburseConfirm::create([
	            'reimburse_request_id' => $id,
	            'admin_id' => Auth::id(),
	            'position' => $position,
	        ]);

            // sends a notification

            if ($position == 'finance') {

                $data = array(
                'message' => "There's a reimbursement request and it has been approved by finance. Please check and validate this request.",
                'redirect' => array('role' => 'auditor', 'event_id' => $task->event_id, 'request_id' => $moneyrequest->id),
                'origin' => 'reimbursement',
                );

                $role_id = Role::where('name', 'auditor')->first()->id;
                $member = Member::whereHas('role', function($q) use ($role_id) {
                    $q->where('role_id', $role_id);
                })->first();
                $member->notify(new SystemNotification($data));

            }
	    }
	    else{
	      return redirect()->back()->withErrors('You already confirm this request');
	    }

	    //Check the status of the Reimbursement request
	    $reimburseconfirms = ReimburseConfirm::where('reimburse_request_id',$id)->get();
	    $isConflict = false;
	    $isPressApproved = false;
	    $count = count($reimburseconfirms);
		//IF accepted by President, automatic approved and vice versa
		if(!ReimburseConfirm::where('reimburse_request_id',$id)->where('confirm_status','accepted')->where('position','president')->get()->isEmpty()){
			$isPressApproved = true;
		}
		elseif(!ReimburseConfirm::where('reimburse_request_id',$id)->where('confirm_status','denied')->where('position','president')->get()->isEmpty()){
			ReimburseRequest::where('id', $id)->update(array('reimburse_status' => 'denied'));
		    return redirect()->back()->withErrors('Reimbursement Request has been denied by President');
		}

	    if($count>1){
	    	//Check if there is no Accepted Status on the Request
	    	if(ReimburseConfirm::where('reimburse_request_id',$id)->where('confirm_status','accepted')->get()->isEmpty()){
	    			ReimburseRequest::where('id', $id)->update(array('reimburse_status' => 'denied'));
				    return redirect()->back()->withErrors('Reimbursement Request has been denied by admins');
	    	}

	    	//check if there is conflict with the status
	    	foreach ($reimburseconfirms as $reimburseconfirm) {

	    		//True: if there is conflict, false: if there's none
	    		if($reimburseconfirm->confirm_status == 'accepted'){
	    			$isConflict = false;
	    		}
	    		else{
	    			$isConflict = true;
	    			break;
	    		}
	    	}
	    }

	    //dd($isConflict);

	    //Needs an approval of President if there is conflict
	    if($isConflict == true){
	    	if($isPressApproved == true){
	    		if(ReimburseConfirm::where('reimburse_request_id',$id)->where('position','finance')->get()->isEmpty()){
	    			return redirect()->back()->withErrors('President Approved, Need to Confirm by Finance');
	    		}
	    		else{
	    			 ReimburseRequest::where('id', $id)->update(array('reimburse_status' => 'accepted'));
	    			 MoneyRequest::where('id', $reimburse->money_request_id)->update(array('mon_req_status' => 'for validation'));
	    		}
	    	}
	    	else{
		      return redirect()->back()->withErrors('Conflict between Finance and Auditor, Needs to be Approve by President');
		      //DITO ung Notification ni president
	    	}
	    }
	    elseif($count>1 and $isConflict == false){
		    if(ReimburseConfirm::where('reimburse_request_id',$id)->where('position','finance')->get()->isEmpty()){
	    		return redirect()->back()->withErrors('Waiting to be Confirm by Finance');
	    	}
	    	else{

    		    ReimburseRequest::where('id', $id)->update(array('reimburse_status' => 'accepted'));
    		    MoneyRequest::where('id', $reimburse->money_request_id)->update(array('mon_req_status' => 'for validation'));

                $data = array(
                'message' => "A reimbursement request has been approved by the Executive Officer for Audit.",
                'redirect' => array('role' => 'finance', 'event_id' => $task->event_id, 'request_id' => $moneyrequest->id),
                'origin' => 'reimbursement',
                );

                $role_id = Role::where('name', 'finance')->first()->id;
                $member = Member::whereHas('role', function($q) use ($role_id) {
                    $q->where('role_id', $role_id);
                })->first();
                $member->notify(new SystemNotification($data));
	    	}
	    }

        if ($position == 'finance') {
            Event::fire(new SystemEvent(auth::id(), 'Approved Reimbursement Request.'));

            if ($task->event_id) {

        	    return redirect('admin/finance/manage-event/' . $task->event_id . '/validate/' . $moneyrequest->id . '/step-3/wait-auditor-approval');
            }

            else {

                return redirect('admin/finance/requested-money/validate/' . $moneyrequest->id . '/step-3');
            }
        }
        elseif ($position == 'auditor') {
            Event::fire(new SystemEvent(auth::id(), 'Approved Reimbursement Request.'));

            return redirect()->back()->with('success', 'Request Approved!');
        }
	  }




	public function req_reimburse_deny(Request $request,$id)
	  {

  		$this->validate($request, [
    	'deny_reason' => 'required',
    	]);

	  	$reimburse = ReimburseRequest::find($id);
	  	$moneyrequest = MoneyRequest::find($reimburse->money_request_id);
	    $task = Task::find($moneyrequest->task_id);
	    if($reimburse == null){
	      return redirect()->back()->withErrors('Reimbursement id is invalid');
	    }
	    //CHECK THE ADMIN POSITION
	    $pres_id = Role::getId('App\Role','name','president');
	    $finance_id = Role::getId('App\Role','name','finance');
	    $auditor_id = Role::getId('App\Role','name','auditor');
	    if(!RoleMember::Where('role_id',$pres_id)->where('member_id',Auth::id())->get()->isEmpty()){
	    	$position = 'president';
	    }
	    elseif (!RoleMember::Where('role_id',$finance_id)->where('member_id',Auth::id())->get()->isEmpty()) {
	    	$position = 'finance';
	    }
	    elseif (!RoleMember::Where('role_id',$auditor_id)->where('member_id',Auth::id())->get()->isEmpty()) {
	    	$position = 'auditor';
	    }
	    else
	    {
	      return redirect()->back()->withErrors('Invalid Position');
	    }

	    //if position is finance, needs an auditor's approval first
	    if($position == 'finance')
	    {
		    if(ReimburseConfirm::where('reimburse_request_id',$id)->where('position','auditor')->get()->isEmpty()){
		      return redirect()->back()->withErrors('Auditor needs an approval');
		    }

	    }


  		if($task->event_id == null)
	    {
		    //Check if MTICSFUND, it cannot be negative
		    $budget = new FormulaMoneyController;
		    $mtics_fund = $budget->CheckMticsFundBudget();
		    $total = $mtics_fund - $reimburse->reimburse_amount;
		    if($total < 0){
		      return redirect()->back()->withErrors('Request denied, Mtics Fund will be negative');
		    }
	    }
	    else
	    {
	    	//Check if EVENTFUND, it cannot be negative
		    $event_fund = $this->CheckEventFund($task->event_id);
		    $total = $event_fund - $reimburse->reimburse_amount;
		    if($total < 0){
		      return redirect()->back()->withErrors('Request denied, Event Fund will be negative');
		    }
	    }


	     //Confirming Reimbursement Request
	    if(ReimburseConfirm::where('reimburse_request_id',$id)->where('admin_id',Auth::id())->where('position',$position)->get()->isEmpty()){
	    	if(!ReimburseConfirm::where('reimburse_request_id',$id)->where('confirm_status','accepted')->where('position','president')->get()->isEmpty()){
		      return redirect()->back()->withErrors('President Approved, You can not deny to this request. Speak with your President');
			}
			else
			{
			    ReimburseConfirm::create([
		            'reimburse_request_id' => $id,
		            'admin_id' => Auth::id(),
		            'position' => $position,
		            'confirm_status' => 'denied',
		            'denied_reason' => $request->deny_reason,
		        ]);
			}
	    }
	    else{
	      return redirect()->back()->withErrors('You already confirm this request');
	    }

	  //Check the status of the Reimbursement request
	    $reimburseconfirms = ReimburseConfirm::where('reimburse_request_id',$id)->get();
	    $isConflict = false;
	    $isPressApproved = false;
	    $count = count($reimburseconfirms);
		//IF accepted by President, automatic approved and vice versa
		if(!ReimburseConfirm::where('reimburse_request_id',$id)->where('confirm_status','accepted')->where('position','president')->get()->isEmpty()){
			$isPressApproved = true;
		}
		elseif(!ReimburseConfirm::where('reimburse_request_id',$id)->where('confirm_status','denied')->where('position','president')->get()->isEmpty()){
			ReimburseRequest::where('id', $id)->update(array('reimburse_status' => 'denied'));
		    return redirect()->back()->withErrors('Reimbursement Request has been denied by President');
		}

	    if($count>1){
	    	//Check if there is no Accepted Status on the Request
	    	if(ReimburseConfirm::where('reimburse_request_id',$id)->where('confirm_status','accepted')->get()->isEmpty()){
	    			ReimburseRequest::where('id', $id)->update(array('reimburse_status' => 'denied'));
				    return redirect()->back()->withErrors('Reimbursement Request has been denied by admins');
	    	}

	    	//check if there is conflict with the status
	    	foreach ($reimburseconfirms as $reimburseconfirm) {

	    		//True: if there is conflict, false: if there's none
	    		if($reimburseconfirm->confirm_status == 'accepted'){
	    			$isConflict = false;
	    		}
	    		else{
	    			$isConflict = true;
	    			break;
	    		}
	    	}
	    }

	    //Needs an approval of President if there is conflict
	    if($isConflict == true){
	    	if($isPressApproved == true){
	    		if(ReimburseConfirm::where('reimburse_request_id',$id)->where('position','finance')->get()->isEmpty()){
	    			return redirect()->back()->withErrors('President Approved, Need to Confirm by Finance');
	    		}
	    		else{
	    			 ReimburseRequest::where('id', $id)->update(array('reimburse_status' => 'accepted'));
	    			 MoneyRequest::where('id', $reimburse->money_request_id)->update(array('mon_req_status' => 'for validation'));
	    		}
	    	}
	    	else{
		      return redirect()->back()->withErrors('Conflict between Finance and Auditor, Needs to be Approve by President');
		      //DITO ung Notification ni president
	    	}
	    }
	    elseif($count>1 and $isConflict == false){
	    	if(ReimburseConfirm::where('reimburse_request_id',$id)->where('position','finance')->get()->isEmpty()){
	    		return redirect()->back()->withErrors('Waiting to be Confirm by Finance');
	    	}
	    	else{

		    ReimburseRequest::where('id', $id)->update(array('reimburse_status' => 'accepted'));
	    	MoneyRequest::where('id', $reimburse->money_request_id)->update(array('mon_req_status' => 'for validation'));

	    	}
	    }
	    return redirect('admin/requested-reimbursement');
	  }


	 private function CheckEventFund($event_id)
  	{
	    $moneyrequests = MoneyRequest::with('task')->where('mon_req_status','ongoing')->orwhere('mon_req_status','cleared')->orwhere('mon_req_status','for validation')->orwhere('excess_amount','!=',null)->get();

        //SUBTRACT REQUEST MONEY
        $requestedmoney = 0;
        $excess_money = 0;
        $reimburse_money = 0;
        if(!$moneyrequests->isEmpty()){
          foreach ($moneyrequests as $moneyrequest) {

            if($moneyrequest->task->event_id == $event_id){
            $requestedmoney = $requestedmoney + $moneyrequest->amount;

              $eventfund_money = (EventPayment::where('event_id',$event_id)->sum('payevent_amt')) - $requestedmoney;

              if($moneyrequest->excess_amount > 0){
                $excess_money = $excess_money + $moneyrequest->excess_amount;
              }
              if(!ReimburseRequest::where('money_request_id',$moneyrequest->id)->where('reimburse_status','accepted')->get()->isEmpty()){
                $reimburse = ReimburseRequest::where('money_request_id',$moneyrequest->id)->where('reimburse_status','accepted')->first();
                $reimburse_money = $reimburse_money + $reimburse->reimburse_amount;
              }
            }
            else
            {
              $eventfund_money = (EventPayment::where('event_id',$event_id)->sum('payevent_amt'));
            }
          }
        }

        else{
          $eventfund_money = (EventPayment::where('event_id',$event_id)->sum('payevent_amt'));
        }

        $eventfund_money = $eventfund_money + $excess_money - $reimburse_money;
        //dd($eventfund_money);
        return $eventfund_money;

  }


}
