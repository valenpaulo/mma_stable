<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Form;
use App\Banner;
use App\MticsEvent;
use App\EventPartialPost;


class HomeController extends Controller
{
    public function index(){

        $post = Post::with('member')->where('status','!=', 'archived')->orderBy('id', 'desc')->first();
         $others = array();
        if($post) {

            $others = Post::with('member')->where('status','!=', 'archived')->orderBy('id', 'desc')->where('id', '!=', $post->id)->simplepaginate(3);
        }

        $form = Form::orderBy('id', 'desc')->where('form_status','!=','done')->first();

        if($form) {

            $otherForms = Form::orderBy('id', 'desc')->where('form_status','!=','done')->where('id', '!=', $form->id)->get();
        }


        $banner = Banner::where('status', 'active')->first();
        $fullpost = MticsEvent::where('post_status','posted')->where('status','done')->whereMonth('updated_at', date('m'))->whereYear('updated_at', date('Y'))->orderBy('id', 'desc')->first();

        if($fullpost) {

            $otherFulls = MticsEvent::where('post_status','posted')->where('status','done')->whereMonth('updated_at', date('m'))->whereYear('updated_at', date('Y'))->orderBy('id', 'desc')->get();
        }


        $partial = EventPartialPost::where('status','published')->whereHas('event', function($q) {
            $q->where('status', 'on-going');
        })->orderBy('id', 'desc')->first();

        if($partial) {

            $otherPosts = EventPartialPost::where('status','published')->whereHas('event', function($q) {
                $q->where('status', 'on-going');
            })->where('id', '!=', $partial->id)->orderBy('id', 'desc')->get();
        }

        return view('post', compact('post', 'form', 'otherForms', 'banner','fullpost','partial', 'otherPosts', 'others', 'otherFulls'));
    }
}
