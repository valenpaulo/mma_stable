<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsWithdrawal extends Model
{
    protected $table = 'mtics_withdrawals';
	protected $fillable = [
    'admin_id','faculty_id','withdrawal_reason','withdrawal_desc','withdrawal_amt','denied_reason','withdrawal_status','mtics_budget_id','external_task_id',
    ];

    public function faculty() {
        return $this->belongsTo(Member::class, 'faculty_id');
    }

    public function receipt() {
        return $this->hasOne(MticsWithdrawalReceipt::class);
    }

     public function deny() {
        return $this->hasOne(MticsWithdrawalDeny::class);
    }
     public function report() {
        return $this->hasOne(MticsWithdrawalReport::class);
    }
  

}
