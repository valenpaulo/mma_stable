@extends('layout/control_panel')

@section('title')
Send a Message
@endsection

@section('middle')

<div class="w3-container w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-envelope-o fa-fw w3-xxlarge"></i>
      <strong>Send SMS</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">

    <a href="#add" class="w3-text-gray" data-toggle="modal"><i class="fa fa-paper-plane-o fa-fw"></i> Send Message to All</a>

  </div>

  <br>

  <div class="row w3-margin-top">
    <div class="table-responsive">
      <table class="table table-hover table-bordered" id="informationTable">
      <thead>
        <tr>
         <th class="w3-center">First Name</th>
         <th class="w3-center">Last Name</th>
         <th class="w3-center">Avatar</th>
         <th class="w3-center">Section</th>
         <th class="w3-center">Mobile No</th>
         <th class="w3-center">Action</th>
        </tr>
      </thead>
      <tbody class="w3-text-gray">
        @foreach($members as $member)
        <tr>
          <td class="w3-center">{{$member->first_name}}</td>
          <td class="w3-center">{{$member->last_name}}</td>
          <td class="w3-center">
            <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$member->avatar) : asset('images/'.$member->avatar)}}" style="width: 64px; height: 64px"></td>
          </td>
          <td class="w3-center">{{$member->section->section_code}}</td>
          <td class="w3-center">{{$member->mobile_no}}</td>
          <td class="w3-center">

            <a href="#send-{{$member->id}}" data-toggle="modal" class="w3-large w3-text-green"><i class="fa fa-paper-plane-o fa-fw" data-toggle="tooltip" data-placement="bottom" title="Send a Message"></i></a>

            <!-- ADD MODAL -->
            <div class="modal fade" id="send-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Send Message</h4>
            </div>
            <form action="{{route('admin.info.send')}}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                 <div class="form-group">
                    <label for="mobile">To:</label>
                    <input type="text" name="mobile" value="{{$member->mobile_no}}" class="form-control">
                 </div>

                  <div class="form-group">
                    <label for="mobile">Message:</label>
                    <textarea name="message" class="form-control" placeholder="Write your message" rows="5"></textarea>
                 </div>

                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default w3-green">
                  <i class="fa fa-send fa-fw"></i> Send Message
                </button>
            </div>
            </form>
            </div>
            </div>
            </div>

            <!-- END MODAL -->
          </td>
        </tr>

        @endforeach
      </tbody>
      </table>
    </div>
  </div>

</div>

<!-- GRID OF DOCUMENTS -->

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>

<script type="text/javascript">
$(document).ready( function () {
  var table = $('#informationTable').DataTable();
  $(table.table().container()).removeClass('form-inline');

} );
</script>

@endsection
