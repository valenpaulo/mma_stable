@extends('layout/control_panel')

@section('title')
Manage Inventory
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-shopping-basket fa-fw w3-xxlarge"></i>
      <strong>Manage Inventory</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">

    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#inventory" class="w3-text-black">Inventory</a></li>

      <li><a data-toggle="tab" href="#category123" class="w3-text-black">Category</a></li>
      <li><a data-toggle="tab" href="#eventoffers" class="w3-text-black">Event Offers</a></li>
    </ul>
  </div>

  <div class="tab-content">
    <!-- start of inventory -->
    <div id="inventory" class="tab-pane fade in active">

      <div class="row w3-margin-top">
        <h5>
          <a href="#addInvent" class="w3-text-gray" data-toggle="modal">
            <i class="fa fa-plus-square fa-fw"></i>
            Add Item
          </a>

          <i class="fa fa-ellipsis-v fa-fw"></i>

          <a href="#import" class="w3-text-gray" data-toggle="modal">
            <i class="fa fa-upload fa-fw"></i>
            Import Excel
          </a>

          <i class="fa fa-ellipsis-v fa-fw"></i>

          <span class="w3-dropdown-hover w3-white">
            <a href="javascript:void(0)" class="w3-text-gray"><i class="fa fa-cloud-download fa-fw"></i>Download Excel Template</a>


            <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" align="left" style="left: 0">
              <a href="{{url('admin/logistics/inventory/template-download/normal')}}" class="w3-text-gray w3-hover-teal"><i class="fa fa-cart-arrow-down fa-fw w3-large"></i> Normal</a>

              <a href="{{url('admin/logistics/inventory/template-download/equip')}}" class="w3-text-gray w3-hover-teal" style="outline: 0px"><i class="fa fa-wrench fa-fw w3-large"></i> Equipment</a>
            </div>

          </span>

          <i class="fa fa-ellipsis-v fa-fw"></i>

          <a href="#reports" class="w3-text-gray" data-toggle="modal">
            <i class="fa fa-print fa-fw"></i>
            Monthly Report for Inventory
          </a>

        </h5>
      </div>

      <!-- Montly report MODAL -->
      <div class="modal fade" id="reports" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">Inventory Monthly Report</h4>
      </div>
      <form action="{{url('admin/logistics/inventory/view-reports')}}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
      <div class="w3-container">
        <div class="form-group">
          <label>select month</label>
          <input id="month" name='month' type="month" class="form-control">
        </div>
      </div>
      </div>
      <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
      </div>
      </form>
      </div>
      </div>
      </div>
      <!-- END MODAL -->

      <!-- ADD MODAL -->
      <div class="modal fade" id="import" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">Import Excel</h4>
      </div>
      <form action="{{url('admin/logistics/inventory/import-excel')}}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
      <div class="w3-container">
        <div class="row">
        <div class="form-group">
          <p>Choose an excel file for bulk uploading of items.</p>
        </div>
        <div class="form-group">
          <label id="import-file-name" for="import_file" style="cursor: pointer;"><i class="fa fa-file-excel-o fa-fw w3-text-green w3-large" data-toggle="tooltip" title="Bulk Add"></i></label>
          <input type="file" name="import_file" id="import_file" style="opacity: 0; position: absolute; z-index: -1;" />
        </div>
        </div>
      </div>
      </div>
      <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
      </div>
      </form>
      </div>
      </div>
      </div>
      <!-- END MODAL -->

      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="inventoryTable">
            <thead>
              <tr>
               <th class="w3-center">Item/s Name</th>
               <th class="w3-center">Quantity</th>
               <th class="w3-center">Category</th>
               <th class="w3-center">Added_at</th>
               <th class="w3-center">Action</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray" id="itemBody">

              @foreach($inventories as $inventory)
              <tr>
                <td class="w3-center">
                @if($inventory->inventory_category_id == $cat_equip_id)
                  <a href="" class="w3-text-gray" data-toggle="modal" data-target="#info-{{$inventory->id}}"><i class="fa fa-info-circle"></i> {{$inventory->inv_name}}</a>
                @else
                  {{$inventory->inv_name}}
                @endif
                </td>
                <td class="w3-center">
                {{$inventory->inv_quantity}}
                </td>
                <td class="w3-center">{{$inventory->Inventory_Category->inv_cat_displayname}}</td>
                <td class="w3-center">{{$inventory->created_at}}</td>

                <td class="w3-center">
                  <a href="#edit-{{$inventory->id}}" data-toggle="modal" class="w3-text-orange w3-large"><i class="fa fa-pencil-square-o fa-fw" data-toggle="tooltip" data-placement="bottom" title="Edit"></i></a>

                  <a href="#delete-{{$inventory->id}}" data-toggle="modal" class="w3-text-red w3-large"><i class="fa fa-trash-o fa-fw" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a>

                  <!-- EDIT MODAL -->
                  <div class="modal fade" id="edit-{{$inventory->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit {{$inventory->inv_name}}</h4>
                  </div>
                  <form action="{{ url('admin/logistics/inventory/'.$inventory->id.'/edit/') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                     <div class="form-group">
                         <p><b>Item name: </b></p>
                          <input type="text" name="inv_name" id="inv_name" value="{{$inventory->inv_name}}" tabindex="1" class="form-control" placeholder="Item name" >
                        </div>

                        <div class="form-group">
                         <p><b>Quantity: </b></p>
                          <input type="number" min="0" disabled name="inv_quantity" value="{{$inventory->inv_quantity}}" id="inv_quantity" tabindex="1" class="form-control" placeholder="Item Quantity" >
                        </div>

                        <div class="form-group">
                         <label>Category:</label>
                          <select class="form-control" id="category" name="category" required placeholder="Select Category">
                           @foreach($inv_categories as $inv_category)
                           @if($inv_category->inv_cat_displayname==$inventory->Inventory_Category->inv_cat_displayname)
                           {<option selected>{{$inv_category->inv_cat_displayname}}</option>}
                           @else
                           {<option>{{$inv_category->inv_cat_displayname}}</option>}
                           @endif
                           @endforeach

                          </select>
                        </div>

                      </div>
                  </div>
                  </div>
                  <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
                  </div>
                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END EDIT MODAL -->

                  <!-- //DELETE -->
                  <div class="modal fade" id="delete-{{$inventory->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                    <div class="modal-dialog modal-md" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                          <h4 class="modal-title w3-text-gray" id="myModalLabel">Delete {{$inventory->inv_name}}</h4>
                        </div>
                      <form action="{{ url('admin/logistics/inventory/'.$inventory->id.'/delete/') }}" method="POST">
                        <div class="modal-body w3-text-black">
                      <div class="w3-container">
                        <div class="row">
                          @if($inventory->inventory_category_id != $cat_equip_id)

                          <div class="form-group">
                              <div class="row">
                                <div class="col-sm-2 w3-center w3-padding">
                                 <p><b>Quantity: </b></p>
                                </div>
                                <div class="col-sm-3">
                                  <select name="del_quan" id="del_quan" class="form-control">
                                  @for ($i = $inventory->inv_quantity; $i > 0; $i--)
                                     <option value="{{$i}}">{{$i}}</option>
                                  @endfor
                                  </select>
                                </div>
                                <div class="col-sm-7"></div>
                              </div>
                            </div>

                          @endif
                          <div class="form-group">
                          @if($inventory->inventory_category_id == $cat_equip_id)
                          <p><b>Choose item you want to remove:</b></p>
                          <div class="checkbox">
                            <div class="row">
                            @foreach($inv_equips as $inv_equip)
                            @if ($inv_equip->inventory_id == $inventory->id)
                              <div class="col-sm-4">
                                <label><input type="checkbox" name="equip_id[]" value="{{$inv_equip->id}}">{{$inv_equip->brand_name}} {{$inv_equip->serial_num}}</label>
                              </div>
                            @endif
                            @endforeach
                          </div>
                          @endif
                          </div>
                        </div>

                          <div class="form-group">
                            <p><b>Reason: </b></p>
                            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" placeholder="Reason" rows="3"></textarea>
                          </div>
                        </div>
                      </div>



                          <div class="modal-footer">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-default w3-red"><i class="fa fa-trash fa-fw"></i> Delete</button>
                          </div>

                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- INFO -->
                  <div class="modal fade" id="info-{{$inventory->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                  <div class="modal-dialog modal-dialog-info modal-m" role="document">
                  <div class="modal-content modal-content-info">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">{{$inventory->inv_name}} Information</h4>
                  </div>
                  <form action="{{ url('admin/logistics/inventory/store') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body modal-body-info">
                  <div class="w3-container">
                    <div class="row">
                       @foreach($inv_equips as $inv_equip)
                        @if ($inv_equip->inventory_id == $inventory->id)
                          <div class="row">
                          <div class="col-sm-6" align="center">
                            <h4><strong>{{$inv_equip->brand_name}}</strong></h4>
                          </div>
                          <div class="col-sm-6">
                            <p>Serial No: {{$inv_equip->serial_num}}</p>
                            <p>Specification: {{$inv_equip->specs}}</p>
                            <p>Status: {{$inv_equip->status}}</p>
                            <div class="col-sm-3 w3-margin-top">
                              <a href="{{url('admin/logistics/damage-report/'.$inv_equip->id.'/'.$inv_equip->serial_num)}}" class="btn btn-default w3-red" title="Borrow"><i class="fa fa-wrench fa-fw"></i>Damage Report</a>
                            </div>
                          </div>
                          </div>
                          <hr>
                          <br>
                          @endif
                      @endforeach
                    </div>
                  </div>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END INFO -->

                </td>
              </tr>


              @endforeach

            </tbody>
          </table>
        </div>
      </div>
    <!-- end of inventory tab -->
    </div>

    <!-- start category -->
    <div id="category123" class="tab-pane fade in">

      <div class="row w3-margin-top">
        <h5>
          <a href="#addCategory" class="w3-text-gray" data-toggle="modal">
            <i class="fa fa-plus-square fa-fw"></i>
            Add Category
          </a>
        </h5>
      </div>

      <!-- ADD CATEGORY-->
      <div class="modal fade" id="addCategory" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
      <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Category </h4>
      </div>
      <form action="{{ url('admin/logistics/inventory/category-name-store') }}" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
      <div class="w3-container">
        <div class="row">

          <div class="form-group">
            <label>Category Name: </label>
            <input type="text" name="category_name" id="category_name" tabindex="1" class="form-control" placeholder="Category Name" >
          </div>

        </div>
      </div>
      </div>
      <div class="modal-footer">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
      </div>
      </form>
      </div>
      </div>
      </div>
      <!-- END ADD -->

      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="categoryTable">
            <thead>
              <tr>
               <th class="w3-center">Category</th>
               <th class="w3-center">No. of Items Under this Category</th>
               <th class="w3-center">Action</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">

              @foreach($inv_categories as $category)
              <tr>
                <td class="w3-center">{{$category->inv_cat_displayname}}</td>
                <td class="w3-center">{{count($category->Inventory)}}</td>
                <td class="w3-center">
                  <a class="w3-text-orange w3-large" href="#editCategory-{{$category->id}}" data-toggle="modal"><i class="fa fa-pencil-square-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Edit Category"></i></a>

                  <a class="w3-text-red w3-large" href="#deleteCategory-{{$category->id}}" data-toggle="modal"><i class="fa fa-trash-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Delete Category"></i></a>

                  <!-- EDIT CATEGORY-->
                  <div class="modal fade" id="editCategory-{{$category->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Category</h4>
                  </div>
                  <form action="{{ url('admin/logistics/inventory/category-name-edit/'.$category->id) }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                         <p><b>Category Name: </b></p>
                          <input type="text" name="category_name" id="category_name" tabindex="1" class="form-control" placeholder="Category Name" value="{{$category->inv_cat_displayname}}">
                        </div>
                    </div>
                  </div>
                  </div>
                  <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
                  </div>
                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END EDIT -->

                  <!-- DELETE CATEGORY-->
                  <div class="modal fade" id="deleteCategory-{{$category->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Delete Category</h4>
                  </div>
                  <form action="{{ url('admin/logistics/inventory/category-name-delete/'.$category->id) }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <p>Do you want to proceed?</p>
                      </div>

                    </div>
                  </div>
                  </div>
                  <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green w3-text-white"> Yes</button>
                  <button type="button" data-dismiss="modal" class="btn btn-default w3-red w3-text-white"> No</button>
                  </div>
                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END DELETE -->
                </td>
              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      </div>
    <!-- end of category tab -->




    </div>


    <!-- EVENT OFFERS TAB -->
    <div id="eventoffers" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="eventofferTable">
            <thead>
              <tr>
               <th class="w3-center">Eventoffers name</th>
               <th class="w3-center">Member Name</th>
               <th class="w3-center">Quantity</th>
               <th class="w3-center">desc</th>
               <th class="w3-center">Item status</th>
               <th class="w3-center">action</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">
                @foreach($eventoffers as $eventoffer)
                <tr>
                  <td class="w3-center">{{$eventoffer->item->offer_name}}</td>
                  <td class="w3-center">{{$eventoffer->member->first_name}} {{$eventoffer->member->last_name}}</td>
                  <td class="w3-center">{{$eventoffer->quantity}}</td>
                  <td class="w3-center">{{$eventoffer->desc}}</td>
                  <td class="w3-center">{{$eventoffer->item_status}}</td>

                  <td class="w3-center">
                     <a class="w3-text-green w3-text-gray" href="#claimed-{{$eventoffer->id}}" data-toggle="modal"><i class="fa fa-thumbs-up-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title=Claimed></i></a>
                  </td>

                  <!-- CLAIMED CATEGORY-->
                  <div class="modal fade" id="claimed-{{$eventoffer->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Claimed Item</h4>
                  </div>
                  <form action="{{ url('admin/logistics/inventory/event-offers-claimed/'.$eventoffer->id) }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <p>Do you want to proceed?</p>
                      </div>

                    </div>
                  </div>
                  </div>
                  <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green w3-text-white"> Yes</button>
                  <button type="button" data-dismiss="modal" class="btn btn-default w3-red w3-text-white"> No</button>
                  </div>
                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END CLAIMED -->
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>

    </div>
    <!-- END EVENT OFFERS TAB -->



  </div>




</div>


<!-- ADD ITEM MODAL -->
<div class="modal fade" id="addInvent" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-md" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
<h4 class="modal-title w3-text-gray" id="myModalLabel">Add Item</h4>
</div>
<form action="{{ url('admin/logistics/inventory/store') }}" method="POST" enctype="multipart/form-data">
<div class="modal-body">
<div class="w3-container">
  <div class="row">

      <div class="form-group">
       <label>Item name: </label>
        <input type="text" name="inv_name" id="inv_name" tabindex="1" class="form-control" placeholder="Item name" >
      </div>

      <div class="form-group here" id="quantity">
       <label>Quantity: </label>
        <input type="number" min="0" name="inv_quantity" id="inv_quantity" tabindex="1" class="form-control" placeholder="Item Quantity" >
      </div>

      <div class="form-group here" id="brand_name" style="display: none">
       <label>Brand Name: </label>
        <input type="text" min="0" name="brand_name" id="brand_name" tabindex="1" class="form-control" placeholder="Item Quantity" >
      </div>


      <div class="form-group here" id="serial" style="display: none">
       <label>Serial No: </label>
        <input type="text" min="0" name="serial" id="serial" tabindex="1" class="form-control" placeholder="Item Quantity" >
      </div>

      <div class="form-group here" id="specs" style="display: none">
       <label>Specification: </label>
        <input type="text" min="0" name="specs" id="specs" tabindex="1" class="form-control" placeholder="Item Quantity" >
      </div>

      <div class="form-group">
       <label>Category:</label>
        <select class="form-control add-category" id="category" name="category" required placeholder="Select Category">
         @foreach($inv_categories as $inv_category)
          <option value="{{$inv_category->inv_cat_displayname}}">{{$inv_category->inv_cat_displayname}}</option>
         @endforeach

        </select>
      </div>

    </div>
</div>
</div>
<div class="modal-footer">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
</div>
</form>
</div>
</div>
</div>
<!-- END MODAL -->

<script>
  var input = document.getElementById( 'import_file' );
  var infoArea = document.getElementById( 'import-file-name' );

  input.addEventListener('change', showFileName);

  function showFileName( event ) {

    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  }
</script>

<script type="text/javascript">
  $(function(){
    $('.add-category').change(function(){
        if ($(".add-category").val() == 'Equipment'){
          $("#quantity").hide();
          $("#brand_name").show();
          $("#serial").show();
          $("#specs").show();
        } else {
          $("#quantity").show();
          $("#brand_name").hide();
          $("#serial").hide();
          $("#specs").hide();
        }
    });
});
</script>

<script>
$(document).ready(function(){
  var inventoryTable = $('#inventoryTable').DataTable();
  var categoryTable = $('#categoryTable').DataTable();
  var eventofferTable = $('#eventofferTable').DataTable();

  $(inventoryTable.table().container()).removeClass('form-inline');
  $(categoryTable.table().container()).removeClass('form-inline');
  $(eventofferTable.table().container()).removeClass('form-inline');
});
</script>

@endsection
