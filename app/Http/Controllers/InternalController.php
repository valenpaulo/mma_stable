<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\Notifications\SystemNotification;
use Illuminate\Support\Facades\Event;
use App\Events\SystemEvent;
use App\FileCategory;
use App\Document;
use App\Task;
use App\Role;
use App\MticsEvent;
use App\Member;
use App\Officerterm;

class InternalController extends Controller
{
    public function view() {

        if(!Gate::allows('internal-only'))
        {
        return redirect('/');
        }

        $file_categories = FileCategory::where('category_name', 'letter')->first();

        // normal view
        if($file_categories !== null)
        {
        $term = Officerterm::where('status','on-going')->first();
        $documents = Document::where('category_id', $file_categories->id)->where('updated_at','>=',date($term->created_at))->orderBy('id', 'desc')->get();
        }



        return view('officers/internal', compact('documents'));

    }

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'import_file' => 'required|mimes:docx,doc,pdf',
        ], [
             'import_file.required' => 'Please make sure to upload a file.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $file = $request->file('import_file');
        if($request->hasFile('import_file'))
        {
            if ($request->task_id) {
                $event_name = Task::findOrFail($request->task_id)->event->event_title;


                  if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                        $file->move('public/documents/Letter/'.date("Y").'/'.$event_name.'/', date("Y").'-'.$file->getClientOriginalName());
                  }
                  else{
                        $file->move('documents/Letter/'.date("Y").'/'.$event_name.'/', date("Y").'-'.$file->getClientOriginalName());
                  }

                $filename =  'Letter/'.date("Y").'/'.$event_name.'/'.date("Y").'-'.$file->getClientOriginalName();

                $category = FileCategory::where('category_name', 'Letter')->first();

            } else {
                if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                    $file->move('public/documents/Letter/'.date("Y").'/', date("Y").'-'.$file->getClientOriginalName());
                }
                else{
                    $file->move('documents/Letter/'.date("Y").'/', date("Y").'-'.$file->getClientOriginalName());
                }

                $filename =  'Letter/'.date("Y").'/'.date("Y").'-'.$file->getClientOriginalName();

                $category = FileCategory::where('category_name', 'Letter')->first();
            }

            $doc = new Document();
            $doc->filename = $filename;
            $doc->category_id = $category->id;

            if ($request->task_id) {
                $doc->task_id = $request->task_id;
            }
            $doc->save();
        }

        Event::fire(new SystemEvent(auth::id(), 'Added a New Document.'));

        return redirect()->back()->with('success', 'Document Added!');

    }

    public function delete($id) {
        $doc = Document::where('id', $id)->first();
        if(url('/') == 'http://mtics-ma.tuptaguig.com'){
            File::delete('public/documents/' . $doc->filename);
          }
          else{
            File::delete('documents/' . $doc->filename);
          }
        $doc->delete();

        Event::fire(new SystemEvent(auth::id(), 'Document Deleted.'));

        return redirect()->back()->with('success', 'Document Deleted!');
    }

    public function download($id) {
        $doc = Document::where('id', $id)->first();
        $file= public_path(). "/documents/". $doc->filename;
        $filename = explode("2018-", $doc->filename)[1];

        return response()->download($file, $filename);
    }


    public function eventView($id) {

        if(!Gate::allows('internal-only'))
        {
        return redirect('/');
        }

        $event = MticsEvent::findOrFail($id);

        $file_categories = FileCategory::where('category_name', 'letter')->first();
        $documents = Document::where('category_id', $file_categories->id)->whereHas('task', function($q) use ($id) {
            $q->where('event_id', $id)->where('task_status', 'done')->where('member_id', Auth::user()->id);
        })->get();

        $tobe_validated_documents = Document::where('category_id', $file_categories->id)->whereHas('task', function($q) use ($id) {
            $q->where('event_id', $id)->where('task_status', 'for validation')->where('member_id', Auth::user()->id);
        })->get();

        $ready_for_validation_documents = Document::where('category_id', $file_categories->id)->whereHas('task', function($q) use ($id) {
            $q->where('event_id', $id)->where('task_status', 'on-going')->where('member_id', Auth::user()->id);
        })->get();

        $disapproved_documents = Document::where('category_id', $file_categories->id)->whereHas('task', function($q) use ($id) {
            $q->where('event_id', $id)->where('task_status', 'disapproved')->where('member_id', Auth::user()->id);
        })->get();

        $tasks = Task::where(function ($q) {
            $q->where('task_status', 'pending')->orwhere('task_status', 'on-going')->orwhere('task_status', 'for validation')->orwhere('task_status', 'disapproved')->orwhere('task_status', 'done');
        })->where('member_id', Auth::user()->id)->where('event_id', $id)->get();

        return view('officers/event/internal', compact('tasks', 'event', 'documents', 'tobe_validated_documents', 'ready_for_validation_documents', 'disapproved_documents','file_categories'));
    }

    public function validateLetter($id) {
        $task = Task::findOrFail($id);
        $task->task_status = 'for validation';
        $task->save();

        $filename = Document::where('task_id', $task->id)->first()->filename;

        $data = array(
            'message' => 'Validate Document: '. explode("/", $filename)[3],
            'redirect' => array('role' => 'docu', 'event_id' => $task->event_id),
            'origin' => 'internal',
            );

        $role_id = Role::where('name', 'docu')->first()->id;
        $member = Member::whereHas('role', function($q) use ($role_id) {
            $q->where('role_id', $role_id);
        })->first();
        $member->notify(new SystemNotification($data));

        Event::fire(new SystemEvent(auth::id(), 'Sent a Request for Letter Validation.'));

        return redirect()->back()->with('success', 'Request Sent!');
    }

}
