<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptEventitem extends Model
{
 	protected $table = 'receipt_eventitems';
	protected $fillable = [
        'receipt_id','event_purchase_list_id','created_at','updated_at'
    ];



}
