<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisapprovedLetter extends Model
{
    protected $fillable = [
        'task_id', 'validator_id', 'reason',
    ];
}
