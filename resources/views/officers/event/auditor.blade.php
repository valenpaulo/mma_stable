@extends('layout/control_panel')

@section('title')
{{$event->event_title}}
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <a href="{{url('admin/external/manage-event/'.$event->id)}}" class="w3-text-black">
        <i class="fa fa-calendar fa-fw w3-xxlarge"></i>
        <strong>{{$event->event_title}}</strong>
      </h3>
    </a>
  </div>

  <hr>

  <div class="row w3-margin-top">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#taskTab" class="w3-text-black">Task</a></li>

      <li><a data-toggle="tab" href="#requestTab" class="w3-text-black">Reimbursement Request @if($reimburseCount > 0)<span class="w3-badge w3-red">{{$reimburseCount}}</span>@endif</a></li>
    </ul>
  </div>

  <div class="tab-content">
    <div id="taskTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th class="w3-center">Task Name</th>
                 <th class="w3-center">Description</th>
                 <th class="w3-center">Due Date</th>
                 <th class="w3-center">Status</th>
                 <th class="w3-center">Action</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($auditor_tasks as $task)
                <tr>
                  <td class="w3-center">{{$task->task_name}}</td>
                  <td class="w3-center">{{$task->task_desc}}</td>
                  <td class="w3-center">{{$task->due_date}}</td>
                  <td class="w3-center">{{$task->task_status}}</td>
                  <td class="w3-center">
                    @if ($task->task_status == 'pending')
                    <a class="w3-text-green" href="{{url('admin/finance/manage-event/start-task/' . $task->id)}}"><i class="fa fa-play fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Start Task"></i></a>
                    @elseif ($task->task_status == 'on-going' || $task->task_status == 'disapproved')

                    <a class="w3-text-green" href="{{url('admin/finance/manage-event/validate/'.$task->id)}}"><i class="fa fa-check-square fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Validate"></i></a>

                    @elseif ($task->task_status == 'for validation')
                    <i class="fa fa-check-square fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Validate"></i>
                    @endif
                  </td>

                </tr>


              @endforeach
             </tbody>
            </table>
        </div>
      </div>
    </div>

    <div id="requestTab" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="reimburseTable">
              <thead>
                <tr>
                 <th class="w3-center">Task</th>
                 <th class="w3-center">Requested Money</th>
                 <th class="w3-center">Reimbursement</th>
                 <th class="w3-center">Status</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($reimburses as $reimburse)
              <tr>
                <td class="w3-center">
                @if (count($reimburse->confirm) >= 1)
                <a href="{{url('admin/auditor/manage-event/' . $event->id . '/validate/' . $reimburse->moneyrequest->id)}}" class="w3-text-gray">
                <i class="fa fa-exclamation-circle fa-fw"></i>
                {{$reimburse->moneyrequest->task->task_name}}
                </a>
                @else
                {{$reimburse->moneyrequest->task->task_name}}
                @endif
                </td>
                <td class="w3-center">{{$reimburse->moneyrequest->amount}}</td>
                <td class="w3-center">{{$reimburse->reimburse_amount}}</td>
                <td class="w3-center">{{$reimburse->reimburse_status}}</td>
              </tr>
              @endforeach
             </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>

</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#reimburseTable').DataTable();
});
</script>
@endsection
