<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
     protected $fillable = [
       'section_code','course_id'
    ];


 public static function getId($model, $table, $value)
   {
    return $model::where($table, $value)->first()->id;
    }

    public function course() {
        return $this->belongsTo('App\Course', 'course_id');

}
}
