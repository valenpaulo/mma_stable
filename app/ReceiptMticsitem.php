<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptMticsitem extends Model
{
 	protected $table = 'receipt_mticsitems';
	protected $fillable = [
        'receipt_id','mtics_purchase_list_id','created_at','updated_at'
    ];
}
