<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Auth;
use App\Action;
use App\Year;
use App\ActionMember;
use App\RoleMember;
use App\Role;
use App\Member;
use App\Section;
use App\ApprovedRequest;
use App\RejectRequest;



class ReqAccntController extends Controller
{

    public function view()
    {

        if(!Gate::allows('accntreq_approval-only'))
        {
        return redirect('/');
        }
        //$members=Member::where('status', 'pending')->get();
        //dd($members);
		 $members = DB::table('members')
            ->join('sections', 'sections.id', '=', 'members.section_id')
            ->join('years', 'years.id', '=', 'members.year_id')
            ->select('members.*','sections.section_code','years.year_desc')
            ->where('status', '=', 'pending')
            ->get();

            //dd($members);
        foreach ($members as $member) {

        $reviews = DB::table('members')
            ->join('sections', 'sections.id', '=', 'members.section_id')
            ->select('members.*','sections.section_code')
            ->where('first_name', 'LIKE', '%'.$member->first_name.'%')
            ->where('last_name', 'LIKE', '%'.$member->last_name.'%')
            ->where('last_name', 'LIKE', '%'.$member->last_name.'%')
            ->get();

        }

        $action_member = ActionMember::where('action_id', 2)->get();
        $confirmations = ApprovedRequest::all();

    	return view('admin/addFeature/request_account', compact('members','reviews', 'action_member', 'confirmations'));
    }


    public function search(Request $request)
    {

        if(!Gate::allows('accntreq_approval-only'))
        {
        return redirect('/');
        }
        //$members=Member::where('status', 'pending')->get();
        //dd($members);
         $members = DB::table('members')
            ->join('sections', 'sections.id', '=', 'members.section_id')
            ->join('years', 'years.id', '=', 'members.year_id')
            ->select('members.*','sections.section_code','years.year_desc')
            ->where('status', '=', 'pending')
            ->where('username', 'LIKE', "%{$request->q}%")
            ->orWhere('first_name', 'LIKE', "%{$request->q}%")
            ->orWhere('last_name', 'LIKE', "%{$request->q}%")
            ->orWhere('address', 'LIKE', "%{$request->q}%")
            ->orWhere('work', 'LIKE', "%{$request->q}%")
            ->orWhere('mobile_no', 'LIKE', "%{$request->q}%")
            ->orWhere('email', 'LIKE', "%{$request->q}%")
            ->orWhere('id_num', 'LIKE', "%{$request->q}%")
            ->orWhere('type', 'LIKE', "%{$request->q}%")
            ->get();

            //dd($members);
        foreach ($members as $member) {

        $reviews = DB::table('members')
            ->join('sections', 'sections.id', '=', 'members.section_id')
            ->select('members.*','sections.section_code')
            ->where('first_name', 'LIKE', '%'.$member->first_name.'%')
            ->where('last_name', 'LIKE', '%'.$member->last_name.'%')
            ->where('last_name', 'LIKE', '%'.$member->last_name.'%')
            ->get();

        }

        $action_member = ActionMember::where('action_id', 2)->get();
        $confirmations = ApprovedRequest::all();

        return view('admin/addFeature/request_account', compact('members','reviews', 'action_member', 'confirmations'));
    }


    public function confirm(Request $request, $id)
	{

		$member = Member::find($id);

		$alumni_id = Year::getId('App\Year','year_code','Alumni');
		$reqfaculty_id = Year::getId('App\Year','year_code','Faculty');
		$accntReqAction_id = Action::getId('App\Action','action_name','acctreq_approval');
		$faculty_id = Role::getId('App\Role','name','faculty');
		$super_admin = Role::getId('App\Role','name','super_admin');
		//dd($member->section_id);



		if($alumni_id == $member->year_id or $reqfaculty_id == $member->year_id )
		{
			if(!RoleMember::where('role_id', $faculty_id)->where('member_id', Auth::id())->get()->isEmpty())
			{
			if(ApprovedRequest::where('req_member_id', $id)->where('admin_member_id', Auth::id())->get()->isEmpty())
				{
					Approved_Request::create([
			            'req_member_id' => $id,
			            'admin_member_id' => Auth::id(),
			        ]);
		        }
			}

			if(count(ApprovedRequest::where('req_member_id', $id)->get())>1)
			{


						if($reqfaculty_id == $member->year_id)
						{
							$acctreq_approval_id = Action::getId('App\Action','action_name','acctreq_approval');
							 ActionMember::create([
                            'action_id' => $acctreq_approval_id,
                            'member_id' => $id,]);
							RoleMember::create([
		                        'role_id' => $faculty_id,
		                        'member_id' => $id,
		                    ]);

                            $status = 'active';
						}
						else
						{
							$status = 'inactive';
						}

						Member::where('id', $id)->update(array('status' => $status));


			}
		}
		else
		{
			if(!RoleMember::where('role_id', $faculty_id)->where('member_id', Auth::id())->get()->isEmpty())
			{
				ApprovedRequest::create([
			            'req_member_id' => $id,
			            'admin_member_id' => Auth::id(),
			        ]);
				Member::where('id', $id)->update(array('status' => 'active'));
			}
			else
			{

				if(count(ApprovedRequest::where('req_member_id', $id)->get())<2)
				{
					if(ApprovedRequest::where('req_member_id', $id)->where('admin_member_id', Auth::id())->get()->isEmpty())
						{
						ApprovedRequest::create([
					            'req_member_id' => $id,
					            'admin_member_id' => Auth::id(),
					        ]);
						}
				}
				else
				{
				Member::where('id', $id)->update(array('status' => 'active'));
				}
			}


		}


        return redirect('admin/request-account');
    }


	public function reject(Request $request, $id)
	{

		if(RejectRequest::where('rej_req_member_id', $id)->where('rej_admin_member_id', Auth::id())->get()->isEmpty())
			{
			RejectRequest::create([
				            'rej_req_member_id' => $id,
				            'rej_admin_member_id' => Auth::id(),
				            'rej_reason' => $request->reason,

				        ]);
			Member::where('id', $id)->update(array('status' => 'deactivated'));

			}

        return redirect('admin/request-account');

	}



}
