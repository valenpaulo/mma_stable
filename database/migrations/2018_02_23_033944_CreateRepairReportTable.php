<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepairReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equip_repair_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('equip_inventory_id');
            $table->integer('admin_id');
            $table->longtext('report_desc');
            $table->enum('status',['damage','working'])->default('damage');
            $table->timestamps();
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
