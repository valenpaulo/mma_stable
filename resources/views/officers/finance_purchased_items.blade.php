@extends('admin/layout/admin_panel')

@section('middle')


@foreach($tasks as $task)
	@if(!$task->moneyrequest_ongoing->isEmpty())
	<b>TASK</b><br>
	ID: {{$task->id}}<br>
	Task Name: {{$task->task_name}}<br>
	Status: {{$task->task_status}}<br><br>
		@foreach($task->moneyrequest_ongoing as $moneyrequest)
		<b>MONEY REQUEST</b><br>
		ID: {{$moneyrequest->id}}<br>
		Amount: {{$moneyrequest->amount}}<br>
		Status: {{$moneyrequest->mon_req_status}}<br><br><br>
		@endforeach

		@if(!$task->mticspurchaselist->isEmpty())
				@foreach($task->mticspurchaselist as $mticspurchaselist)
					item name: {{$mticspurchaselist->mtics_itemname}}<br>
					quatity: {{$mticspurchaselist->mtics_act_quan}}<br>
					price per each: {{$mticspurchaselist->mtics_act_price}}<br>
					total price: {{$mticspurchaselist->mtics_total_price}}<br><br>
					<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#report-{{$mticspurchaselist->id}}" title="Edit"><i class="fa fa-pencil"></i> Report</button><br><br>


					<!-- Report -->
				<div class="modal fade" id="report-{{$mticspurchaselist->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog modal-sm" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
				        <h4 class="modal-title w3-text-gray" id="myModalLabel">Report item</h4>
				      </div>
				  	<form action="{{ url('admin/finance/purchased-items/'.$mticspurchaselist->id.'/report') }}" method="POST" enctype="multipart/form-data">
				      <div class="modal-body">

				      	Financial Matter will set this report as <b>'Critical'</b><br>
				      	<div class="form-group">
					       <p><b>Report Name: </b></p>
					        <input type="text" name="report_name" id="report_name" tabindex="1" class="form-control" placeholder="Report Title" >
					      </div>

					     <div class="form-group">
		                  <p><b>Report Reason: </b></p>
		                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_reason" placeholder="Reason" rows="3"></textarea>
		                </div>

		                <div class="form-group">
		                  <p><b>External Explaination: </b></p>
		                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="external_explain" placeholder="Explaination" rows="3"></textarea>
		                </div>


				        <div class="modal-footer">
				          <input type="hidden" name="_token" value="{{ csrf_token() }}">
				          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
				        </div>

				      </div>
				    </form>
				    </div>
				  </div>
				</div>



				@endforeach
		@endif


		<!-- EVENT DITO -->

		@if(!$task->eventpurchaselist->isEmpty())
				@foreach($task->eventpurchaselist as $eventpurchaselist)
					item name: {{$eventpurchaselist->event_itemname}}<br>
					quatity: {{$eventpurchaselist->event_act_quan}}<br>
					price per each: {{$eventpurchaselist->event_act_price}}<br>
					total price: {{$eventpurchaselist->event_total_price}}<br><br>
					<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#report-{{$eventpurchaselist->id}}" title="Edit"><i class="fa fa-pencil"></i> Report</button><br><br>


					<!-- Report -->
				<div class="modal fade" id="report-{{$eventpurchaselist->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog modal-sm" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
				        <h4 class="modal-title w3-text-gray" id="myModalLabel">Report item</h4>
				      </div>
				  	<form action="{{ url('admin/finance/purchased-items/'.$task->id.'/'.$eventpurchaselist->id.'/report') }}" method="POST" enctype="multipart/form-data">
				      <div class="modal-body">

				      	Financial Matter will set this report as <b>'Critical'</b><br>
				      	<div class="form-group">
					       <p><b>Report Name: </b></p>
					        <input type="text" name="report_name" id="report_name" tabindex="1" class="form-control" placeholder="Report Title" >
					      </div>

					     <div class="form-group">
		                  <p><b>Report Reason: </b></p>
		                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_reason" placeholder="Reason" rows="3"></textarea>
		                </div>

		                <div class="form-group">
		                  <p><b>External Explaination: </b></p>
		                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="external_explain" placeholder="Explaination" rows="3"></textarea>
		                </div>


				        <div class="modal-footer">
				          <input type="hidden" name="_token" value="{{ csrf_token() }}">
				          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
				        </div>

				      </div>
				    </form>
				    </div>
				  </div>
				</div>



				@endforeach
		@endif





		<br><button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#receipt-{{$task->id}}" title="Edit"><i class="fa fa-pencil"></i> Upload Receipt</button>
		<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#done-{{$task->id}}" title="Edit"><i class="fa fa-pencil"></i> Done</button>
		<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#view-{{$task->id}}" title="Edit"><i class="fa fa-pencil"></i> View</button>
	@endif

		<!-- ADD Receipt -->
		<div class="modal fade" id="receipt-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
		        <h4 class="modal-title w3-text-gray" id="myModalLabel">Upload Receipt</h4>
		      </div>
		  	<form action="{{ url('admin/finance/purchased-items/'.$task->id.'/upload-receipt') }}" method="POST" enctype="multipart/form-data">
		      <div class="modal-body">
				  <input type="file" name="receipt" id="receipt">
				  @if(!$task->mticspurchaselist->isEmpty())
					@foreach($task->mticspurchaselist as $mticspurchaselist)
						<div class="checkbox">
			              <label><input type="checkbox" name="mticspurchaselist_id[]" value="{{$mticspurchaselist->id}}">{{$mticspurchaselist->mtics_itemname}} {{$mticspurchaselist->mtics_total_price}}</label>
			            </div>
					@endforeach
				  @endif

				  @if(!$task->eventpurchaselist->isEmpty())
					@foreach($task->eventpurchaselist as $eventpurchaselist)
						<div class="checkbox">
			              <label><input type="checkbox" name="eventpurchaselist_id[]" value="{{$eventpurchaselist->id}}">{{$eventpurchaselist->event_itemname}} {{$eventpurchaselist->event_total_price}}</label>
			            </div>
					@endforeach
				  @endif



				  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="remarks" placeholder="remarks" rows="3"></textarea>



		        <div class="modal-footer">
		          <input type="hidden" name="_token" value="{{ csrf_token() }}">
		          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
		        </div>

		      </div>
		    </form>
		    </div>
		  </div>
		</div>


		<!-- DONE -->
		<div class="modal fade" id="done-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
		        <h4 class="modal-title w3-text-gray" id="myModalLabel">Done</h4>
		      </div>
		  	<form action="{{ url('admin/finance/purchased-items/'.$task->id.'/done') }}" method="POST" enctype="multipart/form-data">
		      <div class="modal-body">

				  <b>You are about to clear this request</b><br>

				  	are you sure you want to clear this request?

		        <div class="modal-footer">
		          <input type="hidden" name="_token" value="{{ csrf_token() }}">
		          <button type="submit" class="btn btn-default w3-green">Yes</button>
		          <button type="button" data-dismiss="modal" class="btn btn-default w3-green">No</button>
		        </div>

		      </div>
		    </form>
		    </div>
		  </div>
		</div>


		<!-- View -->
		<div class="modal fade" id="view-{{$task->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
		        <h4 class="modal-title w3-text-gray" id="myModalLabel">Done</h4>
		      </div>
		  	<form action="{{ url('admin/finance/purchased-items/'.$task->id.'/change') }}" method="POST" enctype="multipart/form-data">
		      <div class="modal-body">




				@if(!$task->mticspurchaselist->isEmpty())
				@foreach($task->mticspurchaselist as $mticspurchaselist)
					item name: {{$mticspurchaselist->mtics_itemname}}<br>
					price: {{$mticspurchaselist->mtics_total_price}}<br>
					@foreach($receipts as $receipt)
						@foreach($receipt->mticspurchaselist as $mticspurchasereceipt)
							@if($mticspurchasereceipt->id == $mticspurchaselist->id)
							receipt image: <br><img src="{{asset('public/images/'.$receipt->receipt_image)}}" style="width: 50%"><br>
				  			<input type="hidden" name="receipt[{{$mticspurchaselist->id}}][item_id]" value="{{$mticspurchaselist->id}}"><br>
				  			<input type="hidden" name="receipt[{{$mticspurchaselist->id}}][orig_receipt]" value="{{$receipt->id}}"><br>
				  			<input type="file" name="receipt[{{$mticspurchaselist->id}}][image]"><br><br>

							@endif
						@endforeach
					@endforeach
				@endforeach
				@endif



				@if(!$task->eventpurchaselist->isEmpty())
				@foreach($task->eventpurchaselist as $eventpurchaselist)
					item name: {{$eventpurchaselist->event_itemname}}<br>
					price: {{$eventpurchaselist->event_total_price}}<br>
					@foreach($receipts as $receipt)
						@foreach($receipt->eventpurchaselist as $eventpurchasereceipt)
							@if($eventpurchasereceipt->id == $eventpurchaselist->id)
							receipt image: <br><img src="{{asset('public/images/'.$receipt->receipt_image)}}" style="width: 50%"><br>
				  			<input type="hidden" name="receipt[{{$eventpurchaselist->id}}][item_id]" value="{{$eventpurchaselist->id}}"><br>
				  			<input type="hidden" name="receipt[{{$eventpurchaselist->id}}][orig_receipt]" value="{{$receipt->id}}"><br>
				  			<input type="file" name="receipt[{{$eventpurchaselist->id}}][image]"><br><br>

							@endif
						@endforeach
					@endforeach
				@endforeach
				@endif

		        <div class="modal-footer">
		          <input type="hidden" name="_token" value="{{ csrf_token() }}">
		          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
		        </div>

		      </div>
		    </form>
		    </div>
		  </div>
		</div>




@endforeach

@endsection
