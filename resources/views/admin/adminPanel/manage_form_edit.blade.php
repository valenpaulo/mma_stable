@extends('layout/control_panel')

@section('title')
Manage Form
@endsection

@section('middle')
<style type="text/css">
  .no-border, .no-border:focus {
    border: 0;
    box-shadow: none;
    outline: none; /* You may want to include this as bootstrap applies these styles too */
  }

  .input-lg {
    font-size: 32px
  }
</style>

<script type="text/javascript">
    var last_count = [];
</script>

<div class="w3-container w3-margin">
    <div class="row">
        <h3>
          <i class="fa fa-pie-chart fa-fw w3-xxlarge"></i>
          <strong>Manage Form</strong>
        </h3>
    </div>

    <hr>
    <br>

    <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-8">
        <div class="w3-card-4 w3-sand w3-margin-bottom">
            <div class="w3-container">
                <form action="{{url('admin/manage-form/edit/'.$form->id)}}" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-group w3-margin-top" align="right">
                    <div class="w3-container">
                        <button class="btn btn-success">Submit <i class="fa fa-arrow-circle-o-right fa-fw w3-large"></i></button>
                    </div>
                </div>
                <div class="form-group w3-margin">
                    <strong>
                        <input type="text" name="title" id="title" class="form-control no-border input-lg w3-text-black w3-sand" placeholder="Title" required value="{{$form->title}}">
                    </strong>
                    <input type="text" name="desc" id="desc" class="form-control no-border w3-text-black w3-sand" placeholder="Form Description" value="{{$form->title}}" required>
                </div>

                <div class="form_wrapper">
                @foreach ($form->question as $parent => $question)
                <div class="w3-card-4 w3-white w3-margin parent-{{++$parent}}">
                <div class="w3-container">
                <div class="row w3-margin">
                    <div class="form-group" align="right">
                        <a href="" class="w3-text-gray w3-large add_card" id="{{$parent}}"><i class="fa fa-plus-square-o"></i></a>
                        @if ($parent != 1)
                        <a href="" class="remove_card w3-large" id="{{$parent}}"><i class="fa fa-trash-o w3-text-gray"></i></a>
                        @endif
                    </div>
                    <hr>
                    <div class="form-group w3-container">
                        <div class="row">
                            <div class="col-sm-8">
                                <input type="text" name="question[]" id="question[]" class="form-control no-border w3-text-black w3-large" placeholder="Question" value="{{$question->question}}" required>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control format" id="format[]" name="format[]">
                                    @if ($question->option_format == 'choice')
                                    <option value="choice" selected>Multiple Choice</option>
                                    <!-- <option value="check">Checkboxes</option> -->
                                    @else
                                    <option value="choice">Multiple Choice</option>
                                    <!-- <option value="check" selected>Checkboxes</option> -->
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="option_wrapper">
                        @foreach ($question->option as $child => $option)
                        <div class="form-group {{$parent}}-option-parent-{{++$child}} col-sm-12">

                        <div class="row">
                            <div class="form-group col-sm-1 w3-padding w3-hide-small">
                                <i class="fa fa-circle fa-fw w3-large w3-text-gray"></i>
                            </div>
                            <div class="form-group col-sm-12 col-lg-8 col-md-8">
                                <input type="text" name="option[{{$parent}}][{{$child}}][]" id="option[{{$parent}}][{{$child}}][]" class="form-control no-border w3-text-black" placeholder="Options" value="{{$option->option}}" required>
                            </div>

                            <div class="form-group w3-padding col-sm-3">
                                <label id="{{$parent}}-filename-{{$child}}" for="option[{{$parent}}][{{$child}}][image][]" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-image-o fa-fw w3-text-green w3-large"></i></label>

                                <input type="file" name="option[{{$parent}}][{{$child}}][image][]" onchange="showFileName(this, event, {{$parent}}, {{$child}})" id="option[{{$parent}}][{{$child}}][image][]" style="opacity: 0; position: absolute; z-index: -1;"/>
                            </div>
                        </div>
                        </div>

                        <script type="text/javascript">
                            last_count[{{$parent}}-1] = {{$child}};
                        </script>

                        @endforeach

                        <div class="form-group w3-center">
                            <a href="" class="w3-text-gray add_option" id="{{$parent}}"><i class="fa fa-plus-square-o w3-large w3-text-gray fa-fw"></i>Add Option</a>

                            <a href="" class="w3-text-gray remove_option" id="{{$parent}}"> |<i class="fa fa-close fa-fw w3-large w3-text-gray fa-fw"></i>Delete Option</a>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                @endforeach
                </div>

                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-2"></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
    var parentWrapper         = $(".form_wrapper"); //Form wrapper
    var x = {{$parent}}; //parent counter

    // OPTION
    $(parentWrapper).on("click",".add_option", function(e){ //add option
        e.preventDefault();
        var id = $(this).attr('id');
        var y = last_count[id-1];
        y++;
        $("."+ id +"-option-parent-" + (y-1)).after('\
            <div class="form-group w3-left '+ id +'-option-parent-'+ y +' col-sm-12">\
                <div class="row">\
                    <div class="form-group col-md-1 col-lg-1 w3-padding w3-hide-small">\
                        <i class="fa fa-circle fa-fw w3-large w3-text-gray"></i>\
                    </div>\
                    \
                    <div class="form-group col-sm-12 col-lg-8 col-md-8">\
                        <input type="text" name="option['+id+']['+y+'][]" id="option['+id+']['+y+'][]" required placeholder="Options" class="form-control no-border w3-text-black">\
                    </div>\
                    <div class="form-group w3-padding col-sm-3 col-md-3 col-lg-3">\
                        <label id="'+id+'-filename-'+y+'" for="option['+id+']['+y+'][image][]" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-image-o fa-fw w3-text-green w3-large"></i></label>\
                        <input type="file" name="option['+id+']['+y+'][image][]" id="option['+id+']['+y+'][image][]" style="opacity: 0; position: absolute; z-index: -1;" onchange="showFileName(this, event, '+id+', '+y+')"/> \
                    </div>\
                </div>\
             </div>\
             '); //add option
        last_count[id-1] = y;
        })

    $(parentWrapper).on("click",".remove_option", function(e){ //remove option
        e.preventDefault();
        var id = $(this).attr('id');
        if(last_count[id-1] != 1){
            $("."+id+"-option-parent-"+last_count[id-1]).remove();
            last_count[id-1]--;
        }
    })
    // END OPTION

    // FORM
    $(parentWrapper).on("click",".add_card", function(e){ //add card
        var id = $(this).attr('id');


        last_count[x] = 1 ;
        e.preventDefault(); x++;
        $('.parent-' + id).after('\
             <div class="w3-card-4 w3-white w3-margin parent-'+ x +'" >\
            <div class="w3-container">\
            <div class="row w3-margin">\
                <div class="form-group w3-large w3-text-gray" align="right">\
                    <a href="" class="w3-text-gray add_card" id="'+ x +'"><i class="fa fa-plus-square-o" data-toggle="tooltip" data-placement="bottom" title="Add Question"></i></a> <a href="" class="remove_card" id="'+ x +'"><i class="fa fa-trash-o w3-text-gray" data-toggle="tooltip" data-placement="bottom" title="Remove Question"></i></a>\
\
                </div>\
               <hr>\
                <div class="form-group w3-container">\
                    <div class="row">\
                        <div class="col-sm-8">\
                            <input type="text" name="question[]" id="question[]" class="form-control no-border w3-text-black w3-large" placeholder="Question" required>\
                        </div>\
                        <div class="col-sm-4">\
                            <select class="form-control format" id="format[]" name="format[]">\
                                <option value="choice">Multiple Choice</option>\
                                \
                            </select>\
                        </div>\
                    </div>\
                </div>\
                <br>\
                <div class="option_wrapper">\
                    <div class="form-group w3-left '+ x +'-option-parent-1 col-sm-12">\
                        <div class="row">\
                            <div class="form-group col-sm-1 w3-padding w3-hide-small">\
                                <i class="fa fa-circle fa-fw w3-large w3-text-gray"></i>\
                            </div>\
                            <div class="form-group col-sm-12 col-lg-8 col-md-8">\
                                <input type="text" name="option['+x+'][1][]" id="option['+x+'][1][]" class="form-control no-border w3-text-black" placeholder="Options" required>\
                            </div>\
                            <div class="form-group w3-padding col-sm-3">\
                                <label id="'+x+'-filename-'+1+'" for="option['+x+'][1][image][]" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-image-o fa-fw w3-text-green w3-large"></i></label>\
                                <input type="file" name="option['+x+'][1][image][]" onchange="showFileName(this, event, '+x+', '+1+')" id="option['+x+'][1][image][]" style="opacity: 0; position: absolute; z-index: -1;"/>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="form-group w3-center">\
                        <a href="" class="w3-text-gray add_option" id='+ x +'><i class="fa fa-plus-square-o w3-large fa-fw"></i>Add Option</a>\
                        <a href="" class="w3-text-gray remove_option" id="'+x+'"> |<i class="fa fa-close fa-fw w3-large fa-fw"></i>Delete Option</a>\
                    </div>\
                </div>\
            </div>\
            </div>\
            </div>\
                ');

    })

    $(parentWrapper).on("click",".remove_card", function(e){ //remove card
        var id = $(this).attr('id');

        e.preventDefault(); $(".parent-" + id).remove(); x--;
    })
    // END FORM
});
</script>

<script>
  // var input = document.getElementById( 'option[1][image][]' );

  // input.addEventListener('change', showFileName);

  function showFileName(parent, action, id, ids) {

    // the change event gives us the input it occurred in
    // var infoArea = document.getElementById( id );
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    // infoArea.textContent = 'hello';

     document.getElementById(id + '-filename-' + ids).innerHTML = fileName;
  };
</script>

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>
@endsection
