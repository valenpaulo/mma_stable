<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAmountBreakdownDeny extends Model
{
     protected $table = 'event_amount_breakdown_denies';
	protected $fillable = [
      'event_id','confirm_admin_id','confirm_admin_role_id','denied_reason'
    ];


    public function admin() {
        return $this->belongsTo(Member::class, 'confirm_admin_id');
    }
}

