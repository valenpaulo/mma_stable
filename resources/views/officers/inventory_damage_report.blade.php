@extends('admin/layout/admin_panel')

@section('middle')

<b> Equipment Inventory </b><br>
name: {{$equip_inventory->brand_name}}<br>
serial num: {{$equip_inventory->serial_num}}<br> 
specs: {{$equip_inventory->specs}}<br> 
status: {{$equip_inventory->status}}<br><br><br>
<b> Report History </b><br>
@if($equip_inventory->equip_repair !== null)
	@foreach($equip_inventory->equip_repair as $equip_repair)
		Report desc: {{$equip_repair->report_desc}}<br>
		Report status: {{$equip_repair->status}}<br><br>
	@endforeach
@endif
 <div class="col-sm-1 w3-margin-top">
    <button class="btn btn-default w3-green" data-toggle="modal" data-target="#add" title="Add"><i class="fa fa-plus-square fa-fw" ></i> Add Report</button>
  </div>


  <!-- ADD MODAL -->
<div class="modal fade" id="add" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-sm" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
<h4 class="modal-title w3-text-gray" id="myModalLabel">Add Report</h4>
</div>
<form action="{{url('admin/logistics/damage-report/'.$equip_inventory->id.'/'.$equip_inventory->serial_num.'/add')}}" method="POST" enctype="multipart/form-data">
<div class="modal-body">
<div class="w3-container">
  <div class="row">
  	<select name="repair_status" id="repair_status" class="form-control">
       <option value="damage">Damage</option>
       <option value="working">Working</option>
    </select>

  	<div class="form-group">
	  <p><b>Item Description: </b></p>
	  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="repair_desc" placeholder="Description" rows="10"></textarea>
	</div>

    </div>
</div>
</div>
<div class="modal-footer">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-plus-square"></i> Add</button>
</div>
</form>
</div>
</div>
</div>


@endsection