<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipInventory extends Model
{
      protected $table = 'equip_inventories';
	protected $fillable = [
        'inventory_id','brand_name','serial_num','specs','status'
    ];

    public function equip_repair()
    {
        return $this->hasMany(EquipRepairReport::class,'equip_inventory_id');

    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class,'inventory_id');
    }
}
