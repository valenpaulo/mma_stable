<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\AmountValidation;
use App\FinanceStepValidation;
use App\StepValidation;
use App\MoneyRequest;
use App\MticsPurchaseList;
use App\ReimburseRequest;
use App\EventPurchaseReport;
use App\MticsPurchaseReport;


class PurchasedAmountValidationController extends Controller
{
    public function mticsValidation($id) {
      if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }
      $validation = AmountValidation::where('money_request_id', $id)->where('status', 'ongoing')->first();

      if (is_null($validation)) {
        $validation = AmountValidation::create([
            'money_request_id' => $id,
            'status' => 'ongoing',
          ]);

        $step = FinanceStepValidation::where('step', 1)->first();

        StepValidation::create([
            'validation_id' => $validation->id,
            'step_id' => $step->id,
            'status' => 'current',
          ]);

        return redirect('admin/finance/requested-money/validate/' . $id . '/' . $step->url);

      } else {
        $step = StepValidation::where('validation_id', $validation->id)->where('status', 'current')->first();

        return redirect('admin/finance/requested-money/validate/' . $id . '/' . $step->step->url);
      }
    }

    public function step1($id) {
      if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }
      $requestsValidation = MoneyRequest::findOrFail($id);

      return view('officers/validation/finance_step1', compact('requestsValidation'));
    }

    public function step2($id) {
      if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }
      $requestsValidation = MoneyRequest::findOrFail($id);


      $withReceipts = MticsPurchaseList::has('receipt')->where('task_id', $requestsValidation->task_id)->get();

      return view('officers/validation/finance_step2', compact('requestsValidation', 'withReceipts'));
    }

    public function step3($id) {
      if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }

      $requestsValidation = MoneyRequest::findOrFail($id);

      // check for reimbursement or excess money
      $reimbursement = ReimburseRequest::where('money_request_id', $requestsValidation->id)->first();

      $excess = $requestsValidation->amount - $requestsValidation->task->mticspurchaselist->sum('mtics_total_price');

      $reportcount = MticsPurchaseReport::where('report_category','critical')->where('solution_fr_critical', null)->whereHas('purchaselist', function($q) use($requestsValidation) {
        $q->where('task_id', $requestsValidation->task_id);
      })->count();

      if ($reimbursement or $excess < 0) {

        if ($excess < 0 and is_null($reimbursement)) {
          return view('officers/validation/finance_step3_reimburse', compact('requestsValidation', 'reimbursement', 'reportcount'));
        }
        elseif(count($reimbursement->confirm) > 0) {
          return redirect('admin/finance/requested-money/validate/' . $id . '/step-3/wait-auditor-approval');
        }
        elseif ($reimbursement) {
          return view('officers/validation/finance_step3_reimburse', compact('requestsValidation', 'reimbursement', 'reportcount'));
        }

      }

      elseif ($excess > 0) {
        return view('officers/validation/finance_step3_excess', compact('requestsValidation', 'excess'));
      }

      else {
        return redirect('admin/finance/requested-money/validate/' . $requestsValidation->id . '/next');
      }
    }

    public function step3WaitView($request_id) {
      if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }


      $requestsValidation = MoneyRequest::findOrFail($request_id);
      $reimburserequest = ReimburseRequest::where('money_request_id',$requestsValidation->id)->first();
      if($reimburserequest->reimburse_status == 'accepted')
      {
        return redirect('admin/finance/requested-money/validate/'.$request_id.'/finish');
      }

      return view('officers/validation/finance_step3_wait_auditors_approval', compact('requestsValidation'));
    }

    public function lastStep($id) {
      if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }

      $requestsValidation = MoneyRequest::findOrFail($id);

      $reportcount = MticsPurchaseReport::where('report_category','critical')->where('solution_fr_critical', null)->whereHas('purchaselist', function($q) use($requestsValidation) {
        $q->where('task_id', $requestsValidation->task_id);
      })->count();

      $withReceipts = MticsPurchaseList::has('receipt')->where('task_id', $requestsValidation->task_id)->get();

      return view('officers/validation/finance_lastStep', compact('requestsValidation', 'withReceipts', 'reportcount'));
    }

    public function next($id) {
      if(!Gate::allows('finance-only'))
        {
        return redirect('/');
        }

      $requestsValidation = MoneyRequest::findOrFail($id);

      $validation = AmountValidation::where('money_request_id', $id)->where('status', 'ongoing')->first();

      $step = StepValidation::where('validation_id', $validation->id)->where('status', 'current')->first();

      $nextStep = FinanceStepValidation::where('step', $step->step->step + 1)->first();

      if(is_null($nextStep)) {
        $nextStep = FinanceStepValidation::where('step', 0)->first();
      }

      $newStepValidation = StepValidation::create([
          'validation_id' => $validation->id,
          'step_id' => $nextStep->id,
          'status' => 'current',
        ]);

      $step->status = 'done';
      $step->save();

      return redirect('admin/finance/requested-money/validate/' . $id . '/' . $newStepValidation->step->url);
    }

}
