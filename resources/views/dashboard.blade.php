@extends('layout/layout')
@section('title')
Dashboard
@endsection

@section('content')
{!! Charts::styles() !!}

<div class="w3-container" style="margin-top: 120px;">
    <!-- USER ROW -->
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="w3-card-4 w3-black w3-text-black w3-padding">
                <div class="w3-container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 w3-padding">
                            <div class="w3-card-4 w3-green w3-padding">
                                <div class="w3-container">
                                    <div class="row" align="center">
                                        <i class="fa fa-user w3-xxlarge w3-text-white"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-8 col-lg-8 w3-text-white" align="center">
                            <h4><strong>Active</strong></h4>
                            <h5><strong>{{$active}}</strong></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="w3-card-4 w3-black w3-text-black w3-padding">
                <div class="w3-container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 w3-padding">
                            <div class="w3-card-4 w3-amber w3-padding">
                                <div class="w3-container">
                                    <div class="row" align="center">
                                        <i class="fa fa-user w3-xxlarge w3-text-white"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-8 col-lg-8 w3-text-white" align="center">
                            <h4><strong>Inactive</strong></h4>
                            <h5><strong>{{$inactive}}</strong></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="w3-card-4 w3-black w3-text-black w3-padding">
                <div class="w3-container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 w3-padding">
                            <div class="w3-card-4 w3-red w3-padding">
                                <div class="w3-container">
                                    <div class="row" align="center">
                                        <i class="fa fa-user w3-xxlarge w3-text-white"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-8 col-lg-8 w3-text-white" align="center">
                            <h4><strong>Deactivated</strong></h4>
                            <h5><strong>{{$deactivated}}</strong></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="w3-card-4 w3-black w3-text-black w3-padding">
                <div class="w3-container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 w3-padding">
                            <div class="w3-card-4 w3-blue w3-padding">
                                <div class="w3-container">
                                    <div class="row" align="center">
                                        <i class="fa fa-user w3-xxlarge w3-text-white"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-8 col-lg-8 w3-text-white" align="center">
                            <h4><strong>Online</strong></h4>
                            <h5><strong>0</strong></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END USER ROW -->

    <br>

    <!-- MISC ROW -->
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="w3-card-4 w3-text-black w3-padding">
                <div class="w3-container">

                    <div class="row" align="center">
                        {!! $paidChart->html() !!}
                    </div>

                </div>
            </div>
        </div>

         <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="w3-card-4 w3-white w3-text-black w3-padding">
                <div class="w3-container">
                    <div class="row" align="center">
                        {!! $postChart->html() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MISC ROW -->

    <br>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="w3-card-4 w3-light-gray w3-text-black w3-padding">
                <div class="w3-container">

                    <div class="row">
                        <h4>
                            <i class="fa fa-calendar fa-fw w3-xlarge"></i>
                            <strong>Events</strong>
                        </h4>
                    </div>

                    <div class="row" align="center">

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th>Event</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Proggress</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ($events as $event)
                                  <tr>
                                    <td>{{$event->event_title}}</td>
                                    <td>{{$event->start_date}}</td>
                                    <td>{{$event->end_date}}</td>
                                    <td>
                                        <div class="progress">
                                          <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar"
                                          aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                                            40%
                                          </div>
                                        </div>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="w3-card-4 w3-white">
                <div class="panel panel-danger">
                  <div class="panel-heading">
                    <h4 class="w3-large w3-text-theme"><i class="fa fa-tasks fa-fw"></i> Activity Logs</h4>
                  </div>

                  <div class="panel-body">
                  <div class="w3-container">
                    <div class="row">
                      @foreach($logs as $log)

                        <p>
                          <i class="fa fa-map-pin fa-fw"></i>
                          @if($log->member !== null)
                          {{$log->member->username}} - {{$log->log}}
                          @endif
                          <span class="w3-right">
                          {{$log->created_at}}
                          </span>
                        </p>

                       <!--  @if(!$loop->last)
                        <hr>
                        @endif -->
                      @endforeach
                    </div>
                  </div>
                  </div>
                </div>
            </div>
        </div>

    </div>

    <!-- <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-12"></div>

        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="w3-card-4 w3-purple w3-text-black w3-padding">
                <div class="w3-container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 w3-padding">
                            <div class="w3-card-4 w3-teal w3-padding">
                                <div class="w3-container">
                                    <div class="row" align="center">
                                        <i class="fa fa-money w3-xxlarge w3-text-white"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-8 col-lg-8 w3-text-white" align="center">
                            <h4><strong>Collection</strong></h4>
                            <h5><strong>Php {{$collection}}</strong></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-12"></div>

        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="w3-card-4 w3-amber w3-text-black w3-padding">
                <div class="w3-container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4 w3-padding">
                            <div class="w3-card-4 w3-orange w3-padding">
                                <div class="w3-container">
                                    <div class="row" align="center">
                                        <i class="fa fa-newspaper-o w3-xxlarge w3-text-white"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-8 col-lg-8 w3-text-white" align="center">
                            <h4><strong>No. of Posts</strong></h4>
                            <h5><strong>{{$post_count}}</strong></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> -->

</div>

{!! Charts::scripts() !!}
{!! $paidChart->script() !!}
{!! $postChart->script() !!}
{!! $paidEventChart->script() !!}
@endsection
