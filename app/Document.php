<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'filename','category_id','task_id'
    ];

    public function category() {
        return $this->belongsTo('App\FileCategory', 'category_id');
    }

    public function task() {
        return $this->belongsTo('App\Task', 'task_id');
    }
}
