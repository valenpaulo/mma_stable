<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
	protected $table = 'post_categories';
     protected $fillable = [
        'type'
    ];

     public function post()
    {
        return $this->hasMany(Post::class)->orderBy('id','DESC');
    }
}
