<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditorFinancialApprove extends Model
{
 	protected $table = 'auditor_financial_approves';
	protected $fillable = [
       'month','remarks','admin_id','event_id'
    ];

   
}
