@extends('layout/layout')

@section('title')
Settings
@endsection

@section('content')


<div class="container-fluid">

<div class="w3-container w3-text-black w3-center">
  <a href="{{route('home')}}">
  <img src="{{asset('public/images/logo/mtics.png')}}" class="w3-margin-bottom w3-margin-top" style="height: 100px; width: 100px"></a>
  <h4>Manila Technician Institute Computer Society</h4>

</div>

<div class="container" align="center">
  <a class="w3-text-teal w3-xlarge" href="{{route('admin.banner')}}" title="Manage Banner"><i class="fa fa-flag fa-fw"></i></a>

  <a class=" w3-text-teal w3-xlarge" href="{{route('admin.filter.words')}}" title="Manage Profanity"><i class="fa fa-language fa-fw"></i></a>

</div>

<hr style="border: 0.5px solid gray ">

<div class="container-fluid w3-margin-right">
  <div class="row">
    <div class="col-sm-8"></div>
    <div class="col-sm-4">
    @if($errors->any())
      @foreach ($errors->all() as $error)
      <div class="alert alert-danger alert-dismissable fade in w3-right col-sm-12" id="success-alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>
      {{$error}}
      </strong>
      </div>
      @endforeach
    @endif
    </div>
  </div>
</div>
<br>

@yield('middle')
</div>
@endsection
