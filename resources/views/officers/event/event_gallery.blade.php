@extends('layout/control_panel')

@section('title')
{{$event->event_title}}
@endsection

@section('middle')

<div class="container-fluid w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-image fa-fw w3-xxlarge"></i>
      <strong>Event Gallery</strong>
    </h3>
  </div>

  <hr>

  <div class="row">
    <p>
      <a href="#gallery" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i> Add Photos</a>
    </p>
  </div>

  <!-- add photo modal -->
  <div class="modal fade" id="gallery" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title w3-text-gray" id="myModalLabel">Gallery</h4>
        </div>
      <form action="{{ url('admin/docu/manage-event/'.$event->id.'/upload-photo') }}" method="POST" enctype="multipart/form-data">
        <div class="modal-body">

        <input type="file" name="upload[]" id="upload" multiple>

        </div>

        <div class="modal-footer">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
        </div>
      </form>
      </div>
    </div>
  </div>
  <!-- end add modal -->

  <div class="row w3-margin-top">
    @foreach($eventphotos as $eventphoto)
      <div class="col-sm-3 w3-center">
        <div class="w3-card-4 w3-sand w3-margin">
          <div class="w3-container w3-padding">
          <div class="w3-center">
          <div align="right" class="w3-margin-bottom">
          <p>

            <a href="#archive-{{$eventphoto->id}}" data-toggle="modal"><i class="fa fa-file-archive-o w3-text-gray w3-large fa-fw w3-large w3-hover-opacity" data-toggle="tooltip" data-placement="bottom" title="Archive"></i></a>

            <a href="#delete-{{$eventphoto->id}}" data-toggle="modal"><i class="fa fa-trash-o w3-text-gray w3-large fa-fw w3-large w3-hover-opacity" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a>
          </p>
          </div>

          <hr style="border-color: black">
          </div>


            <div class="w3-center w3-margin-bottom">
             <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$eventphoto->image_name) : asset('images/'.$eventphoto->image_name)}}" style="width: 200px; height: 200px; overflow: hidden;">
            </div>

          </div>
        </div>
      </div>

      <div class="modal fade" id="archive-{{$eventphoto->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Archive</h4>
            </div>
            <form action="{{ url('admin/docu/manage-event/'.$eventphoto->event_id.'/gallery/'.$eventphoto->id.'/archived') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">

              <b>Are you sure you want to archive this image?</b>

              <div class="form-group" align="center">
                <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$eventphoto->image_name) : asset('images/'.$eventphoto->image_name)}}" style="width: 50%">
              </div>


            </div>

            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green">Yes</button>
              <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
            </div>
            </form>
          </div>
        </div>
      </div>

      <div class="modal fade" id="delete-{{$eventphoto->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Delete</h4>
            </div>
            <form action="{{ url('admin/docu/manage-event/'.$eventphoto->event_id.'/gallery/'.$eventphoto->id.'/delete') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">

              <b>Are you sure you want to delete this image?</b>

              <div class="form-group" align="center">
                <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$eventphoto->image_name) : asset('images/'.$eventphoto->image_name)}}" style="width: 50%">
              </div>

            </div>

            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green"> Yes</button>
              <button type="button" class="btn btn-default w3-red" data-dismiss="modal"> No</button>
            </div>
          </form>
          </div>
        </div>
      </div>

    @endforeach
  </div>
</div>
@endsection
