<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventGallery extends Model
{
    protected $table = 'event_galleries';
	protected $fillable = [
    'event_id','member_id','image_name','image_status'
    ];

     public function event()
    {
        return $this->belongsto(MticsEvent::class);
    }

     public function member()
    {
        return $this->belongsto(Member::class);
    }

}
