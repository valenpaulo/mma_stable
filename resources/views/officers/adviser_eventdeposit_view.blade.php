@extends('admin/layout/admin_panel')

@section('middle')
<h1>EVENT TOH</h1>
<b>Deposit Request</b><br>
@foreach($eventdeposits as $eventdeposit)
<br>deposit id: {{$eventdeposit->id}}<br>
admin id: {{$eventdeposit->admin_id}}<br>
event_id: {{$eventdeposit->event_id}}<br>
desc: {{$eventdeposit->deposit_desc}}<br>
amt: {{$eventdeposit->deposit_amt}}<br>
status: {{$eventdeposit->deposit_status}}<br>
<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#eventdeposit_approved-{{$eventdeposit->id}}" title="Edit"><i class="fa fa-pencil"></i> Approved</button>
<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#eventdeposit_denied-{{$eventdeposit->id}}" title="Edit"><i class="fa fa-pencil"></i> Denied</button>
<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#eventdeposit_receipt-{{$eventdeposit->id}}" title="Edit"><i class="fa fa-pencil"></i> upload Receipt</button>
<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#eventdeposit_report-{{$eventdeposit->id}}" title="Edit"><i class="fa fa-pencil"></i> Report</button>



	<!-- DEPOSIT Receipt -->
	<div class="modal fade" id="eventdeposit_receipt-{{$eventdeposit->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	        <h4 class="modal-title w3-text-gray" id="myModalLabel">Upload Receipt</h4>
	      </div>
	  	<form action="{{ url('admin/adviser/event-bank-transaction-request/'.$eventdeposit->id.'/deposit-receipt') }}" method="POST" enctype="multipart/form-data">
	      <div class="modal-body">
			  <input type="file" name="receipt" id="receipt">

	        <div class="modal-footer">
	          <input type="hidden" name="_token" value="{{ csrf_token() }}">
	          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
	        </div>

	      </div>
	    </form>
	    </div>
	  </div>
	</div>



	<!-- DEPOSIT REPORT -->
	<div class="modal fade" id="eventdeposit_report-{{$eventdeposit->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	        <h4 class="modal-title w3-text-gray" id="myModalLabel">Upload Receipt</h4>
	      </div>
	  	<form action="{{ url('admin/adviser/event-bank-transaction-request/'.$eventdeposit->id.'/deposit-report') }}" method="POST" enctype="multipart/form-data">
	      <div class="modal-body">
			  <div class="form-group">
		       <p><b>Report Name: </b></p>
		        <input type="text" name="report_name" id="report_name" tabindex="1" class="form-control" placeholder="Report Title" >
		      </div>

		      <div class="form-group">
	              <p><b>Report Reason: </b></p>
	              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_reason" placeholder="Reason" rows="3"></textarea>
	            </div>

	        <div class="modal-footer">
	          <input type="hidden" name="_token" value="{{ csrf_token() }}">
	          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
	        </div>

	      </div>
	    </form>
	    </div>
	  </div>
	</div>

<!-- DEPOSIT APPROVED -->
		<div class="modal fade" id="eventdeposit_approved-{{$eventdeposit->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
		        <h4 class="modal-title w3-text-gray" id="myModalLabel">Deposit Approved</h4>
		      </div>
		  <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$eventdeposit->id.'/deposit-approved') }}" method="POST">
		      <div class="modal-body">

		  <b>you are about to approved this requestt</b>

		           Are you sure you want to approved this deposit request {{$eventdeposit->id}}, please review the details please?

		        <div class="modal-footer">
		          <input type="hidden" name="_token" value="{{ csrf_token() }}">
		          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
		        </div>

		      </div>
		    </form>
		    </div>
		  </div>
		</div>

<!-- DEPOSIT DISAPPROVED -->
		<div class="modal fade" id="eventdeposit_denied-{{$eventdeposit->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
		        <h4 class="modal-title w3-text-gray" id="myModalLabel">Deposit Denied</h4>
		      </div>
		  <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$eventdeposit->id.'/deposit-denied') }}" method="POST">
		      <div class="modal-body">

		  <b>you are about to denied this requestt</b>

		           Are you sure you want to denied this deposit request {{$eventdeposit->id}}, please review the details please?
		           <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="denied_reason" placeholder="Denied Reason" rows="3"></textarea>

		        <div class="modal-footer">
		          <input type="hidden" name="_token" value="{{ csrf_token() }}">
		          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
		        </div>

		      </div>
		    </form>
		    </div>
		  </div>
		</div>



@endforeach


<br><br><br><b>WITHDRAW</b><br>
@foreach($eventwithdrawals as $eventwithdrawal)
<br>deposit id: {{$eventwithdrawal->id}}<br>
admin id: {{$eventwithdrawal->admin_id}}<br>
desc: {{$eventwithdrawal->withdrawal_desc}}<br>
reason: {{$eventwithdrawal->withdrawal_reason}}<br>
amt: {{$eventwithdrawal->withdrawal_amt}}<br>
status: {{$eventwithdrawal->withdrawal_status}}<br>
<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#eventwithdaw_approved-{{$eventwithdrawal->id}}" title="Edit"><i class="fa fa-pencil"></i> Approved</button>
<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#eventwithdaw_denied-{{$eventwithdrawal->id}}" title="Edit"><i class="fa fa-pencil"></i> Denied</button>
<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#eventwithdaw_receipt-{{$eventwithdrawal->id}}" title="Edit"><i class="fa fa-pencil"></i> upload Receipt</button>
<button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#eventwithdaw_report-{{$eventwithdrawal->id}}" title="Edit"><i class="fa fa-pencil"></i> Report</button>



	<!-- WITHDRAW Receipt -->
	<div class="modal fade" id="eventwithdaw_receipt-{{$eventwithdrawal->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	        <h4 class="modal-title w3-text-gray" id="myModalLabel">Upload Receipt</h4>
	      </div>
	  	<form action="{{ url('admin/adviser/event-bank-transaction-request/'.$eventwithdrawal->id.'/withdraw-receipt') }}" method="POST" enctype="multipart/form-data">
	      <div class="modal-body">
			  <input type="file" name="receipt" id="receipt">

	        <div class="modal-footer">
	          <input type="hidden" name="_token" value="{{ csrf_token() }}">
	          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
	        </div>

	      </div>
	    </form>
	    </div>
	  </div>
	</div>



	<!-- WITHDRAW REPORT -->
	<div class="modal fade" id="eventwithdaw_report-{{$eventwithdrawal->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	        <h4 class="modal-title w3-text-gray" id="myModalLabel">Upload Receipt</h4>
	      </div>
	  	<form action="{{ url('admin/adviser/event-bank-transaction-request/'.$eventwithdrawal->id.'/withdraw-report') }}" method="POST" enctype="multipart/form-data">
	      <div class="modal-body">
			  <div class="form-group">
		       <p><b>Report Name: </b></p>
		        <input type="text" name="report_name" id="report_name" tabindex="1" class="form-control" placeholder="Report Title" >
		      </div>

		      <div class="form-group">
	              <p><b>Report Reason: </b></p>
	              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_reason" placeholder="Reason" rows="3"></textarea>
	            </div>

	        <div class="modal-footer">
	          <input type="hidden" name="_token" value="{{ csrf_token() }}">
	          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
	        </div>

	      </div>
	    </form>
	    </div>
	  </div>
	</div>

<!-- WITHDRAW APPROVED -->
		<div class="modal fade" id="eventwithdaw_approved-{{$eventwithdrawal->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
		        <h4 class="modal-title w3-text-gray" id="myModalLabel">Withdraw Approved</h4>
		      </div>
		  <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$eventwithdrawal->id.'/withdraw-approved') }}" method="POST">
		      <div class="modal-body">

		  <b>you are about to approved this requestt</b>

		           Are you sure you want to approved this deposit request {{$eventwithdrawal->id}}, please review the details please?

		        <div class="modal-footer">
		          <input type="hidden" name="_token" value="{{ csrf_token() }}">
		          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
		        </div>

		      </div>
		    </form>
		    </div>
		  </div>
		</div>

<!-- WITHDRAW DISAPPROVED -->
		<div class="modal fade" id="eventwithdaw_denied-{{$eventwithdrawal->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog modal-sm" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
		        <h4 class="modal-title w3-text-gray" id="myModalLabel">Deposit Denied</h4>
		      </div>
		  <form action="{{ url('admin/adviser/event-bank-transaction-request/'.$eventwithdrawal->id.'/withdraw-denied') }}" method="POST">
		      <div class="modal-body">

		  <b>you are about to denied this requestt</b>

		           Are you sure you want to denied this deposit request {{$eventwithdrawal->id}}, please review the details please?
		           <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="denied_reason" placeholder="Denied Reason" rows="3"></textarea>

		        <div class="modal-footer">
		          <input type="hidden" name="_token" value="{{ csrf_token() }}">
		          <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
		        </div>

		      </div>
		    </form>
		    </div>
		  </div>
		</div>



@endforeach

@endsection
