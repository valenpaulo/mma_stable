@extends('layout/control_panel')

@section('title')
Purchased Amount Validation
@endsection

@section('middle')
<div class="w3-container w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-check-circle-o fa-fw w3-xxlarge"></i>
      <strong>Validation <i class="fa fa-angle-double-right fa-fw"></i> {{$requestsValidation->task->task_name}}</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top w3-padding">

    <h4>
      <strong>Step 2: Upload Receipt</strong>
      @if (count($withReceipts) == count($requestsValidation->task->mticspurchaselist))
      <a class="w3-text-green" href="{{url('admin/finance/requested-money/validate/' . $requestsValidation->id . '/next')}}"><i class="fa fa-arrow-circle-right fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Next Step"></i></a>
      @endif
    </h4>

  </div>


  <div class="row w3-padding">
    <p>Upload a copy of purchased item receipt. Please refer from the purchased item table below.</p>
  </div>

  <div class="row w3-margin-top" align="center">
    <a href="#uploadReceipt" data-toggle="modal" class="w3-text-gray w3-large" style="outline: 0">
      <i class="fa fa-upload fa-fw w3-xxxlarge"></i><br>
      Upload Receipt
    </a>
  </div>

  <!-- UPLOAD RECEIPT MODAL -->
  <div class="modal fade" id="uploadReceipt" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Upload Receipt</h4>
  </div>
  <form action="{{ url('admin/finance/purchased-items/'.$requestsValidation->task_id.'/upload-receipt') }}" method="POST" enctype="multipart/form-data">
  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

      <div class="form-group">
        <label id="receipt-image-file-name" for="receipt" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-text fa-fw w3-text-green w3-large" ></i> Upload Receipt</label>
        <input type="file" name="receipt" id="receipt" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showFileName1(event)" />
      </div>

      <div class="form-group">
        @if(!$requestsValidation->task->mticspurchaselist->isEmpty())
        @foreach($requestsValidation->task->mticspurchaselist as $item)
          <div class="checkbox">
            <label><input type="checkbox" name="mticspurchaselist_id[]" value="{{$item->id}}">{{$item->mtics_itemname}} - {{$item->mtics_total_price}} pesos</label>
          </div>
        @endforeach
        @endif
      </div>

      <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="remarks" placeholder="remarks" rows="3"></textarea>

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
  </div>

  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->

  <div class="row w3-margin-top">
    <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
             <th class="w3-center">Task</th>
             <th class="w3-center">Item Name</th>
             <th class="w3-center">Quantity</th>
             <th class="w3-center">Amount</th>
             <th class="w3-center">Total</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($requestsValidation->task->mticspurchaselist as $item)
            <tr>
              <td class="w3-center">{{$item->task->task_name}}</td>
              <td class="w3-center">{{$item->mtics_itemname}}</td>
              <td class="w3-center">{{$item->mtics_act_quan}}</td>
              <td class="w3-center">{{$item->mtics_act_price}}</td>
              <td class="w3-center">{{$item->mtics_act_price * $item->mtics_act_quan}}</td>
            </tr>

          @endforeach
         </tbody>
        </table>
    </div>
  </div>

  @if(count($withReceipts) > 0)
  <div class="row w3-margin-top">
    <h4>
      <i class="fa fa-file-text fa-fw"></i>
      <strong>Uploaded Receipt</strong>
    </h4>
  </div>

  <div class="row w3-margin-top">
    @foreach($withReceipts as $item)
    <div class="col-sm-1 col-md-4 col-lg-4">
      <label>
        <i class="fa fa-shopping-bag fa-fw"></i>
        {{$item->mtics_itemname}}
      </label>
      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'. $item->receipt[0]->receipt_image) : asset('images/'. $item->receipt[0]->receipt_image)}}" style="width: 100%">
    </div>
    @endforeach
  </div>
  @endif

</div>

<script>
  // var input = document.getElementById( 'receipt' );

  // input.addEventListener('change', showFileName1);

  function showFileName1( event ) {

    // the change event gives us the input it occurred in
    var infoArea = document.getElementById( 'receipt-image-file-name' );
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  };
</script>

@endsection
