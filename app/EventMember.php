<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventMember extends Model
{
    protected $table = 'event_members';
	protected $fillable = [
   	'event_id','year_id','course_id'
    ];

 	public function year()
    {
        return $this->belongsTo(Year::class);
    }

      public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
