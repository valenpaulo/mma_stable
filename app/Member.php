<?php

namespace App;

use App\Notifications\MemberResetPassword;
use App\Notifications\MticsSms;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','section_id','status','age','birthday','address','work','mobile_no','guardian_name','guardian_mobile_no','username', 'email', 'password','year_id','id_num','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MemberResetPassword($token));
    }

    public function sendSmsNotification($token)
    {
        $this->notify(new MticsSms($token));
    }

    public function mticsPayment() {
        return $this->hasMany(MticsfundPayment::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class, 'section_id');
    }

     public function Post()
    {
        return $this->hasMany(Post::class)->orderBy('id','DESC');
    }

    public function Reply()
    {
        return $this->hasMany(Reply::class)->orderBy('id','DESC');
    }

    public static function getId($model, $table, $value)
    {
        return $model::where($table, $value)->first()->id;
    }

    public function role()
    {
        return $this->belongsToMany(Role::class,'role_members');
    }

     public function action()
    {
        return $this->belongsToMany(Action::class,'action_members');
    }

    public function year()
    {
        return $this->belongsTo(Year::class,'year_id');
    }

    public function attendance() {
        return $this->hasOne(EventPresentAttendee::class);
    }

    public static $rules =
   [
    'first_name' => 'required|max:255',
    'last_name' => 'required|max:255',
    'mobile_no' => 'required|max:11',
    'guardian_mobile_no' => 'max:11',
    'email' => 'required|email|max:255|unique:members',
    'password' => 'required|min:6|confirmed',
    'section' => 'required',

   ];

    public function section_active_currentstudent()
    {
        return $this->belongsTo(section::class, 'section_id')->where('section_code','!=','Alumni')->where('section_code','!=','Faculty')->where('section_status','!=','inactive');
    }
}
