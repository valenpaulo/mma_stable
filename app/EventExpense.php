<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventExpense extends Model
{
     protected $table = 'event_expenses';
	protected $fillable = [
       'purchaser_admin_id','purchaser_admin_role_id','confirm_admin_id','confirm_admin_role_id','expense_name','expense_desc','expense_amt','event_amount_breakdown_id','receipt_id','event_id'
    ];

    public function breakdown()
    {
        return $this->belongsto(EventAmountBreakdown::class,'event_amount_breakdown_id');
    }

     public function receiver() {
        return $this->belongsTo(Member::class, 'purchaser_admin_id');
    }

    public function approvedBy() {
        return $this->belongsTo(Member::class, 'confirm_admin_id');
    }

    public function receiverRole() {
        return $this->belongsTo(Role::class, 'purchaser_admin_role_id');
    }

    public function approvedByRole() {
        return $this->belongsTo(Role::class, 'confirm_admin_role');
    }

     public function receipt() {
        return $this->belongsTo(Receipt::class, 'receipt_id');
    }

    public function event() {
        return $this->belongsTo(MticsEvent::class, 'event_id');
    }


}


