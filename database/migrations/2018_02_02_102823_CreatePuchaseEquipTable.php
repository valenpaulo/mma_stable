<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuchaseEquipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('mtics_purchase_equip_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mtics_purchase_list_id');
            $table->string('mtics_brandname');
            $table->string('mtics_serialnum');
            $table->string('mtics_specs');          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
