<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventWithdrawalReport extends Model
{
    protected $table = 'event_withdrawal_reports';
	protected $fillable = [
   		'event_withdrawal_id','faculty_id','withdrawal_title','withdrawal_desc'
    ];

    public function faculty() {
        return $this->belongsTo(Member::class, 'faculty_id');
    }
}
