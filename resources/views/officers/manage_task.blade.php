@extends('admin/layout/admin_panel')

@section('middle')


	<div class="col-md-4">


        <form action="{{ url('admin/president/manage-task/store') }}" method="POST" >

	<div class="form-group">
       <label>Officers:</label>
        <select class="form-control" id="role" name="role_id" placeholder="Select Role">
         @foreach($roles as $role)
          <option value='{{$role->id}}'>{{$role->officer_name}}</option>
         @endforeach
        </select>
      </div>

     <input type="date" class="form-control" id="due_date" name="due_date">
     <div class="form-group">
      <div class="form-group">
        <label>Task Name: </label>
        <input type="text" name="task_name" id="task_name" tabindex="1" class="form-control" placeholder="Task Name">
      </div>
      <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="task_desc" placeholder="Description" rows="3"></textarea>
    </div>

     <input type="hidden" name="_token" value="{{ csrf_token() }}">
    
     <button type="submit" class="btn btn-default w3-red"><i class="fa fa-pencil fa-fw"></i> Add</button>
       </form>



	</div>
@endsection
