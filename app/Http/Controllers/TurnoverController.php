<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Year;
use App\Member;
use App\Section;
use App\Course;
use App\RoleMember;
use App\ActionMember;
use App\MticsEvent;
use App\Officerterm;
use App\Role;
use App\MticsBudget;
use App\MticsWithdrawal;
use App\MticsDeposit;
use App\EventWithdrawal;
use App\EventDeposit;
use App\Task;
use App\PrevOfficer;
use App\Penalty;


use Illuminate\Support\Facades\DB;

class TurnoverController extends Controller
{
    public function view()
    {
        if(!Gate::allows('mticsadviser-only')) {
            return redirect('/');
        }


    	return view('admin/addFeature/turnover1');
    }



    public function upsection()
    {
    	//UPGRADE THE YEAR OF STUDENT
        $reqfaculty_id = Year::getId('App\Year','year_code','Faculty');
        $year_alumni_id = Year::getId('App\Year','year_code','Alumni');
        $section_alumni_id = Section::getId('App\Section','section_code','Alumni');
    	$members = Member::where('status','active')->where('year_id','!=',$reqfaculty_id)->get();

        foreach ($members as $member) {



            $section = Section::find($member->section_id);
            $course = Course::find($section->course_id);
            $cur_year = Year::find($member->year_id);

            if($cur_year->year_num == $course->course_year_count)
            {

            Member::where('id', $member->id)->update(array('year_id' => $year_alumni_id));
            Member::where('id', $member->id)->update(array('section_id' => $section_alumni_id));
            Member::where('id', $member->id)->update(array('status' => 'inactive'));
            Member::where('id', $member->id)->update(array('type' => null));
            Member::where('id', $member->id)->update(array('grad_course' => $course->course_code));
            Member::where('id', $member->id)->update(array('grad_year' => date("Y")));
             RoleMember::where('member_id', $member->id)->delete();
             ActionMember::where('member_id', $member->id)->delete();
            }
            else
            {
            $inc_year = $cur_year->year_num + 1;
            //dd($cur_year->year_num);
            $year = Year::getId('App\Year','year_num',$inc_year);

            Member::where('id', $member->id)->update(array('year_id' => $year));

            }

        }


    }

    public function endBatch()
    {
        //EXCESS budget need to store
        $budget = new FormulaMoneyController;
        $mticsfund_amt = $budget->CheckMticsFundBudget();

        //done the prev term
        $end_term = Officerterm::where('status','on-going')->first();
        $end_term->status = 'done';
        $end_term->save();

        $fromyear = date('Y');
        $toyear = $fromyear + 1;

        $officerterm = New Officerterm;
        $officerterm->start_term = date('Y-m-d');
        $officerterm->status = 'on-going';
        $officerterm->from_year = $fromyear;
        $officerterm->to_year = $toyear;
        $officerterm->excess_budget = $mticsfund_amt;
        $officerterm->save();

        $roles = RoleMember::wherehas('role', function($q){
            $q->where('assistant','false')->orwhere('assistant','true');
        })->get();

        foreach ($roles as $role) {
            $prevofficer = new PrevOfficer;
            $prevofficer->officerterm_id = $end_term->id;
            $prevofficer->member_id = $role->member_id;
            $prevofficer->role_id = $role->role_id;
            $prevofficer->save();
        }

        RoleMember::wherehas('role', function($q){
            $q->where('assistant','false')->orwhere('assistant','true');
        })->delete();

        MticsBudget::where('budget_status', 'on-going')->update(array('budget_status' => 'cleared'));
        MticsEvent::where('status', 'on-going')->update(array('status' => 'done'));

        $this->upsection();
        return redirect('admin/turn-over');

    }


    public function view_details(Request $request){
        
        $this->validate($request, [
            'year' => 'required',
            ]);
        $year = $request->year;
        if(Officerterm::where('from_year',$year)->get()->isEmpty()){
            return redirect()->back()->withErrors('No batch Found');
        }
        
        return view('admin/addFeature/turnover_view', compact('year'));

    }


    public function accomplishment(Request $request){

        $this->validate($request, [
            'year' => 'required',
            ]);

        $fromyear =$request->year;
        $toyear = $fromyear + 1;
        if(Officerterm::where('from_year',$fromyear)->where('to_year',$toyear)->get()->isEmpty()){
            return redirect()->back()->withErrors('No Batch Result');
        }

        $officerterm = Officerterm::where('from_year',$fromyear)->where('to_year',$toyear)->first();

       


        if($officerterm->status == 'on-going')
        {
            $roles = Role::wherehas('role_member')->where('name','activity')->orwhere('name','asst_activity')->orwhere('name','president')->orderby('id','DESC')->with('member')->get();
            
        }
        else
        {
            $roles = Role::wherehas('prev_officer',function($q) use($officerterm){
            $q->where('officerterm_id',$officerterm->id);
             })->where('name','activity')->orwhere('name','asst_activity')->orwhere('name','president')->get();
                
                
        }


        if(!Officerterm::where('from_year',$toyear)->where('to_year',($toyear + 1))->get()->isEmpty()){
            $nextbatch = Officerterm::where('from_year',$toyear)->where('to_year',($toyear + 1))->first();
            $events = MticsEvent::where('start_date','<=',date($nextbatch->created_at))->where('start_date','>=',date($officerterm->created_at))->where('status','done')->orderBy('start_date','ASC')->get();

        }
        else
        {
            $events = MticsEvent::where('start_date','>=',date($officerterm->created_at))->where('status','done')->orderBy('start_date','ASC')->get();
        }

        if($events->isEmpty()){
            return redirect('admin/turn-over')->withErrors("No Accomplished Event for batch ".$fromyear."-".$toyear);
        }

        $activities = array();
        foreach ($events as $event) {
            $activities[$event->id]['date'] = date('F j,Y',strtotime($event->start_date));
            $activities[$event->id]['event'] = $event;

        }


        $fromDate = date('F Y',strtotime($events->first()->start_date));
        $toDate = date('F Y',strtotime($events->last()->start_date));



        return view('reports/accomplishment_report', compact('activities','fromyear','toyear','roles','fromDate','toDate','officerterm'));

    }

    public function financial_mtics(Request $request){
          $this->validate($request, [
            'year' => 'required',
            ]);

        $fromyear =$request->year;
        $toyear = $fromyear + 1;

        if(Officerterm::where('from_year',$fromyear)->where('to_year',$toyear)->get()->isEmpty()){
            return redirect()->back()->withErrors('No Batch Result');
        }

        $officerterm = Officerterm::where('from_year',$fromyear)->where('to_year',$toyear)->first();
        $excess_budget = $officerterm->excess_budget;
       /* $roles = Role::wherehas('prev_officer',function($q) use($officerterm){
            $q->where('officerterm_id',$officerterm->id);
        })->where('name','finance')->orwhere('name','asst_finance')->orwhere('name','president')->get();
*/
        //FETCHING OFFICERS
        
        if($officerterm->status == 'on-going')
        {
            $roles = Role::wherehas('role_member')->where('name','finance')->orwhere('name','asst_finance')->orwhere('name','auditor')->orwhere('name','asst_auditor')->orwhere('name','president')->orderby('id','DESC')->with('member')->get();
                
                
        }
        else
        {
            $roles = Role::wherehas('prev_officer',function($q) use($officerterm){
                $q->where('officerterm_id',$officerterm->id);
            })->where('name','finance')->orwhere('name','asst_finance')->orwhere('name','auditor')->orwhere('name','asst_auditor')->orwhere('name','president')->with('prev_member')->orderby('id','DESC')->get();
                
        }




        if(!Officerterm::where('from_year',$toyear)->where('to_year',($toyear + 1))->get()->isEmpty()){
            $nextbatch = Officerterm::where('from_year',$toyear)->where('to_year',($toyear + 1))->first();
            //MONTHLY BUDGET AND EXPENSES



            $nextbatch = date($nextbatch->created_at);
            $selectedbatch = date($officerterm->created_at);

            $monthly_budgets = MticsBudget::wherehas('withdraw1', function($q) use($nextbatch,$selectedbatch){ $q->where('created_at','<=',$nextbatch)->where('created_at','>=',$selectedbatch)->where('withdrawal_status','cleared');
            })->with('budget_breakdown.budget_expenses','budget_breakdown.budget_expenses_cat')->orderby('created_at','ASC')->get();

            $withdrawals = MticsWithdrawal::where('created_at','<=',$nextbatch)->where('created_at','>=',$selectedbatch)->where('withdrawal_status','cleared')->get();

            $deposits = MticsDeposit::where('created_at','<=',$nextbatch)->where('created_at','>=',$selectedbatch)->where('deposit_status','cleared')->get();

            $externaltasks = Task::wherehas('mticspurchaselist')->with('mticspurchaselist')->where(function($q) {
            $q->where('task_status','done')->orwhere('task_status','failed');
            })->where('created_at','<=',$nextbatch)->where('created_at','>=',$selectedbatch)->get();

            $penalties = Penalty::where('created_at','<=',$nextbatch)->where('created_at','>=',$selectedbatch)->where('fee','!=','null')->where('penalty_status','paid')->get()->sum('fee');

        $mticsbankamount_current = $this->CheckMticsFundBucket_prev_selected($selectedbatch,$nextbatch);


        }
        else{

            $selectedbatch = date($officerterm->created_at);
            $monthly_budgets = MticsBudget::wherehas('withdraw1', function($q) use($selectedbatch){ $q->where('created_at','>=',$selectedbatch)->where('withdrawal_status','cleared');
            })->with('budget_breakdown.budget_expenses','budget_breakdown.budget_expenses_cat')->orderby('created_at','ASC')->get();


            $withdrawals = MticsWithdrawal::where('created_at','>=',date($officerterm->created_at))->where('withdrawal_status','cleared')->get();


            $deposits = MticsDeposit::where('created_at','>=',date($officerterm->created_at))->where('deposit_status','cleared')->get();

            $externaltasks = Task::wherehas('mticspurchaselist')->with('mticspurchaselist')->where(function($q) {
            $q->where('task_status','done')->orwhere('task_status','failed');
            })->where('created_at','>=',date($officerterm->created_at))->get();

            $penalties = Penalty::where('created_at','>=',date($officerterm->created_at))->where('fee','!=','null')->where('penalty_status','paid')->get()->sum('fee');


        $mticsbankamount_current = $this->CheckMticsFundBucket_current_selected($officerterm->created_at);

        }


        $totalbudget = $monthly_budgets->sum('budget_amt');
        $totalwithdraw = $withdrawals->sum('withdrawal_amt');
        $totaldeposit = $deposits->sum('deposit_amt');


        $mticsbankamount_turnover = $this->CheckMticsFundBucket_turnover($officerterm->created_at);

        $total_batch_money = $mticsbankamount_turnover - ($mticsbankamount_current < 0 ? $mticsbankamount_current*-1 : $mticsbankamount_current);


        return view('reports/financial_mtics_report',compact('monthly_budgets','totalbudget','totalwithdraw','withdrawals','externaltasks','mticsbankamount_turnover','mticsbankamount_current','total_batch_money','deposits','totaldeposit','fromyear','toyear','excess_budget','roles','penalties','officerterm'));


    }


    private function CheckMticsFundBucket_turnover($from){
         //formula for total Mtics fund in bank (this is the virtual bank amount)
    $mticsfundwithdraw = MticsWithdrawal::where('withdrawal_status','cleared')->where('created_at','<=',date($from))->get()->sum('withdrawal_amt');
    $mticsfunddeposit = MticsDeposit::where('deposit_status','cleared')->where('created_at','<=',date($from))->get()->sum('deposit_amt');
    $mticsdeposittoevent = EventDeposit::where('deposit_source','MTICS Fund')->where('created_at','<=',date($from))->get()->sum('deposit_amt');
    $mticsfundbucket = $mticsfunddeposit - $mticsfundwithdraw - $mticsdeposittoevent;

    return $mticsfundbucket;

    }

    private function CheckMticsFundBucket_current_selected($from)
    {
        //formula for total Mtics fund in bank (this is the virtual bank amount)
    $mticsfundwithdraw = MticsWithdrawal::where('withdrawal_status','cleared')->where('created_at','>=',date($from))->get()->sum('withdrawal_amt');
    $mticsfunddeposit = MticsDeposit::where('deposit_status','cleared')->where('created_at','>=',date($from))->get()->sum('deposit_amt');
    $mticsdeposittoevent = EventDeposit::where('deposit_source','MTICS Fund')->where('created_at','>=',date($from))->get()->sum('deposit_amt');
    $mticsfundbucket = $mticsfunddeposit - $mticsfundwithdraw - $mticsdeposittoevent;

    return $mticsfundbucket;

    }

      private function CheckMticsFundBucket_prev_selected($from,$to)
    {
        //formula for total Mtics fund in bank (this is the virtual bank amount)
    $mticsfundwithdraw = MticsWithdrawal::where('withdrawal_status','cleared')->where('created_at','<=',$to)->where('created_at','>=',$from)->get()->sum('withdrawal_amt');
    $mticsfunddeposit = MticsDeposit::where('deposit_status','cleared')->where('created_at','<=',$to)->where('created_at','>=',$from)->get()->sum('deposit_amt');
    $mticsdeposittoevent = EventDeposit::where('deposit_source','MTICS Fund')->where('created_at','<=',$to)->where('created_at','>=',$from)->get()->sum('deposit_amt');
    $mticsfundbucket = $mticsfunddeposit - $mticsfundwithdraw - $mticsdeposittoevent;

    return $mticsfundbucket;

    }





//EVENT FINANCIAL

    public function financial_event_view(Request $request){
         $this->validate($request, [
            'year' => 'required',
            ]);

        $fromyear =$request->year;
        $toyear = $fromyear + 1;

        if(Officerterm::where('from_year',$fromyear)->where('to_year',$toyear)->get()->isEmpty()){
            return redirect()->back()->withErrors('No Batch Result');
        }

        $officerterm = Officerterm::where('from_year',$fromyear)->where('to_year',$toyear)->first();
        $selectedbatch = date($officerterm->created_at);

         if(!Officerterm::where('from_year',$toyear)->where('to_year',($toyear + 1))->get()->isEmpty()){
            $nextbatch = Officerterm::where('from_year',$toyear)->where('to_year',($toyear + 1))->first();
            //Previous Batch
            $nextbatch = date($nextbatch->created_at);
            $events = MticsEvent::where('start_date','<=',$nextbatch)->where('start_date','>=',$selectedbatch)->where('status','done')->get();

        }
        else
        {   //Current batch on-going
            $events = MticsEvent::where('start_date','>=',$selectedbatch)->where('status','done')->get();
        }

        return view('reports/financial_event_report',compact('events'));

    }


}
