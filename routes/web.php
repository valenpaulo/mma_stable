<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return redirect('member/login');});

//HESTO ROUTE
Route::group(['prefix' => 'member'], function () {
  Route::get('/login', 'MemberAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'MemberAuth\LoginController@login');
  Route::get('/logout', 'MemberAuth\LoginController@logout')->name('logout');
  Route::get('/register', 'MemberAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'MemberAuth\RegisterController@register');

  Route::post('/password/email', 'MemberAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'MemberAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'MemberAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'MemberAuth\ResetPasswordController@showResetForm');
Route::get('/verify/{id}','MemberAuth\RegisterController@sendEmailDone')->name('sendEmailDone');

});

Route::get('/check/username', ['as' => 'check.username', 'uses' => 'MemberController@checkUsername']);
Route::get('/check/student-id', ['as' => 'check.student.id', 'uses' => 'MemberController@checkStudentId']);
Route::get('/check/mobile', ['as' => 'check.mobile', 'uses' => 'MemberController@checkMobileNo']);
Route::get('/check/email', ['as' => 'check.email', 'uses' => 'MemberController@checkEmail']);


Route::get('/profile', function () {return redirect('member/profile/post');});
Route::get('/home', function () {return redirect('member/home');});
Route::get('/member', function () {return redirect('member/home');});
Route::get('/admin', function () {return redirect('admin/manage-members');});

// android sync
Route::post('/android-data-sync/', ['as' => 'sync.payment', 'uses' => 'SyncController@store']);

Route::get('/firebase-notification', ['as' => 'firebase.notification', 'uses' => 'FireBaseController@sendNotification']);

Route::post('/sheduler/', ['as' => 'scheduler', 'uses' => 'SchedulerController@schedule']);

Route::post('/android-data-sync/borrowed-item', ['as' => 'sync.borrowed.item', 'uses' => 'SyncController@syncBorrowedItems']);

Route::post('/android-data-sync/returned-item', ['as' => 'sync.returned.item', 'uses' => 'SyncController@syncReturnedItems']);

Route::get('/android-data-sync/get-event', ['as' => 'sync.event', 'uses' => 'SyncController@getEvent']);
Route::post('/android-data-sync/get-event-reply', ['as' => 'sync.event.reply', 'uses' => 'SyncController@getEventReply']);

Route::get('/android-data-sync/get-inventory', ['as' => 'sync.inventory', 'uses' => 'SyncController@getInventory']);

Route::post('/android-data-sync/get-inventory-reply', ['as' => 'sync.inventory.reply', 'uses' => 'SyncController@getInventoryReply']);


Route::post('/android-data-sync/sign-in', ['as' => '2sync.sign.in', 'uses' => 'SyncController@signIn']);

# READ NOTIFICATION
Route::group(['prefix' => 'read-notification', 'middleware' => 'auth:member'], function () {
    Route::get('/all', ['as' => 'read.all', 'uses' => 'ReadNotificationController@readAllNotification']);

    Route::get('/task/{id}', ['as' => 'read.task', 'uses' => 'ReadNotificationController@readTaskNotification']);

    Route::get('/privilege/{id}', ['as' => 'read.privilege', 'uses' => 'ReadNotificationController@readPrivilegeNotification']);

    Route::get('/event/{id}', ['as' => 'read.event', 'uses' => 'ReadNotificationController@readEventNotification']);

    Route::get('/meeting/{id}', ['as' => 'read.meeting', 'uses' => 'ReadNotificationController@readMeetingNotification']);

    Route::get('/form/{id}', ['as' => 'read.form', 'uses' => 'ReadNotificationController@readFormNotification']);

    Route::get('/internal/{id}', ['as' => 'read.internal', 'uses' => 'ReadNotificationController@readInternalNotification']);

    Route::get('/docu/{id}', ['as' => 'read.docu', 'uses' => 'ReadNotificationController@readDocuNotification']);

    Route::get('/external/{id}', ['as' => 'read.external', 'uses' => 'ReadNotificationController@readExternalNotification']);

    Route::get('/finance/{id}', ['as' => 'read.finance', 'uses' => 'ReadNotificationController@readFinanceNotification']);

    Route::get('/logistic/{id}', ['as' => 'read.logistics', 'uses' => 'ReadNotificationController@readLogisticsNotification']);

    Route::get('/reimbursement/{id}', ['as' => 'read.reimburse', 'uses' => 'ReadNotificationController@readReimbursementNotification']);

    Route::get('/report-post/{id}', ['as' => 'read.report.post', 'uses' => 'ReadNotificationController@readReportPostNotification']);

    Route::get('/mticsbudget/{id}', ['as' => 'read.mtics.budget.request', 'uses' => 'ReadNotificationController@readBudgetRequestNotification']);

    Route::get('/mticsbudgetPresident/{id}', ['as' => 'read.approved.request', 'uses' => 'ReadNotificationController@readApprovedBudgetRequestNotification']);

    Route::get('/depositRequest/{id}', ['as' => 'read.bank.request', 'uses' => 'ReadNotificationController@readBankRequestNotification']);
    Route::get('/withdrawRequest/{id}', ['as' => 'read.bank.request', 'uses' => 'ReadNotificationController@readBankRequestNotification']);

    Route::get('/purchase-request/{id}', ['as' => 'read.purchase.request', 'uses' => 'ReadNotificationController@readPurchaseRequestNotification']);

    Route::get('/request-money/{id}', ['as' => 'read.money.request', 'uses' => 'ReadNotificationController@readMoneyRequestNotification']);

    Route::get('/approve-money-request/{id}', ['as' => 'read.approve.money.request', 'uses' => 'ReadNotificationController@readApprovedMoneyRequestNotification']);
    Route::get('/purchased-item/{id}', ['as' => 'read.purchased.item', 'uses' => 'ReadNotificationController@readPurchasedItemNotification']);

    Route::get('/purchased-item-validation/{id}', ['as' => 'read.purchased.item', 'uses' => 'ReadNotificationController@readPurchasedItemValidationNotification']);

    Route::get('/depositDisapprove/{id}', ['as' => 'read.deposit.disapproved', 'uses' => 'ReadNotificationController@readDepositDisapproveNotification']);

    Route::get('/report-deposit/{id}', ['as' => 'read.report.deposit', 'uses' => 'ReadNotificationController@readReportDepositNotification']);
    Route::get('/report-withdraw/{id}', ['as' => 'read.report.withdraw', 'uses' => 'ReadNotificationController@readReportWithdrawNotification']);

    Route::get('/president/{id}', ['as' => 'read.president', 'uses' => 'ReadNotificationController@readPresidentNotification']);

    Route::get('/purchased-item-report/{id}', ['as' => 'read.purchased.item.report', 'uses' => 'ReadNotificationController@readPurchasedItemReportNotification']);

    Route::get('/Document/{id}', ['as' => 'read.document.pres', 'uses' => 'ReadNotificationController@readDocumentNotification']);

    Route::get('/breakdown-request-president/{id}', ['as' => 'read.breakdown.request', 'uses' => 'ReadNotificationController@readBreakdownRequestNotification']);

    Route::get('/logistics-validation/{id}', ['as' => 'read.logistics.validation', 'uses' => 'ReadNotificationController@readLogisticsValidationNotification']);

    Route::get('/bank-transaction-approved/{id}', ['as' => 'read.bank.approved', 'uses' => 'ReadNotificationController@readBankTransactionApprovedNotification']);

    Route::get('/penalty/{id}', ['as' => 'read.bank.approved', 'uses' => 'ReadNotificationController@readPenaltyNotification']);
});


//MEMBER ROUTE
Route::group(['prefix' => 'member', 'middleware' => 'auth:member'], function () {
    Route::get('/dashboard', ['uses' => 'DashboardController@index']);

    Route::get('/android-download', ['uses' => 'AndroidController@download']);

    Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);
    Route::get('/profile', function () {return redirect('member/profile/post');});

    Route::group(['prefix' => 'profile'], function () {

        Route::get('post', array('uses' => 'ProfileController@index','as' => 'profile.post'));
        Route::get('notifications', array('uses' => 'ProfileController@index','as' => 'profile.notif'));
        Route::get('activity-log', array('uses' => 'ProfileController@index','as' => 'profile.actlog'));

        // Route::get('notifications', ['as' => 'profile.notif', function () {
        //     return view('profile/notification');
        // }]);
        // Route::get('activitylog', ['as' => 'profile.actlog', function () {
        //     return view('profile/activityLog');
        // }]);

        //EDIT member's informatin
        Route::post('/edit/{id?}','ProfileController@edit');
        Route::get('/edit/{id?}',function () {return redirect('member/profile');});
        //ADD POST
        Route::post('/post/add','PostController@store');
        Route::get('/post/add', function () {return redirect('member/profile');});
        //VIEW POST
        Route::get('/post/{id}/{slug}','PostController@view');
        Route::get('/post/{id}', function () {return redirect('member/profile');});
        //DELETE POST
        Route::post('/post/{id?}/delete','PostController@delete');
        Route::get('/post/{id?}/delete', function () {return redirect('member/profile');});
        //EDIT POST
        Route::post('/post/{id}/{slug}/edit','PostController@edit');
        Route::get('/post/{id}/{slug}/edit', 'PostController@editView');
        //REPLY/COMMENT POST
        Route::post('/post/{id}/{slug}/reply','PostController@reply');
        Route::get('/post/{id}/{slug}/reply', function () {return redirect('member/profile');});

        Route::post('/post/comment/{id}/delete',['as'=>'member.reply.delete', 'uses' => 'PostController@deleteReply']);

        //REPORT POST
        Route::post('/post/{id}/{slug}/report','PostController@report');
        Route::get('/post/{id}/{slug}/report', function () {return redirect('member/profile');});

    });

    Route::group(['prefix' => 'event'], function () {
        //VIEW POST
        Route::get('/{id}/{slug}','PostController@event_partial_view');
        Route::get('/{id}', function () {return redirect('member/home');});
        //REPLY/COMMENT POST
        Route::post('/{id}/{slug}/reply','PostController@event_partial_reply');
        Route::get('/{id}/{slug}/reply', function () {return redirect()->back();});
    });


    //EVENT PARTIAL AND FULL POST

        Route::get('/{id}/{slug}','PostController@event_view');
        //REPORT POST
        Route::post('/{id}/{slug}/reply','PostController@event_reply');
        Route::get('/{id}/{slug}/reply', function () {return redirect('member/profile');});


});
    //backup
        Route::get('backup/create', 'BackupController@create');
        Route::get('backup/createdb', 'BackupController@createdb');
        Route::get('backup/download/{file_name}', 'BackupController@download');
        Route::get('backup/restoredb/{file_name}', 'BackupController@restoredb');
        Route::get('backup/delete/{file_name}', 'BackupController@delete');
//ADMIN ROUTE
Route::group(['prefix' => 'admin', 'middleware' => 'auth:member'], function () {
Route::get('/layout', function () {return view('layout/control_panel');});
    //backup
  Route::get('admin/backup', ['as'=>'admin.backup', 'uses' => 'BackupController@index']);
    // Admin
    Route::get('/',['as'=>'admin', 'uses' => 'AdminController@index']);

    // MANAGE EVENT
    Route::get('/manage-event',['as'=>'event', 'uses' => 'EventController@view']);

//MANAGE MEMBERS
      Route::get('/additional-feature',['as'=>'admin.additional.feature', 'uses' => 'AdditionalFeatureController@index']);

    Route::group(['prefix' => 'manage-members'], function () {
        Route::get('/',['uses'=>'MemberController@view', 'as'=>'admin.manage.user']);
        Route::get('/search',['uses'=>'MemberController@search', 'as'=>'admin.user.filter']);
        //EXCEL BULK MEMBERS
        Route::post('/import-excel', 'MemberController@importExcel');
        Route::get('/import-excel', function () {return redirect('admin/manage-members');});
        //ADD MEMBERS MANUALLY
        Route::post('/store', 'MemberController@store');
        Route::get('/store', function () {return redirect('admin/manage-members');});
        Route::get('/store', function () {return redirect('admin/manage-members');});
        //EDIT MEMBER
        Route::get('/{id?}/edit', function () {return redirect('admin/manage-members');});
        Route::post('/{id?}/edit', 'MemberController@edit');
        Route::post('/{id?}/status', 'MemberController@status');
        Route::get('/template-download', 'MemberController@template_download');

    });

//MANAGE PRIVILEGE
    Route::group(['prefix' => 'manage-privilege'], function () {
        Route::get('/',['uses'=>'RoleController@view', 'as'=>'admin.manage.privilege']);
        Route::get('/search',['uses'=>'RoleController@search', 'as'=>'admin.privilege.search']);
        Route::post('/role/edit/{id?}', 'RoleController@edit');
        Route::get('/role/edit/{id?}', function () {return redirect('admin/manage-privilege');});
        Route::post('/action/edit/{id?}', 'RoleController@edit_action');
        Route::get('/{id?}/edit', function () {return redirect('admin/manage-privilege');});
        Route::post('/mtics-adviser', 'RoleController@mtics_adviser');
        Route::get('/mtics-adviser/remove/{id}', 'RoleController@mtics_adviser_remove');


    });


//MANAGE REQUEST ACCOUNT
    Route::group(['prefix' => 'request-account'], function () {
        Route::get('/',['uses'=>'ReqAccntController@view', 'as'=>'admin.request.account']);
        Route::get('/search',['uses'=>'ReqAccntController@search', 'as'=>'admin.request.account.search']);
        //CONFIRM REQUEST
        Route::post('/{id?}/confirm', 'ReqAccntController@confirm');
        Route::get('/{id?}/confirm', function () {return redirect('admin/request-account');});
        //REJECT REQUEST
        Route::post('/{id?}/reject', 'ReqAccntController@reject');
        Route::get('/{id?}/reject', function () {return redirect('admin/request-account');});

    });


//MANAGE POSTSFEED
    Route::group(['prefix' => 'manage-postsfeed'], function () {
        Route::get('/',['uses'=>'PostController@postsfeed_view', 'as'=>'admin.manage.postsfeed']);
        Route::post('/filter',['uses'=>'PostController@filter', 'as'=>'admin.postsfeed.filter']);
        Route::post('/status/{id}',['uses'=>'PostController@status', 'as'=>'admin.postsfeed.status']);
        Route::post('/{id}/delete', 'PostController@postsfeed_delete');
        Route::get('/{id}/delete', function () {return redirect('admin/manage-postsfeed');});
    });

// SETTINGS
    Route::get('/settings',['as'=>'admin.setting', 'uses' => 'SettingController@index']);

//MANAGE FILTER WORDS
    Route::group(['prefix' => 'manage-filterwords', 'middleware' => 'auth:member'], function () {
        Route::get('/', array('uses' => 'FilterWordsController@view','as' => 'admin.filter.words'));
        //EXCEL BULK FILTER WORDS
        Route::post('/import-excel', 'FilterWordsController@importExcel');
        Route::get('/import-excel', function () {return redirect('admin/manage-filterwords');});
        //ADD FILTER WORD MANUALLY
        Route::post('/store', 'FilterWordsController@store');
        Route::get('/store', function () {return redirect('admin/manage-filterwords');});
        //EDIT FILTER WORD
        Route::post('/{id}/edit', 'FilterWordsController@edit');
        Route::get('/{id}/edit', function () {return redirect('admin/manage-filterwords');});
         Route::get('/{id}', function () {return redirect('admin/manage-filterwords');});
         //DELETE FILTER WORD
        Route::post('/{id}/delete', 'FilterWordsController@delete');
        Route::get('/{id}/delete', function () {return redirect('admin/manage-filterwords');});

    });

// MANAGE BANNER
    Route::group(['prefix' => 'manage-banner', 'middleware' => 'auth:member'], function () {
        Route::get('/', array('uses' => 'BannerController@view','as' => 'admin.banner'));
        Route::post('/store', array('uses' => 'BannerController@store','as' => 'admin.banner.store'));
        Route::get('/delete/{id}', array('uses' => 'BannerController@delete','as' => 'admin.banner.delete'));
        Route::get('/enable/{id}', array('uses' => 'BannerController@enable','as' => 'admin.banner.enable'));
        Route::get('/disable/{id}', array('uses' => 'BannerController@disable','as' => 'admin.banner.disable'));
    });


//MANAGE SECTION
    Route::group(['prefix' => 'manage-sections', 'middleware' => 'auth:member'], function () {
        Route::get('/', array('uses' => 'SectionController@view','as' => 'admin.manage.section'));
        //EXCEL BULK MANAGE SECTION
        Route::post('/manage-masterlist', 'SectionController@checksection');
        Route::get('/manage-masterlist', function () {return redirect('admin/manage-sections');});
        //download template
        Route::get('/template-download', 'SectionController@template_download');
        //print masterlist
        Route::post('/print-masterlist', 'SectionController@print_masterlist');
        Route::get('/print-masterlist', function () {return redirect('admin/manage-sections');});



        //MAKE CHANGES (THAT MOMENT NA WALA KA LNG MAPANGALAN)
        Route::post('/make-changes', 'SectionController@make_changes');
        Route::get('/make-changes', function () {return redirect('admin/manage-sections');});
        //ADD COURSE
        Route::post('/course-store', 'SectionController@course_store');
        Route::get('/course-store', function () {return redirect('admin/manage-sections');});
        //EDIT COURSE
        Route::post('/{id}/course-edit', 'SectionController@course_edit');
        Route::get('/{id}/course-edit', function () {return redirect('admin/manage-sections');});
        //DELETE COURSE
        Route::post('/{id}/course-delete', 'SectionController@course_delete');
        Route::get('/{id}/course-delete', function () {return redirect('admin/manage-sections');});
        //ADD SECTION
        Route::post('/section-store', 'SectionController@section_store');
        Route::get('/section-store', function () {return redirect('admin/manage-sections');});
        //EDIT SECTION
        Route::post('/{id}/section-edit', 'SectionController@section_edit');
        Route::get('/{id}/section-edit', function () {return redirect('admin/manage-sections');});
        //DELETE SECTION
        Route::post('/{id}/section-delete', 'SectionController@section_delete');
        Route::get('/{id}/section-delete', function () {return redirect('admin/manage-sections');});


    });


//MANAGE REQUEST-TO-BUY
    Route::group(['prefix' => 'request-to-buy', 'middleware' => 'auth:member'], function () {
        Route::get('/', array('uses' => 'ExternalController@reqtobuy_view','as' => 'admin.request.to.buy'));

        //ADD REQUEST TO BUY
            Route::post('/store', 'ExternalController@reqtobuy_store');
            Route::get('/store', function () {return redirect('admin/request-to-buy');});



    });

 //REQUEST-REIMBURSEMENT
    Route::group(['prefix' => 'requested-reimbursement', 'middleware' => 'auth:member'], function () {
        Route::get('/', 'ReimburseController@req_reimburse_view');
        //Accept reimbursement request
        Route::post('/{id}/accept', 'ReimburseController@req_reimburse_accept');
        Route::get('/{id}/accept', function () {return redirect('admin/requested-reimbursement');});
        //Deny reimbursement request
        Route::post('/{id}/deny', 'ReimburseController@req_reimburse_deny');
        Route::get('/{id}/deny', function () {return redirect('admin/requested-reimbursement');});

    });


//OFFICERS ROUTE OFFICERS ROUTE OFFICERS ROUTE OFFICERS ROUTE OFFICERS ROUTE

//PRESIDENT ROUTE
    Route::group(['prefix' => 'president', 'middleware' => 'auth:member'], function () {
        Route::get('/', function () {return redirect('admin/president/manage-event');});

        Route::group(['prefix' => 'requested-event-breakdown', 'middleware' => 'auth:member'], function () {
        //approval of event breakdown
        Route::get('/', array('uses' => 'PresidentController@event_breakdown_view','as' => 'admin.president'));
        Route::post('/{id}/approved', array('uses' => 'PresidentController@event_breakdown_approved','as' => 'admin.president.approve'));
        Route::post('/{id}/denied', array('uses' => 'PresidentController@event_breakdown_denied','as' => 'admin.president.deny'));
        });

        Route::group(['prefix' => 'bank-transaction-request-reported', 'middleware' => 'auth:member'], function () {
        Route::get('/', array('uses' => 'PresidentController@bank_reported_solution_view', 'as' => 'bank.trans.issue'));
        //mtics
        Route::post('/{id}/add-mtics-deposit-solution', array('uses' => 'PresidentController@bank_mtics_deposit_reported_solution'));
        Route::post('/{id}/add-mtics-withdraw-solution', array('uses' => 'PresidentController@bank_mtics_withdraw_reported_solution'));
        //event
        Route::post('/{id}/add-event-deposit-solution', array('uses' => 'PresidentController@bank_event_deposit_reported_solution'));
        Route::post('/{id}/add-event-withdraw-solution', array('uses' => 'PresidentController@bank_event_withdraw_reported_solution'));

        });





    //MANAGE TASK
        Route::group(['prefix' => 'manage-task', 'middleware' => 'auth:member'], function () {
            //Route::get('/', 'PresidentController@task_view');

            Route::post('/store/{id?}', 'TaskController@store');
            Route::post('/edit/{id}', 'TaskController@edit');
            Route::post('/delete/{id}', 'TaskController@delete');

            Route::get('/store', function () {return redirect('admin/president/manage-task');});
        });
    //Purchased-item-reported
        Route::group(['prefix' => 'purchased-item-reported', 'middleware' => 'auth:member'], function () {

            Route::get('/', array('uses' => 'PresidentController@purchase_item_reported_view', 'as' => 'purchased.item.issue'));
            //mtics
            Route::post('/{id}/add-mtics-solution', 'PresidentController@purchase_item_reported_mtics_add');
            Route::get('/{id}/add-mtics-solution', function () {return redirect()->back();});
            //event
            Route::post('/{id}/add-event-solution', 'PresidentController@purchase_item_reported_event_add');
            Route::get('/{id}/add-event-solution', function () {return redirect()->back();});


        });

    //MANAGE EVENT
        Route::group(['prefix' => 'manage-event', 'middleware' => 'auth:member'], function () {
            Route::get('/', 'EventController@presidentView');
            Route::post('/store', 'EventController@store');
            Route::get('/{id}', 'EventController@event_view');
            Route::post('/edit/{id}', 'EventController@edit');
            Route::get('/start/{id}', 'EventController@start');
            Route::get('/done/{id}', 'EventController@done');
            Route::get('/cancel/{id}', 'EventController@cancel');
            Route::post('/delete/{id}', 'EventController@delete');
            //Participant
            Route::post('/{id}/participant', 'EventController@participant');
            Route::get('/{id}/participant', function () {return redirect()->back();});
             //Delete participant
            Route::post('/{id}/participant/delete', 'EventController@participant_delete');
            Route::get('/{id}/participant/delete', function () {return redirect()->back();});

            //Manage task, approved and disapproved
            Route::get('/approve/{id}', 'TaskController@approved');
            Route::post('/disapprove/{id}', 'TaskController@disapprove');

            //MANAGE PENALTY for this event
            Route::get('/{id}/failed-task', 'TaskController@failedtask_view');
            Route::post('/{id}/failed-task/done/{penalty_id}', 'TaskController@failedtask_done');
            Route::post('/{id}/failed-task/edit/{penalty_id}', 'TaskController@failedtask_edit');
            Route::post('/{id}/failed-task/add', 'TaskController@failedtask_add');

        });


    //MANAGE MEETING
        Route::group(['prefix' => 'manage-meeting', 'middleware' => 'auth:member'], function () {
            Route::post('/store/{id}', 'MeetingController@store');
            Route::post('/edit/{id}', 'MeetingController@edit');
            Route::post('/cancel/{id}', 'MeetingController@cancel');
            Route::post('/done/{id}', 'MeetingController@done');
            Route::get('/discussion/{id}', 'MeetingController@discussion');
        });

    //Manage officers
        Route::group(['prefix' => 'manage-officers', 'middleware' => 'auth:member'], function () {
            //change
            Route::post('/{id}/change', 'PresidentController@officers_change');
        });

        //REQUESTED-MTICS-BUDGET from finance
        Route::group(['prefix' => 'requested-mtics-budget', 'middleware' => 'auth:member'], function () {
            Route::get('/', array('uses' => 'MticsBudgetController@president_budget_view', 'as' => 'pres.manage.budget'));
             //Approved
            Route::post('/{id}/approved', 'MticsBudgetController@president_budget_approved');
            Route::get('/{id}/approved', function () {return redirect()->back();});
            //Deny
            Route::post('/{id}/denied', 'MticsBudgetController@president_budget_denied');
            Route::get('/{id}/denied', function () {return redirect()->back();});
        });
    });


//EXTERNAL ROUTE
    Route::group(['prefix' => 'external', 'middleware' => 'auth:member'], function () {
        Route::get('/', array('uses' => 'ExternalController@view','as' => 'admin.external')); // BASAG NA UI
    //Manage event task
      Route::group(['prefix' => 'manage-event', 'middleware' => 'auth:member'], function () {
        Route::get('/{id}', 'ExternalController@eventView');
        Route::get('{id}/task/{task_id}', 'ExternalController@task_view');
      });
//
    //MTICS Task
      Route::group(['prefix' => 'task', 'middleware' => 'auth:member'], function () {
            //EDIT ESTIMATED PRICE
            Route::post('/{id}/edit', 'ExternalController@task_edit');
            Route::get('/{id}/edit', function () {return redirect('admin/external/task');});
            //REQUEST-Money
            Route::post('/{id}/request', 'ExternalController@req_store');
            Route::get('/{id}/request', function () {return redirect('admin/external/task');});
            //VIEW TASK
            Route::get('/{id}', 'ExternalController@task_details_view');
            //Add EQUIPMENT
            Route::post('/{id}/{pur_id}/addequip', 'ExternalController@task_details_addequip');
            Route::get('/{id}/{pur_id}/addequip', function () {return redirect()->back();});
            //Purchase
            Route::post('/{id}/{pur_id}/purchased', 'ExternalController@task_details_purchased');
            Route::get('/{id}/{pur_id}/purchased', function () {return redirect()->back();});
            //Reimbursement
            Route::post('/{id}/reimburse', 'ExternalController@reimburse');
            Route::get('/{id}/reimburse', function () {return redirect()->back();});
      });

    });


//LOGISTICS ROUTE
    Route::group(['prefix' => 'logistics', 'middleware' => 'auth:member'], function () {
        Route::get('/', function () {return redirect('admin/logistics/inventory');});
    //LOGISTICS INVENTORY
        Route::group(['prefix' => 'inventory', 'middleware' => 'auth:member'], function () {
            Route::get('/',  array('uses' => 'LogisticsController@inv_view','as' => 'admin.logistics.inventory'));
            //INVENTORY MANUAL ADD
            Route::post('/store', 'LogisticsController@inv_add');
            Route::get('/store', function () {return redirect('admin/logistics/inventory');});
            //BULK EXCEL ADD
            Route::post('/import-excel', 'LogisticsController@importExcel');
            Route::get('/import-excel', function () {return redirect('admin/logistics/inventory');});
            //EDIT
            Route::post('/{id}/edit', 'LogisticsController@inv_edit');
            Route::get('/{id}/edit', function () {return redirect('admin/logistics/inventory');});
            //ARCHIVED
            Route::post('/{id}/delete', 'LogisticsController@inv_delete');
            Route::get('/{id}/delete', function () {return redirect('admin/logistics/inventory');});

            //INVENTORY MONTHLY REPORTS
            Route::post('/view-reports', 'ReportsController@logistics_monthly_reports');
            Route::get('/view-reports', function () {return redirect('admin/logistics/inventory');});
            //Download excel template
            Route::get('/template-download/normal', 'LogisticsController@template_download_normal');
            Route::get('/template-download/equip', 'LogisticsController@template_download_equip');


            //MANAGE-CATEGORY
            //Add inventory category
            Route::post('/category-name-store', 'LogisticsController@category_store');
            Route::get('/category-name-store', function () {return redirect('admin/logistics/inventory');});
            //Edit inventory category
            Route::post('/category-name-edit/{id}', 'LogisticsController@category_edit');
            Route::get('/category-name-edit/{id}', function () {return redirect('admin/logistics/inventory');});
             //Edit inventory category
            Route::post('/category-name-delete/{id}', 'LogisticsController@category_delete');
            Route::get('/category-name-delete/{id}', function () {return redirect('admin/logistics/inventory');});
            //Claimed Event offers
            Route::post('/event-offers-claimed/{id}', 'EventOffersController@claimed');

        });
    //LOGISTICS Borrowed/Returned
        Route::group(['prefix' => 'borrow-return', 'middleware' => 'auth:member'], function () {
            Route::get('/', array('uses' => 'LogisticsController@brw_rtn_view','as' => 'admin.borrow.return'));

            Route::post('/borrow', 'LogisticsController@brw_rtn_borrow');
            Route::post('/borrow', 'LogisticsController@brw_rtn_borrow');Route::get('/borrow', function () {return redirect('admin/logistics/borrow-return');});
            Route::post('/return', 'LogisticsController@brw_rtn_return');
            Route::get('/return', function () {return redirect('admin/logistics/borrow-return');});
        });
        //VIEW PURCHASED ITEM
        Route::get('manage-event/{id}', 'LogisticsController@eventView');

        Route::group(['prefix' => 'purchased-items', 'middleware' => 'auth:member'], function () {
            Route::get('/', array('uses' => 'LogisticsController@purchased_item_view', 'as' => 'admin.manage.purchases'));
            //Pass items
            Route::post('/{task_id}/{id}/pass', 'LogisticsController@purchased_item_pass');
            Route::get('/{task_id}/{id}/pass', function () {return redirect('admin/logistics/purchased-items');});
            //Report items
            Route::post('/{task_id}/{id}/report', 'LogisticsController@purchased_item_report');
            Route::get('/{task_id}/{id}/report', function () {return redirect('admin/logistics/purchased-items');});
        });

        Route::group(['prefix' => 'damage-report', 'middleware' => 'auth:member'], function () {
            Route::get('/{id}/{serial}', 'LogisticsController@damage_report_view');
            Route::post('/{id}/{serial}/add', 'LogisticsController@damage_report_add');
            Route::get('/{id}/{serial}/add', function () {return redirect()->back();});
        });



    });


    //FINANCE ROUTE
     Route::group(['prefix' => 'finance', 'middleware' => 'auth:member'], function () {
        Route::get('/', array('uses' => 'FinanceController@pay_view','as' => 'admin.manage.payment'));
        Route::group(['prefix' => 'payment', 'middleware' => 'auth:member'], function () {
            Route::get('/', array('uses' => 'FinanceController@pay_view','as' => 'admin.manage.payment'));
            Route::post('/search', 'FinanceController@mtics_search');
            Route::get('/ajax-call/{id}', 'FinanceController@mtics_ajax_call');
            Route::get('event/ajax-call/{id}', 'FinanceController@eventPayment_ajax_call');
            //MTICS FUND PAYMENT
            Route::post('/pay-store', 'FinanceController@pay_mtics_store');
            Route::get('/pay-store', function () {return redirect('admin/finance/payment');});
            Route::post('/{id}/delete', 'FinanceController@pay_mtics_delete');
            Route::get('/{id}/delete', function () {return redirect('admin/finance/payment');});
            Route::post('/{id}/event-delete', 'FinanceController@pay_event_delete');
            Route::get('/{id}/event-delete', function () {return redirect('admin/finance/payment');});

            // MANAGE EVENT
            Route::get('/manage-event/{id}', 'FinanceController@eventView');

            //EVENT FUND PAYMENT
            Route::post('/pay-event-store', 'FinanceController@pay_event_store');
            Route::get('/pay-event-store', function () {return redirect('admin/finance/payment');});

            //MTICS RECEIPT
            Route::post('/{id}/mtics-receipt', 'ReportsController@finance_mtics_receipt');
            //EVENT RECEIPT
            Route::post('/{id}/event-receipt', 'ReportsController@finance_event_receipt');
            //SELECTED EVENT RECEIPT (student all payment)
            Route::post('/selected-event-receipt', 'ReportsController@finance_selected_event_receipt');


            //PENALTY PEYMENT
            Route::post('/pay-penalty', 'TaskController@penalty_payment');
            Route::get('/get-penalty-amount', 'TaskController@getPenaltyAmount');
            //EVENT OFFERS PAYMENT
            Route::post('/pay-event-offers', 'EventOfferController@event_offers_payment');


        //MANAGE MTICS FUND PAYMENT
            //ADD MTICS FUND
            Route::post('/mtics-store', 'FinanceController@mtics_store');
            Route::get('/mtics-store', function () {return redirect('admin/finance/payment');});
            //EDIT MTICS FUND
            Route::post('/{id}/mtics-edit', 'FinanceController@mtics_edit');
            Route::get('/{id}/mtics-edit', function () {return redirect('admin/finance/payment');});
            //DELETE MTICS FUND
            Route::post('/{id}/mtics-delete', 'FinanceController@mtics_delete');
            Route::get('/{id}/mtics-delete', function () {return redirect('admin/finance/payment');});
        //EXCESS MONEY FROM EXTERNAL
            Route::post('/excess-money', 'FinanceController@excess_money');
            Route::get('/excess-money', function () {return redirect('admin/finance/payment');});

         });

        Route::get('/manage-mtics-payment', array('uses' => 'FinanceController@manageFund_view','as' => 'admin.manage.fund'));
        Route::post('/manage-mtics-payment', 'FinanceController@manageFund_view');

        //MANEGE-EVENT
        Route::group(['prefix' => 'manage-event', 'middleware' => 'auth:member'], function () {

            Route::get('/{id}', array('uses' => 'FinanceController@event_view','as' => 'admin.manage.event.payment'));

            Route::get('/{event_id}/validate-purchased-amount/{request_id}', 'FinanceController@eventValidation');
            Route::get('/{event_id}/validate/{request_id}/step-1', 'FinanceController@step1');
            Route::get('/{event_id}/validate/{request_id}/step-2', 'FinanceController@step2');
            Route::get('/{event_id}/validate/{request_id}/step-3', 'FinanceController@step3');
            Route::get('/{event_id}/validate/{request_id}/step-3/wait-auditor-approval', 'FinanceController@step3WaitView');
            Route::get('/{event_id}/validate/{request_id}/finish', 'FinanceController@lastStep');
            Route::get('/{event_id}/validate/{request_id}/next', 'FinanceController@next');

        //FINANCE TASK
            Route::get('/start-task/{id}', 'TaskController@start');
            Route::get('/validate/{id}', 'TaskController@forvalidation');



        });

        //MANEGE-EVENT-PAYMENT
        Route::group(['prefix' => 'manage-event-payment', 'middleware' => 'auth:member'], function () {
            //ADD BREAKDOWN
            Route::post('/{id}/store', 'FinanceController@event_add');
            Route::get('/{id}/store', function () {return redirect('admin/finance/manage-event-payment');});
            //EDIT BREAKDOWN
            Route::post('/{id}/edit', 'FinanceController@event_edit');
            Route::get('/{id}/edit', function () {return redirect('admin/finance/manage-event-payment');});
            //SUBMIT BREAKDOWN
            Route::post('/{id}/submit', 'FinanceController@event_submit');
            Route::get('/{id}/submit', function () {return redirect('admin/finance/manage-event-payment');});

        });

        //VIEW REQUESTED MONEY
        Route::group(['prefix' => 'requested-money', 'middleware' => 'auth:member', 'as' => 'finance.request.money'], function () {
            Route::get('/', 'FinanceController@req_money_view');
            //Add Fare
            Route::post('/{id}/add-fare', 'FinanceController@addfare');
            Route::get('/{id}/add-fare', function () {return redirect('admin/finance/requested-money');});
            //Add Meal
            Route::post('/{id}/add-meal', 'FinanceController@addmeal');
            Route::get('/{id}/add-meal', function () {return redirect('admin/finance/requested-money');});
            //Edit est price
            Route::post('/{id}/edit/{task_id}', 'FinanceController@est_price_edit');
            Route::get('/{id}/edit/{task_id}', function () {return redirect('admin/finance/requested-money');});
            //Accept Money Request
            Route::post('/{id}/accept', 'FinanceController@accept');
            Route::get('/{id}/accept', function () {return redirect('admin/finance/requested-money');});
            //Deny Money Request
            Route::post('/{id}/deny', 'FinanceController@deny');
            Route::get('/{id}/deny', function () {return redirect('admin/finance/requested-money');});

            // Validation of Purchased Amount
            Route::get('/validate/{id}', 'PurchasedAmountValidationController@mticsValidation');
            Route::get('/validate/{id}/step-1', 'PurchasedAmountValidationController@step1');
            Route::get('/validate/{id}/step-2', 'PurchasedAmountValidationController@step2');
            Route::get('/validate/{id}/step-3', 'PurchasedAmountValidationController@step3');
            Route::get('/validate/{id}/step-3/wait-auditor-approval', 'PurchasedAmountValidationController@step3WaitView');
            Route::get('/validate/{id}/finish', 'PurchasedAmountValidationController@lastStep');
            Route::get('/validate/{id}/next', 'PurchasedAmountValidationController@next');

        });

        //MTICS FUND
        Route::group(['prefix' => 'mtics-fund', 'middleware' => 'auth:member'], function () {
            Route::get('/', 'FinanceController@mticsfund_view');
        });

        //EVENT FUND
        Route::group(['prefix' => 'event-fund', 'middleware' => 'auth:member'], function () {
           // Route::get('/', 'FinanceController@eventfund_view');
            Route::get('/{id}/manage-payment', array('uses' => 'FinanceController@event_view','as' => 'admin.manage.event.payment'));
            //EDIT BREAKDOWN
            Route::post('/{task_id}/manage-payment/{id}/edit', 'FinanceController@event_edit');
            Route::get('/{task_id}/manage-payment/{id}/edit', function () {return redirect()->back();});
             //ADD BREAKDOWN
            Route::post('/{id}/manage-payment/store', 'FinanceController@event_add');
            Route::get('/{id}/manage-payment/store', function () {return redirect('admin/finance/manage-event-payment');});
             //SUBMIT BREAKDOWN
            Route::post('/{id}/manage-payment/submit', 'FinanceController@event_submit');
            Route::get('/{id}/manage-payment/submit', function () {return redirect('admin/finance/manage-event-payment');});

            //VIEW EVENT FUND
            Route::get('/{id}', 'FinanceController@eventfund_details');

            //EVENT EXPENSES
            //Route::get('/{id}/expenses', 'MticsBudgetController@finance_event_expenses_view');
            //EVENT EXPENSES STORE
            Route::post('/{id}/expenses/store', 'MticsBudgetController@finance_event_expenses_add');
            Route::get('/{id}/expenses/store', function () {return redirect()->back();});
            //EVENT EXPENSES RECEIPT
            Route::post('/{id}/expenses/receipt', 'MticsBudgetController@finance_event_expenses_uploadreceipt');
            Route::get('/{id}/expenses/receipt', function () {return redirect()->back();});

            //EVENT OFFERS EDIT
            Route::post('/{id}/event-offers/{eventoffer_id}/edit', 'EventOffersController@finance_eventoffers_edit');
            Route::get('/{id}/expenses/store', function () {return redirect()->back();});


        });

        //VIEW PURCHASED ITEMS
        Route::group(['prefix' => 'purchased-items', 'middleware' => 'auth:member'], function () {
            //to be deleted Route::get('/', 'FinanceController@purchased_item_view');
           //UPLOAD RECEIPT
            Route::post('/{id}/upload-receipt', 'FinanceController@purchased_item_upload_receipt');
            Route::get('/{id}/upload-receipt', function () {return redirect('admin/finance/purchased-items');});
            //DONE REQUEST
            Route::get('/{id}/done', 'FinanceController@purchased_done');
            //CHANGE RECEIPT
            Route::post('/{id}/change', 'FinanceController@purchased_change');
            Route::get('/{id}/change', function () {return redirect('admin/finance/purchased-items');});
            //REPORT ITEM
            Route::post('/{task_id}/{id}/report', 'FinanceController@purchased_report');
            Route::get('/{task_id}/{id}/report', function () {return redirect('admin/finance/purchased-items');});

        });



       //manage-bank-transaction
        Route::group(['prefix' => 'manage-bank-transaction', 'middleware' => 'auth:member'], function () {
            //MTICS
            Route::group(['prefix' => 'mtics', 'middleware' => 'auth:member'], function () {
            Route::get('/', array('uses' => 'BankTransactionController@mtics_view', 'as' => 'admin.bank.transaction'));
             //MTICS DEPOSIT
            Route::post('/deposit', 'BankTransactionController@mtics_deposit');
            Route::get('/deposit', function () {return redirect('admin/finance/manage-bank-transaction/mtics');});
             //MTICS WITHDRAW
            Route::post('/withdraw', 'BankTransactionController@mtics_withdraw');
            Route::get('/withdraw', function () {return redirect('admin/finance/manage-bank-transaction/mtics');});
             //TRANSFER
            Route::post('/transfer', 'BankTransactionController@transfer');
            Route::get('/transfer', function () {return redirect('admin/finance/manage-bank-transaction/mtics');});

            });

            //EVENT
            Route::group(['prefix' => 'event', 'middleware' => 'auth:member'], function () {
            //EVENT DEPOSIT
            Route::post('/deposit', 'BankTransactionController@event_deposit');
            Route::get('/deposit', function () {return redirect('admin/finance/manage-bank-transaction/event');});
            //EVENT WITHDRAW
            Route::post('/withdraw', 'BankTransactionController@event_withdraw');
            Route::get('/withdraw', function () {return redirect('admin/finance/manage-bank-transaction/event');});
            });
        });

        Route::group(['prefix' => 'manage-mtics-budget', 'middleware' => 'auth:member'], function () {

            Route::get('/', array('uses' => 'MticsBudgetController@finance_budget_view', 'as' => 'admin.manage.budget'));
            Route::post('/create-budget', 'MticsBudgetController@finance_budget_create');
            Route::get('/deposit', function () {return redirect('admin/finance/manage-mtics-budget');});
            Route::post('/{id}/edit', 'MticsBudgetController@finance_budget_edit');
            Route::get('/{id}/edit', function () {return redirect('admin/finance/manage-mtics-budget');});
            Route::post('/{id}/submit', 'MticsBudgetController@finance_budget_submit');
            Route::get('/{id}/submit', function () {return redirect('admin/finance/manage-mtics-budget');});


            Route::group(['prefix' => 'expenses', 'middleware' => 'auth:member'], function () {
            Route::get('/', array('uses' => 'MticsBudgetController@finance_mtics_expenses_view','as' => 'admin.manage.expenses'));
            Route::post('/store', 'MticsBudgetController@finance_mtics_expenses_add');
            Route::get('/store', function () {return redirect('admin/finance/manage-mtics-budget/expenses');});
            //upload receipt for this expenses
            Route::post('/{id}/upload-receipt', 'MticsBudgetController@finance_mtics_expenses_uploadreceipt');
            Route::get('/{id}/upload-receipt', function () {return redirect('admin/finance/manage-mtics-budget/expenses');});


            });
        });

        //FINANCIAL REPORTS

        Route::group(['prefix' => 'financial-reports', 'middleware' => 'auth:member'], function () {
                Route::get('/', array('uses' => 'ReportsController@finance_mtics_view','as' => 'view.financial.reports'));

            Route::group(['prefix' => 'mtics', 'middleware' => 'auth:member'], function () {

                //view month
                Route::post('/view-month', 'ReportsController@finance_mtics_view_month');
                Route::get('/view-month', function () {return redirect('admin/finance/financial-reports');});
                //print
                Route::post('/view-month/print', 'ReportsController@finance_mtics_view_print');
                Route::get('/view-month/print', function () {return redirect('admin/finance/financial-reports');});
                //verify by auditor
                Route::post('/view-month/verify', 'ReportsController@auditor_mtics_verify');
                Route::get('/view-month/verify', function () {return redirect('admin/finance/financial-reports');});
            });

            Route::group(['prefix' => 'event', 'middleware' => 'auth:member'], function () {

                Route::post('/{id}/view-reports', 'ReportsController@finance_event_reports');
                Route::get('/{id}/view-reports', function () {return redirect('admin/finance/financial-reports');});
                //print
                Route::post('/{id}/view-reports/print', 'ReportsController@finance_event_print');
                Route::get('/{id}/view-reports/print', function () {return redirect('admin/finance/financial-reports');});
                //verify by auditor
                Route::post('/view-month/{id}/verify', 'ReportsController@auditor_event_verify');
                Route::get('/view-month/{id}verify', function () {return redirect('admin/finance/financial-reports');});
            });
        });

    });


    //AUDITOR ROUTE
    Route::group(['prefix' => 'auditor', 'middleware' => 'auth:member'], function () {

        Route::get('/', array('uses' => 'ReimburseController@req_reimburse_view','as' => 'reimburse.request'));

        Route::get('/view-reimburse-info/{reimburse_id}', array('uses' => 'ReimburseController@validateReimbursement','as' => 'view.reimburse.request'));

        Route::group(['prefix' => 'manage-event', 'middleware' => 'auth:member'], function () {
            Route::get('/{id}', array('uses' => 'AuditorController@eventView','as' => 'admin.manage.event.payment'));
            Route::get('/{event_id}/validate/{request_id}', 'AuditorController@validateReimbursement');
        });

        Route::group(['prefix' => 'audit-reports', 'middleware' => 'auth:member'], function () {
            Route::group(['prefix' => 'mtics', 'middleware' => 'auth:member'], function () {
                Route::get('/', array('uses' => 'ReportsController@auditor_mtics_view'));
                //view month summary
                Route::post('/view-month', 'ReportsController@auditor_mtics_view_month');
                Route::get('/view-month', function () {return redirect('admin/auditor/audit-reports/mtics');});
                //view month print
                Route::post('/view-month/print', 'ReportsController@auditor_mtics_view_print');
                Route::get('/view-month/print', function () {return redirect('admin/auditor/audit-reports/mtics');});

            });
        });
    });



     // Docu Route
    Route::group(['prefix' => 'docu', 'middleware' => 'auth:member'], function () {
        Route::get('/', array('uses' => 'DocumentationController@view','as' => 'admin.document'));
        Route::post('/', array('uses' => 'DocumentationController@view','as' => 'admin.document'));
        Route::post('/store', array('uses' => 'DocumentationController@store','as' => 'admin.document.store'));
        Route::get('/download/{id}', array('uses' => 'DocumentationController@download'));
        Route::get('/delete/{id}', array('uses' => 'DocumentationController@delete'));

        // bylaws
        Route::get('/bylaws', array('uses' => 'DocumentationController@mticsBylaws','as' => 'admin.document.bylaws'));
        Route::post('/bylaws/update', array('uses' => 'DocumentationController@mticsBylawsUpdate','as' => 'admin.document.bylaws.update'));

        // EVENT
        Route::group(['prefix' => 'manage-event', 'middleware' => 'auth:member'], function () {

        Route::get('/{id}', array('uses' => 'DocumentationController@eventView'));
        Route::get('/approve/{id}', array('uses' => 'DocumentationController@approve'));
        Route::post('/disapprove/{id}', array('uses' => 'DocumentationController@disapprove'));
        Route::post('/store/{id}', array('uses' => 'DocumentationController@event_store','as' => 'admin.eventdocument.store'));
        Route::get('/validate/{id}', array('uses' => 'DocumentationController@validation'));
        //theme upload
        Route::post('/{id}/upload-theme', array('uses' => 'DocumentationController@upload_theme'));
        Route::get('/{id}/upload-theme', function () {return redirect()->back();});

        //Edit Event information
        Route::post('/{id}/edit', array('uses' => 'EventController@event_info_edit'));
        Route::get('/{id}/edit', function () {return redirect()->back();});


        //MANAGE GALLERY
        Route::get('/{id}/gallery', array('uses' => 'DocumentationController@gallery_view'));
        //upload multiple phot
        Route::post('/{id}/upload-photo', array('uses' => 'DocumentationController@upload_photo'));
        Route::get('/{id}/upload-photo', function () {return redirect()->back();});
        //archive photo
        Route::post('/{id}/gallery/{photo_id}/archived', array('uses' => 'DocumentationController@archived_photo'));
        Route::get('/{id}/gallery/{photo_id}/archived', function () {return redirect()->back();});
        //delete photo
        Route::post('/{id}/gallery/{photo_id}/delete', array('uses' => 'DocumentationController@delete_photo'));
        Route::get('/{id}/gallery/{photo_id}/delete', function () {return redirect()->back();});




        //MANAGE ATTENDANCE
        Route::get('/{id}/attendance', array('uses' => 'DocumentationController@attendance_view'));
        //set absent
        Route::post('/{id}/attendance/set-absent', array('uses' => 'DocumentationController@attendance_setabsent'));
        //set present
        Route::post('/{id}/attendance/{absent_id}/set-present', array('uses' => 'DocumentationController@attendance_setpresent'));

        Route::get('/{event_id}/attendance/present/{member_id}', array('uses' => 'DocumentationController@presentAttendee'));


        //MANAGE MEETING
        Route::get('/{id}/meeting', array('uses' => 'DocumentationController@meeting_view'));
        Route::get('/{id}/meeting/{meeting_id}/attendance', array('uses' => 'DocumentationController@meeting_attendance_view'));
        //set absent
        Route::post('/{id}/meeting/{meeting_id}/attendance/set-absent', array('uses' => 'DocumentationController@meeting_attendance_setabsent'));
        //set present
        Route::post('/{id}/meeting/{meeting_id}/attendance/{absent_id}/set-present', array('uses' => 'DocumentationController@meeting_attendance_setpresent'));

        });
      });

      // INTERNAL Route
     Route::group(['prefix' => 'internal', 'middleware' => 'auth:member'], function () {
        Route::get('/', array('uses' => 'InternalController@view','as' => 'admin.internal'));
        Route::post('/', array('uses' => 'InternalController@view','as' => 'admin.internal.post'));
        Route::post('/store', array('uses' => 'InternalController@store','as' => 'admin.internal.store'));
        Route::get('/delete/{id}', array('uses' => 'InternalController@delete'));
        Route::get('/download/{id}', array('uses' => 'InternalController@download'));

        // EVENT
         Route::group(['prefix' => 'manage-event', 'middleware' => 'auth:member'], function () {

            Route::get('/{id}', array('uses' => 'InternalController@eventView'));
            Route::get('/start-task/{id}', array('uses' => 'TaskController@start'));
            Route::get('/validate/{id}', array('uses' => 'InternalController@validateLetter'));
         });


      });


    // Activities route
    Route::group(['prefix' => 'activities', 'middleware' => 'auth:member'], function () {
        Route::get('/', array('uses' => 'ActivitiesController@view','as' => 'admin.activities'));

        Route::get('/manage-event/{id}', array('uses' => 'EventOffersController@eventoffers_view','as' => 'admin.event.offers.view'));
        Route::post('/manage-event/store', array('uses' => 'ActivitiesController@storeEvent','as' => 'admin.activities.store'));
        Route::get('/manage-event/store', function () {return redirect()->back();});
        Route::get('/print', array('uses' => 'ActivitiesController@print_calendar','as' => 'admin.activities.print'));

        //MANAGE EVENT
        Route::group(['prefix' => 'manage-event', 'middleware' => 'auth:member'], function () {
            Route::post('/{id}/add-event-program', array('uses' => 'ActivitiesController@event_program_add'));
            Route::get('/{id}/add-event-program', function () {return redirect()->back();});
        });

        //MANAGE EVENT OFFERS
        Route::group(['prefix' => 'event-offers', 'middleware' => 'auth:member'], function () {
        Route::get('/{id}', array('uses' => 'EventOffersController@eventoffers_view','as' => 'admin.event.offers.view'));
        //ADD EVENT OFFERS
        Route::post('/{id}/store', array('uses' => 'EventOffersController@eventoffers_add','as' => 'admin.event.offers.add'));
        Route::get('/{id}/store', function () {return redirect()->back();});
       //ADD EVENT OFFERS
        Route::post('/{id}/edit/{eventoffer_id}', array('uses' => 'EventOffersController@eventoffers_edit','as' => 'admin.event.offers.edit'));
        Route::get('/{id}/edit/{eventoffer_id}', function () {return redirect()->back();});

        });
    });

    // INFO route
    Route::group(['prefix' => 'info', 'middleware' => 'auth:member'], function () {
        Route::post('/send', array('uses' => 'InformationController@itexmo','as' => 'admin.info.send'));
        Route::post('/', array('uses' => 'InformationController@view','as' => 'admin.info.post'));
        Route::get('/', array('uses' => 'InformationController@view','as' => 'admin.info'));

        Route::group(['prefix' => 'manage-event', 'middleware' => 'auth:member'], function () {
            Route::get('/{id}', array('uses' => 'InformationController@eventView'));
            //post partial event information
            Route::post('/{id}/add', array('uses' => 'InformationController@post_partial_event'));
            Route::get('/{id}/add', function () {return redirect()->back();});
            //editview partial event information
            Route::get('/{id}/{partial_id}/{slug}/edit-view', array('uses' => 'InformationController@post_partial_edit_view'));
             //edit partial event information
            Route::post('/{id}/{partial_id}/{slug}/edit', array('uses' => 'InformationController@post_partial_edit'));
            Route::get('/{id}/{partial_id}/{slug}/edit', function () {return redirect()->back();});
             //delete partial event information
            Route::post('/{id}/{partial_id}/{slug}/delete', array('uses' => 'InformationController@post_partial_delete'));
            Route::get('/{id}/{partial_id}/{slug}/delete', function () {return redirect()->back();});

            //post event full detailes
            Route::post('/{id}/post', array('uses' => 'InformationController@post_event'));
            Route::get('/{id}/post', function () {return redirect()->back();});

        });
      });



  //Adviser
    Route::group(['prefix' => 'adviser', 'middleware' => 'auth:member'], function () {
        Route::get('/', array('uses' => 'BankTransactionController@mtics_banktrans_view','as' => 'admin.adviser.mtics.banktrans'));
    //mtics-bank-transaction-request
      Route::group(['prefix' => 'mtics-bank-transaction-request', 'middleware' => 'auth:member'], function () {
        Route::get('/', array('uses' => 'BankTransactionController@mtics_banktrans_view','as' => 'admin.adviser.mtics.banktrans'));
        //DEPOSIT approved
        Route::post('/{id}/deposit-approved', 'BankTransactionController@mtics_deposit_approved');
        Route::get('/{id}/deposit-approved', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
        //DEPOSIT disapproved
        Route::post('/{id}/deposit-denied', 'BankTransactionController@mtics_deposit_denied');
        Route::get('/{id}/deposit-denied', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
         //DEPOSIT receipt
        Route::post('/{id}/deposit-receipt', 'BankTransactionController@mtics_deposit_receipt');
        Route::get('/{id}/deposit-receipt', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
        //DEPOSIT report
        Route::post('/{id}/deposit-report', 'BankTransactionController@mtics_deposit_report');
        Route::get('/{id}/deposit-report', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
        //DEPOSIT cleared
        Route::post('/{id}/deposit-cleared', 'BankTransactionController@mtics_deposit_cleared');
        Route::get('/{id}/deposit-cleared', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});



        //WITHDRAW approved
        Route::post('/{id}/withdraw-approved', 'BankTransactionController@mtics_withdraw_approved');
        Route::get('/{id}/withdraw-approved', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
        //WITHDRAW disapproved
        Route::post('/{id}/withdraw-denied', 'BankTransactionController@mtics_withdraw_denied');
        Route::get('/{id}/withdraw-denied', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
         //WITHDRAW receipt
        Route::post('/{id}/withdraw-receipt', 'BankTransactionController@mtics_withdraw_receipt');
        Route::get('/{id}/withdraw-receipt', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
         //WITHDRAW report
        Route::post('/{id}/withdraw-report', 'BankTransactionController@mtics_withdraw_report');
        Route::get('/{id}/withdraw-report', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});

        //WITHDRAW cleared
        Route::post('/{id}/withdraw-cleared', 'BankTransactionController@mtics_withdraw_cleared');
        Route::get('/{id}/withdraw-cleared', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});


      });

      //event-bank-transaction-request
      Route::group(['prefix' => 'event-bank-transaction-request', 'middleware' => 'auth:member'], function () {
       /*to be deleted
       Route::get('/', array('uses' => 'BankTransactionController@event_banktrans_view','as' => 'admin.adviser.event.banktrans'));*/

        //DEPOSIT approved
        Route::post('/{id}/deposit-approved', 'BankTransactionController@event_deposit_approved');
        Route::get('/{id}/deposit-approved', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
         //DEPOSIT disapproved
        Route::post('/{id}/deposit-denied', 'BankTransactionController@event_deposit_denied');
        Route::get('/{id}/deposit-denied', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
         //DEPOSIT report
        Route::post('/{id}/deposit-report', 'BankTransactionController@event_deposit_report');
        Route::get('/{id}/deposit-report', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
        //DEPOSIT receipt
        Route::post('/{id}/deposit-receipt', 'BankTransactionController@event_deposit_receipt');
        Route::get('/{id}/deposit-receipt', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
        //WITHDRAW cleared
        Route::post('/{id}/deposit-cleared', 'BankTransactionController@event_deposit_cleared');
        Route::get('/{id}/deposit-cleared', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});


        //WITHDRAW approved
        Route::post('/{id}/withdraw-approved', 'BankTransactionController@event_withdraw_approved');
        Route::get('/{id}/withdraw-approved', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
         //WITHDRAW disapproved
        Route::post('/{id}/withdraw-denied', 'BankTransactionController@event_withdraw_denied');
        Route::get('/{id}/withdraw-denied', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
         //WITHDRAW report
        Route::post('/{id}/withdraw-report', 'BankTransactionController@event_withdraw_report');
        Route::get('/{id}/withdraw-report', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});
         //WITHDRAW receipt
        Route::post('/{id}/withdraw-receipt', 'BankTransactionController@event_withdraw_receipt');
        Route::get('/{id}/withdraw-receipt', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});

        //WITHDRAW cleared
        Route::post('/{id}/withdraw-cleared', 'BankTransactionController@event_withdraw_cleared');
        Route::get('/{id}/withdraw-cleared', function () {return redirect('admin/adviser/mtics-bank-transaction-request');});

      });

    });



// MANAGE FORM
    Route::group(['prefix' => 'manage-form', 'middleware' => 'auth:member'], function () {
        Route::get('/',['uses'=>'FormController@view', 'as'=>'admin.manage.form']);
        Route::get('/answer/{id}',['uses'=>'FormController@answer_form_view', 'as'=>'admin.manage.form.answer']);
        Route::get('/view/{id}',['uses'=>'FormController@form_view', 'as'=>'admin.manage.form.view']);
        Route::get('/store', function () {return view('admin/adminPanel/manage_form');});
        Route::post('/store',['uses'=>'FormController@store']);
        Route::post('/delete/{id}',['uses'=>'FormController@delete']);
        Route::post('/answer/{id}/{items}',['uses'=>'FormAnswerController@store', 'as' => 'admin.form.answer']);
        Route::get('/edit-view/{id}',['uses'=>'FormController@edit_view']);
        Route::post('/edit/{id}',['uses'=>'FormController@edit']);
        Route::get('/view-response/{id}',['uses'=>'FormAnswerController@view']);
        Route::post('/done/{id}',['uses'=>'FormController@done']);
    });

//TURN OVER
    Route::group(['prefix' => 'turn-over', 'middleware' => 'auth:member'], function () {
        Route::get('/',['uses'=>'TurnoverController@view', 'as'=>'admin.turn-over']);
        Route::post('/end','TurnoverController@endBatch');
        Route::get('/end', function () {return redirect('admin/turn-over');});
        Route::post('/view-details','TurnoverController@view_details');
        Route::get('/view-details', function () {return redirect('admin/turn-over');});
        
        Route::post('accomplishment-report','TurnoverController@accomplishment');
        Route::get('accomplishment-report', function () {return redirect('admin/turn-over');});

        Route::post('financial-report/mtics','TurnoverController@financial_mtics');
        Route::get('financial-report/mtics', function () {return redirect('admin/turn-over');});

        Route::post('financial-report/event','TurnoverController@financial_event_view');
        Route::get('financial-report/event', function () {return redirect('admin/turn-over');});


    });



});

Route::get('bylaws/download',['as'=>'admin.additional.feature', 'uses' => 'ByLawsController@download', 'middleware' => 'auth:member']);
Route::get('documents/bylaws/mtics_bylaws.pdf',['as'=>'admin.additional.feature', 'uses' => 'ByLawsController@download', 'middleware' => 'auth:member']);


/*
Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
*/
