<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
     protected $fillable = [
        'member_id','post_category_id','title','body','slug','image','remember_token'
    ];

   public static function getId($model, $table, $value)
   	{
    return $model::where($table, $value)->first()->id;
    }

      public function reply()
    {
        return $this->hasMany(Reply::class);
    }

    public function member(){
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function category(){
        return $this->belongsTo('App\PostCategory', 'post_category_id');
    }

    public function report(){
        return $this->belongsTo('App\ReportPost');
    }

}
