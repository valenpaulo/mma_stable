<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Charts;
use DateTime;

use App\Post;
use App\Member;
use App\MticsEvent;
use App\ActivityLog;
use App\MticsfundPayment;

class DashboardController extends Controller
{
    function index() {
        $active = Member::where('status', 'active')->count();
        $inactive = Member::where('status', 'inactive')->count();
        $deactivated = Member::where('status', 'deactivated')->count();
        $post_count = Post::where('status', 'published')->whereYear('updated_at', date('Y'))->count();

        $events = MticsEvent::where('status', 'on-going')->whereYear('updated_at', date('Y'))->get();

        $logs = ActivityLog::orderBy('id', 'desc')->paginate(5);

        $paidChart = $this->paidUsersChart();
        $paidEventChart = $this->paidUsersEventChart();
        $postChart = $this->postChart();

        $formula = new FormulaMoneyController();
        $collection = $formula->CheckMticsFundPayment();

        return view(
                'dashboard',
                compact(
                        'active', 'inactive', 'deactivated', 'paidChart', 'postChart', 'collection', 'post_count', 'paidEventChart', 'events', 'logs'
                    )
            );
    }

    function paidUsersChart() {
        $active = Member::where('status', 'active')->count();

        $paid = MticsfundPayment::where('paymticsfund_status', 'paid')
                ->whereYear('updated_at', date('Y'))
                ->count();

        $partial = MticsfundPayment::where('paymticsfund_status', 'balance')
                ->whereYear('updated_at', date('Y'))
                ->count();

        $unpaid = $active - $paid;

        $chart = Charts::create('donut', 'highcharts')
            ->title('Paid Members')
            ->labels(['Paid', 'Partial', 'Unpaid'])
            ->values([$paid, $partial, $unpaid])
            ->colors(['teal', 'pink', 'purple'])
            ->backgroundColor('purple')
            ->width('0')
            ->height('250')
            ->responsive(false);

        return $chart;
    }

    function postChart() {
        $current_month = Date('F');
        $previous_month = Date('F', strtotime("last month"));
        $previous_2month = Date('F', strtotime("-2 month"));

        $post_now = Post::where('status', 'published')->whereMonth('updated_at', Date('m'))->count();

        $post_last_month = Post::where('status', 'published')->whereMonth('updated_at', Date('m', strtotime("last month")))->count();

        $post_last_2month = Post::where('status', 'published')->whereMonth('updated_at', Date('m', strtotime("-2 month")))->count();

        $chart = Charts::create('line', 'highcharts')
            ->title('No. of Posts per Month')
            ->elementLabel('No. of Posts per Month')
            ->backgroundColor('orange')
            ->labels([$previous_2month, $previous_month, $current_month])
            ->values([$post_last_2month, $post_last_month, $post_now])
            ->colors(['orange'])
            ->width('0')
            ->height('250');

        return $chart;
    }

    function paidUsersEventChart() {
        $active = Member::where('status', 'active')->count();

        $paid = MticsfundPayment::where('paymticsfund_status', 'paid')
                ->whereYear('updated_at', date('Y'))
                ->count();

        $partial = MticsfundPayment::where('paymticsfund_status', 'balance')
                ->whereYear('updated_at', date('Y'))
                ->count();

        $unpaid = $active - $paid;

        $chart = Charts::create('donut', 'highcharts')
            ->title('Paid Members')
            ->labels(['Paid', 'Partial', 'Unpaid'])
            ->values([$paid, $partial, $unpaid])
            ->colors(['red', 'yellow', 'blue'])
            ->backgroundColor('blue')
            ->width('0')
            ->height('250')
            ->responsive(false);

        return $chart;
    }
}
