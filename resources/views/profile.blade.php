@extends('layout/layout')

@section('title')

Member's Profile

@endsection

@section('content')
<style type="text/css">

  .side {
    background-color: #F8F8F8;
    position: fixed;
    overflow-y: scroll;
    bottom: 0;
  }

  .log {
    position: fixed;
  }

  @media only screen and (max-width: 1365px) {
    .main {
      margin-top: 60px;
    }

    .side {
      top: 54px;
    }
  }

  @media only screen and (max-width: 1365px) {
    .log {
      width: 220px;
    }
  }

  @media (min-width: 1366px) {
    .main {
      margin-top: 105px;
    }

    .side {
      top: 105px;
    }

    .log {
      width: 300px;
    }
  }
</style>

<div class="w3-container-fluid main">

<div class="row">

<!-- LEFT GRID -->
<div class="col-lg-2 col-md-2">
  <div class="w3-card-4 w3-blue-gray col-lg-2 col-md-2 w3-hide-small w3-hide-medium side" id="information">

    <div class="w3-container-fluid">

      <div class="row w3-center">
        <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.Auth::user()->avatar) : asset('images/'.Auth::user()->avatar)}}" class="w3-margin-bottom " style="width: 100%;">

        <h4 data-toggle="tooltip" title="Edit Information" data-placement="bottom">
          {{Auth::user()->first_name}} {{Auth::user()->last_name}}
        </h4>

        <a href="" data-toggle="modal" data-target="#edit" class="w3-text-white" style="outline: 0px">
          Edit Information<i class="fa fa-pencil fa-fw w3-opacity"></i>
        </a>

        @foreach(Auth::user()->role as $role)
          <h5>{{$role->display_name}}</h5>
        @endforeach
      </div>

      <hr>

      @if ($member_section)
      <p class="w3-text-white"><i class="fa fa-id-badge fa-fw w3-margin-right w3-large w3-text-light-green"></i>{{Auth::user()->id_num}}</p>
      <p class="w3-text-white"><i class="fa fa-users fa-fw w3-margin-right w3-large w3-text-light-green"></i>{{ $member_section->section_code }}</p>
      @endif
      <p class="w3-text-white"><i class="fa fa-tag fa-fw w3-margin-right w3-large w3-text-light-green"></i>{{ Auth::user()->status}}</p>
     <!--  <p class="w3-text-white" id="payment"><i class="fa fa-money fa-fw w3-margin-right w3-large w3-text-light-green"></i></p>
 -->
      <br>
      <hr>
      <h5 class="w3-center">
        Contact Information
      </h5>
      <hr>

      <p class="w3-text-white"><i class="fa fa-mobile fa-fw w3-margin-right w3-large w3-text-light-green"></i>{{ Auth::user()->mobile_no}}</p>
      <p class="w3-text-white"><i class="fa fa-envelope-o fa-fw w3-margin-right w3-large w3-text-light-green"></i>{{ Auth::user()->email}}</p>

      <br>
      <hr>
      <h5 class="w3-center">
        Personal Information
      </h5>
      <hr>

      <p class="w3-text-white"><i class="fa fa-info-circle fa-fw w3-margin-right w3-large w3-text-light-green "></i>{{Auth::user()->age ? Auth::user()->age . ' years old' : '' }} </p>
      <p class="w3-text-white"><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-large w3-text-light-green"></i>{{ Auth::user()->birthday}}</p>
      <p class="w3-text-white"><i class="fa fa-address-card-o fa-fw w3-margin-right w3-large w3-text-light-green"></i>{{ Auth::user()->address}}</p>
      <p class="w3-text-white"><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-light-green"></i>{{ Auth::user()->work}}</p>

      <br>
      <hr>
      <h5 class="w3-center">
        In case of emergency
      </h5>
      <hr>

      <p class="w3-text-white"><i class="fa fa-user fa-fw w3-margin-right w3-large w3-text-light-green"></i>{{ Auth::user()->guardian_name}}</p>
      <p class="w3-text-white"><i class="fa fa-mobile fa-fw w3-margin-right w3-large w3-text-light-green"></i>{{ Auth::user()->guardian_mobile_no}}</p>
      <br>

    </div>

  </div>
<!-- END LEFT GRID -->
</div>

<!--  EDIT MEMBER INFO -->
<div class="modal fade" id="edit" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-md" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
<h4 class="modal-title w3-text-gray" id="myModalLabel">Profile</h4>
</div>
<form action="{{ url('member/profile/edit/'.Auth::user()->id) }}" method="POST" enctype="multipart/form-data">
<div class="modal-body">
  <div class="row">
    <div class="form-group w3-margin-left">
      <label id="avatar-file-name" for="avatar" style="cursor: pointer;" title="Edit Picture"><i class="fa fa-file-image-o fa-fw w3-text-deep-orange w3-large w3-left" data-toggle="tooltip" title="Change Photo" data-placement="right"></i></label>
        <input type="file" name="avatar" id="avatar" style="opacity: 0; position: absolute; z-index: -1;" />
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </div>
  </div>

  <div class="row">
    <div class="form-group w3-center">
        <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.Auth::user()->avatar) : asset('images/'.Auth::user()->avatar)}}" style="width: 50%" id="profileImageContainer">
    </div>
    <br>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>First Name: </label>
        <input type="text" name="first_name" id="first_name" tabindex="1" class="form-control" required placeholder="First Name" value="{{Auth::user()->first_name}}">
      </div>

      <div class="form-group">
       <label>Last Name: </label>
        <input type="text" name="last_name" id="last_name" tabindex="1" class="form-control" required placeholder="Last Name" value="{{Auth::user()->last_name}}">
      </div>
      @if(Auth::user()->section->section_code != 'Faculty')
    <!-- start student div -->
    <div class="student-edit">
    <div class="form-group{{ $errors->has('id_num') ? ' has-error' : '' }}">
      <label>Student Id:</label>
      <input type="text" class="form-control" id="id_num" name="id_num" placeholder="Student ID" value="{{Auth::user()->id_num}}">
    </div>

    <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
      <label>Year:</label>
        <input type="text" name="" value="{{Auth::user()->year->year_desc}}" disabled class="form-control">
    </div>

    <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
      <label>Section:</label>
        <input type="text" name="" value="{{Auth::user()->section->section_code}}" disabled class="form-control">
    </div>

    <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
      <label>Type:</label>
      <input type="text" name="" value="{{Auth::user()->type}}" disabled class="form-control">
    </div>

    </div>
    @endif

<div class="form-group">
      <label>Username: </label>
        <input type="text" name="username" id="username" tabindex="1" class="form-control" required placeholder="Username" value="{{Auth::user()->username}}">
      </div>
      <div class="form-group">
        <label>Password: </label>
         <input id="password" type="password" tabindex="1" class="form-control" name="password" placeholder="Password">
      </div>

      @if(Auth::user()->section->section_code == 'Faculty')

       <div class="form-group">
       <label>Email: </label>
        <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->email}}">
      </div>
      @endif
</div>

    <div class="col-md-6">
      @if(Auth::user()->section->section_code != 'Faculty')

       <div class="form-group">
       <label>Email: </label>
        <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->email}}">
      </div>
      @endif

      <div class="form-group">
        <label>Mobile No.: </label>
        <input type="text" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->mobile_no}}">
      </div>

      <div class="form-group">
       <label>Age: </label>
        <input type="number" min="0" name="age" id="age" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->age}}">
      </div>

      <div class="form-group">
        <label for="bday">Birthday:</label>
        <input type="date" class="form-control" id="birthday" name="birthday" value="{{Auth::user()->birthday}}">
      </div>

      <div class="form-group">
       <label>Address: </label>
        <input type="text" name="address" id="address" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->address}}">
      </div>

      <div class="form-group">
       <label>Work: </label>
        <input type="text" name="work" id="work" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->work}}">
      </div>
      @if(Auth::user()->section->section_code != 'Faculty')
      <div class="form-group">
       <label>Guardian Name: </label>
        <input type="text" name="guardian_name" id="guardian_name" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->guardian_name}}">
      </div>


      <div class="form-group">
       <label>Guardian Mobile No.: </label>
        <input type="text" name="guardian_mobile_no" id="guardian_mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->guardian_mobile_no}}">
      </div>
      @endif
    </div>
  </div>
</div>
<div class="modal-footer">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
</div>
</form>
</div>
</div>
</div>
<!-- END EDIT MEMBER INFO -->

<!-- CENTER GRID -->
<div class="col-lg-7 col-md-7">

<div class="container-fluid w3-margin-right">
  <div class="row">
    <div class="col-sm-8"></div>
    <div class="col-sm-4">
    @if($errors->any())
      @foreach ($errors->all() as $error)
      <div class="alert alert-danger alert-dismissable fade in w3-right col-sm-12" id="success-alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>
      {{$error}}
      </strong>
      </div>
      @endforeach
    @endif
    </div>
  </div>
</div>
@yield('middle')
 <!-- END CENTER GRID -->
</div>

<!-- RIGHT GRID -->
<div class="col-lg-2 col-md-2">
<div class="panel panel-danger w3-margin-top w3-hide-small w3-hide-medium log">
  <div class="panel-heading">
    <h4 class="w3-large w3-text-theme" align="center"><i class="fa fa-tasks fa-fw"></i> Activity Logs</h4>
  </div>

  <div class="panel-body">
  <div class="w3-container">
    <div class="row">
      @foreach($logs as $log)
        <p>
          <i class="fa fa-map-pin fa-fw"></i>
          {{$log->log}}
        </p>

        <p>
          <i class="fa fa-angle-double-right fa-fw"></i>
          {{$log->created_at}}
        </p>

        @if(!$loop->last)
        <hr>
        @endif
      @endforeach
    </div>
    <br>
    <div class="row" align="center">
      <p>Read More ...</p>
    </div>
  </div>
  </div>
</div>
</div>
<!-- END RIGHT GRID -->
</div>

</div>

<script>
  var avatar_input = document.getElementById( 'avatar' );
  var avatar_name = document.getElementById( 'avatar-file-name' );

  avatar_input.addEventListener('change', showFileName);

  function showFileName( event ) {
    var input = event.srcElement;
    var fileName = avatar_input.files[0].name;
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#profileImageContainer").attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);

    avatar_name.textContent = fileName;
  };
</script>

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>

<script type="text/javascript">
  $(window).on('load', function() {
    if("{{session('success')}}" != ""){
      swal("{{session('success')}}", "Congratulations!", "success")
    }
  });
</script>

<script>
$( document ).ready(function() {
  var xhttp;

  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("payment").innerHTML = '<i class="fa fa-money fa-fw w3-margin-right w3-large w3-text-light-green"></i>' + 'Php ' + this.responseText;
    }
  };
  xhttp.open("GET", "{{url('admin/finance/payment/ajax-call')}}/" + {{Auth::user()->id}}, true);
  xhttp.send();
});
</script>

@endsection
