@extends('layout/layout')

@section('title')

Profile

@endsection

@section('content')


<!-- The Grid -->
<div class="w3-row-padding w3-margin-top">

<!-- Left Column -->
<div class="w3-col m3">
  <div class="w3-white w3-card-4">
  <!-- errors -->
    <div class="w3-display-container w3-center">
      <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.Auth::user()->avatar) : asset('images/'.Auth::user()->avatar) }}" style="width:100%" alt="Avatar">
    </div>

    <div class="w3-container">
      <div class=" col-sm-10">
      <a href="" data-toggle="modal" data-target="#edit">
        <p class="w3-large w3-text-black"><b>{{ Auth::user()->first_name}} {{ Auth::user()->last_name}} </b><i class="fa fa-pencil fa-fw w3-margin-right w3-large w3-opacity w3-text-gray"></i></p>
      </a>
      </div>
    </div>

    <div class="row">
      @if ($member_section)
      <p class="w3-text-gray"><i class="fa fa-users fa-fw w3-margin-right w3-large w3-text-teal"></i>{{ $member_section->section_code }}</p>
      @endif
      <p class="w3-text-gray"><i class="fa fa-tag fa-fw w3-margin-right w3-large w3-text-teal"></i>{{ Auth::user()->status}}</p>
      <p class="w3-text-gray"><i class="fa fa-money fa-fw w3-margin-right w3-large w3-text-teal"></i>0</p>
      <hr>

      <p class="w3-large"><b>Contact Information</b></p>
      <p class="w3-text-gray"><i class="fa fa-mobile fa-fw w3-margin-right w3-large w3-text-teal"></i>{{ Auth::user()->mobile_no}}</p>
      <p class="w3-text-gray"><i class="fa fa-envelope-o fa-fw w3-margin-right w3-large w3-text-teal"></i>{{ Auth::user()->email}}</p>
      <hr>

      <p class="w3-large w3-text-theme"><b>Personal Information</b></p>

      <p class="w3-text-gray"><i class="fa fa-info-circle fa-fw w3-margin-right w3-large w3-text-teal "></i>{{ Auth::user()->age}} years old</p>
      <p class="w3-text-gray"><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-large w3-text-teal"></i>{{ Auth::user()->birthday}}</p>
      <p class="w3-text-gray"><i class="fa fa-address-card-o fa-fw w3-margin-right w3-large w3-text-teal"></i>{{ Auth::user()->address}}</p>
      <p class="w3-text-gray"><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>{{ Auth::user()->work}}</p>
      <hr>

      <p class="w3-large w3-text-theme"><b>In case of emergency</b></p>
      <p class="w3-text-gray"><i class="fa fa-user fa-fw w3-margin-right w3-large w3-text-teal"></i>{{ Auth::user()->guardian_name}}</p>
      <p class="w3-text-gray"><i class="fa fa-mobile fa-fw w3-margin-right w3-large w3-text-teal"></i>{{ Auth::user()->guardian_mobile_no}}</p>
      <br>
    </div>
  </div><br>

<!-- End Left Column -->
</div>
<div class="w3-col m7">
@if($errors)
@foreach ($errors->all() as $error)
  <div class="alert alert-danger alert-dismissable fade in w3-center" id="success-alert">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>{{ $errors->first() }}</strong>
  </div>
@endforeach
@endif

<div class="container w3-col m12 w3-center">
  <table class="table">
    <tbody>
      <tr>
        <td>
        <a href="{{route('profile.post')}}">
        <i
          @if(Route::current()->getName() == 'profile.post')
          class="fa fa-pencil-square-o w3-xlarge w3-text-teal w3-hover-opacity w3-hover-text-teal"
          @else
          class="fa fa-pencil-square-o w3-xlarge w3-text-gray w3-hover-opacity w3-hover-text-teal"
          @endif
          >
        </i>
        </a>
        </td>
        <td>
          <a href="{{route('profile.notif')}}">
          <i
          @if(Route::current()->getName() == 'profile.notif')
          class="fa fa-bell-o w3-xlarge w3-text-amber w3-hover-opacity w3-hover-text-amber"
          @else
          class="fa fa-bell-o w3-xlarge w3-text-gray w3-hover-opacity w3-hover-text-amber"
          @endif
          ></i>
          </a>
        </td>
        <td>
          <a href="{{route('profile.actlog')}}">
          <i
          @if(Route::current()->getName() == 'profile.actlog')
          class="fa fa-tasks w3-xlarge w3-text-red w3-hover-opacity w3-hover-text-red"
          @else
          class="fa fa-tasks w3-xlarge w3-text-gray w3-hover-opacity w3-hover-text-red"
          @endif
          ></i>
          </a>
        </td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>
@yield('middle')
<!-- End Middle Column -->
</div>


<!-- Right Column -->
<div class="w3-col m2">
<div class="w3-card w3-round w3-white">
  <div class="w3-container">
  <br>
    <p class="w3-large w3-text-theme w3-center"><i class="fa fa-tasks fa-fw w3-text-red"></i><b> Activity Logs</b></p>
    <p><i class="fa fa-check fa-fw w3-text-green"></i> Holiday tomorrow! yahoo!</p>
    <p><i class="fa fa-check fa-fw w3-text-green"></i> Holiday tomorrow! yahoo!</p>
    <p><i class="fa fa-check fa-fw w3-text-green"></i> Holiday tomorrow! yahoo!</p>
    <p><i class="fa fa-check fa-fw w3-text-green"></i> Holiday tomorrow! yahoo!</p>
    <p><i class="fa fa-check fa-fw w3-text-green"></i> Holiday tomorrow! yahoo!</p>
    <p><i class="fa fa-check fa-fw w3-text-green"></i> Holiday tomorrow! yahoo!</p>
    <p><i class="fa fa-check fa-fw w3-text-green"></i> Holiday tomorrow! yahoo!</p>
    <p><i class="fa fa-check fa-fw w3-text-green"></i> Holiday tomorrow! yahoo!</p>
    <p><i class="fa fa-check fa-fw w3-text-green"></i> Holiday tomorrow! yahoo!</p>
    <p><i class="fa fa-check fa-fw w3-text-green"></i> Holiday tomorrow! yahoo!</p>
    <div class="w3-center">
        <a href="{{ route('profile.actlog') }}" class="w3-text-gray">More...</a>
    </div>
    <br>
  </div>
</div>
<br>

<!-- End Right Column -->
</div>

<!-- End Grid -->
</div>


<!--  edit member info -->
         <div class="modal fade" id="edit" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-md" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
<h4 class="modal-title w3-text-gray" id="myModalLabel">Profile</h4>
</div>
<form action="{{ url('member/profile/edit/'.Auth::user()->id) }}" method="POST" enctype="multipart/form-data">
<div class="modal-body">
  <div class="row">
    <div class="form-group w3-margin-left">
      <label id="avatar-file-name" for="avatar" style="cursor: pointer;" title="Edit Picture"><i class="fa fa-file-image-o fa-fw w3-text-green w3-large w3-left"></i></label>
        <input type="file" name="avatar" id="avatar" style="opacity: 0; position: absolute; z-index: -1;" />
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </div>
  </div>

  <div class="row">
    <div class="form-group w3-center">
        <img src="{{asset('public/images/'.Auth::user()->avatar)}}" style="width: 50%">
    </div>
    <br>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>First Name: </label>
        <input type="text" name="first_name" id="first_name" tabindex="1" class="form-control" required placeholder="First Name" value="{{Auth::user()->first_name}}">
      </div>

      <div class="form-group">
       <label>Last Name: </label>
        <input type="text" name="last_name" id="last_name" tabindex="1" class="form-control" required placeholder="Last Name" value="{{Auth::user()->last_name}}">
      </div>
      @if(Auth::user()->section->section_code != 'Faculty')
    <!-- start student div -->
    <div class="student-edit">
    <div class="form-group{{ $errors->has('id_num') ? ' has-error' : '' }}">
      <label>Student Id:</label>
      <input type="text" class="form-control" id="id_num" name="id_num" placeholder="Student ID" disabled value="{{Auth::user()->id_num}}">
    </div>

    <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
      <label>Year:</label>
        <input type="text" name="" value="{{Auth::user()->year->year_desc}}" disabled class="form-control">
    </div>

    <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
      <label>Section:</label>
        <input type="text" name="" value="{{Auth::user()->section->section_code}}" disabled class="form-control">
    </div>

    <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
      <label>Type:</label>
      <input type="text" name="" value="{{Auth::user()->type}}" disabled class="form-control">
    </div>

    </div>
    @endif

<div class="form-group">
      <label>Username: </label>
        <input type="text" name="username" id="username" tabindex="1" class="form-control" required placeholder="Username" value="{{Auth::user()->username}}">
      </div>
      <div class="form-group">
        <label>Password: </label>
         <input id="password" type="password" tabindex="1" class="form-control" name="password" placeholder="Password">
      </div>

      @if(Auth::user()->section->section_code == 'Faculty')

       <div class="form-group">
       <label>Email: </label>
        <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->email}}">
      </div>
      @endif
</div>

    <div class="col-md-6">
      @if(Auth::user()->section->section_code != 'Faculty')

       <div class="form-group">
       <label>Email: </label>
        <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->email}}">
      </div>
      @endif

      <div class="form-group">
        <label>Mobile No.: </label>
        <input type="text" name="mobile_no" id="mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->mobile_no}}">
      </div>

      <div class="form-group">
       <label>Age: </label>
        <input type="number" min="0" name="age" id="age" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->age}}">
      </div>

      <div class="form-group">
        <label for="bday">Birthday:</label>
        <input type="date" class="form-control" id="birthday" name="birthday" value="{{Auth::user()->birthday}}">
      </div>

      <div class="form-group">
       <label>Address: </label>
        <input type="text" name="address" id="address" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->address}}">
      </div>

      <div class="form-group">
       <label>Work: </label>
        <input type="text" name="work" id="work" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->work}}">
      </div>
      @if(Auth::user()->section->section_code != 'Faculty')
      <div class="form-group">
       <label>Guardian Name: </label>
        <input type="text" name="guardian_name" id="guardian_name" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->guardian_name}}">
      </div>


      <div class="form-group">
       <label>Guardian Mobile No.: </label>
        <input type="text" name="guardian_mobile_no" id="guardian_mobile_no" tabindex="1" class="form-control" placeholder="optional" value="{{Auth::user()->guardian_mobile_no}}">
      </div>
      @endif
    </div>
  </div>
</div>
<div class="modal-footer">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<button type="submit" class="btn btn-default w3-orange w3-text-white"><i class="fa fa-pencil"></i> Edit</button>
</div>
</form>
</div>
</div>
</div>

<script>
  var avatar_input = document.getElementById( 'avatar' );
  var avatar_name = document.getElementById( 'avatar-file-name' );

  avatar_input.addEventListener('change', showFileName);

  function showFileName( event ) {

    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = avatar_input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    avatar_name.textContent = fileName;
  };

  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);

</script>
@endsection
