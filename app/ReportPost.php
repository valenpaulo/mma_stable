<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportPost extends Model
{
    protected $table = 'report_posts';
     protected $fillable = [
        'member_id','post_id','reason'
    ];

    public function member(){
        return $this->belongsTo('App\Member', 'member_id');
    }

     public function post(){
        return $this->belongsTo('App\Post', 'post_id');
    }
}
