<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsBudgetExpCat extends Model
{
      protected $table = 'mtics_budget_exp_cats';
	protected $fillable = [
       'name'
   ];

   public function breakdowns()
    {
        return $this->hasMany(MticsBudgetBreakdown::class);
    }

}
