<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPresentAttendee extends Model
{
    protected $fillable = [
        'event_id', 'member_id', 'admin_id', 'role_id',
    ];
}
