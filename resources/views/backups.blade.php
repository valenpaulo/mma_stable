@extends('layout/control_panel')

@section('title')
Database Back up
@endsection

@section('middle')
<div class="w3-container w3-margin">


<section class="content">
<!-- Exportable Table -->


<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    DATABASE BACKUP
                </h2>
            </div>
            <div class="body">

           <!--  <a id="create-new-backup-button" href="{{ url('backup/create') }}" type="button" class="btn bg-orange btn-circle-lg waves-effect waves-circle waves-float">
           <i class="material-icons" data-toggle="tooltip" data-placement="top" title="System Backup">backup</i></a> &nbsp;
 -->
            <a id="create-new-backup-button" href="{{ url('backup/createdb') }}" type="button" class="btn bg-lime btn-circle-lg waves-effect waves-circle waves-float"><i class="material-icons" data-toggle="tooltip" data-placement="top" title="Database Backup">Back Up</i></a>

        <br>

        <div class="row">
            <div class="col-md-12">
                <section class="panel" style="margin-bottom: 0;">
                    <div class="panel-heading tab-bg-dark-navy-blue">

                        <span class="tools pull-right hidden-phone">
                            <span class="wht-color">
                              <!--   Full <span class="badge bg-success">{{ $count['full'] }}</span> |
                                Site Only <span class="badge bg-warning">{{ $count['site'] }}</span> | -->
                                Database Backup <span class="badge bg-info">{{ $count['db'] }}</span>
                            </span>
                        </span>
                    </div>
                </section>
                <section class="panel">
                    <div class="panel-body">
                        <div class="adv-table">


            @if (count($backups))

                <table class="table table-stripped">
                    <thead>
                    <tr>
                        <th>File</th>
                        <th class="hidden-phone">Size</th>
                        <th class="hidden-phone">Date</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($backups as $backup)
                        <tr>
                            <td>{{ $backup['file_name'] }}</td>
                            <td  class="hidden-phone">{{ ($backup['file_size']) }}</td>
                            <td  class="hidden-phone">
                                {{ $backup['last_modified'] }}
                            </td>
                            <td>
                                {{-- {{ diffTimeStamp($backup['last_modified']) }} --}}
                            </td>
                            <td class="text-right">
                                <a type="button" class="btn bg-indigo waves-effect"
                                   href="{{ url('backup/download/'.$backup['file_name']) }}"><i
                                        class="material-icons" data-toggle="tooltip" data-placement="top" title="Download">Download</i></a>
                                        @if (strstr($backup['file_name'], 'db'))
                                        <a type="button" class="btn bg-blue-grey waves-effect"
                                        href="{{ url('backup/restoredb', $backup['file_name']) }}"><i class="material-icons" data-toggle="tooltip" data-placement="top" title="Restore Database">Restore</i></a>
                                        @endif
                                <a type="button" class="btn bg-red waves-effect" data-button-type="delete"
                                   href="{{ url('backup/delete/'.$backup['file_name']) }}"><i class="material-icons" data-toggle="tooltip" data-placement="top" title="Delete">Delete Permanently</i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="well">
                    <h4>There are no backups</h4>
                </div>
            @endif
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


@endsection
