<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipPurchaseReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('event_purchase_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->integer('event_purchase_list_id');
            $table->enum('reportedby',['finance','logistics']);
            $table->string('report_name');
            $table->longtext('report_reason');
            $table->longtext('external_explanation');
            $table->enum('report_category',['minor','critical'])->default('minor');
            $table->longtext('solution_fr_critical')->nullable();
            $table->timestamps();
        });    

         Schema::create('receipt_eventitems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('receipt_id');
            $table->integer('event_purchase_list_id'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
