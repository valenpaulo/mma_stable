@extends('layout/control_panel')

@section('title')
Manage Member
@endsection

@section('middle')

<div class="w3-container w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-calendar-check-o fa-fw w3-xxlarge"></i>
      <strong>Manage Attendance</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="attendanceTable">
          <thead>
            <tr>
             <th class="w3-center">Student ID</th>
             <th class="w3-center">First Name</th>
             <th class="w3-center">Last Name</th>
             <th class="w3-center">Year</th>
             <th class="w3-center">Course</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($members as $member)
          @foreach($event_participants as $event_participant)
          @if($event_participant->year_id == $member->year_id and $event_participant->course_id == $member->section->course_id)

            <tr>
             <td class="w3-center">{{$member->id_num}}</td>
             <td class="w3-center">{{$member->first_name}}</td>
             <td class="w3-center">{{$member->last_name}}</td>
             <td class="w3-center">{{$event_participant->year->year_desc}}</td>
             <td class="w3-center">{{$event_participant->course->course_code}}</td>
             <td class="w3-center">
              @if(is_null($member->attendance))
               <label><input type="checkbox" value="{{$member->id}}" name="member_id[]" id="attendanceMarker-{{$member->id}}" onchange="manageAttendance({{$event->id}}, {{$member->id}})"></label>
              @else
             <label><input type="checkbox" value="{{$member->id}}" name="member_id[]" id="attendanceMarker-{{$member->id}}" onchange="manageAttendance({{$event->id}}, {{$member->id}})" checked disaabled></label>
              @endif
             </td>
            </tr>

          @endif
          @endforeach
          @endforeach
         </tbody>
        </table>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#attendanceTable').DataTable();
} );
</script>


<script>
function manageAttendance(eid, mid) {
  var xhttp;
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      $("#attendanceMarker-" + mid).attr("disabled", true);
    }
  };
  xhttp.open("GET", "{{url('admin/docu/manage-event')}}/" + eid + "/attendance/present/" + mid, true);
  xhttp.send();
}
</script>
@endsection
