@extends('layout/layout')

@section('title')
Settings
@endsection

@section('content')
<div class="container" style="margin-top: 120px">
    <div class="row">
        <h3>
            <i class="fa fa-gears fa-fw w3-xxlarge"></i>
            <strong>Settings</strong>
        </h3>
    </div>

    <hr>

    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.banner')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-flag-o fa-fw w3-xxlarge"></i>
                            <strong>Manage Banner</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-lg-3"></div>

        <div class="col-lg-6 col-lg-6">
            <a href="{{route('admin.filter.words')}}" class="w3-text-black">
                <div class="w3-card-4 w3-teal w3-margin w3-padding-large w3-hover-opacity">
                    <div class="w3-container">
                        <div class="row">
                            <i class="fa fa-language fa-fw w3-xxlarge"></i>
                            <strong>Manage Profanity</strong>
                            <span class="w3-right"><i class="fa fa-external-link-square fa-fw"></i></span>
                        </div>
                        <hr>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-lg-3"></div>
    </div>
</div>
@endsection
