<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;
use App\InventoryCategory;
use App\EquipInventory;
use App\ArchivedInventory;
use App\Member;
use App\Borrower;
use App\BorrowerInventory;
use App\BorrowinvEquip;
use App\Section;
use App\Task;
use App\Role;
use App\MticsPurchaseList;
use App\MticsPurchaseEquipList;
use App\EventPurchaseList;
use App\EventPurchaseEquipList;
use App\RoleMember;
use App\MticsPurchaseReport;
use App\EventPurchaseReport;
use App\EquipRepairReport;
use App\MticsEvent;
use App\Penalty;
use App\EventOffer;
use App\EventOfferMember;
use App\Officerterm;
use App\Notifications\SystemNotification;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\FinanceController;

use App\Events\SystemEvent;
use Illuminate\Support\Facades\Event;


class LogisticsController extends Controller
{


//INVENTORY INVENTORY INVENTORY INVENTORY INVENTORY
 	public function inv_view()
 	{
 		if(!Gate::allows('logistics-only'))
        {
        return redirect('/');
        }
 		$inventories = Inventory::with('Inventory_Category')->get();
 		$inv_categories = InventoryCategory::all();

 		$inv_equips = EquipInventory::where('status','!=','archive')->get();
 		$cat_equip_id = Inventory::getId('App\InventoryCategory','inv_cat_name','equip');

        $term = Officerterm::where('status','on-going')->first();

        $eventoffers = EventOfferMember::where('item_status','pending')->where('updated_at','>=',date($term->created_at))->get();
 		return view('officers/inventory',compact('inventories','inv_categories','inv_equips','cat_equip_id','eventoffers'));
 	}

 	public function inv_add(Request $request)
 	{
         $this->validate($request, [
            'inv_name' => 'required|max:255',
        ]);

 		if(Inventory::where('inv_name', $request->inv_name)->get()->isEmpty())
 		{
            if ($request->category == 'Equipment'){

                $this->validate($request, [
                'brand_name' => 'required',
                'serial' => 'required',
                'specs' => 'required',
                ]);

                $inv_cat_id = Inventory::getId('App\InventoryCategory','inv_cat_displayname',$request->category);
                $inventory = Inventory::create([
                'inv_name' => $request->inv_name,
                'inv_quantity' => 1,
                'inventory_category_id' => $inv_cat_id,
                ]);

                EquipInventory::create([
                'inventory_id' => $inventory->id,
                'brand_name' => $request->brand_name,
                'serial_num' => $request->serial,
                'specs' => $request->specs,
                ]);

            } else {

            $this->validate($request, [
                'inv_quantity' => 'required',
            ]);

 		 	$inv_cat_id = Inventory::getId('App\InventoryCategory','inv_cat_displayname',$request->category);
	 		Inventory::create([
	            'inv_name' => $request->inv_name,
	            'inv_quantity' => $request->inv_quantity,
	            'inventory_category_id' => $inv_cat_id,

	        ]);
            }
 		 }
 		 else
 		 {
            if ($request->category == 'Equipment'){

                $this->validate($request, [
                'brand_name' => 'required',
                'serial' => 'required',
                'specs' => 'required',
                ]);

                $inv_id = Inventory::getId('App\Inventory','inv_name',$request->inv_name);
                $inv = Inventory::find($inv_id);
                $invTotalquan = $inv->inv_quantity + 1;

                $inventory = Inventory::where('id', $inv_id)->update(array('inv_quantity' => $invTotalquan));

                EquipInventory::create([
                'inventory_id' => $inv_id,
                'brand_name' => $request->brand_name,
                'serial_num' => $request->serial,
                'specs' => $request->specs,
                ]);

            } else {

            $this->validate($request, [
                'inv_quantity' => 'required',
            ]);

            $inv_id = Inventory::getId('App\Inventory','inv_name',$request->inv_name);
            $inv = Inventory::find($inv_id);
            $invTotalquan = $inv->inv_quantity + $request->inv_quantity;

            Inventory::where('id', $inv_id)->update(array('inv_quantity' => $invTotalquan));

            }

            Inventory::where('id', $inv_id)->update(array('sync_status' => "0"));

 		 }

        Event::fire(new SystemEvent(auth::id(), 'Item Added.'));

 		return redirect('admin/logistics/inventory')->with('success', 'Item Added');
 	}

	public function inv_edit(Request $request, $id)
 	{
 		 $this->validate($request, [
        'inv_name' => 'required|max:255',
        ]);

	 	if(!InventoryCategory::where('inv_cat_displayname', $request->category)->get()->isEmpty())
			{
				$inv_cat_id = Inventory::getId('App\InventoryCategory','inv_cat_displayname',$request->category);

	   			Inventory::where('id', $id)->update(array('inv_name' => $request->inv_name));
	   			Inventory::where('id', $id)->update(array('inventory_category_id' => $inv_cat_id));
                Inventory::where('id', $id)->update(array('sync_status' => "0"));

		}
		else
			{ $errors[] = $request->category.' is not belongs to category.'; }

 		if (isset($errors)){
            return redirect('admin/logistics/inventory')->withErrors($errors);
        }

        Event::fire(new SystemEvent(auth::id(), 'Item Edited.'));

        return redirect('admin/logistics/inventory')->with('success', 'Item Edited!');
 	}

	public function inv_delete(Request $request, $id)
 	{
        // dd($request);
 		$inv = Inventory::find($id);
		$inv_cat_id = Inventory::getId('App\InventoryCategory','inv_cat_name','equip');

		if($inv->inventory_category_id == $inv_cat_id)
		{
            if(isset($request->equip_id)){
			 if($request->reason)
			 {
			 	$final_quan = $inv->inv_quantity - count($request->equip_id);
	   		Inventory::where('id', $id)->update(array('inv_quantity' => $final_quan));
	   		ArchivedInventory::create([
		            'inventory_id' => $id,
		            'archived_quantity' => count($request->equip_id),
		            'archived_reason' => $request->reason,
			        ]);

	   			foreach ($request->equip_id as $key => $value) {
	   				# code...
	   		EquipInventory::where('id', $value)->update(array('status' => 'archive'));

	   			}
			 }
			 else
			 {
			$errors[] = 'Please indicate a reason for removing the said item.';

			 }
            } else
            {
                return redirect()->back()->withErrors('Please check the rejected item.');
            }
		}
		elseif ($inv->inv_quantity<$request->del_quan) {
			$errors[] = 'must be atleast '.$inv->inv_quantity;
		}
		else
		{
			$final_quan = $inv->inv_quantity - $request->del_quan;
	   		Inventory::where('id', $id)->update(array('inv_quantity' => $final_quan));
	   		ArchivedInventory::create([
		            'inventory_id' => $id,
		            'archived_quantity' => $request->del_quan,
		            'archived_reason' => $request->reason,
			        ]);
		}



	   	 if (isset($errors)){
            return redirect('admin/logistics/inventory')->withErrors($errors);
        }

        Inventory::where('id', $id)->update(array('sync_status' => "0"));

        Event::fire(new SystemEvent(auth::id(), 'Item Deleted.'));


        return redirect('admin/logistics/inventory')->with('success', 'Item Deleted!');
 	}



    public function template_download_normal()
    {
        if(!Gate::allows('logistics-only'))
        {
        return redirect('/');
        }


        $file= public_path(). "/documents/excel_template/inventory_normal.xlsx";

        return response()->download($file);
    }

    public function template_download_equip()
    {
        if(!Gate::allows('logistics-only'))
        {
        return redirect('/');
        }


        $file= public_path(). "/documents/excel_template/inventory_equipment.xlsx";

        return response()->download($file);
    }

 	public function importExcel(Request $request)
 	{
 		if(Input::hasFile('import_file'))
 		{
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader){
            })->get();
            if(!empty($data) && $data->count()){

                foreach ($data as $key => $value) {


                	//EQUIPMENT BULK
                	if(isset($value->item_name) and isset($value->brand_name) and isset($value->serial_number) and isset($value->specs))
                	{
                		$inv_cat_id = Inventory::getId('App\InventoryCategory','inv_cat_name','equip');
                		$invname = $value->item_name;
                		if(!Inventory::where('inv_name', $value->item_name)->get()->isEmpty())
						{
								$inv_id = Inventory::getId('App\Inventory','inv_name',$value->item_name);
					 		 	$inv = Inventory::find($inv_id);
					 		 	$invTotalquan = $inv->inv_quantity + 1;
					 		 	Inventory::where('id', $inv_id)->update(array('inv_quantity' => $invTotalquan));
							$insert[] = ['inventory_id' => $inv_id,
                                 'brand_name' => $value->brand_name,
                                 'serial_number' => $value->serial_number,
                                 'specs' => $value->specs
                                ];
					 		 	continue;

						}
						elseif(is_null($value->item_name))
						{
							continue;
						}
						else
						{
							//dd($value->brand_name);
							Inventory::create([
				            'inv_name' => $invname,
				            'inv_quantity' => 1,
				            'inventory_category_id' => $inv_cat_id,
					        ]);

							$inv_id = Inventory::getId('App\Inventory','inv_name',$invname);
							$insert[] = ['inventory_id' => $inv_id,
                                 'brand_name' => $value->brand_name,
                                 'serial_number' => $value->serial_number,
                                 'specs' => $value->specs
                                ];
					 		 	continue;

						}
                	}
                	//GENERAL BULK
                	elseif(isset($value->item_name) and isset($value->item_category) and isset($value->item_quantity))
                	{
					//CHECK IF HASCATEGORY
	                	if(!InventoryCategory::where('inv_cat_displayname', $value->item_category)->get()->isEmpty())
						{$inv_cat_id = Inventory::getId('App\InventoryCategory','inv_cat_displayname',$value->item_category);}
                        elseif(is_null($value->item_category))
                        {
                            $errors[] = 'for item '. $value->item_name . ', category is required';
                            continue;

                        }
						else
						{$errors[] = $value->item_category.' is not belong to category.';
	                            continue;}

	                    //SUM THE QUANTITY IF ITEM HAS BEEN ADDED
	                	if(!Inventory::where('inv_name', $value->item_name)->get()->isEmpty())
						{
                            if($value->item_quantity == 0)
                            {
                                 $errors[] = 'for item '. $value->item_name . ', Please input a quantity';
                                 continue;
                            }
                            else
                            {
							$inv_id = Inventory::getId('App\Inventory','inv_name',$value->item_name);

					 		 	$inv = Inventory::find($inv_id);
					 		 	$invTotalquan = $inv->inv_quantity + $value->item_quantity;
					 		 	Inventory::where('id', $inv_id)->update(array('inv_quantity' => $invTotalquan));
					 		 	continue;
                            }
						}
                        elseif(is_null($value->item_name))
                        {
                            continue;
                        }

						else
						{
                              if($value->item_quantity == 0)
                            {
                                 $errors[] = 'for item '. $value->item_name . ', Please input a quantity';
                                 continue;
                            }else
                            {
	 					    $insert[] = ['inv_name' => $value->item_name,
	                                 'inv_quantity' => $value->item_quantity,
	                                 'inventory_category_id' => $inv_cat_id
	                                ];
                            }
						}
                	}
                    else
                    {
                        return redirect()->back()->withErrors('Excel template is not compatible, Please check your excel');
                    }

                }

                //dd($insert);
                if(!empty($insert))
                {

                 foreach ($insert as $key => $value)
                 {
                 	//dd($value);
                 	if(isset($value['brand_name']) and isset($value['serial_number']) and isset($value['specs']))
                	{
                		EquipInventory::create([
		            'inventory_id' => $value['inventory_id'],
		            'brand_name' => $value['brand_name'],
		            'serial_num' => $value['serial_number'],
		            'specs' => $value['specs'],

			        ]);

                	}
                	else
                	{

						Inventory::create([
		            'inv_name' => $value['inv_name'],
		            'inv_quantity' => $value['inv_quantity'],
		            'inventory_category_id' => $value['inventory_category_id'],
			        ]);

                	}

                 	}
                }
        }

 	}
        if (isset($errors)){
            return redirect('admin/logistics/inventory')->withErrors($errors);
        }
        return redirect('admin/logistics/inventory');

	}


//INVENTORY INVENTORY INVENTORY INVENTORY INVENTORY INVENTORY INVENTORY


//MANAGE-CATEGORY //MANAGE-CATEGORY //MANAGE-CATEGORY //MANAGE-CATEGORY

    public function category_store(Request $request)
    {
         $this->validate($request, [
        'category_name' => 'required|max:255',
        ]);

         if(InventoryCategory::where('inv_cat_displayname',$request->category_name)->get()->isEmpty()){

                $tolower = strtolower($request->category_name);
                $inv_cat_name = preg_replace('/\s+/', '_', $tolower);
                InventoryCategory::create([
                    'inv_cat_name' => $inv_cat_name,
                    'inv_cat_displayname' => $request->category_name,
                ]);

            }
        else{

            return redirect()->back()->withErrors("This category has been added, cannot duplicate category name");

        }
        Event::fire(new SystemEvent(auth::id(), 'Category added.'));

        return redirect('admin/logistics/inventory')->with('success', 'Category Added!');
    }

    public function category_edit(Request $request, $id)
    {

        $equip = InventoryCategory::where('inv_cat_name','equip')->first();
        $office_supply =InventoryCategory::where('inv_cat_name','office_supply')->first();

        if($equip->id == $id or $office_supply->id == $id){
            return redirect()->back()->withErrors("This category is default, make changes are prohibited");
        }
        $this->validate($request, [
        'category_name' => 'required|max:255',
        ]);

        $inventory = Inventory::where('inventory_category_id',$id)->get()->isEmpty();
        $mticspurchaselist = MticsPurchaseList::where('inventory_category_id',$id)->get()->isEmpty();
        $eventpurchaselist = EventPurchaseList::where('inventory_category_id',$id)->get()->isEmpty();



        if($inventory and $mticspurchaselist and $eventpurchaselist )
            {
                $tolower = strtolower($request->category_name);
                $inv_cat_name = preg_replace('/\s+/', '_', $tolower);

                $inv_cat = InventoryCategory::find($id);
                $inv_cat->inv_cat_name = $inv_cat_name;
                $inv_cat->inv_cat_displayname = $request->category_name;
                $inv_cat->save();
            }
        else
        {
            return redirect()->back()->withErrors("This category is being used, make changes are prohibited");
        }

        Event::fire(new SystemEvent(auth::id(), 'Category Edited.'));

        return redirect('admin/logistics/inventory')->with('success', 'Category Edited!');

    }

     public function category_delete(Request $request, $id)
    {

        $inventory = Inventory::where('inventory_category_id',$id)->get()->isEmpty();
        $mticspurchaselist = MticsPurchaseList::where('inventory_category_id',$id)->get()->isEmpty();
        $eventpurchaselist = EventPurchaseList::where('inventory_category_id',$id)->get()->isEmpty();

        $equip = InventoryCategory::where('inv_cat_name','equip')->first();
        $office_supply = InventoryCategory::where('inv_cat_name','office_supply')->first();

            if($equip->id == $id or $office_supply->id == $id){
                return redirect()->back()->withErrors("This category is default, make changes are prohibited");
            }



        if($inventory and $mticspurchaselist and $eventpurchaselist )
            {
                $inv_cat = InventoryCategory::find($id)->delete();
            }
        else
        {
            return redirect()->back()->withErrors("This category is being used, make changes are prohibited");
        }

        Event::fire(new SystemEvent(auth::id(), 'Category Deleted.'));

        return redirect('admin/logistics/inventory')->with('success', 'Category Deleted!');

    }



//MANAGE-CATEGORY //MANAGE-CATEGORY //MANAGE-CATEGORY //MANAGE-CATEGORY


//BORROW-RETURN BORROW-RETURN BORROW-RETURN BORROW-RETURN BORROW-RETURN

	public function brw_rtn_view()
 	{
        if(!Gate::allows('BorrowReturn-only'))
        {
        return redirect('/');
        }

        $faculty_id = Section::getId('App\Section','section_code','Faculty');

 		$members = Member::where('section_id','!=',$faculty_id)->where('status','=','active')->orwhere('status','=','inactive')->get();
 		$inventories = Inventory::with('Inventory_Category')->get();
 		$inv_categories = InventoryCategory::all();
 		$inv_borrows = Borrower::where('borrower_status','Borrowed')->get();
        $logs = Borrower::where('borrower_status','Returned')->get();
 		$inv_equips = EquipInventory::where('status','working')->get();
 		$cat_equip_id = Inventory::getId('App\InventoryCategory','inv_cat_name','equip');
 		
 		return view('officers/borrow_return',compact('inventories','inv_categories','members','inv_borrows','inv_equips','cat_equip_id', 'logs'));
 	}

	public function brw_rtn_borrow(Request $request)
 	{
 		$inv = array();
		$keycount = count($request->inventory);
 		$key = 0;
        $inv_with_equip = [];
        $inv_equips = EquipInventory::where('status','!=','archive')->get();

        foreach ($inv_equips as $equip) {
            if (!in_array($equip->inventory_id, $inv_with_equip)) {
                $inv_with_equip[] = $equip->inventory_id;
            }
        }

 		//FETCHING AND MERGING ARRAYS (inv_id,quan,desc)
        try {
 		foreach ($request->inventory as $inv_id) {
 			if($inv_id)
 			{
 			$inv[$key]['inv_id']=$inv_id;

            if (!in_array($inv_id, $inv_with_equip)){
                if(is_null($request->quan[$inv_id])){
                    return redirect()->back()->withErrors("Please make sure to indicate the quantity of the item you want to borrow.");
                }
                $inv[$key]['quan']=$request->quan[$inv_id];
            }
            else {
                $equip_cnt = count($request->equip_id[$inv_id]);
                if ($equip_cnt == 0) {
                    return redirect()->back()->withErrors("Please make sure to check the equipment you want to borrow.");
                }
                $inv[$key]['quan']=count($request->equip_id[$inv_id]);

            }

            $inv[$key]['desc']=$request->desc[$inv_id];
 			$key++;
 			}
 		}
        } catch (\Exception $e) {
            return redirect()->back()->withErrors("No item to be borrowed.");
        }

 		$borrower = New Borrower;
 		$borrower->member_id = $request->member_id;
 		$borrower->borrower_status = 'Borrowed';



		foreach ($inv as $key => $value) {

			$inventory = Inventory::find($value['inv_id']);

			if($inventory->inv_quantity < $value['quan'])
			{
				$errors[] = $inventory->inv_name.' has '.$inventory->inv_quantity.' pc/s only';

	            continue;
			}

			//PROCESS THE DEDUCTION OF QUANTITY AND PROCESS THE BORROWER INVETORY

			$borrower->save();

			$finalquan = $inventory->inv_quantity - $value['quan'];
			$borrower_inventory = New BorrowerInventory;
			$borrower_inventory->inventory_id = $value['inv_id'];
			$borrower_inventory->borrower_id = $borrower->id;
            $borrower_inventory->borrower_desc = $value['desc'];
			$borrower_inventory->borrower_quantity = $value['quan'];
            $borrower_inventory->save();
			// $desc = $value['desc'];
			$inv_id = $value['inv_id'];
			// $borrowerid = $borrower->id;


	 		Inventory::where('id', $inv_id)->update(array('inv_quantity' => $finalquan));
            Inventory::where('id', $inv_id)->update(array('sync_status' => "0"));

		}


			$equipdesc = "";
			$arraykey = 0;

			//FOR EQUIPMENT BORROW ITEM
            if(isset($request->equip_id)) {

			foreach ($request->equip_id as $equips) {
                foreach ($equips as $value) {
                    EquipInventory::where('id', $value)->update(array('status' => 'borrowed'));
                    $equip_inv = EquipInventory::find($value);
                    $borrower_invs = BorrowerInventory::where('borrower_id',$borrower->id)->where('inventory_id',$equip_inv->inventory_id)->get();

                    //GETTING THE DESCRIPTION INPUTED BY USER AND MERGING WITH THE EQUIP BRANDNAME AND SPECIFICATION
                    foreach ($borrower_invs as $borrower_inv) {
                    $borrow_inv_desc = $borrower_inv->borrower_desc;
                    $array[$borrower_inv->inventory_id]['orig'] = $borrow_inv_desc;
                        if($borrower_inv->inventory_id !== $arraykey)
                        {
                            unset($equipdesc);
                            $equipdesc="";
                        }
                        $equipdesc = $equipdesc." "."-".$equip_inv->brand_name." ".$equip_inv->serial_num;

                            $array[$borrower_inv->inventory_id]['specs'] = $equipdesc;
                            $arraykey = $borrower_inv->inventory_id;
                        BorrowinvEquip::create([
                                'borrower_inventory_id' => $borrower_inv->id,
                                'equip_inventory_id' => $value,
                                ]);

                    }

                }
	   		}


	   		//UPDATING THE BORROWER INVENTORY DESCRIPTION WITH AUTOMATIC BRANDNAME AND SPECIFICATION
	   		foreach ($array as $key => $value) {

	   		if(!BorrowerInventory::where('inventory_id', $key)->where('borrower_id', $borrower->id)->get()->isEmpty())
	   			{

	   				$totaldesc= $value['orig']." (".$value['specs'].") ";
	   				$borrower_invs = BorrowerInventory::where('borrower_id',$borrower->id)->where('inventory_id',$key)->get();
	   				foreach ($borrower_invs as $borrower_inv) {
					BorrowerInventory::where('id', $borrower_inv->id)->update(array('borrower_desc' =>$totaldesc));

	   				}

	   			}

	   		}
            }



        Event::fire(new SystemEvent(auth::id(), 'Processed Borrow Item.'));

        return redirect('admin/logistics/borrow-return')->with('success', 'Item Borrowed!');
	   }


public function brw_rtn_return(Request $request)
 	{

 		$datetimenow = date("Y-m-d H:i:s");

 		Borrower::where('id', $request->borrower_id)->update(array('borrower_status' => 'Returned'));
	 	Borrower::where('id', $request->borrower_id)->update(array('returned_datetime' => $datetimenow));
	 	$borrower = Borrower::find($request->borrower_id);
	 	$bor_invs = BorrowerInventory::where('borrower_id',$request->borrower_id)->get();

	 	foreach ($bor_invs as $bor_inv) {

	 		$inv = Inventory::find($bor_inv->inventory_id);
	 		$totalquan = $inv->inv_quantity + $bor_inv->borrower_quantity;
	 		Inventory::where('id', $inv->id)->update(array('inv_quantity' =>$totalquan));

	 		if(!BorrowinvEquip::where('borrower_inventory_id',$bor_inv->id)->get()->isEmpty())
	 			{
	 				$borinv_equips = BorrowinvEquip::where('borrower_inventory_id',$bor_inv->id)->get();

			 		foreach ($borinv_equips as $borinv_equip) {
			 			$equips_id = $borinv_equip->equip_inventory_id;
						EquipInventory::where('id', $equips_id)->update(array('status' =>'working'));
			 		}
	 			}




	 	}

        Event::fire(new SystemEvent(auth::id(), 'Processed Return Item.'));

        return redirect('admin/logistics/borrow-return')->with('success', 'Item Returned!');

 	}


//BORROW-RETURN BORROW-RETURN BORROW-RETURN BORROW-RETURN BORROW-RETURN



//PURCHAsED-ITEMS //PURCHAsED-ITEMS //PURCHAsED-ITEMS //PURCHAsED-ITEMS
    public function purchased_item_view()
    {
        if(!Gate::allows('logistics-only'))
        {
        return redirect('/');
        }
        //EXTERNAL MEMBER_ID
        $external_id = Role::getId('App\Role','name','external');
        $role_members = RoleMember::where('role_id',$external_id)->get();
        foreach ($role_members as $role_member) {
          $ext_member_id = $role_member->member_id;
        }

        $all_external = Role::with('member')->where('name','external')->orwhere('name','asst_external')->get();
        $tasks = Task::with('mticspurchaselist')->with('eventpurchaselist')->where('member_id',$ext_member_id)->where('task_status','for validation')->orwhere('task_status','on-going')->get();

        $mtics_purchases_validation = MticsPurchaseList::whereHas('task', function($q) {
            $q->where('task_status', 'for validation');
        })->where('mtics_itemstatus', 'purchased')->where('mtics_inv_status', 'pending')->get();

        $mtics_purchased_items = MticsPurchaseList::whereHas('task', function($q) {
            $q->where('task_status', 'done');
        })->where('mtics_itemstatus', 'purchased')->where(function($q) {
            $q->where('mtics_inv_status', 'moved')->orWhere('mtics_inv_status', 'unmoved');
        })->get();

        $inventory_categories = InventoryCategory::all();


        return view('officers/logistics_purchased_items',compact('tasks','mtics_purchases_validation','all_external','inventory_categories','event_purchases', 'mtics_purchased_items'));

    }

    public function eventView($id)
    {
        if(!Gate::allows('logistics-only'))
        {
        return redirect('/');
        }
        $event = MticsEvent::findOrFail($id);

        $itemsForValidation = EventPurchaseList::whereHas('task', function($q) use($id) {
          $q->where('event_id', $id)->where('task_status', 'for validation');
        })->where('event_itemstatus', 'purchased')->where('event_inv_status', 'pending')->where(function($q){
          $q->where('event_itemname', '!=', 'meal')->where('event_itemname', '!=', 'fare');
        })->get();

        $event_purchased_items = EventPurchaseList::whereHas('task', function($q) use($event) {
            $q->where('task_status', 'done')->where('event_id', $event->id);
        })->where('event_itemstatus', 'purchased')->where(function($q) {
            $q->where('event_inv_status', 'moved')->orWhere('event_inv_status', 'unmoved');
        })->get();

         $logistic_tasks = Task::where('event_id', $id)->where(function($q) {
                $q->where('task_status', 'pending')->orWhere('task_status', 'on-going')->orWhere('task_status', 'for validation')->orWhere('task_status', 'disapproved');
            })->whereHas('role', function($q){
                $q->where('name', 'logistic');
            })->get();

        $inventory_categories = InventoryCategory::all();

        return view('officers/event/logistic',compact('event', 'itemsForValidation', 'inventory_categories','logistic_tasks', 'event_purchased_items'));
    }


    public function purchased_item_pass(Request $request,$task_id,$id)
    {

       $this->validate($request, [
        'inv_cat_id' => 'required',

        ]);

      $task = Task::find($task_id);
      $equip_id = Inventory::getId('App\InventoryCategory','inv_cat_name','equip');
      $checkifcanmove = false;

      if($task->event_id == null)
      {
        $mticspurchaselist = MticsPurchaseList::find($id);
        if($mticspurchaselist->mtics_inv_status == 'moved'){
            return redirect()->back()->withErrors('This item has been moved');
        }
         if($mticspurchaselist->mtics_act_quan == null){
            return redirect()->back()->withErrors('not yet purchased');
        }

      if(!MticsPurchaseReport::where('mtics_purchase_list_id',$id)->where('report_category','critical')->get()->isEmpty()){
                    return redirect()->back()->withErrors("You can not pass this item; item has critical report");
              }

        //CHECK IF THERE IS EQUIPMENT, the category will be automatically 'equip'
        if(!MticsPurchaseEquipList::where('mtics_purchase_list_id',$id)->get()->isEmpty()){
            $category = $equip_id;
        }
        else{
            $category = $request->inv_cat_id;
        }


        if($category == $equip_id)
        {
            $checkifcanmove = true;
        }

        if(!isset($request->dontmove))
        {
            $checkifcanmove = true;
        }


        if($checkifcanmove == true)
        {
            if($mticspurchaselist->mtics_orig_quan > $mticspurchaselist->mtics_act_quan)
            {
                if($mticspurchaselist->mtics_inv_quan !== null)
                {
                    $quan = $mticspurchaselist->mtics_act_quan - $mticspurchaselist->mtics_inv_quan;
                }
                else
                {
                    $quan = $mticspurchaselist->mtics_act_quan;
                }


            }
            else
            {
                if($mticspurchaselist->mtics_inv_quan !== null)
                {
                    $quan = $mticspurchaselist->mtics_act_quan - $mticspurchaselist->mtics_inv_quan;
                }
                else
                {
                    $quan = $mticspurchaselist->mtics_act_quan;
                }
                  MticsPurchaseList::where('id', $id)->update(array('mtics_inv_status' => 'moved'));
            }

                  MticsPurchaseList::where('id', $id)->update(array('mtics_inv_quan' => $mticspurchaselist->mtics_act_quan));
                  MticsPurchaseList::where('id', $id)->update(array('inventory_category_id' => $category));
                  MticsPurchaseList::where('id', $id)->update(array('logistics_id' => Auth::id()));



            if(Inventory::where('inv_name',$mticspurchaselist->mtics_itemname)->where('inventory_category_id',$category)->get()->isEmpty()){

                $inventory = New Inventory;
                $inventory->inv_name = $mticspurchaselist->mtics_itemname;
                $inventory->inv_quantity = $quan;
                $inventory->inventory_category_id = $category;
                $inventory->save();
                $inventory_id = $inventory->id;


            }
            else{

                $first_inventory = Inventory::where('inv_name',$mticspurchaselist->mtics_itemname)->where('inventory_category_id',$category)->first();
                $totalquan = $quan + $first_inventory->inv_quantity;
                $first_inventory->inv_quantity = $totalquan;
                $first_inventory->save();
                $inventory_id = $first_inventory->id;
            }




            if(!MticsPurchaseEquipList::where('mtics_purchase_list_id',$id)->get()->isEmpty())
            {


                $mticspurchaseequiplists = MticsPurchaseEquipList::where('mtics_purchase_list_id',$id)->get();
                foreach ($mticspurchaseequiplists as $mticspurchaseequiplist) {
                    if(EquipInventory::where('inventory_id',$inventory_id)->where('brand_name',$mticspurchaseequiplist->mtics_brandname)->where('serial_num',$mticspurchaseequiplist->mtics_serialnum)->where('specs',$mticspurchaseequiplist->mtics_specs)->get()->isEmpty())
                        {
                            $equip_inventory = New EquipInventory;
                            $equip_inventory->inventory_id = $inventory_id;
                            $equip_inventory->brand_name = $mticspurchaseequiplist->mtics_brandname;
                            $equip_inventory->serial_num = $mticspurchaseequiplist->mtics_serialnum;
                            $equip_inventory->specs = $mticspurchaseequiplist->mtics_specs;
                            $equip_inventory->save();
                        }
                }

            }

        }
        else{
            MticsPurchaseList::where('id', $id)->update(array('mtics_inv_status' => 'unmoved'));
            MticsPurchaseList::where('id', $id)->update(array('logistics_id' => Auth::id()));
        }

      }
      else
      {
        $eventpurchaselist = EventPurchaseList::find($id);
        if($eventpurchaselist->event_inv_status == 'moved'){
            return redirect()->back()->withErrors('This item has been moved');
        }

        if(!EventPurchaseReport::where('event_purchase_list_id',$id)->where('report_category','critical')->get()->isEmpty()){
                return redirect()->back()->withErrors("You can not pass this item; item has critical report");
          }

        if(!EventPurchaseEquipList::where('event_purchase_list_id',$id)->get()->isEmpty()){
            $category = $equip_id;

        }
        else{
            $category = $request->inv_cat_id;
        }

        if($category == $equip_id)
        {
            $checkifcanmove = true;
        }

        if(!isset($request->dontmove))
        {
            $checkifcanmove = true;
        }


        if($checkifcanmove == true)
        {
            if(Inventory::where('inv_name',$eventpurchaselist->event_itemname)->where('inventory_category_id',$category)->get()->isEmpty()){

                $inventory = New Inventory;
                $inventory->inv_name = $eventpurchaselist->event_itemname;
                $inventory->inv_quantity = $eventpurchaselist->event_act_quan;
                $inventory->inventory_category_id = $category;
                $inventory->save();
                $inventory_id = $inventory->id;
            }
            else{
                $first_inventory = Inventory::where('inv_name',$eventpurchaselist->event_itemname)->where('inventory_category_id',$category)->first();
                $totalquan = $eventpurchaselist->event_act_quan + $first_inventory->inv_quantity;
                $first_inventory->inv_quantity = $totalquan;
                $first_inventory->save();
                $inventory_id = $first_inventory->id;
            }


            if(!EventPurchaseEquipList::where('event_purchase_list_id',$id)->get()->isEmpty()){
                $eventpurchaseequiplists = EventPurchaseEquipList::where('event_purchase_list_id',$id)->get();

                foreach ($eventpurchaseequiplists as $eventpurchaseequiplist) {
                    $equip_inventory = New EquipInventory;
                    $equip_inventory->inventory_id = $inventory_id;
                    $equip_inventory->brand_name = $eventpurchaseequiplist->event_brandname;
                    $equip_inventory->serial_num = $eventpurchaseequiplist->event_serialnum;
                    $equip_inventory->specs = $eventpurchaseequiplist->event_specs;
                    $equip_inventory->save();
                }


            }

            EventPurchaseList::where('id', $id)->update(array('event_inv_status' => 'moved'));
            EventPurchaseList::where('id', $id)->update(array('inventory_category_id' => $category));
            EventPurchaseList::where('id', $id)->update(array('logistics_id' => Auth::id()));

        }
        else
        {
            EventPurchaseList::where('id', $id)->update(array('event_inv_status' => 'unmoved'));
            EventPurchaseList::where('id', $id)->update(array('logistics_id' => Auth::id()));

        }
      }

        $this->isTaskdone($task_id);
        return redirect()->back()->with('success', 'Item Added!');

    }


    public function purchased_item_report(Request $request,$task_id, $id)
    {

        $this->validate($request, [
        'report_cat' => 'required',
        'report_name' => 'required',
        'report_reason' => 'required',
        'external_explain' => 'required',
        ]);

        //dd($task_id);
        $task = Task::find($task_id);
        $equip_id = Inventory::getId('App\InventoryCategory','inv_cat_name','equip');
        $checkifcanmove = false;
        if($task->event_id == null)
        {
            $mticspurchaselist = MticsPurchaseList::find($id);
            if($mticspurchaselist->mtics_inv_status == 'moved')
            {
                return redirect()->back()->withErrors('You can not report this item; item has been moved');
            }

            if(!MticsPurchaseReport::where('mtics_purchase_list_id',$id)->get()->isEmpty()) {
                return redirect()->back()->withErrors('You already reported this item');
                }


            $logisticsmticsreport = New MticsPurchaseReport;
            $logisticsmticsreport->admin_id = Auth::id();
            $logisticsmticsreport->reportedby = 'logistics';
            $logisticsmticsreport->mtics_purchase_list_id = $id;
            $logisticsmticsreport->report_name = $request->report_name;
            $logisticsmticsreport->report_reason = $request->report_reason;
            $logisticsmticsreport->external_explanation = $request->external_explain;
            $logisticsmticsreport->report_category = $request->report_cat;
            $logisticsmticsreport->save();


            if($request->report_cat == 'minor')
            {
                $this->validate($request, [
                'inv_cat_id' => 'required',
                ]);


                  if(!MticsPurchaseReport::where('mtics_purchase_list_id',$id)->where('report_category','critical')->get()->isEmpty()){
                        return redirect()->back()->withErrors("You can not report this item as 'minor'; item has critical report");
                  }


                  if(!MticsPurchaseEquipList::where('mtics_purchase_list_id',$id)->get()->isEmpty())
                    {

                        $category = Inventory::getId('App\InventoryCategory','inv_cat_name','equip');
                    }
                    else
                    {
                        $category = $request->inv_cat_id;
                    }



                    if($category == $equip_id)
                    {
                        $checkifcanmove = true;
                    }

                    if(!isset($request->dontmove))
                    {
                        $checkifcanmove = true;
                    }


                    if($checkifcanmove == true)
                    {
                        if(Inventory::where('inv_name',$mticspurchaselist->mtics_itemname)->where('inventory_category_id',$category)->get()->isEmpty())
                        {
                            $inventory = New Inventory;
                            $inventory->inv_name = $mticspurchaselist->mtics_itemname;
                            $inventory->inv_quantity = $mticspurchaselist->mtics_act_quan;
                            $inventory->inventory_category_id = $category;
                            $inventory->save();
                            $inventory_id = $inventory->id;
                        }
                        else
                        {
                            $first_inventory = Inventory::where('inv_name',$mticspurchaselist->mtics_itemname)->where('inventory_category_id',$category)->first();
                            $totalquan = $mticspurchaselist->mtics_act_quan + $first_inventory->inv_quantity;
                            $first_inventory->inv_quantity = $totalquan;
                            $first_inventory->save();
                            $inventory_id = $first_inventory->id;

                        }


                        if(!MticsPurchaseEquipList::where('mtics_purchase_list_id',$id)->get()->isEmpty())
                        {

                            $mticspurchaseequiplists = MticsPurchaseEquipList::where('mtics_purchase_list_id',$id)->get();

                            foreach ($mticspurchaseequiplists as $mticspurchaseequiplist) {

                                $equip_inventory = New EquipInventory;
                                $equip_inventory->inventory_id = $inventory_id;
                                $equip_inventory->brand_name = $mticspurchaseequiplist->mtics_brandname;
                                $equip_inventory->serial_num = $mticspurchaseequiplist->mtics_serialnum;
                                $equip_inventory->specs = $mticspurchaseequiplist->mtics_specs;
                                $equip_inventory->save();
                            }

                        }

                          MticsPurchaseList::where('id', $id)->update(array('logistics_id' => Auth::id()));
                          MticsPurchaseList::where('id', $id)->update(array('mtics_inv_status' => 'moved'));
                          MticsPurchaseList::where('id', $id)->update(array('inventory_category_id' => $category));
                    }
                    else
                    {
                        MticsPurchaseList::where('id', $id)->update(array('logistics_id' => Auth::id()));
                        MticsPurchaseList::where('id', $id)->update(array('mtics_inv_status' => 'unmoved'));
                    }


                $data = array(
                'message' => "Item validated by logistics.",
                'redirect' => 'admin/finance/requested-money/',
                'origin' => 'purchased-item-validation',
                );

                $role_id = Role::where('name', 'finance')->first()->id;
                $member = Member::whereHas('role', function($q) use ($role_id) {
                    $q->where('role_id', $role_id);
                })->first();
                $member->notify(new SystemNotification($data));

            }

            else {

                $data = array(
                'message' => "Executive for Logistics Affairs raised a critical issue upon purchased item.",
                'redirect' => 'admin/president/purchased-item-reported',
                'origin' => 'purchased-item-validation',
                );

                $role_id = Role::where('name', 'president')->first()->id;
                $member = Member::whereHas('role', function($q) use ($role_id) {
                    $q->where('role_id', $role_id);
                })->first();
                $member->notify(new SystemNotification($data));

            }

            $this->isTaskdone($task_id);

            Event::fire(new SystemEvent(auth::id(), 'Reported an Item.'));

            return redirect('admin/logistics/purchased-items')->with('success', 'Reported!');

        }
        else
        {
            //dd('dito');
            $eventpurchaselist = EventPurchaseList::find($id);
            $equip_id = Inventory::getId('App\InventoryCategory','inv_cat_name','equip');
            $checkifcanmove = false;
            if($eventpurchaselist->event_inv_status == 'moved')
            {
                return redirect()->back()->withErrors('You can not report this item; item has been moved');
            }

            if(!EventPurchaseReport::where('event_purchase_list_id',$id)->get()->isEmpty()) {
                return redirect()->back()->withErrors('You already reported this item');
                }

            $logisticseventreport = New EventPurchaseReport;
            $logisticseventreport->admin_id = Auth::id();
            $logisticseventreport->reportedby = 'logistics';
            $logisticseventreport->event_purchase_list_id = $id;
            $logisticseventreport->report_name = $request->report_name;
            $logisticseventreport->report_reason = $request->report_reason;
            $logisticseventreport->external_explanation = $request->external_explain;
            $logisticseventreport->report_category = $request->report_cat;
            $logisticseventreport->save();
            //dd($logisticseventreport);

            if($request->report_cat == 'minor')
            {
                $this->validate($request, [
                'inv_cat_id' => 'required',
                ]);

                  if(!EventPurchaseReport::where('event_purchase_list_id',$id)->where('report_category','critical')->get()->isEmpty()){
                        return redirect()->back()->withErrors("You can not report this item as 'minor'; item has critical report");
                  }


                  if(!EventPurchaseEquipList::where('event_purchase_list_id',$id)->get()->isEmpty())
                    {
                        $category = Inventory::getId('App\InventoryCategory','inv_cat_name','equip');
                    }
                    else
                    {
                        $category = $request->inv_cat_id;
                    }


                    if($category == $equip_id)
                    {
                        $checkifcanmove = true;
                    }

                    if(!isset($request->dontmove))
                    {
                        $checkifcanmove = true;
                    }


                    if($checkifcanmove == true)
                    {
                          if(Inventory::where('inv_name',$eventpurchaselist->event_itemname)->where('inventory_category_id',$category)->get()->isEmpty())
                            {
                                $inventory = New Inventory;
                                $inventory->inv_name = $eventpurchaselist->event_itemname;
                                $inventory->inv_quantity = $eventpurchaselist->event_act_quan;
                                $inventory->inventory_category_id = $category;
                                $inventory->save();
                                $inventory_id = $inventory->id;
                            }
                            else
                            {
                                $first_inventory = Inventory::where('inv_name',$eventpurchaselist->event_itemname)->where('inventory_category_id',$category)->first();
                                $totalquan = $eventpurchaselist->event_act_quan + $first_inventory->inv_quantity;
                                $first_inventory->inv_quantity = $totalquan;
                                $first_inventory->save();
                                $inventory_id = $first_inventory->id;

                            }


                          if(!EventPurchaseEquipList::where('event_purchase_list_id',$id)->get()->isEmpty())
                            {

                                $eventpurchaseequiplists = EventPurchaseEquipList::where('event_purchase_list_id',$id)->get();

                                foreach ($eventpurchaseequiplists as $eventpurchaseequiplist) {

                                    $equip_inventory = New EquipInventory;
                                    $equip_inventory->inventory_id = $inventory_id;
                                    $equip_inventory->brand_name = $eventpurchaseequiplist->event_brandname;
                                    $equip_inventory->serial_num = $eventpurchaseequiplist->event_serialnum;
                                    $equip_inventory->specs = $eventpurchaseequiplist->event_specs;
                                    $equip_inventory->save();
                                }

                            }

                              EventPurchaseList::where('id', $id)->update(array('logistics_id' => Auth::id()));
                              EventPurchaseList::where('id', $id)->update(array('event_inv_status' => 'moved'));
                              EventPurchaseList::where('id', $id)->update(array('inventory_category_id' => $category));

                    }
                    else
                    {
                        EventPurchaseList::where('id', $id)->update(array('logistics_id' => Auth::id()));
                        EventPurchaseList::where('id', $id)->update(array('event_inv_status' => 'unmoved'));
                    }

            }

            else {

                $data = array(
                'message' => "Executive for Logistics Affairs raised a critical issue upon purchased item.",
                'redirect' => 'admin/president/purchased-item-reported',
                'origin' => 'purchased-item-validation',
                );

                $role_id = Role::where('name', 'president')->first()->id;
                $member = Member::whereHas('role', function($q) use ($role_id) {
                    $q->where('role_id', $role_id);
                })->first();
                $member->notify(new SystemNotification($data));

            }

            $this->isTaskdone($task_id);

            Event::fire(new SystemEvent(auth::id(), 'Reported an Item.'));

            return redirect('admin/logistics/manage-event/'.$task->event_id)->with('success', 'Reported!');
        }

    }


    private function isTaskdone($task_id)
    {
        $task = Task::find($task_id);
        if($task->event_id == null)
        {
          if(MticsPurchaseList::where('task_id',$task_id)->where('mtics_inv_status','pending')->get()->isEmpty()){

            $mticspurchaselists = MticsPurchaseList::where('task_id',$task_id)->get();
            foreach ($mticspurchaselists as $mticspurchaselist) {


                if(MticsPurchaseReport::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->isEmpty()) {
                    $status = 'done';
                }
                elseif(!MticsPurchaseReport::where('mtics_purchase_list_id',$mticspurchaselist->id)->where('report_category','minor')->get()->isEmpty()){
                    $status = 'failed';

                    $message = "Executive for Logistics Affairs reported an item. You failed the task.";

                    break;
                }

                if(!MticsPurchaseReport::where('mtics_purchase_list_id',$mticspurchaselist->id)->where('report_category','critical')->get()->isEmpty()){
                    $status = 'for validation';
                }
            }

            $date = date('Y-m-d');
            $due_date = $task->due_date;
            $duedate_with3days = date('Y-m-d', strtotime($due_date. ' + 3 days'));

            if($duedate_with3days < $date){

                $status = 'failed';
                $message = "Task is overdue. You failed the task.";
            }

            if ($status == 'failed') {

                $data = array(
                'message' => $message,
                'redirect' => 'admin/request-to-buy',
                'origin' => 'logistics-validation',
                );

                $role_id = Role::where('name', 'external')->first()->id;
                $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
                })->first();
                $member->notify(new SystemNotification($data));
            }

            if ($status != 'for validation') {

                $reimbursement = $task->requestmoney->amount - $task->mticspurchaselist->sum('mtics_total_price');

                if ($reimbursement < 0) {

                    $data = array(
                    'message' => "You can now request for reimbursement.",
                    'redirect' => 'admin/external/task/' . $task->id,
                    'origin' => 'logistics-validation',
                    );

                    $role_id = Role::where('name', 'external')->first()->id;
                    $member = Member::whereHas('role', function($q) use ($role_id) {
                    $q->where('role_id', $role_id);
                    })->first();
                    $member->notify(new SystemNotification($data));

                }

                $data = array(
                'message' => "Item validated by logistics.",
                'redirect' => 'admin/finance/requested-money/',
                'origin' => 'purchased-item-validation',
                );

                $role_id = Role::where('name', 'finance')->first()->id;
                $member = Member::whereHas('role', function($q) use ($role_id) {
                    $q->where('role_id', $role_id);
                })->first();
                $member->notify(new SystemNotification($data));

                Event::fire(new SystemEvent(auth::id(), 'Item Validated.'));
            }


                //failed for penalty

            Task::where('id', $task_id)->update(array('task_status' => $status));

          }

        }
        else
        {
            if(EventPurchaseList::where('task_id',$task_id)->where('event_inv_status','pending')->get()->isEmpty()){

            $eventpurchaselists = EventPurchaseList::where('task_id',$task_id)->get();
            foreach ($eventpurchaselists as $eventpurchaselist) {


                if(EventPurchaseReport::where('event_purchase_list_id',$eventpurchaselist->id)->get()->isEmpty()) {
                    $status = 'done';
                }

                if(!EventPurchaseReport::where('event_purchase_list_id',$eventpurchaselist->id)->where('report_category','minor')->get()->isEmpty()){
                    $status = 'failed';

                    break;
                }

                if(!EventPurchaseReport::where('event_purchase_list_id',$eventpurchaselist->id)->where('report_category','critical')->get()->isEmpty()){
                    $status = 'for validation';
                    break;
                }
            }
                $reason = 'has minor report, reported by logistics';
                $date = date('Y-m-d');
                $due_date = $task->due_date;
                $duedate_with3days = date('Y-m-d', strtotime($due_date. ' + 3 days'));


                if($duedate_with3days < $date){

                    $status = 'failed';
                    $reason = 'overdue task';
                }



                Task::where('id', $task_id)->update(array('task_status' => $status));


                //failed for penalty
                if($status == 'failed'){

                    $penaltylastrow = Penalty::where('member_id',$task->member_id)->orderby('id','desc')->first();
                    $penalty = New Penalty;
                    $penalty->task_id = $task_id;
                    $penalty->member_id = $task->member_id;
                    $penalty->validator_id = Auth::id();
                    $penalty->reason = $reason;
                    if($penaltylastrow == null){
                    $penalty->penalty_type = 'verbal';
                    $message = "Executive Officer for Logistics reported a purchased item. You've failed the task. Please talk to your president for the said issue.";
                    }
                    elseif($penaltylastrow->penalty_type == 'fee'){
                    $penalty->penalty_type = 'verbal';
                    $message = "Executive Officer for Logistics reported a purchased item. You've failed the task. Please talk to your president to resolve the said issue.";
                    }
                    elseif($penaltylastrow->penalty_type == 'verbal'){
                    $penalty->penalty_type = 'written';
                    $message = "Executive Officer for Logistics reported a purchased item. You've failed the task. Please submit a letter of explanation to resolve the said issue.";
                    }
                    elseif($penaltylastrow->penalty_type == 'written'){
                    $penalty->penalty_type = 'fee';
                    $penalty->fee = 10;
                    $message = "Executive Officer for Logistics reported a purchased item. You've failed the task. You have a fine of 10 pesos.";
                    }

                    $penalty->save();

                    $data = array(
                    'message' => $message,
                    'redirect' => 'admin/external/manage-event/' . $task->event_id . '/task/' . $task->id,
                    'origin' => 'purchased-item-validation',
                    );

                    $role_id = Role::where('name', 'external')->first()->id;
                    $member = Member::whereHas('role', function($q) use ($role_id) {
                        $q->where('role_id', $role_id);
                    })->first();
                    $member->notify(new SystemNotification($data));

                    $data = array(
                    'message' => "An officer failed a task. Please review the officer's penalty.",
                    'redirect' => 'admin/president/manage-event/' . $task->event_id . '/failed-task',
                    'origin' => 'purchased-item-validation',
                    );

                    $role_id = Role::where('name', 'president')->first()->id;
                    $member = Member::whereHas('role', function($q) use ($role_id) {
                        $q->where('role_id', $role_id);
                    })->first();
                    $member->notify(new SystemNotification($data));

                }


                $finance = new FormulaMoneyController;

                if($finance->checkExternalBudget($task_id) == 0) {
                    if ($status == 'done' or $status == 'failed') {

                        $data = array(
                        'message' => "There are purchased items to be validated",
                        'redirect' => array('role' => 'finance', 'event_id' => $task->event_id),
                        'origin' => 'logistic',
                        );

                        $role_id = Role::where('name', 'finance')->first()->id;
                        $member = Member::whereHas('role', function($q) use ($role_id) {
                            $q->where('role_id', $role_id);
                        })->first();
                        $member->notify(new SystemNotification($data));
                    }
                }
                elseif($finance->checkExternalBudget($task_id) < 0) {

                    if ($status == 'done' or $status == 'failed') {

                        $data = array(
                        'message' => "There are purchased items to be validated",
                        'redirect' => array('role' => 'finance', 'event_id' => $task->event_id),
                        'origin' => 'logistic',
                        );

                        $role_id = Role::where('name', 'finance')->first()->id;
                        $member = Member::whereHas('role', function($q) use ($role_id) {
                            $q->where('role_id', $role_id);
                        })->first();
                        $member->notify(new SystemNotification($data));

                        $data = array(
                        'message' => "You can now request for reimbursement.",
                        'redirect' => 'admin/external/manage-event/' . $task->event_id . '/task/' . $task->id,
                        'origin' => 'purchased-item-validation',
                        );

                        $role_id = Role::where('name', 'external')->first()->id;
                        $member = Member::whereHas('role', function($q) use ($role_id) {
                            $q->where('role_id', $role_id);
                        })->first();
                        $member->notify(new SystemNotification($data));
                    }
                }

                elseif($finance->checkExternalBudget($task_id) > 0) {

                    if ($status == 'done' or $status == 'failed') {

                        $data = array(
                        'message' => "There are purchased items to be validated",
                        'redirect' => array('role' => 'finance', 'event_id' => $task->event_id),
                        'origin' => 'logistic',
                        );

                        $role_id = Role::where('name', 'finance')->first()->id;
                        $member = Member::whereHas('role', function($q) use ($role_id) {
                            $q->where('role_id', $role_id);
                        })->first();
                        $member->notify(new SystemNotification($data));
                    }
                }

          }
        Event::fire(new SystemEvent(auth::id(), 'Item Validated.'));

        }


    }


//DAMAGE-REPORT //DAMAGE-REPORT //DAMAGE-REPORT //DAMAGE-REPORT //DAMAGE-REPORT

    public function damage_report_view($id,$serial)
    {
        if(!Gate::allows('logistics-only'))
        {
        return redirect('/');
        }
        $cat_equip_id = Inventory::getId('App\InventoryCategory','inv_cat_name','equip');
        $equip_inventory = EquipInventory::with('equip_repair')->where('id',$id)->first();

        if($equip_inventory->serial_num !== $serial)
        {
            return redirect()->back()->withErrors('invalid equipment id and serial');
        }

        return view('officers/inventory_damage_report', compact('equip_inventory'));

    }


     public function damage_report_add(Request $request,$id,$serial)
    {

         $this->validate($request, [
                'repair_desc' => 'required',
                'repair_status' => 'required',
                ]);

        $equip_inventory = EquipInventory::with('equip_repair')->where('id',$id)->first();

        if($equip_inventory->serial_num !== $serial)
        {
            return redirect()->back()->withErrors('invalid equipment id and serial');
        }

        if($request->repair_status == 'working')
        {
            EquipInventory::where('id', $id)->update(array('status' => 'working'));
        }
        elseif ($request->repair_status == 'damage') {
            EquipInventory::where('id', $id)->update(array('status' => 'repair'));
        }
        else
        {
            return redirect()->back()->withErrors('invalid status');
        }

        $equip_repair = New EquipRepairReport;
        $equip_repair->equip_inventory_id = $id;
        $equip_repair->admin_id = Auth::id();
        $equip_repair->report_desc = $request->repair_desc;
        $equip_repair->status = $request->repair_status;
        $equip_repair->save();


        return redirect('admin/logistics/damage-report/'.$id.'/'.$serial);
    }





}
