@if(count($moneyrequests) > 0)<span class="w3-badge w3-red">{{count($moneyrequests)}}</span>@endif


<script type="text/javascript">
$(document).ready( function () {
  var moneyRequestTable = $('#moneyRequestTable').DataTable();
  $('#validationTable').DataTable();
  $('#transactionTable').DataTable();

  $(moneyRequestTable.table().container()).removeClass('form-inline');

} );
</script>


Event::fire(new SystemEvent(auth::id(), 'Uploaded Receipt.'));

->with('success', 'Receipt Uploaded!')

use App\Events\SystemEvent;
use Illuminate\Support\Facades\Event;

