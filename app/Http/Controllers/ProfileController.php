<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Event;

use App\Events\SystemEvent;

use App\Member;
use App\Section;
use App\Post;
use App\PostCategory;
use Carbon\Carbon;
use App\ActivityLog;



class ProfileController extends Controller
{

	public function __construct()
    {
        Auth::shouldUse('user');

    }

    public function index()
    { 	$id = auth::id();
    	//$posts = Member::find(Auth::id())->Post;

        $Post_Categories = PostCategory::all();
   		$member_section = Section::find(Auth::user()->section_id);
    	$sections = Section::all();



        $posts = Post::with('category')->where('member_id', Auth::user()->id)->orderby('id', 'desc')->where('posts.status','!=','archived')->get();

        $logs = ActivityLog::where("member_id", $id)->orderBy('id', 'desc')->paginate(5);


         // $posts = DB::table('posts')
         //    ->join('post_categories', 'post_categories.id', '=', 'posts.post_category_id')
         //    ->select('posts.*','post_categories.type')
         //    ->orderby('posts.id','DESC')
         //    ->get();


        if (Route::current()->getName() == 'profile.post'){
        	return view('profile/post', compact('member_section','sections','Post_Categories','posts', 'logs'));
        }
        if (Route::current()->getName() == 'profile.notif'){
            return view('profile/notification', compact('member_section','sections','Post_Categories','posts'));
        }
        if (Route::current()->getName() == 'profile.actlog'){
            return view('profile/activityLog', compact('member_section','sections','Post_Categories','posts'));
        }
    }

    public function edit(Request $request, $id)
    {
    	
    	$member = Member::find($id);
        // $current_section_code = Section::find($member->section_id)->section_code;
        $remember_token = $request->_token;
        //$section_id = Section::getId('App\Section','section_code',$request->section);

	    if($request->password)
        {$this->validate($request, ['password' => 'min:8']);}

        if($request->email !== $member->email)
        {$this->validate($request, ['email' => 'required|email|max:255|unique:members']);}

        if($request->username !== $member->username)
        {$this->validate($request, ['username' => 'required|max:255|unique:members']);}

        if($request->id_num !== $member->id_num)
        {$this->validate($request, ['id_num' => 'required|max:255|unique:members']);}

        $this->validate($request, [
        'avatar' => 'mimes:jpeg,bmp,png,jpg', //image only
        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'mobile_no' => 'required|max:11',
        'guardian_mobile_no' => 'max:11',

    	]);

        //dd($request);


       // image upload in public/images/Members folder.
        $file = $request->file('avatar');
        if($request->hasFile('avatar'))
        {
        	$originalavatar = $member->avatar;
            if($originalavatar !== "members/default.png" )
            {
                 if(url('/') == 'http://mtics-ma.tuptaguig.com'){
                      File::delete('public/images/' . $originalavatar);
                    }
                  else{ 
                      File::delete('images/' . $originalavatar);
                    }
                  
            }

         if(url('/') == 'http://mtics-ma.tuptaguig.com'){
            $file->move('public/images/members', $request->username.$file->getClientOriginalName());
            }
          else{ 
            $file->move('images/members', $request->username.$file->getClientOriginalName());
            }
        $imgname =  "members/".$request->username.$request->avatar->getClientOriginalName();
        Member::where('id', $id)->update(array('avatar' => $imgname));
        }


        if($request->password)
        {
            Member::where('id', $id)->update(array('password' => bcrypt($request['password'])));
        }

         //CHECK IF ALUMNI
        // if($request->section != $current_section_code)
        // {
        //     if($request->section == 'ALUMNI')
        //     {
        //     Member::where('id', $id)->update(array('status' => 'inactive'));
        //     }
        //     else
        //     {
        //     Member::where('id', $id)->update(array('status' => 'active'));
        //     }
        // }



        Member::where('id', $id)->update(array('first_name' => $request->first_name));
        Member::where('id', $id)->update(array('last_name' => $request->last_name));
        //Member::where('id', $id)->update(array('section_id' => $section_id));
        Member::where('id', $id)->update(array('username' => $request->username));
        Member::where('id', $id)->update(array('id_num' => $request->id_num));
        Member::where('id', $id)->update(array('email' => $request->email));
        Member::where('id', $id)->update(array('mobile_no' => $request->mobile_no));
        Member::where('id', $id)->update(array('age' => $request->age));
        Member::where('id', $id)->update(array('birthday' => $request->birthday));
        Member::where('id', $id)->update(array('address' => $request->address));
        Member::where('id', $id)->update(array('work' => $request->work));
        Member::where('id', $id)->update(array('guardian_name' => $request->guardian_name));
        Member::where('id', $id)->update(array('guardian_mobile_no' => $request->guardian_mobile_no));
        Member::where('id', $id)->update(array('remember_token' => $remember_token));



        Event::fire(new SystemEvent($id, "Profile has been updated."));


    	return redirect('member/profile/post')->with('success', 'Saved!');
    }



}
