
@extends('layout/layout')
@section('title')

{{$form->title}}

@endsection

@section('content')

<style type="text/css">
    .survey-progress {
        position: fixed;
    }
    @media only screen and (max-width: 1365px) {
        .main {
          margin-top: 50px;
        }
    }

    @media only screen and (min-width: 1024px) {
        .survey-progress {
          width: 300px;
        }
    }

    @media (min-width: 1366px) {
        .main {
          margin-top: 120px;
        }
        .survey-progress {
            width: 350px;
        }
    }
</style>

<div class="container main">
    <div class="row w3-padding">
        <h3>
            <i class="fa fa-pie-chart fa-fw w3-xxlarge"></i>
            <strong>{{$form->title}}</strong>
        </h3>
    </div>

    <hr>

    <div class="row w3-padding">
        <div class="col-lg-8 col-md-8">
            <form action="{{url('admin/manage-form/answer/'.$form->id.'/'. count($form->question))}}" method="post" id="formSurvey">

                {!! csrf_field() !!}

                @foreach ($form->question as $key => $question)
                <h4>{{++$key}}. {{$question->question}}</h4>

                <br>

                @foreach($question->option as $option)
                @if($question->option_format == 'choice')
                    <input type="radio" name="answer{{$key}}[]" value="{{$option->id}}" required onclick="updateProgress({{$key}})"> {{$option->option}}<br>
                    @elseif($question->option_format == 'check')
                    <input type="checkbox" name="answer{{$key}}[]" required> {{$option->option}}<br>
                @endif
                @if($option->image)
                <div class="w3-container w3-margin-top w3-margin-bottom">
                <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$option->image) : asset('images/'.$option->image)}}" style="width: 50%">
                </div>
                @endif
                @endforeach

                <br>

                <hr>
                @endforeach

            </form>

        </div>

        <div class="col-lg-4 col-md-4">
            <div class="panel panel-danger w3-hide-small w3-hide-medium survey-progress" id="progressPanel">
                <div class="panel-heading">Progress</div>
                <div class="panel-body">
                    <div class="progress">
                      <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" id="progressBarForm"
                      aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                      </div>
                    </div>

                    <hr>

                    @foreach ($form->question as $key => $question)
                        <i class="fa fa-circle-o fa-fw w3-text-red" id="item-{{++$key}}"></i> {{$key}}
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    @if($flag == 'answer')
    <div class="row w3-margin-bottom" align="center">
        <button class="btn btn-success" type="submit" form="formSurvey">Submit <i class="fa fa-arrow-circle-o-right fa-fw w3-large"></i></button>
    </div>
    @endif
</div>

<script type="text/javascript">
    var ctr = 0;
    var progress = 0;
    var current_id = 0;
    function updateProgress(id) {
        if (progress != 100 && current_id != id) {
            ctr += 1;
            progress = (ctr/{{count($form->question)}}) * 100;
            progress = Math.round(progress);

            $("#item-" + id).attr('class', 'fa fa-circle fa-fw w3-text-green');

            current_id = id;

            if (ctr == {{count($form->question)}}) {
                progress = 100;
                $("#progressPanel").attr('class', 'panel panel-success');
                $("#progressBarForm").attr('style', 'width:100%;');
                $("#progressBarForm").attr('class', 'progress-bar progress-bar-success progress-bar-striped');
                $("#progressBarForm").attr('aria-valuenow', '100');
                $("#progressBarForm").html('100%');
            } else {
                $("#progressBarForm").attr('style', 'width:' + progress + '%;');
                $("#progressBarForm").attr('aria-valuenow', progress);
                $("#progressBarForm").html(progress + '%');
            }
        }
    }
</script>
@endsection
