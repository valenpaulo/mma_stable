<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipRepairReport extends Model
{
    protected $table = 'equip_repair_reports';
	protected $fillable = [
        'equip_inventory_id','admin_id','report_desc','created_at','updated_at'
    ];

    public function equip_inv()
    {
        return $this->belongsto(EquipInventory::class,'equip_inventory_id');
    }
}
