@extends('layout/control_panel')

@section('title')
Manage Letter
@endsection

@section('middle')
<div class="w3-container w3-margin">

`<div class="row">
    <h3>
      <i class="fa fa-file-word-o fa-fw w3-xxlarge"></i>
      <strong>Manage Letters</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">

    <a href="#add" class="w3-text-gray" data-toggle="modal"><i class="fa fa-plus-square-o fa-fw"></i>Add</a>

  </div>

<!--
      <form action="{{route('admin.internal')}}" method="POST">
      {{ csrf_field() }}
        <div class="w3-display-container">
          <input type="text" class="form-control" placeholder="Search" name="q">
          <div class="w3-display-right">
            <button class="btn btn-default w3-light-gray w3-text-black" type="submit">
              <i class="glyphicon glyphicon-search"></i>
            </button>
          </div>
        </div>
      </form> -->

  <div class="row w3-margin-topp">
    @if(isset($documents))
    @foreach($documents as $document)
      <div class="col-sm-3 w3-center">

        <div class="w3-card-4 w3-pale-yellow w3-margin">
          <div class="w3-container w3-padding">

          <div align="right">
            <a href="{{url('admin/internal/download/'.$document->id)}}"><i class="fa fa-cloud-download fa-fw w3-large w3-text-green w3-hover-opacity"></i></a>

            <a href="#delete-{{$document->id}}" data-toggle="modal"><i class="fa fa-trash-o fa-fw w3-large w3-text-red w3-hover-opacity"></i></a>
          </div>

          <hr>

          <div class="w3-center">
          <h1>
           <i class="fa fa-file-word-o  w3-text-black"></i>
          </h1>
          </div>

          <a href="">
          <div class="w3-center w3-text-black">
          <a href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/documents/'.$document->filename) : asset('documents/'.$document->filename)}}" target="_blank" class="w3-text-black">
            <p>{{explode("/", $document->filename)[2]}}</p>
          </a>
          <p>Category: {{$document->category->category_name}}</p>
          </div>
          </a>

          </div>

        </div>

      </div>

      <!-- Delete MODAL -->
        <div class="modal fade" id="delete-{{$document->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Delete Letter</h4>
        </div>
        <form action="{{url('admin/internal/delete/'.$document->id)}}" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="w3-container">
            <div class="row">

              <div class="form-group">
              <p>Are you sure you want to delete <strong>{{explode("/", $document->filename)[2]}}</strong>?</p>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-default w3-red w3-text-white"><i class="fa fa-trash fa-fw"></i> Delete</button>
        </div>

        </form>
        </div>
        </div>
        </div>
      <!-- END DELETE MODAL -->
    @endforeach
    @endif
  </div>

  <!-- ADD MODAL -->
  <div class="modal fade" id="add" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Letter</h4>
  </div>
  <form action="{{route('admin.internal.store')}}" method="POST" enctype="multipart/form-data">
  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

        <div class="form-group filename" style="display: none">
              <p id="import-file-name"></p>
        </div>

        <div class="form-group">
          <div align="right">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <label for="import_file" style="cursor: pointer;" class="btn btn-default w3-red" title="Bulk Add" align="right"><i class="fa fa-paperclip fa-fw w3-text-white w3-large"></i></label>
            <input type="file" name="import_file" id="import_file" style="opacity: 0; position: absolute; z-index: -1;" />
            <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-plus-square"></i> Add</button>
          </div>
        </div>

      </div>
    </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->

</div>

<script type="text/javascript">
  var input = document.getElementById( 'import_file' );
  var infoArea = document.getElementById( 'import-file-name' );

  input.addEventListener('change', showFileName);

  function showFileName( event ) {

    $(".filename").show()
    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  }

</script>

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>
@endsection
