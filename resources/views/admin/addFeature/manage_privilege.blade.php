@extends('layout/control_panel')

@section('title')
Manage Privilege
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-key fa-fw w3-xxlarge"></i>
      <strong>Manage Privilege</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">
    <a href="#addAdviser" class="w3-text-gray" data-toggle="modal" style="outline: 0">
      <i class="fa fa-gear fa-fw"></i>
      Manage Adviser
    </a>
  </div>

  <!-- MTICS ADVISER  -->
  <div class="modal fade" id="addAdviser" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title w3-text-gray" id="myModalLabel">Manage MTICS Adviser</h4>
        </div>
        <form action="{{ url('admin/manage-privilege/mtics-adviser') }}" method="POST">
        <div class="modal-body">

        <div class="w3-container">
          <div class="row" align="center">
            <div class="dropdown">
              <button class="btn btn-default dropdown-toggle form-control" type="button" data-toggle="dropdown" id="adviserSelect">'Select Adviser'
              <span class="caret"></span></button>
              <ul class="dropdown-menu" style="right: 0">
                <input class="form-control" id="myInput" type="text" placeholder="Search..">
                @foreach($faculty->member as $member)
                    <li value="{{$member->id}}"><a href="javascript:void(0)" onclick="setAdviserName('{{$member->first_name}} {{$member->last_name}}', {{$member->id}})">{{$member->first_name}} {{$member->last_name}}</a></li>
                @endforeach
              </ul>
            </div>

            <input type="hidden" name="member_id" id="member_id" value="">

           <br>
            <p><b>Current MTICS Adviser</b></p>
            @if(!$mtics_adviser->isEmpty())
            @foreach($mtics_adviser as $adviser)
            <p>
              - {{$adviser->member->first_name . ' ' . $adviser->member->last_name}} 
              <a class="w3-red" href="{{url('admin/manage-privilege/mtics-adviser/remove/'.$adviser->id)}}" data-toggle="tooltip" data-placement="bottom" title="remove"><i class="fa fa-trash"></i></a>

            </p>
            @endforeach
            @else
            <p>Please set MTICS Adviser</p>
            @endif
            <p></p>
          </div>
        </div>



        </div>

        <div class="modal-footer">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- END MTICS ADVISER -->

  <div class="row w3-margin-top">

    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#role" class="w3-text-black">Privilege</a></li>

      <li><a data-toggle="tab" href="#officer" class="w3-text-black">Officer</a></li>
    </ul>
  </div>

  <div class="tab-content">
  <!-- start of inventory -->
  <div id="role" class="tab-pane fade in active">

  <div class="row w3-margin-top">
  <div class="table-responsive">
    <table class="table table-bordered table-hover" id="privilegeTable">
      <thead>
        <tr>
         <th class="w3-center">First Name</th>
         <th class="w3-center">Last Name</th>
         <th class="w3-center">Member Roles</th>
         <th class="w3-center">Member Actions</th>
         <th class="w3-center">Action</th>
        </tr>
      </thead>
      <tbody class="w3-text-gray" id="roleBody">
        @foreach($members as $member)
        <tr>
          <td class="w3-center">{{$member->first_name}}</td>
          <td class="w3-center">{{$member->last_name}}</td>
          <td class="w3-center">
            @foreach($adminmembers as $adminmember)
              @if($adminmember->username == $member->username)
             - {{$adminmember->display_name}}
              <br>
              @endif
            @endforeach
          </td>
          <td class="w3-center">
            @foreach($actionMembers as $actionmember)
              @if($actionmember->member->username == $member->username)
             - {{$actionmember->action->action_display_name}}
              <br>
              @endif
            @endforeach
          </td>
          <td class="w3-center">
            <a class="w3-text-orange" href="#role-{{$member->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-id-badge fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Set Role"></i></a>

            <a class="w3-text-red" href="#action-{{$member->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-legal fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Set Action"></i></a>

          </td>
        </tr>

<!-- EDIT ROLE -->

        <div class="modal fade" id="role-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Manage Role</h4>
              </div>

              <form action="{{ url('admin/manage-privilege/role/edit/'.$member->id) }}" method="POST" enctype="multipart/form-data">

              <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">



                @if(in_array($member->username, $member_with_role))
                @foreach($roles as $role)
                @if(in_array($role->id, $role_member[$member->username]))
                <div class="checkbox">
                  <label><input type="checkbox" name="roles[]" value="{{$role->id}}" checked>{{$role->display_name}}</label>
                </div>
                @else
                <div class="checkbox">
                  <label><input type="checkbox" name="roles[]" value="{{$role->id}}">{{$role->display_name}}</label>
                </div>
                @endif
                @endforeach
                @else
                @foreach($roles as $role)
                <div class="checkbox">
                  <label><input type="checkbox" name="roles[]" value="{{$role->id}}">{{$role->display_name}}</label>
                </div>
                @endforeach
                @endif
              </div>
              <div class="modal-footer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
              </div>
              </form>
            </div>
          </div>
        </div>
<!-- END EDIT ROLE -->

<!-- EDIT ACTION -->
        <div class="modal fade" id="action-{{$member->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Manage Action</h4>
              </div>

              <form action="{{url('admin/manage-privilege/action/edit/'.$member->id)}}" method="POST" enctype="multipart/form-data">

              <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @if(in_array($member->username, $member_with_action))
                @foreach($actions as $action)
                @if(in_array($action->id, $action_member[$member->username]))
                <div class="checkbox">
                  <label><input type="checkbox" name="actions[]" value="{{$action->id}}" checked>{{$action->action_display_name}}</label>
                </div>
                @else
                <div class="checkbox">
                  <label><input type="checkbox" name="actions[]" value="{{$action->id}}">{{$action->action_display_name}}</label>
                </div>
                @endif
                @endforeach
                @else
                @foreach($actions as $action)
                <div class="checkbox">
                  <label><input type="checkbox" name="actions[]" value="{{$action->id}}">{{$action->action_display_name}}</label>
                </div>
                @endforeach
                @endif
           </div>
              <div class="modal-footer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save"></i> Save</button>
              </div>
              </form>
            </div>
          </div>
        </div>

<!-- END EDIT ACTION -->

        @endforeach
      </tbody>
    </table>
  </div>
  </div>

  <!-- end role tab -->
  </div>

  <div id="officer" class="tab-pane fade in">
    <div class="row w3-margin-top">
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="officersTable">
      <thead>
        <tr>
         <th class="w3-center">Position</th>
         <th class="w3-center">Assigned Person</th>
         <th class="w3-center">Action</th>
        </tr>
      </thead>
      <tbody class="w3-text-gray" id="roleBody">
        @foreach($officers as $officer)
        <tr>
          <td class="w3-center">{{$officer->display_name}}</td>
          <td class="w3-center">{{(count($officer->member) > 0) ? $officer->member[0]->first_name : ''}} {{(count($officer->member) > 0) ? $officer->member[0]->last_name : ''}}</td>
          <td class="w3-center">
            <a class="w3-text-green" href="#change-{{$officer->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-gear fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Set Role"></i></a>
          </td>
        </tr>


        <div class="modal fade" id="change-{{$officer->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Manage Officer</h4>
              </div>
            <form action="{{ url('admin/president/manage-officers/'.$officer->id.'/change') }}" method="POST" >
              <div class="modal-body">
                <div class="form-group">
                  <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle form-control" type="button" data-toggle="dropdown" id="officerSelect-{{$officer->id}}">Select From Member
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu" style="right: 0">
                      <input class="form-control" id="myInputOfficers-{{$officer->id}}" type="text" placeholder="Search..">
                      @foreach($memberNoRole as $member)
                      <li><a href="javascript:void(0)" onclick="setOfficerName( '{{$member->first_name}} {{$member->last_name}}', {{$member->id}}, {{$officer->id}} )">{{$member->first_name}} {{$member->last_name}}</a></li>
                      @endforeach
                    </ul>
                  </div>
                </div>

                <input type="hidden" name="member_id" id="member_id-officer-{{$officer->id}}" value="">

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                </div>

              </div>
            </form>
            </div>
          </div>
        </div>

        <script type="text/javascript">
            $("#myInputOfficers-{{$officer->id}}").on("keyup", function() {
              var value = $(this).val().toLowerCase();
              $(".dropdown-menu li").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              });
            });
        </script>

        @endforeach
      </tbody>
      </table>
    </div>
    </div>
  <!-- end officer tab -->
  </div>

  </div>

</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#privilegeTable').DataTable();
  $('#officersTable').DataTable();

  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
} );
</script>

<script>
function setAdviserName(name, id) {

  $("#adviserSelect").html(name + '<span class="caret"></span></button>');
  $("#member_id").attr('value', id);
}

function setOfficerName(name, member_id, officer_id) {

  $("#officerSelect-" + officer_id).html(name + ' <span class="caret"></span></button>');
  $("#member_id-officer-" + officer_id).attr('value', member_id);
}
</script>
@endsection
