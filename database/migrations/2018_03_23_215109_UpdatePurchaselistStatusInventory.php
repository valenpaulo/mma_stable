<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePurchaselistStatusInventory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
        Schema::table('mtics_purchase_lists', function ($table) {
            $table->dropColumn('mtics_inv_status');
        });

        Schema::table('mtics_purchase_lists', function ($table) {
            $table->enum('mtics_inv_status',['pending','moved','unable to move','unmoved'])->default('pending');
        });

        Schema::table('event_purchase_lists', function ($table) {
            $table->dropColumn('event_inv_status');
        });

        Schema::table('event_purchase_lists', function ($table) {
            $table->enum('event_inv_status',['pending','moved','unable to move','unmoved'])->default('pending');
        });
    
         Schema::table('mtics_budget_expenses', function ($table) {
            $table->integer('receipt_id')->after('expense_amt')->nullable();
            
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
