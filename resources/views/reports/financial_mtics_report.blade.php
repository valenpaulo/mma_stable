<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/w3.css') : asset('css/w3.css') }}">
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/bootstrap.min.css') : asset('css/bootstrap.min.css') }}">
    <link href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/fullcalendar.css') : asset('css/fullcalendar.css')}}" rel='stylesheet' />
    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/jquery-3.2.1.min.js') : asset('js/jquery-3.2.1.min.js')}}"></script>

    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/font-awesome.min.css') : asset('css/font-awesome.min.css') }}">
    <link rel="shortcut icon" href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.ico') : asset('images/logo/mtics.ico')}}" />

    <title>Financial Report</title>
</head>
<body>

<div class="container">
	<div class="row" align="center">
		<img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mticsBanner.png') : asset('images/logo/mticsBanner.png')}}"  style="width: 85%">
		<h2>Financial Report</h2>
		<p>MTICS Fund (S.Y. {{$fromyear}} - {{$toyear}})</p>
	</div>
</div>

<div class="container" style="padding-left: 5em; padding-right: 5em;">

	<div class="row w3-margin-top">
		<h3>I. Bank Transaction</h3>
	</div>
		<div style="padding-left: 5em;">
			<div class="row w3-margin-top">

			<p><strong> Turn Over MTICS Fund</strong> : Php {{$mticsbankamount_turnover + $excess_budget}}.00</p>
			<p><strong> MTICS Fund in Bank ({{$fromyear}} - {{$toyear}})</strong>: Php {{$total_batch_money < 0 ? $total_batch_money*-1 : $total_batch_money  }}.00
			<?php $mticsbank = $total_batch_money < 0 ? $total_batch_money*-1 : $total_batch_money; ?> 
			</p>	
			</div>
		</div>
	<hr>
	<div style="padding-left: 5em;">
	<div class="row w3-margin-top">
		<h4>1. Deposit Transaction:</h4>
	</div>

	<div class="row">
		<div class="table-responsive">
          <table class="w3-table">
            <thead>
              <tr>
               <th>Source</th>
               <th class="w3-right">Amount</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">
            @foreach($deposits as $deposit)
             <tr>
               <td>{{$deposit->deposit_source}}</td>
               <td class="w3-right">Php {{$deposit->deposit_amt}}.00</td>
              </tr>
            @endforeach
           </tbody>
          </table>
		</div>

		<hr>

		<div class="table-responsive">
          <table class="w3-table">
            <tbody>
             <tr>
	            <td><p><strong>Total Cash Deposit</strong></p></td>
              	<td class="w3-right">Php {{$totaldeposit}}.00
             </tr>
           </tbody>
          </table>
		</div>
	</div>


	<div class="row w3-margin-top">
		<h4>2. Withdraw Transaction:</h4>
	</div>

	<div class="row">
		<div class="table-responsive">
          <table class="w3-table">
            <thead>
              <tr>
               <th>Reason</th>
               <th class="w3-right">Amount</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">
            @foreach($withdrawals as $withdrawal)
             <tr>
               <td>{{$withdrawal->withdrawal_reason}}</td>
               <td class="w3-right">Php {{$withdrawal->withdrawal_amt}}.00</td>
              </tr>
            @endforeach
           </tbody>
          </table>
		</div>

		<hr>

		<div class="table-responsive">
          <table class="w3-table">
            <tbody>
             <tr>
	            <td><p><strong>Total Cash Withdraw</strong></p></td>
              	<td class="w3-right">Php {{$totalwithdraw}}.00</td>
             </tr>
           </tbody>
          </table>
		</div>
	</div>
	</div>

	<?php $expensestotal=0; ?>
	<?php $expensestotalall=0; ?>

	<div class="row">
		<h3>II. Expenses</h3>
	</div>

	<hr>

	<div style="padding-left: 5em;">

	@foreach($monthly_budgets as $key => $monthly_budget)
	<div class="row w3-margin-top">
		<h4>{{++$key}}. Monthly Budget: {{$monthly_budget->budget_name}} (Php {{$monthly_budget->budget_amt}}.00)</h4>
	</div>



	<div class="row">
		<div class="table-responsive">
	      <table class="w3-table">
	        <tbody class="w3-text-gray">
		    @foreach($monthly_budget->budget_breakdown as $budget_breakdown)
		    @if(!$budget_breakdown->budget_expenses->isEmpty())
	        @foreach($budget_breakdown->budget_expenses as $budget_expenses)
	         <tr>
	            <td>- {{$budget_expenses->expense_name}}</td>
	           <td class="w3-right">Php {{$budget_expenses->expense_amt}}.00</td>
	         </tr>
	         <?php $expensestotal = $expensestotal + $budget_expenses->expense_amt; ?>
	        @endforeach
			@endif
		    @endforeach
	       </tbody>
	      </table>
		</div>
	</div>

    <?php $expensestotalall = $expensestotalall + $expensestotal;  $expensestotal = 0; ?>
    <hr>
	@endforeach

	<div class="row">
		<h4>Purchased Request Expenses</h4>
	</div>

	<div class="row">
		<div class="table-responsive">
	      <table class="w3-table">
	        <tbody class="w3-text-gray">
			@foreach($externaltasks as $externaltask)
			@foreach($externaltask->mticspurchaselist as $mticspurchaselist)
	          <tr>
		        <td>- {{$mticspurchaselist->mtics_itemname}} ({{$mticspurchaselist->mtics_act_quan}} pc/s)</td>
		        <td class="w3-right">Php {{$mticspurchaselist->mtics_total_price}}.00</td>
	          </tr>
	         <?php $expensestotal = $expensestotal + $mticspurchaselist->mtics_total_price; ?>
	        @endforeach
	        <?php $expensestotalall = $expensestotalall + $expensestotal; $expensestotal = 0; ?>
			@endforeach
	       </tbody>
	      </table>
		</div>

		<hr>

		<div class="table-responsive">
          <table class="w3-table">
            <tbody>
             <tr>
	            <td><p><strong>Total Expenses</strong></p></td>
              	<td class="w3-right">Php {{$expensestotalall}}.00</td>
             </tr>
             @if($penalties > 0)
             <tr>
              <td><p><strong>Penalty Fee</strong></p></td>
                <td class="w3-right">Php {{$penalties}}.00</td>
             </tr>
             @endif
           </tbody>
          </table>
		</div>

		<hr>

		<div class="table-responsive">
          <table class="w3-table">
          	<thead>
          		<th>Withdraw</th>
          		<th>Expenses</th>
          		<th >Total Cash in Hand</th>
          		<th >Total MTICS Fund</th>
          	</thead>
            <tbody>
              <tr>
              	<td>Php {{$totalwithdraw}}.00</td>
              	<td>Php {{$expensestotalall}}.00</td>
              	<td >Php {{$totalwithdraw - $expensestotalall + $excess_budget + $penalties}}.00</td>
              	<td >Php {{$totalwithdraw - $expensestotalall + $mticsbank + $excess_budget + $penalties}}.00</td>

              </tr>
           </tbody>
          </table>
		</div>

</div>

<br>
<p><strong>Prepared by:</strong></p>
          @foreach($roles as $role)
            @if($officerterm->status !== 'on-going')
              @if(!$role->prev_member->isEmpty())
                @foreach($role->prev_member as $member)
                    @if($role->name == 'president')
                      <br>
                      <p><strong>Approved by:</strong></p>
                    @endif
              <p>{{$member->first_name}} {{$member->last_name}}</p>
              <p><i>{{$role->display_name}}</i></p>
                @endforeach
              @endif
            @else
              @if(!$role->member->isEmpty())
                @foreach($role->member as $member)
                @if($role->name == 'president')
                  <br>
                  <p><strong>Approved by:</strong></p>
                @endif
                @if($member !== 'null')
              <p>{{$member->first_name}} {{$member->last_name}}</p>
              <p><i>{{$role->display_name}}</i></p>
                @endif
                @endforeach
              @endif
            @endif

          @endforeach

</body>
</html>


<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    window.print()
  });
</script>
