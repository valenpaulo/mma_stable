@extends('admin/layout/admin_panel')

@section('middle')
EVENT PAYMENT:<h1>{{$eventbucket}}</h1>



<br><br><br>
<div class="container">
<div class="row">
  <div class="w3-margin w3-card-4 w3-white">
    <table class="table table-hover table-responsive">
      <thead class="w3-teal">
        <tr>
         <th class="w3-center">Name</th>
         <th class="w3-center">Recipient</th>
         <th class="w3-center">payment</th>
         <th class="w3-center">Status</th>
         <th class="w3-center">Created At</th>
                       

        </tr>
      </thead>
      <tbody class="w3-text-gray">
        @foreach($payments as $payment)
       
        <tr>
          <td class="w3-center">{{$payment->member->first_name}} {{$payment->member->last_name}}</td>
          <td class="w3-center">{{$payment->admin->first_name}} {{$payment->admin->last_name}}</td>
          <td class="w3-center">{{$payment->payevent_amt}}</td>
          <td class="w3-center">{{$payment->paymevent_status}}</td>
          <td class="w3-center">{{$payment->created_at}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>

@endsection