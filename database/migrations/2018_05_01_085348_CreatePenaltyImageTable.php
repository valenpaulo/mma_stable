<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenaltyImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
         Schema::create('penalty_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penalty_id');
            $table->integer('admin_id');
            $table->longtext('image_name');
            $table->timestamps();
        });


        Schema::create('meeting_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meeting_id');
            $table->integer('admin_id');
            $table->longtext('image_name');
            $table->timestamps();
        });


         Schema::table('event_expenses', function ($table) {
            $table->integer('receipt_id')->nullable();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
