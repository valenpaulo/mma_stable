<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    protected $fillable = [
        'member_id', 'log',
    ];

    public function member() {
        return $this->belongsTo(Member::class, 'member_id');
    }
}
