<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmountValidation extends Model
{
    protected $fillable = [
        'money_request_id', 'status',
    ];
}
