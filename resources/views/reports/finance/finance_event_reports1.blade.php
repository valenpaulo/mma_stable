@extends('layout/control_panel')

@section('title')
Financial Summary
@endsection

@section('middle')
<div class="container-fluid w3-margin">
	<div class="row">
        <h3>
          <i class="fa fa-file-text-o fa-fw w3-xxlarge"></i>
          <strong>Financial Statement Verification</strong>
        </h3>
    </div>

    <hr>

	<div class="row w3-margin-top">

		@if(!$auditapprove->isEmpty())
		<a href="#submit" class="w3-text-black" data-toggle="modal"><i class="fa fa-print fa-fw"></i>Print</a>
		@elseif($auditapprove->isEmpty())
		<a href="#verify" class="w3-text-black" data-toggle="modal" title="Auditor needs to verify this Financial Statement"><i class="fa fa-check-circle fa-fw" ></i> Verify</a>
		@endif

		<i class="fa fa-ellipsis-v fa-fw"></i>

		<a href="javascript:void(0)" class="w3-text-black" onclick="showSummary()"><i class="fa fa-file-text fa-fw"></i> Summary</a>
	</div>

	<!-- VERIFY -->
	<div class="modal fade" id="verify" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-md" role="document">
	<div class="modal-content modal-content-info">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	<form action="{{ url('admin/finance/financial-reports/event/view-month/'.$event->id.'/verify') }}" method="POST" enctype="multipart/form-data">

	  <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">Verify</h4>
	  </div>
	  <div class="modal-body modal-body-info">
	  <div class="w3-container">
	    <div class="row">
	    	<p>Are you sure you want to proceed?</p>

	         <div class="form-group">
	            <label>Remarks:</label>
	            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="remarks" placeholder="Remarks" rows="3"></textarea>
	          </div>

	    </div>
	  </div>
	  </div>

	  <div class="modal-footer">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<button type="submit" class="btn btn-default w3-green w3-text-white" title="Edit"> Yes</button>
		<button type="button" data-dismiss="modal" class="btn btn-default w3-red w3-text-white" title="Edit"> No</button>
	  </div>

	</form>
	</div>
	</div>
	</div>
	<!-- END VERIFY -->

	<!-- PRINT-->
	<div class="modal fade" id="submit" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-sm" role="document">
	<div class="modal-content modal-content-info">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	<form action="{{ url('admin/finance/financial-reports/event/'.$event->id.'/view-reports/print') }}" method="POST" enctype="multipart/form-data">

	  <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">Print</h4>
	  </div>
	  <div class="modal-body modal-body-info">
	  <div class="w3-container">
	    <div class="row">
	        <p>Do you want to print the summary?</p>
	    </div>
	  </div>
	  </div>

	  <div class="modal-footer">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<button type="submit" class="btn btn-default w3-green w3-text-white" title="Edit">Yes</button>
		<button type="button" data-dismiss="modal" class="btn btn-default w3-red w3-text-white" title="Edit"> No</button>
	  </div>
	</form>
	</div>
	</div>
	</div>
	<!-- END PRINT -->


    <div class="row w3-margin-top">

        <div class="col-lg-3 col-md-3 col-sm-12"></div>

        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="w3-card-4 w3-amber w3-text-black w3-padding w3-container">

            <div class="w3-container">
              <div class="row" align="center">
                <h4>
                  <i class="fa fa-money w3-xxxlarge fa-fw"></i>
                  <strong>Total Payment Collection</strong>
                </h4>
              </div>
              <hr style="border-color: black">
              <div class="row">
                <h4>
                  <span>
                    <i class="fa fa-ellipsis-v fa-fw"></i>
                    <strong>P{{$eventpayments->sum('payevent_amt')}}.00</strong>
                  </span>
                </h4>
              </div>
            </div>

          </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="w3-card-4 w3-teal w3-text-black w3-padding">

            <div class="w3-container">
              <div class="row" align="center">
                <h4>
                  <i class="fa fa-calculator w3-xxxlarge fa-fw"></i>
                  <strong>Number of Student Paid:</strong>
                </h4>
              </div>

              <hr style="border-color: black">

              <div class="row">
                <h4>
                  <span>
                    <i class="fa fa-ellipsis-v fa-fw"></i>
                    <strong>{{count($eventpayments->groupby('member_id'))}} Student</strong>
                  </span>
                </h4>
              </div>
            </div>

          </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12"></div>

      </div>

      <hr>

   	<div id="summaryReport" style="display: none">

   		<div class="container">
		    <div class="row" align="center">
		    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mticsBanner.png') : asset('images/logo/mticsBanner.png')}}"  style="width: 85%">

		        <h2>{{$event->event_title}}</h2>
		        <p>Financial Report (S.Y. {{$fromyear}} - {{$toyear}})</p>

		    </div>
		</div>

		<div class="container" style="padding-left: 5em; padding-right: 10em;">

		    <div class="row w3-margin-top">
		      <div class="table-responsive">
		        <table class="w3-table">
		          <tbody>
		           <tr>
		              <td><p><strong>Total Collection</strong></p></td>
		              <td class="w3-right">Php {{$payment}}.00</td>
		           </tr>
		         </tbody>
		        </table>
		      </div>
		    </div>

		    <div class="row w3-margin-top">
		      <div class="table-responsive">
		        <table class="w3-table">
		          <tbody>
		            <?php $fromOtherTotal = 0; ?>
		            Other Collectibles
		            @foreach($fromOthers as $fromOther)
		           <tr>
		              <td><p><strong>{{$fromOther->deposit_desc}}</strong></p></td>
		              <td class="w3-right">Php {{$fromOther->deposit_amt}}.00</td>
		              <?php $fromOtherTotal = $fromOtherTotal + $fromOther->deposit_amt?>
		           </tr>
		            @endforeach
		         </tbody>
		        </table>
		      </div>
		    </div>

		    <hr>

		    <?php $expensestotal=0; ?>
		    <?php $expensestotalall=0; ?>

		    @if (count($expenses) > 0)
		    <div class="row">
		      <h3>Expenses</h3>
		    </div>

		    <div style="padding-left: 5em;">

		    <div class="row">
		        <div class="table-responsive">
		          <table class="w3-table">
		            <tbody class="w3-text-gray">
		            @foreach($expenses as $expense)
		              <tr>
		                <td><h4><b>{{$expense->breakdown_name}}</b></h4></td>
		                <td class="w3-right">Php {{$expense->expenses->sum('expense_amt')}}.00</td>
		              </tr>
				    	
				    	@foreach($expense->expenses as $details)
		              <tr>
		                <td>- {{$details->expense_name}}</td>
		                <td class="w3-right">Php {{$details->expense_amt}}.00</td>
		              </tr>
		              @endforeach

		              <?php $expensestotal = $expensestotal + $expense->expenses->sum('expense_amt'); ?>
		            @endforeach
		            <?php $expensestotalall = $expensestotalall + $expensestotal;  $expensestotal = 0; ?>
		           </tbody>
		          </table>
		        </div>

		    </div>
		    </div>

		    <hr>
		    @endif

		    <div class="row">
		        <h3>Purchased Items</h3>
		    </div>

		    <div style="padding-left: 5em;">

		    <div class="row">
		        <div class="table-responsive">
		          <table class="w3-table">
		            <tbody class="w3-text-gray">
		            @foreach($externaltasks as $externaltask)
		            @foreach($externaltask->eventpurchaselist as $eventpurchaselist)
		              <tr>
		                <td>- {{$eventpurchaselist->event_itemname}} ({{$eventpurchaselist->event_act_quan}} pc/s)</td>
		                <td class="w3-right">Php {{$eventpurchaselist->event_total_price}}.00</td>
		              </tr>
		              <?php $expensestotal = $expensestotal + $eventpurchaselist->event_total_price; ?>
		            @endforeach
		            <?php $expensestotalall = $expensestotalall + $expensestotal;  $expensestotal = 0; ?>
		            @endforeach
		           </tbody>
		          </table>
		        </div>

		        <hr>

		        <div class="table-responsive">
		          <table class="w3-table">
		            <tbody>
		             <tr>
		                <td><p><strong>Total Expenses</strong></p></td>
		                <td class="w3-right">Php {{$expensestotalall}}.00</td>
		             </tr>
		           </tbody>
		          </table>
		        </div>
		    </div>
		    </div>



		  <div class="row">
		    <div class="table-responsive">
		      <table class="w3-table">
		        <thead>
		            <th>Total Collection</th>
		            <th>Subsidy by MTICS fund</th>
		            <th>Total Expenses</th>
		            <th>Remaining Total Budget</th>
		            <th>Remaining Event Fund</th>
		        </thead>
		        <tbody>
		         <tr>
		          <?php $totalcollectibles = $fromOtherTotal + $payment?>
		            <td>Php {{$totalcollectibles}}.00</td>
		            <td>Php {{$fromMTICSfund}}.00</td>
		            <td>Php {{$expensestotalall}}.00</td>
		            <td>Php {{$eventwithdraws->sum('withdrawal_amt') - $expensestotalall}}.00</td>
		            <td>Php {{($totalcollectibles + $fromMTICSfund) - $eventwithdraws->sum('withdrawal_amt') + ($eventwithdraws->sum('withdrawal_amt') - $expensestotalall) }}.00</td>
		         </tr>
		       </tbody>
		      </table>
		    </div>
		  </div>


		  <br><br><br>
		  <p><strong>Prepared by:</strong></p>
          @foreach($roles as $role)
            @if($officerterm->status !== 'on-going')
              @if(!$role->prev_member->isEmpty())
                @foreach($role->prev_member as $member)
                    @if($role->name == 'president')
                      <br>
                      <p><strong>Approved by:</strong></p>
                    @endif
              <p>{{$member->first_name}} {{$member->last_name}}</p>
              <p><i>{{$role->display_name}}</i></p>
                @endforeach
              @endif
            @else
              @if(!$role->member->isEmpty())
                @foreach($role->member as $member)
                @if($role->name == 'president')
                  <br>
                  <p><strong>Approved by:</strong></p>
                @endif
                @if($member !== 'null')
              <p>{{$member->first_name}} {{$member->last_name}}</p>
              <p><i>{{$role->display_name}}</i></p>
                @endif
                @endforeach
              @endif
            @endif

          @endforeach
          <p></p>
   	</div>
   </div>

   	<!-- END SUMMARY -->



  <div id="financialStatement">

    <div class="row w3-margin-top">
        <h4>Deposit Transaction</h4>
    </div>

    <div class="row">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
               <th class="w3-center">Source</th>
               <th class="w3-center">Amount</th>
               <th class="w3-center">Description</th>
               <th class="w3-center">receipt</th>
               <th class="w3-center">status</th>

              </tr>
            </thead>
            <tbody class="w3-text-gray">
            @foreach($eventdeposits as $alldeposit)
             <tr>
               <td><a href="" class="w3-text-gray" data-toggle="modal" data-target="#infodeposit-{{$alldeposit->id}}"><i class="fa fa-exclamation-circle fa-fw"></i> {{$alldeposit->deposit_source}}</a></td>
               <td class="w3-center">{{$alldeposit->deposit_amt}}</td>
               <td class="w3-center">{{$alldeposit->deposit_desc}}</td>
               @if($alldeposit->receipt !== null)
               <td class="w3-center">
                <a href="" class="w3-text-gray" data-toggle="modal" data-target="#receiptdeposit-{{$alldeposit->id}}">
               <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $alldeposit->receipt->receipt_image) : asset('images/' . $alldeposit->receipt->receipt_image)}}" style="width: 100px"></a>

               </td>
               @else
               <td class="w3-center"></td>
               @endif
               <td class="w3-center">{{$alldeposit->deposit_status}}</td>
              </tr>

              <!-- INFO RECEIPT VIEW -->
              <div class="modal fade" id="receiptdeposit-{{$alldeposit->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-dialog-info modal-m" role="document">
              <div class="modal-content modal-content-info">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">Deposit Receipt</h4>
              </div>
              <div class="modal-body modal-body-info">
              <div class="w3-container">
                <div class="row">
               @if($alldeposit->receipt !== null)
                 <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $alldeposit->receipt->receipt_image) : asset('images/' . $alldeposit->receipt->receipt_image)}}" style="width: 500px"></a>
                @endif
                </div>
              </div>
              </div>


              </div>
              </div>
              </div>
              <!-- END INFO -->


              <!-- INFO DEPOSIT-->
              <div class="modal fade" id="infodeposit-{{$alldeposit->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-dialog-info modal-m" role="document">
              <div class="modal-content modal-content-info">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$alldeposit->deposit_source}}' Deposit Source Information</h4>
              </div>
              <div class="modal-body modal-body-info">
              <div class="w3-container">
                <div class="row">
                    @if($alldeposit->deny !== null)
                        <b>Denied</b><br>
                        Faculty Name: {{$alldeposit->deny->faculty->first_name}} {{$alldeposit->deny->faculty->last_name}}<br>
                        Denied Reason: {{$alldeposit->deny->denied_reason}}<br>

                        <div class="form-group">
                          <label>Denied by</label>
                          <input type="text" disabled class="form-control" value="{{$alldeposit->deny->faculty->first_name}} {{$alldeposit->deny->faculty->last_name}}">
                        </div>
                         <div class="form-group">
                          <label>Denied Reason</label>
                          <input type="text" disabled class="form-control" value="{{$alldeposit->deny->denied_reason}}">
                        </div>
                    @endif

                    @if($alldeposit->report !== null)
                       <b>Issue Report/s</b>
                        <div class="form-group">
                          <label>Reported by</label>
                          <input type="text" disabled class="form-control" value="{{$alldeposit->report->faculty->first_name}} {{$alldeposit->report->faculty->last_name}}">
                        </div>
                        <div class="form-group">
                          <label>Reported Title</label>
                          <input type="text" disabled class="form-control" value="{{$alldeposit->report->deposit_title}}">
                        </div>
                        <div class="form-group">
                          <label>Reported Reason</label>
                          <input type="text" disabled  class="form-control" value="{{$alldeposit->report->deposit_desc}}">
                        </div>
                        <div class="form-group">
                          <label>Solution</label>
                          <input type="text" disabled class="form-control" value="{{$alldeposit->report->solution}}">
                        </div>
                        <br><br>
                    @endif


                </div>
              </div>
              </div>


              </div>
              </div>
              </div>
              <!-- END INFO -->

            @endforeach
           </tbody>
          </table>
        </div>
    </div>


    <div class="row w3-margin-top">
        <h4>Withdraw Transaction:</h4>
    </div>

    <div class="row">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
               <th class="w3-center">Source</th>
               <th class="w3-center">Amount</th>
               <th class="w3-center">Description</th>
               <th class="w3-center">receipt</th>
               <th class="w3-center">status</th>

              </tr>
            </thead>
            <tbody class="w3-text-gray">
            @foreach($eventwithdraws as $allwithdrawal)
             <tr>
               <td><a href="" class="w3-text-gray" data-toggle="modal" data-target="#infowithdraw-{{$allwithdrawal->id}}"><i class="fa fa-exclamation-circle fa-fw"></i> {{$allwithdrawal->withdrawal_reason}}</a></td>
               <td class="w3-center">{{$allwithdrawal->withdrawal_amt}}</td>
               <td class="w3-center">{{$allwithdrawal->withdrawal_desc}}</td>
               @if($allwithdrawal->receipt !== null)
               <td class="w3-center">
                <a href="" class="w3-text-gray" data-toggle="modal" data-target="#receiptwithdraw-{{$allwithdrawal->id}}">
               <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $allwithdrawal->receipt->receipt_image) : asset('images/' . $allwithdrawal->receipt->receipt_image)}}" style="width: 100px"></a>
               </td>
               @else
               <td class="w3-center"></td>
               @endif
               <td class="w3-center">{{$allwithdrawal->withdrawal_status}}</td>
              </tr>

              <!-- INFO RECEIPT VIEW -->
              <div class="modal fade" id="receiptwithdraw-{{$allwithdrawal->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-dialog-info modal-m" role="document">
              <div class="modal-content modal-content-info">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">Withdraw Receipt</h4>
              </div>
              <div class="modal-body modal-body-info">
              <div class="w3-container">
                <div class="row">
               @if($allwithdrawal->receipt !== null)
                 <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $allwithdrawal->receipt->receipt_image) : asset('images/' . $allwithdrawal->receipt->receipt_image)}}" style="width: 500px">
                @endif
                </div>
              </div>
              </div>


              </div>
              </div>
              </div>
              <!-- END INFO -->
              <!-- INFO WITHDRAW-->
              <div class="modal fade" id="infowithdraw-{{$allwithdrawal->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-dialog-info modal-m" role="document">
              <div class="modal-content modal-content-info">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$allwithdrawal->withdrawal_reason}}' Withdrawal Information</h4>
              </div>
              <div class="modal-body modal-body-info">
              <div class="w3-container">
                <div class="row">
                    @if($allwithdrawal->deny !== null)
                        <b>Denied</b><br>
                        <div class="form-group">
                          <label>Denied by</label>
                          <input type="text" disabled class="form-control" value="{{$allwithdrawal->deny->faculty->first_name}} {{$allwithdrawal->deny->faculty->last_name}}">
                        </div>
                         <div class="form-group">
                          <label>Denied Reason</label>
                          <input type="text" disabled class="form-control" value="{{$allwithdrawal->deny->denied_reason}}">
                        </div>
                    @endif

                    <hr>
                    @if($allwithdrawal->report !== null)
                       <b>Issue Report/s</b>
                        <div class="form-group">
                          <label>Reported by</label>
                          <input type="text" disabled class="form-control" value="{{$allwithdrawal->report->faculty->first_name}} {{$allwithdrawal->report->faculty->last_name}}">
                        </div>
                        <div class="form-group">
                          <label>Reported Title</label>
                          <input type="text" disabled class="form-control" value="{{$allwithdrawal->report->deposit_title}}">
                        </div>
                        <div class="form-group">
                          <label>Reported Reason</label>
                          <input type="text" disabled  class="form-control" value="{{$allwithdrawal->report->deposit_desc}}">
                        </div>
                        <div class="form-group">
                          <label>Solution</label>
                          <input type="text" disabled class="form-control" value="{{$allwithdrawal->report->solution}}">
                        </div>
                        <br><br>


                    @endif


                </div>
              </div>
              </div>


              </div>
              </div>
              </div>
              <!-- END INFO -->
            @endforeach
           </tbody>
          </table>
        </div>
    </div>



    <div class="row w3-margin-top">
        <h4>Breakdown Expenses:</h4>
    </div>
    @foreach($expenses as $expense)

    	<h4> <a href="" class="w3-text-gray" data-toggle="modal" data-target="#infobreakdown-{{$expense->id}}">'{{$expense->breakdown_name}}'</a></h4>

    	<!-- INFO Breakdown-->
          <div class="modal fade" id="infobreakdown-{{$expense->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-dialog-info modal-sm" role="document">
          <div class="modal-content modal-content-info">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$expense->breakdown_name}}' Breakdown Information</h4>
          </div>
          <div class="modal-body modal-body-info">
          <div class="w3-container">
            <div class="row">

                    <div class="form-group">
                      <label>Breakdown Name</label>
                      <input type="text" class="form-control" value="{{$expense->breakdown_name}}" disabled>
                    </div>

                    <div class="form-group">
                      <label>Breakdown Description</label>
	                    <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" placeholder="Reason" rows="3" disabled>{{$expense->breakdown_desc}}</textarea>
                    </div>

                    <div class="form-group">
                      <label>Breakdown Amount</label>
                      <input type="text" class="form-control" value="{{$expense->breakdown_amt}}" disabled>
                    </div>


            </div>
          </div>
          </div>


          </div>
          </div>
          </div>
          <!-- END INFO -->

    <div class="row">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
               <th class="w3-center">Item</th>
               <th class="w3-center">Amount</th>
               <th class="w3-center">Description</th>
               <th class="w3-center">receipt</th>

              </tr>
            </thead>
            <tbody class="w3-text-gray">
    	@foreach($expense->expenses as $expense)
            	
             <tr>

               <td class="w3-center">{{$expense->expense_name}}</td>
               <td class="w3-center">{{$expense->expense_amt}}</td>
               <td class="w3-center">{{$expense->expense_desc}}</td>
               <td class="w3-center">
                @if($expense->receipt !== null)

               		<a href="" class="w3-text-gray" data-toggle="modal" data-target="#receipt-{{$expense->id}}">
	               <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $expense->receipt->receipt_image) : asset('images/' . $expense->receipt->receipt_image)}}" style="width: 100px"></a>
	             @endif
               </td>
              </tr>

              	<!-- INFO RECEIPT VIEW -->
              <div class="modal fade" id="receipt-{{$expense->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-dialog-info modal-m" role="document">
              <div class="modal-content modal-content-info">
              <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">Expenses Receipt</h4>
              </div>
              <div class="modal-body modal-body-info">
              <div class="w3-container">
                <div class="row">
                @if($expense->receipt !== null)

                 <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $expense->receipt->receipt_image) : asset('images/' . $expense->receipt->receipt_image)}}" style="width: 500px">
                 @endif
                </div>
              </div>
              </div>


              </div>
              </div>
              </div>
              <!-- END INFO -->


            @endforeach
           </tbody>
          </table>
        </div>
    </div>
            @endforeach


            	<div class="row w3-margin-top">
        <h3>Purchase Request Expenses</h3>
    </div>
    @foreach($externaltasks as $externaltask)
      @foreach($externaltask->moneyrequest_all as $moneyrequest)
      <h4> <a href="" class="w3-text-gray" data-toggle="modal" data-target="#infomoneyreq-{{$moneyrequest->id}}"> Money Request: P{{$moneyrequest->amount}}.00</a></h4>
      <!-- INFO Money Request-->
          <div class="modal fade" id="infomoneyreq-{{$moneyrequest->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-dialog-info modal-sm" role="document">
          <div class="modal-content modal-content-info">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$externaltask->task_name}}' Money Request Information</h4>
          </div>
          <div class="modal-body modal-body-info">
          <div class="w3-container">
            <div class="row">

                    <div class="form-group">
                      <label>Requested Money</label>
                      <input type="text" class="form-control" value="{{$moneyrequest->amount}}" disabled>
                    </div>
                    @if($moneyrequest->excess_amount !== null)
                     <div class="form-group">
                      <label>Excess Money</label>
                      <input type="text" class="form-control" value="{{$moneyrequest->excess_amount}}" disabled>
                    </div>
                    @endif

                    @if($moneyrequest->reimburse !== null)
                     <div class="form-group">
                      <label>Reimbursed Money</label>
                      <input type="text" class="form-control" value="{{$moneyrequest->reimburse->reimburse_amount}}" disabled>
                    </div>
                    @endif
                    <div class="form-group">
                      <label>Money Request Status</label>
                      <input type="text" class="form-control" value="{{$moneyrequest->mon_req_status}}" disabled>
                    </div>

            </div>
          </div>
          </div>


          </div>
          </div>
          </div>
          <!-- END INFO -->





    <div class="row">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
               <th class="w3-center">Expenses</th>
               <th class="w3-center">Amount</th>
               <th class="w3-center">Receipt</th>

              </tr>
            </thead>
            <tbody class="w3-text-gray">



            @foreach($externaltask->eventpurchaselist as $eventpurchaselist)
              <tr>
                <td><a href="" class="w3-text-gray" data-toggle="modal" data-target="#inforeqexpenses-{{$eventpurchaselist->id}}"><i class="fa fa-exclamation-circle fa-fw"></i> {{$eventpurchaselist->event_itemname}}({{$eventpurchaselist->event_act_quan}} pc/s)</a></td>
                <td class="w3-center">Php {{$eventpurchaselist->event_total_price}}.00</td>
                <td class="w3-center">
                  @foreach($eventpurchaselist->receipt as $receipt)
                  	@if($receipt !== null)
                    <a href="" class="w3-text-gray" data-toggle="modal" data-target="#receiptreqexpenses-{{$eventpurchaselist->id}}">
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $receipt->receipt_image) : asset('images/' . $receipt->receipt_image)}}" style="width: 100px"></a>
                    @endif
                  @endforeach
                </td>
              </tr>
                      <!-- Receipt Request to buy expenses -->
                      <div class="modal fade" id="receiptreqexpenses-{{$eventpurchaselist->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog modal-dialog-info modal-m" role="document">
                      <div class="modal-content modal-content-info">
                      <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                      <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$eventpurchaselist->event_itemname}}' Expense Receipt</h4>
                      </div>
                      <div class="modal-body modal-body-info">
                      <div class="w3-container">
                        <div class="row">
                           @foreach($eventpurchaselist->receipt as $receipt)
		                  	@if($receipt !== null)
                            <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/' . $receipt->receipt_image) : asset('images/' . $receipt->receipt_image)}}" style="width: 500px">
                            @endif
                           @endforeach

                        </div>
                      </div>
                      </div>


                      </div>
                      </div>
                      </div>
                      <!-- END INFO -->


                       <!-- INFO Request to buy expenses -->
                      <div class="modal fade" id="inforeqexpenses-{{$eventpurchaselist->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog modal-dialog-info modal-m" role="document">
                      <div class="modal-content modal-content-info">
                      <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                      <h4 class="modal-title w3-text-gray w3-center" id="myModalLabel">'{{$eventpurchaselist->event_itemname}}' Expense Information</h4>
                      </div>
                      <div class="modal-body modal-body-info">
                      <div class="w3-container">
                        <div class="row">

                           @if(!$eventpurchaselist->eventpurchaseequiplist->isEmpty())

                           @foreach($eventpurchaselist->eventpurchaseequiplist as $eventpurchaseequiplist)

                          <strong>Equipment Information</strong>

                           <div class="form-group">
                            <label>Brand Name</label>
                            <input type="text" disabled class="form-control" value="{{$eventpurchaseequiplist->event_brandname}}">
                          </div>

                          <div class="form-group">
                            <label>Serial No. </label>
                            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" placeholder="Reason" rows="3" disabled>{{$eventpurchaseequiplist->event_serialnum}}</textarea>
                          </div>

                           <div class="form-group">
                            <label>Specification </label>
                            <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reason" placeholder="Reason" rows="3" disabled>{{$eventpurchaseequiplist->event_specs}}</textarea>
                          </div>
                           @endforeach
                           @endif
                        </div>
                      </div>
                      </div>


                      </div>
                      </div>
                      </div>
                      <!-- END INFO -->


            @endforeach



            @endforeach
           </tbody>
          </table>
        </div>
    </div>
      @endforeach




</div>

</div>





<script type="text/javascript">
function showSummary() {
    if (document.getElementById('summaryReport').style.display == 'none'){
        $('#summaryReport').show();
        $('#financialStatement').hide();
    } else {
        $('#financialStatement').show();
        $('#summaryReport').hide();
    }
}
</script>

@endsection
