<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\Task;
use App\ReimburseRequest;
use App\InventoryCategory;
use App\MticsPurchaseEquipList;
use App\Payment;
use App\MticsfundPayment;
use App\MticsPurchaseList;
use App\Mticsfund;
use App\MoneyRequest;
use App\Role;
use App\RoleMember;
use App\MticsEvent;
use App\EventPurchaseList;



class AuditorController extends Controller
{
    public function eventView($id) {
         if(!Gate::allows('auditor-only'))
        {
        return redirect('/');
        }
        $event = MticsEvent::findOrFail($id);

        $reimburses = ReimburseRequest::with('moneyrequest')->whereHas('moneyrequest', function($q) use($event) {
            $q->whereHas('task', function($q) use($event) {
                $q->where('event_id', $event->id);
            });
        })->get();

        $reimburseCount = ReimburseRequest::with('moneyrequest')->whereHas('moneyrequest', function($q) use($event) {
            $q->whereHas('task', function($q) use($event) {
                $q->where('event_id', $event->id);
            });
        })->where('reimburse_status', 'pending')->count();

        $auditor_tasks = Task::where('event_id', $id)->where(function($q) {
                $q->where('task_status', 'pending')->orWhere('task_status', 'on-going')->orWhere('task_status', 'for validation')->orWhere('task_status', 'disapproved');
            })->whereHas('role', function($q){
                $q->where('name', 'auditor');
            })->get();

        return view('officers/event/auditor', compact('event', 'reimburses','auditor_tasks', 'reimburseCount'));
    }

    public function validateReimbursement($event_id, $request_id) {
        $event = MticsEvent::findOrFail($event_id);

        $moneyRequestsTaskDone = MoneyRequest::findOrFail($request_id);

        $reimbursement = ReimburseRequest::where('money_request_id', $moneyRequestsTaskDone->id)->first();

        $withReceipts = EventPurchaseList::has('receipt')->where('task_id', $moneyRequestsTaskDone->task_id)->get();


       return view('officers/event/auditor_validation', compact('event', 'moneyRequestsTaskDone', 'reimbursement', 'withReceipts'));
    }

}
