@extends('layout/control_panel')

@section('title')
{{$event->event_title}}
@endsection

@section('middle')
<div class="container-fluid w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-paper-plane-o fa-fw w3-xxlarge"></i>
      <strong>{{$event->event_title}} <i class="fa fa-angle-double-right fa-fw"></i> {{$moneyRequestsTaskDone->task->task_name}}</strong>
    </h3>
  </div>

  <hr>

  @if(count($reimbursement->confirm) < 2)
  <div class="row w3-margin-top" align="center">
    <a class="w3-text-green w3-large" href="#acceptReimburse" data-toggle="modal"><i class="fa fa-thumbs-up fa-fw" data-toggle="tooltip" data-placement="bottom" title="Accept Request"></i> Approve</a>

    |

    <a class="w3-text-red w3-large" href="#denyReimburse" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw" data-toggle="tooltip" data-placement="bottom" title="Deny Request"></i>Disapprove</a>
  </div>
  @endif


  <!-- ACCEPT REIMBURSEMENT MODAL -->
  <div class="modal fade" id="acceptReimburse" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Estimated Price</h4>
  </div>
  <form action="{{ url('admin/requested-reimbursement/'.$reimbursement->id.'/accept') }}" method="POST" enctype="multipart/form-data">
  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

      <div class="form-group">
        <p>Please review all the receipts before accepting this request.</p>
      </div>

      <div class="form-group">
        <p>Do you want to proceed?</p>
      </div>

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-default w3-green">Yes</button>
    <button type="button" data-dismiss="modal" class="btn btn-default w3-red">No</button>
  </div>

  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->

  <!-- DENY REIMBURSEMENT MODAL -->
  <div class="modal fade" id="denyReimburse" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Estimated Price</h4>
  </div>
  <form action="{{ url('admin/requested-reimbursement/'.$reimbursement->id.'/deny') }}" method="POST" enctype="multipart/form-data">
  <div class="modal-body">
    <div class="w3-container">
      <div class="row">

      <div class="form-group">
        <p>Please review all the receipts before denying this request.</p>
      </div>

      <div class="form-group">
        <p>Are you sure you want to deny this request?</p>
      </div>

      <div class="form-group">
        <label>Reason:</label>
        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="deny_reason" placeholder="Deny Reason" rows="3"></textarea>
      </div>

      </div>
    </div>
  </div>

  <div class="modal-footer">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-default w3-green">Yes</button>
    <button type="button" data-dismiss="modal" class="btn btn-default w3-red">No</button>
  </div>

  </form>
  </div>
  </div>
  </div>
  <!-- END MODAL -->

  <br>

  <div class="row w3-margin-top">

    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="w3-card-4 w3-amber w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxxlarge fa-fw"></i>
              <strong>Requested Money</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$moneyRequestsTaskDone->amount}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>

    </div>

    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="w3-card-4 w3-purple w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-shopping-bag w3-xxxlarge fa-fw"></i>
              <strong>Purchased Amount</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$moneyRequestsTaskDone->task->eventpurchaselist->sum('event_total_price')}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="w3-card-4 w3-teal w3-text-black w3-padding">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-reply w3-xxxlarge fa-fw"></i>
              <strong>Reimbursement</strong>
            </h4>
          </div>

          <hr style="border-color: black">

          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$reimbursement->reimburse_amount}} pesos</strong>
              </span>
            </h4>
          </div>

        </div>

      </div>
    </div>

  </div>

  <br>

  <div class="row w3-margin-top">
    <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
             <th class="w3-center">Item Name</th>
             <th class="w3-center">Quantity</th>
             <th class="w3-center">Amount</th>
             <th class="w3-center">Total</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($moneyRequestsTaskDone->task->eventpurchaselist as $item)
            <tr>
              <td class="w3-center">{{$item->event_itemname}}</td>
              <td class="w3-center">{{$item->event_act_quan}}</td>
              <td class="w3-center">{{$item->event_act_price}}</td>
              <td class="w3-center">{{$item->event_act_price * $item->event_act_quan}}</td>
            </tr>

          @endforeach
         </tbody>
        </table>
    </div>
  </div>

  <div class="row w3-margin-top">
    <h4>
      <i class="fa fa-file-text-o"></i>
      <strong>Receipt</strong>
    </h4>
  </div>

  <div class="row w3-margin-top">
    @foreach($withReceipts as $item)
    <div class="col-sm-1 col-md-4 col-lg-4">
      <label>
        <i class="fa fa-shopping-bag fa-fw"></i>
        {{$item->event_itemname}}
      </label>
      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'. $item->receipt[0]->receipt_image) : asset('images/'. $item->receipt[0]->receipt_image)}}" style="width: 100%">
    </div>
    @endforeach
  </div>

</div>

@endsection
