@extends('layout/control_panel')

@section('title')
Manage Banner
@endsection

@section('middle')
<div class="container-fluid w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-flag-o fa-fw w3-xxlarge"></i>
      <strong>Manage Banner</strong>
    </h3>
  </div>
  <hr>
  <div class="row">
    <p>
      <a href="#add" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i> Add</a>
    </p>
  </div>

  <div class="row">
  @foreach($banners as $banner)
    <div class="col-sm-4">

      <div class="w3-card-4 w3-white w3-margin w3-padding">
        <div class="w3-container">

          <div align="row w3-padding-small">
            <strong class="w3-left">
              <i class="fa fa-circle fa-fw {{ $banner->status == 'inactive' ? 'w3-text-red' : 'w3-text-green' }}"></i>
              {{$banner->banner_name}}
            </strong>

            <span class="w3-dropdown-hover w3-right w3-white">
              <a href="javascript:void(0)" class="w3-text-gray"><i class="fa fa-sort-down fa-fw w3-large"></i></a>


              <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" align="left" style="right: 0">
                @if($banner->status == 'active')
                <a href="{{url('admin/manage-banner/disable/'.$banner->id)}}" class="w3-text-gray w3-hover-teal"><i class="fa fa-toggle-on fa-fw w3-large"></i> Disable Banner</a>
                @else
                <a href="{{url('admin/manage-banner/enable/'.$banner->id)}}" class="w3-text-gray w3-hover-teal"><i class="fa fa-toggle-off fa-fw w3-large"></i> Enable Banner</a>
                @endif

                <a href="javascript:void(0)" onclick="deleteBanner( {{$banner->id}} )" class="w3-text-gray w3-hover-teal" style="outline: 0px"><i class="fa fa-trash-o fa-fw w3-large"></i> Delete Banner</a>
              </div>

            </span>

          </div>
          <br>
          <hr>
          <div class="row w3-center">
            <h1>
             <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$banner->banner_path) : asset('images/'.$banner->banner_path)}}" style="width: 100%">
            </h1>
          </div>

        </div>

      </div>

    </div>

    <form action="{{url('admin/manage-banner/delete/'.$banner->id)}}" id="formBanner-{{$banner->id}}" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>

    @endforeach
  </div>
</div>

<!-- ADD MODAL -->
<div class="modal fade" id="add" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-sm" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
<h4 class="modal-title w3-text-gray" id="myModalLabel">Add Banner</h4>
</div>
<form action="{{route('admin.banner.store')}}" method="POST" enctype="multipart/form-data">
<div class="modal-body">
  <div class="w3-container">
    <div class="row">

      <div class="form-group">
        <label for="banner_name">Banner Name: </label>
        <input type="text" name="banner_name" placeholder="Banner Name" class="form-control" required>

      </div>
      <div class="form-group filename" style="display: none">
            <p id="import-file-name"></p>
      </div>

      <div class="form-group">
        <div align="right">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <label for="image" style="cursor: pointer;" align="right"><i class="fa fa-file-image-o fa-fw w3-text-deep-orange fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Choose a file"></i></label>
          <input type="file" accept="image/*" name="image" id="image" style="opacity: 0; position: absolute; z-index: -1;" required />
          <button type="submit" class="btn btn-default w3-green w3-text-white"><i class="fa fa-save fa-fw"></i> Save</button>
        </div>
      </div>

    </div>
  </div>
</div>
</form>
</div>
</div>
</div>
<!-- END MODAL -->

<script type="text/javascript">
  var input = document.getElementById( 'image' );
  var infoArea = document.getElementById( 'import-file-name' );

  input.addEventListener('change', showFileName);

  function showFileName( event ) {

    $(".filename").show()
    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;

  }

</script>

<script type="text/javascript">
function deleteBanner( id ) {
  swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to recover this file!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formBanner-" + id).submit();
    }
  });
}
</script>

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>
@endsection
