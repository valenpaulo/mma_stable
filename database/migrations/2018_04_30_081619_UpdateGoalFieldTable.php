<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGoalFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
         Schema::table('events', function ($table) {
            $table->longtext('goal')->nullable()->after('description');
            $table->longtext('accomplishment')->nullable()->after('goal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function($table)
        {
            $table->dropColumn('accomplishment');
            $table->dropColumn('goal');
        });
    }
}
