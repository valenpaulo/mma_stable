@extends('layout/control_panel')

@section('title')
{{$event->event_title}}
@endsection

@section('middle')
<div class="w3-container w3-margin">

  <div class="row">
    <h3>
      <i class="fa fa-calendar fa-fw w3-xxlarge"></i>
      <strong>{{$event->event_title}}</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-amber w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-file-text-o w3-xxxlarge fa-fw"></i>
              <strong>Total Breakdown</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$breakdown}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>

    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-purple w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxxlarge fa-fw"></i>
              <strong>Collection</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$totalPayment}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-light-green w3-text-black w3-padding">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-group w3-xxxlarge fa-fw"></i>
              <strong>Paid Members</strong>
            </h4>
          </div>

          <hr style="border-color: black">

          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$noPaid}}</strong>
              </span>
            </h4>
          </div>

        </div>

      </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12">
      <div class="w3-card-4 w3-blue w3-text-black w3-padding w3-container">

        <div class="w3-container">
          <div class="row" align="center">
            <h4>
              <i class="fa fa-money w3-xxxlarge fa-fw"></i>
              <strong>Budget</strong>
            </h4>
          </div>
          <hr style="border-color: black">
          <div class="row">
            <h4>
              <span>
                <i class="fa fa-ellipsis-v fa-fw"></i>
                <strong>{{$event_budget}} pesos</strong>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>

  </div>

  <br>

  <div class="row w3-margin-top">
    <ul class="nav nav-tabs">
      @if($event->amount_confirm !== 'approved')
      <li class="active"><a data-toggle="tab" href="#payment_breakdown" class="w3-text-red" title="Please Set Event Payment">Event Payment and Breakdown</a></li>
      @else
      <li class="active"><a data-toggle="tab" href="#payment_breakdown" class="w3-text-black">Event Payment and Breakdown</a></li>
      @endif

      @if(count($event_offers) > 0)
      <li><a data-toggle="tab" href="#event_offer" class="w3-text-black">Event Offer</a></li>
      @endif

      <li><a data-toggle="tab" href="#moneyRequest" class="w3-text-black">
      Money Requests
      @if(count($moneyRequests) > 0)
      <span class="w3-badge w3-red">{{count($moneyRequests) + count($requestsForReimbursement)}}</span>
      @endif
      </a></li>

      <li><a data-toggle="tab" href="#validation" class="w3-text-black">
      Validation
      @if(count($moneyRequestsTaskDone) > 0)
      <span class="w3-badge w3-red">{{count($moneyRequestsTaskDone)}}</span>
      @endif
      </a></li>

      <li><a data-toggle="tab" href="#task" class="w3-text-black">
      Task
      @if(count($finance_tasks) > 0)
      <span class="w3-badge w3-red">{{count($finance_tasks)}}</span>
      @endif
      </a></li>

      <li><a data-toggle="tab" href="#expenses" class="w3-text-black">Expenses</a></li>


    </ul>
  </div>

    <div class="tab-content">

      <div id="payment_breakdown" class="tab-pane fade in active">
       <div class="row w3-margin-top">

          <h4>
              <i class="fa fa-file-text-o fa-fw"></i>
              <strong>Event Payment Breakdown</strong>
              @if($event->amount_confirm == null || $event->amount_confirm == 'denied')
              <a href="#breakdown" class="w3-text-gray w3-hover-text-green" data-toggle="modal">
                <i class="fa fa-plus-square fa-fw" data-toggle="tooltip" data-placement="bottom" title="Add Payment Breakdown"></i>
              </a>

              <a href="#submitBreakdown" class="w3-text-gray w3-hover-text-green" data-toggle="modal">
                <i class="fa fa-paper-plane-o fa-fw" data-toggle="tooltip" data-placement="bottom" title="submit Payment Breakdown"></i>
              </a>
              @endif


          </h4>

        </div>

        <div class="row">
          <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                   <th class="w3-center">Breakdown</th>
                   <th class="w3-center">Description</th>
                   <th class="w3-center">Amount</th>
                   <th class="w3-center">Status</th>


                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($event->breakdown as $breakdown)
                  <tr>
                    <td class="w3-center">{{$breakdown->breakdown_name}}
                    @if($event->amount_confirm == null || $event->amount_confirm == 'denied')
                      <a class="w3-text-orange" href="#edit-{{$breakdown->id}}" data-toggle="modal"><i class="fa fa-pencil fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Edit Payment Breakdown"></i></a>
                    @endif
                    </td>
                    <td class="w3-center">{{$breakdown->breakdown_desc}}</td>
                    <td class="w3-center">{{$breakdown->breakdown_amt}}</td>
                    <td class="w3-center">{{$event->amount_confirm}}</td>

                  </tr>

                  <!-- EDIT PAYMENT BREAKDOWN MODAL -->
                  <div class="modal fade" id="edit-{{$breakdown->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Event Payment Breakdown</h4>
                  </div>
                  <form action="{{ url('admin/finance/event-fund/'.$event->id.'/manage-payment/'.$breakdown->id.'/edit') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Payment Breakdown Name:</label>
                          <input type="text" name="bname" class="form-control" tabindex="1" placeholder="Payment Breakdown Name" value="{{$breakdown->breakdown_name}}">
                        </div>

                        <div class="form-group">
                          <label>Breakdown:</label>
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="bdesc" placeholder="Payment Breakdown" rows="4">{{$breakdown->breakdown_desc}}</textarea>
                        </div>

                        <div class="form-group">
                          <label>Amount:</label>
                          <input type="number" name="bamt" id="bamt" tabindex="1" class="form-control" placeholder="Amount" value="{{$breakdown->breakdown_amt}}">
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- SUBMIT PAYMENT BREAKDOWN MODAL -->
                  <div class="modal fade" id="submitBreakdown" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Submit Event Payment Breakdown</h4>
                  </div>
                  <form action="{{ url('admin/finance/event-fund/'.$event->id.'/manage-payment/submit') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <p>You are about to submit the event payment breakdown, please double check all the informations. This will be validated by your President.</p>
                        </div>

                        <div class="form-group">
                          <p>Are you sure you want to submit this breakdown?</p>
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green">Yes</button>
                    <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                @endforeach
               </tbody>
              </table>
          </div>
        </div>

        <!-- ADD PAYMENT BREAKDOWN MODAL -->
        <div class="modal fade" id="breakdown" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Event Payment Breakdown</h4>
        </div>
        <form action="{{ url('admin/finance/event-fund/'.$event->id.'/manage-payment/store') }}" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="w3-container">
            <div class="row">

              <div class="form-group">
                <label>Breakdown:</label>
                <input type="text" name="bname" class="form-control" tabindex="1" placeholder="Payment Breakdown Name">
              </div>

              <div class="form-group">
                <label>Description:</label>
                <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="bdesc" placeholder="Payment Breakdown" rows="4"></textarea>
              </div>

              <div class="form-group">
                <label>Amount:</label>
                <input type="number" name="bamt" id="bamt" tabindex="1" class="form-control" placeholder="Amount">
              </div>

            </div>
          </div>
        </div>

        <div class="modal-footer">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
        </div>

        </form>
        </div>
        </div>
        </div>
        <!-- END MODAL -->

        <div class="row w3-margin-top">
          <h4>
            <a href="{{url('admin/finance/payment')}}" class="w3-text-black w3-hover-text-green">
              <i class="fa fa-money fa-fw"></i>
              <strong>Payments</strong>
            </a>
          </h4>
        </div>

        <div class="row w3-margin-top">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th class="w3-center">Name</th>
                 <th class="w3-center">Event</th>
                 <th class="w3-center">Amount</th>
                 <th class="w3-center">Status</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($eventPayments as $payment)
                <tr>
                  <td class="w3-center">{{$payment->member->first_name}} {{$payment->member->last_name}}</td>
                  <td class="w3-center">{{$payment->event->event_title}}</td>
                  <td class="w3-center">{{$payment->payevent_amt}}</td>
                  <td class="w3-center">{{$payment->paymevent_status}}</td>
                </tr>
              @endforeach
             </tbody>
            </table>
          </div>
        </div>
      <!-- end of payment_breakdown tab -->
      </div>

      <div id="moneyRequest" class="tab-pane fade">
        <div class="row w3-margin-top">
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="moneyRequestTable">
                <thead>
                  <tr>
                   <th class="w3-center">Task</th>
                   <th class="w3-center">Items</th>
                   <th class="w3-center">Requested Money</th>
                   <th class="w3-center">Status</th>
                   <th class="w3-center">Miscellaneous</th>
                   <th class="w3-center">Action</th>

                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($moneyRequests as $request)
                  <tr>
                    <td class="w3-center">{{$request->task->task_name}}</td>
                    <td class="w3-center">
                      @foreach($request->task->eventpurchaselist as $item)
                        {{$item->event_itemname}}
                        <a href="#editEstPrice-{{$item->id}}" class="w3-text-orange" data-toggle="modal"><i class="fa fa-pencil" data-toggle="tooltip" title="Edit Estimated Price"></i></a>

                        <br>

                        <!-- EDIT ESTIMATED PRICE MODAL -->
                        <div class="modal fade" id="editEstPrice-{{$item->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                        <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Estimated Price</h4>
                        </div>
                        <form action="{{ url('admin/finance/requested-money/'.$item->id.'/edit/'.$request->task_id) }}" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                          <div class="w3-container">
                            <div class="row">

                              <div class="form-group">
                                <label>Item Name:</label>
                                <input type="text" class="form-control" value="{{$item->event_itemname}}" disabled>
                              </div>

                              <div class="form-group">
                                <label>Quantity:</label>
                                <input type="text" class="form-control" value="{{$item->event_orig_quan}}" disabled>
                              </div>

                              <div class="form-group">
                                <input type="number" name="est_price" id="est_price" tabindex="1" class="form-control" placeholder="Estimated Amount" value="{{$item->event_est_price}}">
                              </div>


                            </div>
                          </div>
                        </div>

                        <div class="modal-footer">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                        </div>

                        </form>
                        </div>
                        </div>
                        </div>
                        <!-- END MODAL -->
                      @endforeach
                    </td>
                    <td class="w3-center">{{$request->amount}}</td>
                    <td class="w3-center">{{$request->mon_req_status}}</td>
                    <td class="w3-center">
                      <a class="w3-text-red" href="#fare-{{$request->id}}" data-toggle="modal"><i class="fa fa-automobile fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Add Fare Budget"></i></a>

                      <a class="w3-text-orange" href="#meal-{{$request->id}}" data-toggle="modal"><i class="fa fa-cutlery fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Add Meal Budget"></i></a>
                    </td>
                    <td class="w3-center">
                      <a class="w3-text-green" href="#accept-{{$request->id}}" data-toggle="modal"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Accept Request"></i></a>

                      <a class="w3-text-red" href="#denial-{{$request->id}}" data-toggle="modal"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Deny Request"></i></a>

                    </td>
                  </tr>

                  <!-- FARE MODAL -->
                  <div class="modal fade" id="fare-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Fare Budget</h4>
                  </div>
                  <form action="{{ url('admin/finance/requested-money/'.$request->task_id.'/add-fare') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Estimated Fare Amount</label>
                          <input type="number" class="form-control" placeholder="Estimated Fare Amount" name="est_fare">
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-plus-square fa-fw"></i> Add</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- MEAL MODAL -->
                  <div class="modal fade" id="meal-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Meal Budget</h4>
                  </div>
                  <form action="{{ url('admin/finance/requested-money/'.$request->task_id.'/add-meal') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Estimated Meal Amount</label>
                          <input type="number" class="form-control" placeholder="Estimated Meal Amount" name="est_meal">
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green"><i class="fa fa-plus-square fa-fw"></i> Add</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- ACCEPT MODAL -->
                  <div class="modal fade" id="accept-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Accept Request</h4>
                  </div>
                  <form action="{{ url('admin/finance/requested-money/'.$request->task_id.'/accept') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <p>Note: Please review the request details before accepting this request.</p>
                          <p>Do you want to proceed?</p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green">Yes</button>
                    <button type="button" data-dismiss="modal" class="btn btn-default w3-red">No</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                  <!-- DENY MODAL -->
                  <div class="modal fade" id="denial-{{$request->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Deny Request</h4>
                  </div>
                  <form action="{{ url('admin/finance/requested-money/'.$request->task_id.'/deny') }}" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="w3-container">
                      <div class="row">

                        <div class="form-group">
                          <label>Reason:</label>
                          <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="deny_reason" placeholder="Please indicate the reason for denying this request" rows="4"></textarea>
                        </div>

                        <div class="form-group">
                          <p>Note: Please review the request details before denying this request.</p>
                          <p>Do you want to proceed?</p>
                        </div>


                      </div>
                    </div>
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green">Yes</button>
                    <button type="button" class="btn btn-default w3-red" data-dismiss="modal">No</button>
                  </div>

                  </form>
                  </div>
                  </div>
                  </div>
                  <!-- END MODAL -->

                @endforeach
               </tbody>
              </table>
          </div>
        </div>

        <!-- end if count of money request is not 0 -->

        <!-- @if(count($moneyRequestTransactions) > 0)
        <div class="row w3-margin-top">

          <h4>
              <i class="fa fa-envelope-open-o fa-fw"></i>
              <strong>Money Request Transaction</strong>
          </h4>

        </div>

        <div class="row">
          <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                   <th class="w3-center">Amount</th>
                   <th class="w3-center">Task</th>
                   <th class="w3-center">Description</th>
                   <th class="w3-center">Status</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($moneyRequestTransactions as $request)
                  <tr>
                    <td class="w3-center">{{$request->amount}}</td>
                    <td class="w3-center">{{$request->task->task_name}}</td>
                    <td class="w3-center">{{$request->remarks}}</td>
                    <td class="w3-center">{{$request->mon_req_status}}</td>
                  </tr>

                @endforeach
               </tbody>
              </table>
          </div>
        </div>
        @endif -->
      <!-- end of money request tab -->
      </div>

      <div id="validation" class="tab-pane fade">
        <div class="row">
          <div class="table-responsive">
              <table class="table table-bordered table-hover" id="validationTable">
                <thead>
                  <tr>
                   <th class="w3-center">Task</th>
                   <th class="w3-center">Requested Amount</th>
                   <th class="w3-center">Purchased Amount</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($moneyRequestsTaskDone as $request)
                  <tr>
                    <td class="w3-center">{{$request->task->task_name}}</td>
                    <td class="w3-center">{{$request->amount}}</td>
                    <td class="w3-center">{{$request->task->eventpurchaselist->sum('event_total_price')}}</td>
                    <td class="w3-center">
                      <a class="w3-text-green" href="{{url('admin/finance/manage-event/' . $event->id . '/validate-purchased-amount/' . $request->id)}}"><i class="fa fa-arrow-circle-right fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Validate"></i></a>
                    </td>
                  </tr>

                @endforeach
               </tbody>
              </table>
          </div>
        </div>

      <!-- end of validation tab -->
      </div>

      @if(count($event_offers) > 0)
       <div id="event_offer" class="tab-pane fade">

        <div class="row">
          <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                   <th class="w3-center">Item</th>
                   <th class="w3-center">Amount</th>
                   <th class="w3-center">Image</th>
                   <th class="w3-center">Description</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($event_offers as $offer)
                  <tr>
                    <td class="w3-center">{{$offer->offer_name}}</td>
                    <td class="w3-center">{{$offer->offer_amt}}</td>
                    <td class="w3-center">
                      <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$offer->offer_image) : asset('images/'.$offer->offer_image)}}" style="width: 140px">
                    </td>
                    <td class="w3-center">{{$offer->offer_desc}}</td>
                    <td class="w3-center">
                      <a href="#setAmount-{{$offer->id}}" class="w3-text-orange" data-toggle="modal">
                        <i class="fa fa-pencil fa-fw" data-toggle="tooltip" data-placement="bottom" title="Set an amount"></i>
                      </a>
                    </td>
                  </tr>

                  <!-- SET AMOUNT OFFER -->
                  <div class="modal fade" id="setAmount-{{$offer->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-sm" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                          <h4 class="modal-title w3-text-gray" id="myModalLabel">Set Amount for Event Offer</h4>
                        </div>
                      <form action="{{ url('admin/finance/event-fund/'.$event->id.'/event-offers/'.$offer->id.'/edit') }}" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">

                        <div class="w3-container">
                          <div class="row">
                            <div class="form-group">
                              <input type="number" name="offer_amount" id="offer_amount" tabindex="1" class="form-control" placeholder="Amount">
                            </div>
                          </div>
                        </div>

                        </div>

                        <div class="modal-footer">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                  <!-- END MODAL -->
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
      <!-- end of validation tab -->
      </div>
      @endif

      <!-- FINANCE TASK -->
      <div id="task" class="tab-pane fade">
        <div class="row w3-margin-top">

          <h4>
              <i class="fa fa-tasks fa-fw"></i>
              <strong>Task</strong>
          </h4>

        </div>

        <div class="row">
          <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                   <th class="w3-center">Task Name</th>
                   <th class="w3-center">Description</th>
                   <th class="w3-center">Due Date</th>
                   <th class="w3-center">Status</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($finance_tasks as $task)
                  <tr>
                    <td class="w3-center">{{$task->task_name}}</td>
                    <td class="w3-center">{{$task->task_desc}}</td>
                    <td class="w3-center">{{$task->due_date}}</td>
                    <td class="w3-center">{{$task->task_status}}</td>
                    <td class="w3-center">
                      @if ($task->task_status == 'pending')
                      <a class="w3-text-green" href="{{url('admin/finance/manage-event/start-task/' . $task->id)}}"><i class="fa fa-play fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Start Task"></i></a>
                      @elseif ($task->task_status == 'on-going' || $task->task_status == 'disapproved')

                      <a class="w3-text-green" href="{{url('admin/finance/manage-event/validate/'.$task->id)}}"><i class="fa fa-check-square fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Validate"></i></a>

                      @elseif ($task->task_status == 'for validation')
                      <i class="fa fa-check-square fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Validate"></i>
                      @endif
                    </td>

                  </tr>


                @endforeach
               </tbody>
              </table>
          </div>
        </div>
      </div>

      <div id="expenses" class="tab-pane fade">

          <div class="row w3-margin-top">
            <h5>
              <a href="#addExpenses" class="w3-text-gray" data-toggle="modal" style="outline: 0">
                <i class="fa fa-plus-square-o fa-fw"></i>
                Add Expenses
              </a>
            </h5>
          </div>

           <!-- ADD EXPENSES MODAL -->
        <div class="modal fade" id="addExpenses" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Expenses</h4>
        </div>
        <form action="{{ url('admin/finance/event-fund/'.$event->id.'/expenses/store') }}" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="w3-container">
            <div class="row">

              <div class="form-group">
                <label>Receiver:</label>
                <select name="role_id" class="form-control">
                @foreach($roles as $role)
                    <option value="{{$role->id}}">{{$role->officer_name}}</option>
                @endforeach
                </select>
              </div>

              <div class="form-group">
                  <select name="breakdown_id" class="form-control">
                  @foreach($breakdowns as $breakdown)
                      <option value="{{$breakdown->id}}">{{$breakdown->breakdown_name}}</option>
                  @endforeach
                  </select>
              </div>

              <hr>

              <div class="form-group">
                <label>Expenses Name:</label>
                <input type="text" name="expense_name" id="expense_name" tabindex="1" class="form-control" placeholder="Expense Name">
              </div>

              <div class="form-group">
                <label>Amount:</label>
                <input type="number" name="expense_amt" id="expense_amt" tabindex="1" class="form-control" placeholder="Amount">
              </div>

              <div class="form-group">
                <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="expense_desc" placeholder="Description" rows="3"></textarea>
              </div>


            </div>
          </div>
        </div>

        <div class="modal-footer">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
        </div>

        </form>
        </div>
        </div>
        </div>
        <!-- END MODAL -->


        <!-- EXPENSES TABLE -->

        <div class="row">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="expensesTable">
          <thead>
            <tr>
             <th class="w3-center">ID</th>
             <th class="w3-center">Expense</th>
             <th class="w3-center">Amount</th>
             <th class="w3-center">Received by</th>
             <th class="w3-center">Approved By</th>
             <th class="w3-center">Receipt</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($event_expenses as $expenses)
           <tr>
             <td class="w3-center">{{$expenses->id}}</td>
             <td class="w3-center">{{$expenses->expense_name}}</td>
             <td class="w3-center">{{$expenses->expense_amt}}</td>
             <td class="w3-center">{{$expenses->receiver->first_name}} {{$expenses->receiver->last_name}} - {{$expenses->receiverRole->officer_name}}</td>
             <td class="w3-center">{{$expenses->approvedBy->first_name}} {{$expenses->approvedBy->last_name}} - {{$expenses->approvedByRole->officer_name}}</td>

            <td class="w3-center">
            @if($expenses->receipt !== null)
              <a href="" class="w3-text-gray" data-toggle="modal" data-target="#receiptview-{{$expenses->id}}"><img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$expenses->receipt->receipt_image) : asset('images/'.$expenses->receipt->receipt_image)}}" style="width: 100px"></a>
            @endif
            </td>


             <td class="w3-center"><a class="w3-text-red" href="#receipt-{{$expenses->id}}" data-toggle="modal"><i class="fa fa-upload fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Upload Receipt"></i></a></td>

               <!-- RECEIPT MODAL-->
            <div class="modal fade" id="receiptview-{{$expenses->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Receipt</h4>
            </div>
            <form action="" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">
                @if($expenses->receipt !== null)
                  <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$expenses->receipt->receipt_image) : asset('images/'.$expenses->receipt->receipt_image)}}" style="width:500px">
                @endif
                </div>
              </div>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->


             <!-- UPLOAD WITHDRAW RECEIPT MODAL -->
                <div class="modal fade" id="receipt-{{$expenses->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Upload Receipt</h4>
                </div>
                <form action="{{ url('admin/finance/event-fund/'.$expenses->id.'/expenses/receipt') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                     <div class="form-group">
                        <label id="ew-receipt-image-file-name-{{$expenses->id}}" for="receipt{{$expenses->id}}" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-text fa-fw w3-text-green w3-large" ></i> Upload Receipt</label>
                        <input type="file" name="receipt" id="receipt{{$expenses->id}}" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showEventWithdrawFileName(event, {{$expenses->id}})" />
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                </div>

                </form>
                </div>
                </div>
                </div>
                <!-- END MODAL -->



            </tr>
          @endforeach


         </tbody>
        </table>
    </div>
  </div>



      </div>


    </div>

</div>


<script type="text/javascript">
$(document).ready( function () {
  $('#expensesTable').DataTable();
} );
</script>


<script>
  // var input = document.getElementById( 'receipt' );

  // input.addEventListener('change', showFileName1);

  function showEventWithdrawFileName( event, id ) {

    // the change event gives us the input it occurred in
    var infoArea = document.getElementById( 'ew-receipt-image-file-name-' + id );
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  };
</script>

<script type="text/javascript">
$(document).ready( function () {
  var moneyRequestTable = $('#moneyRequestTable').DataTable();
  $('#validationTable').DataTable();
  $('#transactionTable').DataTable();

  $(moneyRequestTable.table().container()).removeClass('form-inline');

} );
</script>

@endsection
