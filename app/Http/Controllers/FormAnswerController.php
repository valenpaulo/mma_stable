<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Event;

use App\Events\SystemEvent;

use App\Answer;
use App\Form;
use App\Respondent;
use Charts;

class FormAnswerController extends Controller
{
    public function store(Request $request, $id, $items) {
        $form = Form::select('title')->findOrFail($id);

        for ($i=1; $i <= $items; $i++) {
            $validator = Validator::make($request->all(), [
             'answer'.$i => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors('Please make sure to provide answer to all of the questions.');
            }
        }

        $respondent = Respondent::where('form_id', $id)->where('member_id', Auth::user()->id)->first();

        if ($respondent) {
            return redirect()->back()->withErrors("Sorry, but you've already answer this form.");
        }

        $response = New Respondent;
        $response->member_id = Auth::user()->id;
        $response->form_id = $id;
        $response->save();

        for ($i=1; $i <= $items; $i++) {
            foreach ($request['answer'.$i] as $answer) {
                $ans = New Answer;
                $ans->member_id = Auth::user()->id;
                $ans->option_id = $answer;
                $ans->save();
            }
        }

        Event::fire(new SystemEvent(auth::id(), 'Answered "' . $form->title . '" form.'));

        return redirect('member/home')->with('success', 'Done!');
    }

    public function view($id) {
        $charts = array();

        $form = Form::findOrFail($id);
        $title = $form->title;

        foreach ($form->question as $outer => $question) {
            $answer_count = array();
            $answers = array();
            foreach ($question->option as $inner => $option) {
                $answer = Answer::where('option_id', $option->id)->get();
                $answers[$inner] = $option->option;
                $answer_count[$inner] = $answer->count();
            }

            $charts[$outer] = Charts::create('pie', 'highcharts')
                ->title($question->question)
                ->labels($answers)
                // ->template('material')
                ->values($answer_count)
                ->width(400)
                ->height(0)
                ->responsive(false);
        }

        return view('admin/adminPanel/form_answer_view', compact('charts', 'title'));
    }
}
