<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Officerterm extends Model
{
	protected $table = 'officerterms';
	protected $fillable = [
        'start_term','status','from_year','to_year','excess_budget'
    ];
    
}
