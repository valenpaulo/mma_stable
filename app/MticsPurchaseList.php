<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsPurchaseList extends Model
{
   protected $table = 'mtics_purchase_lists';
	protected $fillable = [
        'task_id','mtics_itemname','mtics_itemdesc','mtics_orig_quan','mtics_est_price','mtics_act_quan','mtics_act_price','mtics_remarks','inventory_category_id','mtics_total_price','mtics_inv_status','mtics_inv_quan'

    ];



     public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }

     public function mticspurchaseequiplist()
    {
        return $this->hasMany(MticsPurchaseEquipList::class);
    }

     public function mticspurchasereport()
    {
        return $this->hasMany(MticsPurchaseReport::class);
    }

     public function receipt()
    {
        return $this->belongsToMany(Receipt::class, 'receipt_mticsitems','mtics_purchase_list_id','receipt_id');
    }

    public function category() {
        return $this->belongsTo(InventoryCategory::class, 'inventory_category_id');
    }

}
