@extends('layout/layout')

@section('title')
Manage User
@endsection

@section('content')

<div class="containter-fluid">

<div class="w3-container w3-text-black w3-center">
  <a href="{{route('home')}}">
  <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.png') : asset('images/logo/mtics.png')}}" class="w3-margin-bottom w3-margin-top" style="height: 100px; width: 100px"></a>

  <h4>Manila Technician Institute Computer Society</h4>
  <hr style="border: 0.5px solid gray ">
</div>

<div class="container w3-text-gray">
 <h3><i class="fa fa-superpowers"></i> Additional Feature</h3>
</div>

<div class="container w3-margin-top">
    <div class="row">
        <div class="col-sm-6">
        <a href="{{route('admin.manage.user')}}" class="w3-text-white">
        <div class="w3-card-4 w3-round w3-green w3-center w3-container">
        <h1><i class="fa fa-group"></i></h1>
        <h3><strong><p>Manage Members</p></strong></h3>
        </div>
        </a>
        </div>

        <div class="col-sm-6">
        <a href="{{route('admin.manage.privilege')}}" class="w3-text-white">
       <div class="w3-card-4 w3-round w3-orange w3-center w3-text-white w3-container">
        <h1><i class="fa fa-user"> </i><i class="fa fa-key"></i></h1>
        <h3><strong>Manage Privilege</strong></h3>
        </div>
        </a>
        </div>
    </div>

</div>

</div>

@endsection
