@extends('layout/control_panel')

@section('title')
Manage Profanity
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-language fa-fw w3-xxlarge"></i>
      <strong>Manage Profanity</strong>
    </h3>
  </div>

  <hr>

  <div class="row">
    <p>
      <a href="#add" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i> Add</a>
      <i class="fa fa-ellipsis-v fa-fw"></i>
      <a href="#importExcel" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-file-excel-o fa-fw"></i> Import</a>
    </p>
  </div>

  <br>

  <div class="row">
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="profanityTable">
        <thead>
          <tr>
           <th class="w3-center">Profanity Word</th>
           <th class="w3-center">Date Created</th>
           <th class="w3-center">Action</th>
          </tr>
        </thead>
        <tbody class="w3-text-gray">
          @foreach($filterwords as $filterword)
          <tr>
            <td class="w3-center">{{$filterword->Filter_word}}</td>
            <td class="w3-center">{{$filterword->created_at}}</td>
            <td class="w3-center">
              <a href="#edit-{{$filterword->id}}" data-toggle="modal" class="w3-text-orange" style="outline: 0"><i class="fa fa-pencil-square-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Edit Profanity Word"></i></a>

              <a href="javascript:void(0)" onclick="deleteProfanity( {{$filterword->id}} )" class="w3-text-red" data-toggle="modal" style="outline: 0"><i class="fa fa-trash-o fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Delete Profanity Word"></i></a>

              <form action="{{url('admin/manage-filterwords/'.$filterword->id.'/delete')}}" method="POST" id="formProfanity-{{$filterword->id}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </form>
            </td>
          </tr>

          <!-- EDIT PROFANITY -->
          <div class="modal fade" id="edit-{{$filterword->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                  <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Profanity Word</h4>
                </div>
              <form action="{{url('/admin/manage-filterwords/'.$filterword->id.'/edit')}}" method="POST">
                <div class="modal-body">

                  <div class="form-group">
                    <input type="text" name="Filter_word" id="Filter_word" tabindex="1" class="form-control" required placeholder="Filter Word" value="{{$filterword->Filter_word}}">
                  </div>

                  <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-default w3-green">Yes</button>
                    <button type="button" data-dismiss="modal" class="btn btn-default w3-red">No</button>
                  </div>

                </div>
              </form>
              </div>
            </div>
          </div>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  <!-- ADD PROFANITY -->

  <div class="modal fade" id="add" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Profanity Word</h4>
        </div>
        <form action="{{ url('admin/manage-filterwords/store') }}" method="POST" enctype="multipart/form-data">
          <div class="modal-body">

            <div class="form-group">
              <input type="text" name="Filter_word" id="Filter_word" tabindex="1" class="form-control" required placeholder="Filter Word">
            </div>

            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
            </div>

          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- END -->

  <!-- ADD PROFANITY -->

  <div class="modal fade" id="importExcel" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title w3-text-gray" id="myModalLabel">Import File</h4>
        </div>
        <form action="{{url('admin/manage-filterwords/import-excel')}}" method="POST" enctype="multipart/form-data">
          <div class="modal-body">
            <div class="w3-container">
              <div class="row">
                <div class="form-group">
                  <label id="import-file-name" for="import_file" style="cursor: pointer;"><i class="fa fa-file-excel-o fa-fw w3-large"></i> Choose a File</label>
                  <input type="file" name="import_file" id="import_file" style="opacity: 0; position: absolute; z-index: -1;" />
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default w3-green"><i class="fa fa-cloud-upload fa-fw"></i> Upload</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- END -->

</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#profanityTable').DataTable();
} );
</script>

<script type="text/javascript">
function deleteProfanity( id ) {
  swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formProfanity-" + id).submit();
    }
  });
}
</script>

<script>
  var input = document.getElementById( 'import_file' );
  var infoArea = document.getElementById( 'import-file-name' );

  input.addEventListener('change', showFileName);

  function showFileName( event ) {

    // the change event gives us the input it occurred in
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  };

   window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>
@endsection
