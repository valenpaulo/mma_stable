@extends('layout/control_panel')

@section('title')
MTICS Budget Request
@endsection

@section('middle')
<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-paper-plane-o fa-fw w3-xxlarge"></i>
      <strong>Budget Request</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="requestTable">
          <thead>
            <tr>
             <th class="w3-center">Budget</th>
             <th class="w3-center">Month</th>
             <th class="w3-center">Amount</th>
             <th class="w3-center">Remarks</th>
             <th class="w3-center">Status</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
          @foreach($mticsbudgetsRequest as $budget)
           <tr>

             <td class="w3-center"><a href="" class="w3-text-gray" data-toggle="modal" data-target="#info-{{$budget->id}}"><i class="fa fa-info-circle"></i> {{$budget->budget_name}}</a>
               </td>
             <td class="w3-center">{{$budget->budget_month}}</td>
             <td class="w3-center">{{$budget->budget_amt}}</td>
             <td class="w3-center">{{$budget->budget_remarks}}</td>
             <td class="w3-center">{{$budget->budget_status}}</td>
             <td class="w3-center">
              @if ($budget->budget_status == "for validation")
              <a class="w3-text-green" href="javascript:void(0)" onclick="approveRequest( {{$budget->id}} )"><i class="fa fa-thumbs-up fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Approve Request"></i></a>

              <a class="w3-text-red" href="#denyBudget-{{$budget->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-thumbs-down fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Deny Request"></i></a>
              @elseif ($budget->budget_status != "denied")
              <i class="fa fa-check-circle-o fa-fw w3-large w3-text-green" data-toggle="tooltip" data-placement="bottom" title="Approved"></i>
              @endif

              @if (!($budget->deny->isEmpty()))
              <a class="w3-text-red" href="#denyinfo-{{$budget->id}}" data-toggle="modal" style="outline: 0"><i class="fa fa-flag fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Deny info"></i></a>
              @endif



              <form action="{{ url('admin/president/requested-mtics-budget/'.$budget->id.'/approved') }}" method="POST" enctype="multipart/form-data" id="formApproved-{{$budget->id}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </form>
             </td>
            </tr>


            <!-- Denied Reason MODAL-->
            <div class="modal fade" id="denyinfo-{{$budget->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Denied Reason/s</h4>
            </div>
            <form action="" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  @foreach($budget->deny as $deny)
                  <div class="form-group">
                    <p><b>Denied by : </b>{{$deny->admin->first_name}} {{$deny->admin->last_name}}</p>
                    <p><b>Denied reason : </b>{{$deny->denied_reason}}</p>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->


             <!--INFO MODAL-->
            <div class="modal fade" id="info-{{$budget->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Budget Details</h4>
            </div>
            <form action="" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  @foreach($budget->budget_breakdown as $budget_breakdown)
                  <div class="form-group">
                    <p><b>{{$budget_breakdown->budget_expenses_cat->name}} : </b> P{{$budget_breakdown->budget_bd_amt}}.00</p>
                    <p>Description :  {{$budget_breakdown->budget_bd_desc}}</p>
                  </div>
                  @endforeach

                  ______________________
                  <p><b>Total </b> :  {{$budget->budget_breakdown->sum('budget_bd_amt')}}</p>
                </div>
              </div>
            </div>

            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->



            <!-- DENY Request -->
            <div class="modal fade" id="denyBudget-{{$budget->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title w3-text-gray" id="myModalLabel">Deny Budget Request</h4>
            </div>
            <form action="{{ url('admin/president/requested-mtics-budget/'.$budget->id.'/denied') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">
                  <div class="form-group">
                    <p>Please indicate the reason for denying the request.</p>
                  </div>

                  <div class="form-group">
                    <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="denied_reason" placeholder="Reason" rows="3"></textarea>
                  </div>

                  <div class="form-group">
                    <p>Do you want to proceed?</p>
                  </div>

                </div>
              </div>
            </div>

            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green"> Yes</button>
              <button type="button" data-dismiss="modal" class="btn btn-default w3-red"> No</button>

            </div>

            </form>
            </div>
            </div>
            </div>
            <!-- END MODAL -->

          @endforeach
         </tbody>
        </table>
    </div>
  </div>

</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#requestTable').DataTable();
} );
</script>

<script type="text/javascript">
function approveRequest( id ) {
  swal({
  title: "Are you sure?",
  text: "Once approved, you will not be able to undo this action!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#formApproved-" + id).submit();
    }
  });
}
</script>
@endsection
