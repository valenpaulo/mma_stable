@extends('layout/layout')

@section('title')
MTICS
@endsection

@section('content')
<style>
  body, html {height: 100%;}
  p {line-height: 2;}

  @media only screen and (max-width: 1365px) {
    .main {
      margin-top: 30px;
    }
  }

  @media (min-width: 1366px) {
    .main {
      margin-top: 60px;
    }
  }
</style>

<!-- error messages -->
<div class="container-fluid main">
<div class="row">
    <div class="col-md-8"></div>

  <div class="col-sm-12 col-md-4">
  @if($errors->any())
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger alert-dismissable fade in" id="success-alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>
      {{$error}}
      </strong>
    </div>
    @endforeach
  @endif
  </div>

</div>
</div>
<br>
<!-- END error -->

<br>

<header class="w3-container-fluid" id="home">
  @if($banner)
  <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$banner->banner_path) : asset('images/'.$banner->banner_path)}}" style="width: 100%; background-position: center; background-size: cover;">
  @else
  <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/banner/default.jpg') : asset('images/banner/default.jpg')}}" style="width: 100%; background-position: center; background-size: cover;">
  @endif
</header>

<br>
<!-- END BANNER -->

<div class="w3-container w3-margin">
  @if($fullpost)
  <div class="row w3-margin">
    <div class="w3-card-4 w3-padding w3-sand">
      <div class="w3-container">
        <div class="row w3-padding">
          <p>
            <i class="fa fa-thumb-tack w3-xxlarge"></i>
            Pinned Post

            @if(count($otherFulls) > 0)
            <span class="w3-right w3-padding">
              <a href="#viewFullPosts" data-toggle="modal" class="w3-text-gray">See more post</a>
            </span>
            @endif
          </p>
        </div>

        <hr style="border-color: black">

        <div class="row w3-margin">
          <div class="col-sm-12 col-md-5 col-lg-5">
            <div class="w3-container">
              <div class="row">
                <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$fullpost->theme_image) : asset('images/'.$fullpost->theme_image) }}" style="width: 100%" class="img-round">
              </div>
            </div>
          </div>

          <div class="col-sm-12 col-md-7 col-lg-7">
            <div class="w3-container">
              <div class="row">
                  <a href="{{url('/member/'.$fullpost->id.'/'.$fullpost->slug)}}">
                  <h3 class="w3-text-gray"><strong>{{$fullpost->event_title}}</strong></h3>
                </a>
              </div>

              <div class="row" align="justify">
                <p>
                 <?php
                  $string = strip_tags($fullpost->description);


                  if (strlen($string) > 300) {

                      // truncate string
                      $stringCut = substr($string, 0, 400);

                      // make sure it ends in a word so assassinate doesn't become ass...
                      $string = substr($stringCut, 0, strrpos($stringCut, ' '))."... <a href=".'/member/'.$fullpost->id.'/'.$fullpost->slug." class='w3-text-gray'> read more</a>";
                  }
                  echo $string;
                 ?>
                </p>
              </div>
            </div>

          </div>
        </div>


      </div>
    </div>
  </div>
  @endif

  <br>

  @if($partial)
  <div class="row w3-margin">
    <div class="w3-card-4 w3-padding w3-sand">
      <div class="w3-container">
        <div class="row w3-padding">
          <p>
            <i class="fa fa-thumb-tack w3-xxlarge"></i>
            Pinned Post

            @if(count($otherPosts) > 0)
            <span class="w3-right w3-padding">
              <a href="#viewPartialPosts" data-toggle="modal" class="w3-text-gray">See more post</a>
            </span>
            @endif
          </p>
        </div>

        <hr style="border-color: black">

        <div class="row w3-margin">
          <a href="{{url('/member/event/'.$partial->id.'/'.$partial->slug)}}">
            <h3 class="w3-text-gray"><strong>{{$partial->title}}</strong></h3>
          </a>
        </div>

        <div class="row w3-margin" align="justify">
          <p>
           <?php
            $string = strip_tags($partial->body);


            if (strlen($string) > 300) {

                // truncate string
                $stringCut = substr($string, 0, 400);

                // make sure it ends in a word so assassinate doesn't become ass...
                $string = substr($stringCut, 0, strrpos($stringCut, ' '))."... <a href=".'/member/event/'.$partial->id.'/'.$partial->slug." class='w3-text-gray'> read more</a>";
            }
            echo $string;
           ?>
          </p>
        </div>
      </div>
    </div>
  </div>
  @endif

  <!-- Partial Post -->
  <div class="modal fade" id="viewPartialPosts" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Partial Event Posts</h4>
      </div>
        <div class="modal-body">
        <div class="w3-container">
        @if($partial)
        @foreach($otherPosts as $partial)
          <div class="row">
            <a href="{{url('/member/event/'.$partial->id.'/'.$partial->slug)}}">
              <h3 class="w3-text-gray"><strong>{{$partial->title}}</strong></h3>
            </a>
          </div>

          <div class="row" align="justify">
            <p>
             <?php
              $string = strip_tags($partial->body);


              if (strlen($string) > 300) {

                  // truncate string
                  $stringCut = substr($string, 0, 400);

                  // make sure it ends in a word so assassinate doesn't become ass...
                  $string = substr($stringCut, 0, strrpos($stringCut, ' '))."... <a href=".'/member/event/'.$partial->id.'/'.$partial->slug." class='w3-text-gray'> read more</a>";
              }
              echo $string;
             ?>
            </p>
          </div>

          @if(!$loop->last)
            <hr>
          @endif
        @endforeach
        @endif
        </div>
        </div>


        <div class="modal-footer">
          <button type="submit" class="btn btn-default w3-red">Exit</button>
        </div>

      </div>
    </div>
  </div>
  <!-- END Partial Post -->

  <!-- Full Post -->
  <div class="modal fade" id="viewFullPosts" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Event Posts</h4>
      </div>
        <div class="modal-body">
        <div class="w3-container">
        @if($fullpost)
        @foreach($otherFulls as $full)
          <div class="row">
            <a href="{{url('/member/'.$full->id.'/'.$full->slug)}}">
              <h3 class="w3-text-gray"><strong>{{$full->event_title}}</strong></h3>
            </a>
          </div>

          <div class="row" align="justify">
            <p>
             <?php
              $string = strip_tags($full->description);


              if (strlen($string) > 300) {

                  // truncate string
                  $stringCut = substr($string, 0, 400);

                  // make sure it ends in a word so assassinate doesn't become ass...
                  $string = substr($stringCut, 0, strrpos($stringCut, ' '))."... <a href=".'/member/'.$full->id.'/'.$full->slug." class='w3-text-gray'> read more</a>";
              }
              echo $string;
             ?>
            </p>
          </div>

          @if(!$loop->last)
            <hr>
          @endif
        @endforeach
        @endif
        </div>
        </div>


        <div class="modal-footer">
          <button type="submit" class="btn btn-default w3-red">Exit</button>
        </div>

      </div>
    </div>
  </div>
  <!-- END Full Post -->

  <!-- Form Survey -->
  <div class="modal fade" id="viewForms" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title w3-text-gray" id="myModalLabel">Form Survey</h4>
      </div>
        <div class="modal-body">
        <div class="w3-container">
        @if(count($otherForms) > 0)
        @foreach($otherForms as $key => $form)
          <div class="form-group">
            <label class="form-inline"><strong>{{++$key}}. {{$form->title}} </strong></label>

            <a class="btn btn-default w3-green" href="{{url('admin/manage-form/answer/' . $form->id)}} form-inline"><i class="fa fa-pencil-square-o fa-fw"></i> Answer Form</a>
          </div>

          @if(!$loop->last)
            <hr>
          @endif
        @endforeach
        @endif
        </div>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-default w3-red">Exit</button>
        </div>

      </div>
    </div>
  </div>
  <!-- END Survey -->

  <br>

  <div class="row w3-margin">
    <hr>
  </div>

  <div class="w3-row w3-margin-top">
    <!-- Left column -->
    <div class="col-md-8 col-lg-8 col-sm-12">
    <!-- FIRST POST -->
      <div class="w3-container">
        @if($post)
        <div class="row">
          <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$post->image) : asset('images/'.$post->image) }}" style="width: 100%">
        </div>

        <div class="row w3-margin" align="center">
          <a href="{{url('/member/profile/post/'.$post->id.'/'.$post->slug)}}">
            <h3 class="w3-text-gray"><strong>{{$post->title}}</strong></h3>
          </a>
          <p class="w3-text-gray">by {{$post->member['first_name']}} {{$post->member['last_name']}}</p>
        </div>

        <div class="row w3-margin" align="justify">
          <p>
           <?php
            $string = strip_tags($post->body);


            if (strlen($string) > 300) {

                // truncate string
                $stringCut = substr($string, 0, 400);

                // make sure it ends in a word so assassinate doesn't become ass...
                $string = substr($stringCut, 0, strrpos($stringCut, ' '))."... <a href=".'/member/profile/post/'.$post->id.'/'.$post->slug." class='w3-text-gray'> more</a>";
            }
            echo $string;
           ?>
          </p>
        </div>
        @endif
      </div>
    <!-- END FIRST POST -->
    </div>
    <!-- End Left -->

    <div class="col-md-4 col-lg-4 col-sm-12">
      <div class="w3-container">
        <div class="row">
          <!-- FORM -->
          @if($form)
            <div class="w3-card-4 w3-pale-yellow">
              <div class="container-fluid">

              @if(count($otherForms) > 0)
              <div class="row w3-margin-top">
                <a href="#viewForms" data-toggle="modal" class="w3-text-gray w3-padding">See other form surveys</a>
              </div>

              <hr style="border-color: black">
              @endif

              <div class="row w3-margin w3-center">
                <h1><strong>{{$form->title}}</strong></h1>
                <p><strong>Form Survey</strong></p>
              </div>

              <div class="row w3-margin w3-center">
                <i class="fa fa-square-o fa-fw w3-xxlarge"></i>
                <i class="fa fa-square-o fa-fw w3-xxlarge"></i>
                <i class="fa fa-check-square-o fa-fw w3-xxlarge w3-text-green"></i>
                <i class="fa fa-square-o fa-fw w3-xxlarge"></i>
              </div>

              <div class="row w3-margin w3-center">
                <a class="btn btn-default w3-green" href="{{url('admin/manage-form/answer/' . $form->id)}}"><i class="fa fa-pencil-square-o fa-fw"></i> Answer Form</a>
              </div>
              </div>
            </div>
          @endif
          <!-- END FORM -->
        </div>

        <div class="row w3-margin-top">
          <div class="w3-card-4 w3-white">
            <div class="container-fluid">
              <div class="row w3-margin">
                <p>
                  <i class="fa fa-newspaper-o w3-xlarge"></i>
                  Posts
                </p>
              </div>
              @if(count($others) > 0)
                @foreach($others as $post)
                  <div class="row w3-margin">
                    <a href="{{url('/member/profile/post/'.$post->id.'/'.$post->slug)}}">
                      <h3 class="w3-text-gray"><i class="fa fa-circle fa-fw w3-xlarge w3-text-green"></i> <strong>{{$post->title}}</strong></h3>
                    </a>
                    <p class="w3-text-gray">by {{$post->member['first_name']}} {{$post->member['last_name']}}</p>
                  </div>
                  <hr>
                @endforeach

                <div align="center">
                  {{ $others->links() }}
                </div>
              @endif

            </div>
          </div>
        </div>

      </div>
    </div>


  </div>
</div>

<script type="text/javascript">
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>


@endsection
