@extends('layout/control_panel')

@section('title')
Borrow and Return
@endsection

@section('middle')

<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-suitcase fa-fw w3-xxlarge"></i>
      <strong>Borrow And Return</strong>
    </h3>
  </div>

  <hr>

  <div class="row w3-margin-top">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#borrowTab" class="w3-text-black">Borrow</a></li>

      <li><a data-toggle="tab" href="#returnTab" class="w3-text-black">Return</a></li>

      <li><a data-toggle="tab" href="#logsTab" class="w3-text-black">logs</a></li>
    </ul>
  </div>

  <div class="tab-content">

    <div id="borrowTab" class="tab-pane fade in active">
      <div class="row w3-margin-top">
        <!-- BORROW -->
        <div class="col-sm-12 col-md-2 col-lg-2"></div>

        <div class="col-sm-12 col-md-8 col-lg-8">
          <div class="w3-card-4 w3-white">
          <div class="w3-container w3-margin">
          <div class="row">
          <div class="w3-container">
            <div class="row">
            <div class="col-sm-6">
              <h3>
                <i class="fa fa-file-text-o fa-fw"></i> Borrower's Slip
              </h3>
            </div>

            <div class="col-sm-6" align="right">
              <button type="submit" class="btn btn-default w3-green w3-margin-top" align="right" form="borrow"><i class="fa fa-file-text-o fa-fw"></i> Borrow</button>
            </div>
            </div>
          </div>
            <hr>
            <form action="{{ url('admin/logistics/borrow-return/borrow') }}" method="POST" id="borrow">
              <div class="form-group">
                <label for="member_id">Borrower's Name:</label>

                <div class="dropdown">
                <button class="btn btn-default dropdown-toggle form-control" type="button" data-toggle="dropdown" id="borrowerSelect">
                Select Student Name
                <span class="caret"></span></button>
                <ul class="dropdown-menu" style="right: 0">
                  <input class="form-control" id="myInput-borrower" type="text" placeholder="Search..">
                  @foreach($members as $member)
                  <li value="{{$member->id}}"><a href="javascript:void(0)" onclick="setBorrower('{{$member->first_name}} {{$member->last_name}}', {{$member->id}})">{{$member->first_name}} {{$member->last_name}}</a></li>
                  @endforeach
                </ul>
              </div>

              <input type="hidden" name="member_id" id="member_id-borrower" value="">
            </div>

            <br>

            <div class="w3-container inventory-body">
            <div class="row">
            <div class="form-group">
                <p><strong>Items:</strong></p>
            </div>
            @foreach($inventories as $inventory)
              <div class="col-sm-6">
                <div class="checkbox">
                  <label><input type="checkbox" name="inventory[]" value="{{$inventory->id}}" class="inventory" data-related-item="{{$inventory->id}}-div">{{$inventory->inv_name}}</label>
                </div>

              <div id="{{$inventory->id}}-div" style="display: none">
                @if($inventory->inventory_category_id != $cat_equip_id)
                <div class="form-group">
                  <input type="text" name="quan[{{$inventory->id}}]" id="del_quan" tabindex="1" class="form-control" placeholder="Quantity: {{$inventory->inv_quantity}}">
                </div>

                @else
                <div class="w3-card-4 w3-margin">
                  <div class="w3-container">
                    @foreach($inv_equips as $inv_equip)
                        @if($inv_equip->inventory_id == $inventory->id)
                        <div class="checkbox">
                          <label><input type="checkbox" name="equip_id[{{$inventory->id}}][]" value="{{$inv_equip->id}}" >{{$inv_equip->brand_name}} {{$inv_equip->serial_num}}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                </div>
                @endif

                <div class="form-group">
                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="desc[{{$inventory->id}}]" placeholder="Description" rows="3"></textarea>
                </div>

            </div>
            </div>
            @endforeach
          </div>
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </form>

          <hr>

          </div>
        </div>

        </div>
        </div>
        <!-- END BORROW -->

        <div class="col-sm-12 col-md-2 col-lg-2"></div>
      </div>
    </div>

    <div id="returnTab" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <table class="table table-bordered table-hover" id="returnTable">
          <thead>
            <tr>
             <th class="w3-center">Student ID</th>
             <th class="w3-center">Borrower</th>
             <th class="w3-center">Item Borrowed</th>
             <th class="w3-center">Date Borrowed</th>
             <th class="w3-center">Action</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
            @foreach($inv_borrows as $inv_borrow)
            <tr>
              <td class="w3-center">{{$inv_borrow->member->id_num}}</td>
              <td class="w3-center">{{$inv_borrow->member->first_name}} {{$inv_borrow->member->last_name}}</td>
              <td class="w3-center">
                
                @foreach($inv_borrow->borrower_inv as $borrowed)
               
                  {{$borrowed->inventory->inv_name}},
                  
                @endforeach
                
              </td>
              <td class="w3-center">{{$inv_borrow->created_at}}</td>
              <td class="w3-center">
                <a class="w3-text-red w3-large" data-toggle="modal" href="#return-{{$inv_borrow->id}}" style="outline: 0"><i class="fa fa-reply" data-toggle="tooltip" data-placement="bottom" title="Return Item"></i></a>

                <!-- RETURN ITEM MODAL -->
                <div class="modal fade" id="return-{{$inv_borrow->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Return Item</h4>
                </div>
                <form action="{{ url('admin/logistics/borrow-return/return') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="w3-container">
                    <div class="row">

                      <div class="form-group">
                        <label for="borrower_id">ID:</label>
                        <input type="text" name="borrower_id" class="form-control" value="{{$inv_borrow->id}}" readonly>
                      </div>

                      <div class="form-group">
                        <label for="borrower_id">Borrower's Name:</label>
                        <input type="text" class="form-control" value="{{$inv_borrow->member->first_name}} {{$inv_borrow->member->last_name}}" disabled>
                      </div>

                      <div class="form-group">
                        <p><strong>Items Borrowed:</strong></p>
                        <ul>
                          <div class="row">
                            @foreach($inv_borrow->borrower_inv as $borrowed)
                            <div class="col-sm-6">
                              <li>
                              <p>{{$borrowed->inventory->inv_name}}</p>
                              <p>Quantity: {{$borrowed->borrower_quantity}}</p>

                              @foreach($borrowed->borrower_equip as $borrow_equip)
                                <p><i class="fa fa-check-square-o"></i> <b>{{$borrow_equip->equip->brand_name}} {{$borrow_equip->equip->serial_num}}</b></p>
                              @endforeach
                          </div>
                          @endforeach
                          </div>
                        </ul>

                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-default w3-red w3-text-white"><i class="fa fa-reply"></i> Return</button>
                </div>
                </form>
                </div>
                </div>
                </div>
                <!-- END RETURN ITEM -->
              </td>

            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

     <div id="logsTab" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <table class="table table-bordered table-hover" id="logsTable">
          <thead>
            <tr>
             <th class="w3-center">Student ID</th>
             <th class="w3-center">Borrower</th>
             <th class="w3-center">Item Borrowed</th>
             <th class="w3-center">Date Borrowed</th>
             <th class="w3-center">Date Returned</th>
            </tr>
          </thead>
          <tbody class="w3-text-gray">
            @if(count($logs) > 0)
            @foreach($logs as $inv_borrow)
            <tr>
              <td class="w3-center">{{$inv_borrow->member->id_num}}</td>
              <td class="w3-center">{{$inv_borrow->member->first_name}} {{$inv_borrow->member->last_name}}</td>
              <td class="w3-center">
                @foreach($inv_borrow->borrower_inv as $borrowed)
                 
                  {{$borrowed->inventory->inv_name}},
                 
                @endforeach
              </td>
              <td class="w3-center">{{$inv_borrow->created_at}}</td>
              <td class="w3-center">{{$inv_borrow->returned_datetime}}</td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>

  </div>

</div>

<script type="text/javascript">
$(document).ready( function () {
  $('#logsTable').DataTable();
  var table = $('#returnTable').DataTable();
  $(table.table().container()).removeClass('form-inline');

  $("#myInput-borrower").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

});
</script>

<script>
function setBorrower(name, id) {

  $("#borrowerSelect").html(name + ' <span class="caret"></span></button>');
  $("#member_id-borrower").attr('value', id);
}
</script>

<script type="text/javascript">
  function evaluate(){
    var item = $(this);
    var relatedItem = $("#" + item.attr("data-related-item")).parent();

    if(item.is(":checked")){
        $("#" + item.attr("data-related-item")).fadeIn();
    }else{
        $("#" + item.attr("data-related-item")).fadeOut();
    }
}

$('.inventory').click(evaluate).each(evaluate);
</script>

<script type="text/javascript">
 window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>
@endsection
