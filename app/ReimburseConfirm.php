<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReimburseConfirm extends Model
{
    protected $table = 'reimburse_confirms';
	protected $fillable = [
        'reimburse_request_id','admin_id','position','confirm_status','denied_reason','created_at','updated_at'
    ];
}
