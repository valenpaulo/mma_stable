<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsBudgetExpense extends Model
{
     protected $table = 'mtics_budget_expenses';
	protected $fillable = [
       'purchaser_admin_id','purchaser_admin_role_id','confirm_admin_id','confirm_admin_role','mtics_budget_breakdown_id','expense_name','expense_desc','expense_amt','receipt_id'
    ];

    public function receiver() {
        return $this->belongsTo(Member::class, 'purchaser_admin_id');
    }

    public function approvedBy() {
        return $this->belongsTo(Member::class, 'confirm_admin_id');
    }

    public function receiverRole() {
        return $this->belongsTo(Role::class, 'purchaser_admin_role_id');
    }

    public function approvedByRole() {
        return $this->belongsTo(Role::class, 'confirm_admin_role');
    }

     public function receipt() {
        return $this->belongsTo(Receipt::class, 'receipt_id');
    }

    public function breakdown() {
        return $this->belongsTo(MticsBudgetBreakdown::class, 'mtics_budget_breakdown_id');
    }

}
