<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsPurchaseEquipList extends Model
{
     protected $table = 'mtics_purchase_equip_lists';
	protected $fillable = [
       'mtics_purchase_list_id','mtics_brandname','mtics_serialnum','mtics_specs','created_at','updated_at','mtics_equip_price'

    ];
}
