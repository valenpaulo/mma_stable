<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivedInventory extends Model
{
     protected $table = 'archived_inventories';
	protected $fillable = [
        'inventory_id','archived_quantity','archived_reason'
    ];

     public function inventory()
    {
        return $this->belongsto(Inventory::class);
    }
}
