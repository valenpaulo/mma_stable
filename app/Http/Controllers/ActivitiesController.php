<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\MticsEvent;
use App\EventProgram;
use App\Role;
use App\RoleMember;
use App\Officerterm;

use App\Events\SystemEvent;
use Illuminate\Support\Facades\Event;

class ActivitiesController extends Controller
{
    public function view() {
         if(!Gate::allows('activity-only') and !Gate::allows('docu-only') and !Gate::allows('president-only') )
        {
        return redirect('/');
        }
        $events = MticsEvent::all();

        return view('officers/activities', compact('events'));
    }


    public function print_calendar() {

         if(!Gate::allows('activity-only') and !Gate::allows('docu-only') and !Gate::allows('president-only') )
            {
            return redirect('/');
            }

        $officerterm = Officerterm::where('status','on-going')->first();

        $fromyear = date('Y',strtotime($officerterm->start_term));
        $toyear = $fromyear + 1;
        $events = MticsEvent::where('start_date','>=',date($officerterm->created_at))->orderBy('start_date','ASC')->get();
        $activities = array();
        foreach ($events as $event) {
            $activities[$event->id]['date'] = date('F j,Y',strtotime($event->start_date));
            $activities[$event->id]['event'] = $event;

        }

        $roles = Role::where('name','activity')->orwhere('name','asst_activity')->orwhere('name','president')->with('member')->get();

        if($activities == "")
        {
            return redirect()->back()->withErrors('No Event yet, Please organize your Calendar of Activities');
        }

        return view('reports/calendar_report', compact('activities','fromyear','toyear','roles'));


    }


    public function storeEvent(Request $request) {
        $this->validate($request, [
            'event_title' => 'required|max:255',
            'description' => 'required|max:255',
            'goal' => 'required|max:255',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);


        $event = MticsEvent::create([
            'event_title' => $request->event_title,
            'description' => $request->description,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ]);

        return redirect()->back();
    }




//MANAGE EVENT PROGRAM //MANAGE EVENT PROGRAM //MANAGE EVENT PROGRAM //MANAGE EVENT PROGRAM //MANAGE EVENT PROGRAM

    public function event_program_add(Request $request,$id) {
        $event = MticsEvent::find($id);

        if($event->status !== 'on-going'){
            return redirect()->back()->withErrors('Event Status is '.$event->status.', make changes are prohibited');
        }

        if($event->post_status == 'posted'){
            return redirect()->back()->withErrors('Event has been posted, make changes are prohibited');
        }

        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            ]);

        $activity_id = Role::where('name','activity')->first()->id;
        $activity_asst_id = Role::where('name','asst_activity')->first()->id;
        $role_id = '';
        if(!RoleMember::where('role_id',$activity_id)->where('member_id',Auth::id())->get()->isEmpty()){
            $role_id = $activity_id;
        }
        elseif(!RoleMember::where('role_id',$activity_asst_id)->where('member_id',Auth::id())->get()->isEmpty()){
            $role_id = $activity_asst_id;
        }
        else{
            return redirect()->back()->withErrors('invalid role');
        }



        if(!EventProgram::where('event_id',$id)->get()->isEmpty()){
                $eventprogram = EventProgram::where('event_id',$id)->first();
        }
        else{
                $eventprogram = New EventProgram;
        }

        $eventprogram->admin_id = Auth::id();
        $eventprogram->admin_role_id = $role_id;
        $eventprogram->event_id = $id;
        $eventprogram->title = $request->title;
        $eventprogram->body = $request->body;
        $eventprogram->save();

        Event::fire(new SystemEvent(auth::id(), 'Programme Added.'));

        return redirect()->back()->with('success', 'Programme Added!');

    }
}
