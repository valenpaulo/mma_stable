@extends('layout/login')

@section('title')
Reset Password
@endsection

@section('content')
<div class="w3-container">
    <div class="w3-display-middle w3-card-4 w3-white w3-col m4">
        <div class="w3-container w3-margin-top">
            <div class="w3-container w3-text-black w3-center">
            <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.png') : asset('images/logo/mtics.png')}}" class="w3-margin-bottom w3-margin-top" style="height: 100px; width: 100px">
            <h4>Manila Technician Institute Computer Society</h4>
            </div>
            <br>

            <p class="w3-text-gray" align="center"><strong>Reset Password</strong></p>

            <hr>

            <form action="{{ url('/member/password/reset') }}" method="post">

              {{ csrf_field() }}

              <input type="hidden" name="token" value="{{ $token }}">

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="username">Email</label>
                <input type="text" class="form-control" id="email" name="email" required value="{{old('email')}}" autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" required autofocus>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>

              <div class="form-group{{ $errors->has('password_confirmation)') ? ' has-error' : '' }}">
                <label for="password-confirm">Confirm Password</label>
                <input type="password" class="form-control" id="password-confirm" name="password_confirmation" required autofocus>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-default w3-text-white w3-teal" style="background-color: #2471A3">Reset Password</button>
              </div>
            </form>
            <br>
        </div>

    </div>
</div>

<script type="text/javascript">
  $(window).on('load', function() {
    if("{{session('status')}}" != ""){
      swal("{{session('statu')}}", "Done", "success")
    }
  });
</script>
@endsection

