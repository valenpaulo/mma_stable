@extends('layout/control_panel')

@section('title')
Form Analytics
@endsection

@section('middle')
{!! Charts::styles() !!}
{!! Charts::scripts() !!}

<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-pie-chart fa-fw w3-xxlarge"></i>
      <strong>Form Analytics</strong>
    </h3>
  </div>

  <hr>

  <div class="row" align="center">
    @foreach($charts as $chart)
    <div class="col-sm-12 col-lg-6 col-md-6">
      <div class="w3-card-4 w3-white w3-margin">
        {!! $chart->html() !!}
        {!! $chart->script() !!}
      </div>
    </div>
    @endforeach
  </div>
</div>

@endsection
