<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespositWithdrawTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('mtics_deposits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->integer('faculty_id')->nullable();
            $table->longtext('deposit_desc')->nullable();
            $table->integer('deposit_amt');
            $table->longtext('denied_reason')->nullable();
            $table->enum('deposit_status',['pending','approved','denied','cleared'])->default('pending');
            $table->timestamps();
        });

         Schema::create('mtics_deposit_receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mtics_deposit_id');
            $table->integer('faculty_id'); 
            $table->string('receipt_image');            
            $table->timestamps();
        });

        Schema::create('mtics_withdrawals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->integer('faculty_id')->nullable();
            $table->longtext('withdrawal_reason');
            $table->longtext('withdrawal_desc')->nullable();
            $table->integer('withdrawal_amt');
            $table->longtext('denied_reason')->nullable();
            $table->enum('withdrawal_status',['pending','approved','denied','cleared'])->default('pending');
            $table->timestamps();
        });

        Schema::create('mtics_withdrawal_receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mtics_withdrawal_id');
            $table->integer('faculty_id'); 
            $table->string('receipt_image');            
            $table->timestamps();
        });

        Schema::create('mtics_withdrawal_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mtics_withdrawal_id');
            $table->integer('faculty_id'); 
            $table->string('withdrawal_title');          
            $table->longtext('withdrawal_desc');         
            $table->timestamps();
        });

        Schema::create('mtics_deposit_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mtics_deposit_id');
            $table->integer('faculty_id'); 
            $table->string('deposit_title');          
            $table->longtext('deposit_desc');         
            $table->timestamps();
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('mtics_deposits');
         Schema::drop('mtics_deposit_receipts');
         Schema::drop('mtics_deposit_reports');
         
         Schema::drop('mtics_withdrawals');
         Schema::drop('mtics_withdrawal_receipts');
         Schema::drop('mtics_withdrawal_reports');

        
    }
}
