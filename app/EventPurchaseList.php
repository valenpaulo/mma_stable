<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPurchaseList extends Model
{
    protected $table = 'event_purchase_lists';
	protected $fillable = [
        'task_id','event_itemname','event_itemdesc','event_orig_quan','event_est_price','event_act_quan','event_act_price','event_remarks','inventory_category_id','event_total_price','event_inv_status','external_id','event_itemstatus','logistics_id',

    ];

	 public function eventpurchaseequiplist()
    {
        return $this->hasMany(EventPurchaseEquipList::class);
    }


     public function eventpurchasereport()
    {
        return $this->hasMany(EventPurchaseReport::class);
    }

    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }

     public function receipt()
    {
        return $this->belongsToMany(Receipt::class,'receipt_eventitems','event_purchase_list_id','receipt_id');
    }

    public function category() {
        return $this->belongsTo(InventoryCategory::class, 'inventory_category_id');
    }

}
