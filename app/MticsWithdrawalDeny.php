<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsWithdrawalDeny extends Model
{
    protected $table = 'mtics_withdrawal_denies';
	protected $fillable = [
   		'mtics_withdrawal_id','faculty_id','denied_reason'
    ];

    public function faculty() {
        return $this->belongsTo(Member::class, 'faculty_id');
    }
}

