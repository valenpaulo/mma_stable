@extends('layout/control_panel')

@section('title')
Manage Member
@endsection

@section('middle')


<b>ATTENDEE:</b><br>


 <button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#absent" title="Edit"><i class="fa fa-pencil"></i> Absence</button><br><br><br>


@foreach($officers as $officer)

name: {{$officer->member->first_name}} {{$officer->member->last_name}}<br>



 @foreach($absences as $absence)
    @if($absence->member_id == $officer->member->id)
      <b>Absent</b><br>
       
       <!-- <button class="btn btn-default w3-orange w3-text-white" data-toggle="modal" data-target="#present-{{$absence->id}}" title="Edit"><i class="fa fa-pencil"></i> present</button><br> -->


      <!-- present -->
    <!-- <div class="modal fade" id="present-{{$absence->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>

          <form action="{{ url('admin/docu/manage-event/'.$event->id.'/meeting/'.$meeting->id.'/attendance/'.$absence->id.'/set-present') }}" method="POST" enctype="multipart/form-data">

            <h4 class="modal-title w3-text-gray" id="myModalLabel">present</h4>
            </div>
           <div class="modal-body">

              are you sure, you want to set present this student?
            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
            </div>

          </div>
        </form>
        </div>
      </div>
    </div> -->

    @endif
  @endforeach 


<form action="{{ url('admin/docu/manage-event/'.$event->id.'/meeting/'.$meeting->id.'/attendance/set-absent') }}" method="POST" enctype="multipart/form-data">
<label><input type="checkbox" value="{{$officer->member->id}}" name="officer_id[]">
absent</label><br><br>

@endforeach


<!-- Absent -->
    <div class="modal fade" id="absent" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    
            <h4 class="modal-title w3-text-gray" id="myModalLabel">edit offers</h4>
          </div>
          <div class="modal-body">

              are you sure, you want to set absent those following student?
            <div class="modal-footer">
              <button type="submit" class="btn btn-default w3-red"><i class="fa fa-plus-square fa-fw"></i> Yes</button>
            </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">

          </form>
          </div>
        
        </div>
      </div>
    </div>


@endsection