<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReimburseRequest extends Model
{
	protected $table = 'reimburse_requests';
	protected $fillable = [
        'money_request_id','reimburse_amount','reimburse_remarks','reimburse_status','created_at','updated_at'
    ];

     public function moneyrequest()
    {
        return $this->belongsTo(MoneyRequest::class, 'money_request_id');
    }

     public function confirm()
    {
        return $this->hasMany(ReimburseConfirm::class);
    }

}
