<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerInventory extends Model
{
    protected $table = 'borrower_inventories';
	protected $fillable = [
        'inventory_id','borrower_id','borrower_quantity','borrower_desc'
    ];

   public function borrower_equip()
    {
        return $this->hasMany('App\BorrowinvEquip', 'borrower_inventory_id');
    }

    public function inventory()
    {
        return $this->belongsTo('App\Inventory', 'inventory_id');
    }
}
