<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEventTransferStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {

        Schema::table('mtics_deposits', function ($table) {
            $table->dropColumn('deposit_source');

        });

        Schema::table('mtics_deposits', function ($table) {
            $table->enum('deposit_source',['MTICS Payment','MTICS Donation','Event','other','Event Collection'])->default('other')->after('deposit_amt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
