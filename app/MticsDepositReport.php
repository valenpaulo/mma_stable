<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsDepositReport extends Model
{
    protected $table = 'mtics_deposit_reports';
	protected $fillable = [
      'mtics_deposit_id','faculty_id','deposit_title','deposit_desc'
    ];

     public function faculty() {
        return $this->belongsTo(Member::class, 'faculty_id');
    }
}
