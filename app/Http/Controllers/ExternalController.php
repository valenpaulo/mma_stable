<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use App\Notifications\SystemNotification;
use App\Task;
use App\MticsPurchaseList;
use App\MticsPurchaseEquipList;
use App\EventPurchaseList;
use App\EventPurchaseEquipList;
use App\Role;
use App\RoleMember;
use App\MoneyRequest;
use App\InventoryCategory;
use App\ReimburseRequest;
use App\Member;
use App\Officerterm;
use App\MticsEvent;

use App\Events\SystemEvent;
use Illuminate\Support\Facades\Event;

use App\MticsPurchaseReport;
use App\EventPurchaseReport;


class ExternalController extends Controller
{
    public function view()
    {
    	return 'EXTERNAL VIEW';
    }


    //REQUEST-TO-BUY //REQUEST-TO-BUY //REQUEST-TO-BUY //REQUEST-TO-BUY //REQUEST-TO-BUY //REQUEST-TO-BUY

    public function reqtobuy_view()
    {
      $term = Officerterm::where('status','on-going')->first();
      $tasks = Task::with('admin')->where('event_id', null)->where('updated_at','>=',date($term->created_at))->orderBy('id', 'desc')->get();

      // dd($tasks[0]->mticspurchaselist);

      return view('admin/addFeature/request_to_buy', compact('tasks'));
    }


    public function reqtobuy_store(Request $request)
    {
      //dd($request);

        $this->validate($request, [
        'due_date' => 'required',
        'task_name' => 'required',
        ]);

        $external_id = Role::getId('App\Role','name','external');
        $role_members = RoleMember::where('role_id',$external_id)->get();
        foreach ($role_members as $role_member) {
          $member_id = $role_member->member_id;
        }

        if(!isset($member_id))
            {
              return redirect()->back()->withErrors('Chief External Officer is not set yet, please set External Officer first');
            }
        $task = New Task;
        $task->member_id = $member_id;
        $task->admin_id = Auth::id();
        $task->due_date = $request->due_date;
        $task->task_name = $request->task_name;
        $task->role_id = $external_id;
        if($request->task_desc == null)
        {
        $task->task_desc = 'please buy the following..';
        }
        else
        {
        $task->task_desc = $request->task_desc;
        }

        if(isset($request->urgent))
        {
          $task->task_status = 'urgent';
        }
        $task->save();

          foreach ($request->itemname as $key => $item) {

            $mticspurchaselist = New MticsPurchaseList;
            $mticspurchaselist->task_id = $task->id;
            $mticspurchaselist->mtics_itemname = $item;
            $mticspurchaselist->mtics_itemdesc = $request->itemdesc[$key];
            $mticspurchaselist->mtics_orig_quan = $request->itemquan[$key];
            $mticspurchaselist->save();

          }

            $member = Member::findOrFail($task->member_id);

            $data = array(
                      'message' => "New Purchase Request.",
                      'redirect' => "admin/external/task/" . $task->id,
                      'origin' => 'purchase-request',
                      );

              $member->notify(new SystemNotification($data));

              Event::fire(new SystemEvent(auth::id(), 'Purchase Request Sent.'));

        return redirect('admin/request-to-buy')->with('success', 'Request Sent!');
    }


    public function eventView($id) {

      if(!Gate::allows('external-only'))
        {
        return redirect('/');
        }

      $event = MticsEvent::findOrFail($id);

      $tasks = Task::where(function ($q) {
          $q->where('task_status', 'pending')->orwhere('task_status', 'on-going')->orwhere('task_status', 'for validation')->orwhere('task_status', 'disapproved')->orwhere('task_status', 'done');
      })->whereHas('role', function ($q) {
          $q->where('name', 'external');
      })->where('member_id', Auth::user()->id)->where('event_id', $id)->get();

      return view('officers/event/external', compact('event', 'tasks'));

    }

    //TASK PURCHASE LIST //TASK PURCHASE LIST //TASK PURCHASE LIST //TASK PURCHASE LIST //TASK PURCHASE LIST

    public function task_view($id, $task_id)
    {
      if(!Gate::allows('external-only'))
        {
        return redirect('/');
        }
      $event = MticsEvent::findOrFail($id);

      $task = Task::where(function ($q) {
          $q->where('task_status', 'pending')->orwhere('task_status', 'on-going')->orwhere('task_status', 'for validation')->orwhere('task_status', 'disapproved')->orwhere('task_status', 'done')->orwhere('task_status', 'failed');
      })->whereHas('role', function ($q) {
          $q->where('name', 'external');
      })->where('member_id', Auth::user()->id)->where('event_id', $id)->where('id', $task_id)->first();
      $purchaseList = EventPurchaseList::where('task_id', $task_id)->get();

      $purchasedItems = EventPurchaseList::where('task_id', $task_id)->where('event_itemstatus', 'purchased')->where(function($q){
          $q->where('event_itemname', '!=', 'meal')->where('event_itemname', '!=', 'fare');
      })->get();

        //Check if there is Money Request for this task
        $moneyrequest = MoneyRequest::where('task_id', $task_id)->first();
        if($moneyrequest !== null){
           $sum_equip_price = 0;
           $sum_price = 0;
             if($task->event_id == null)
             {
                 foreach ($task->mticspurchaselist as $mticspurchaselist) {
                   if(!MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->isEmpty()){
                    $sum_equip_price = $sum_equip_price + MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->sum('mtics_equip_price');
                   }
                   else{
                    $sum_price = $sum_price + $mticspurchaselist->mtics_total_price;
                   }
                 }
             }
             else
             {
                  foreach ($task->eventpurchaselist as $eventpurchaselist) {
                   if(!EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get()->isEmpty()){
                    $sum_equip_price = $sum_equip_price + EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get()->sum('event_equip_price');
                   }
                   else{
                    $sum_price = $sum_price + $eventpurchaselist->event_total_price;

                   }
                 }
             }
                


             $total_deduction = $sum_price + $sum_equip_price;
             //COMPUTING TOTAL MONEY BUCKET
             $total_money_bucket = $moneyrequest->amount - $total_deduction;
             
             if($moneyrequest->mon_req_status == 'cleared' or $moneyrequest->mon_req_status == 'denied' or $moneyrequest->excess_amount !== null or $moneyrequest->mon_req_status == 'for validation' or $moneyrequest->mon_req_status == 'pending' ){
              $total_money_bucket = 0;
             }
                



          }
          else
          {
            $total_money_bucket = 0;
          }



      return view('officers/event/external_task', compact('event', 'task', 'total_money_bucket', 'purchaseList', 'purchasedItems'));

    }



    public function task_edit(Request $request,$id)
    {

      if(!Task::where('id',$id)->where('task_status','done')->get()->isEmpty()){
           return redirect()->back()->withErrors('This Task is already done');
        }
      if(!MoneyRequest::where('task_id',$id)->get()->isEmpty()){
           return redirect()->back()->withErrors('You already requested money for this task, make changes are prohibited');
        }

      $task = Task::find($id);


      foreach ($request->est_amt as $key => $value) {
        if($value['price']==null){
          continue;
        }
        else{
          if($request->est_amt < 1){
            return redirect()->back()->withErrors('estimated price can not be 0 or negative');
          }
          else
          {

            if($task->event_id == null)
            {
           MticsPurchaseList::where('id', $key)->update(array('mtics_est_price' => $value['price']));
            }
            else
            {
           EventPurchaseList::where('id', $key)->update(array('event_est_price' => $value['price']));
            }
          }
        }
      }



      Event::fire(new SystemEvent(auth::id(), 'Indicated an Estimate Price.'));

      return redirect()->back()->with('success', 'Done!');
    }



    // REQUEST-MONEY // REQUEST-MONEY // REQUEST-MONEY // REQUEST-MONEY // REQUEST-MONEY // REQUEST-MONEY

    public function req_store(Request $request, $id)
    {
        //dd($request);
       $task = Task::find($id);
       if(!MoneyRequest::where('task_id',$task->id)->get()->isEmpty())
        {
          return redirect()->back()->withErrors('You already requested money for this task');
        }

       if(!MticsPurchaseList::where('task_id',$task->id)->get()->isEmpty())
        { //FOR TASK WITH REQUEST TO BUY (MTICS)
          $expected_budget=0;
          $mticspurchaselists = MticsPurchaseList::where('task_id',$task->id)->get();
          foreach ($mticspurchaselists as $mticspurchaselist) {
            $total_price_product = $mticspurchaselist->mtics_orig_quan * $mticspurchaselist->mtics_est_price;
            $expected_budget = $expected_budget + $total_price_product;
          }

          $request_money = New MoneyRequest;
          $request_money->amount = $expected_budget;

          $data = array(
            'message' => "External is requesting money for " . $task->task_name,
            'redirect' => "admin/finance/requested-money",
            'origin' => 'request-money',
            );

          $role_id = Role::where('name', 'finance')->first()->id;
          $member = Member::whereHas('role', function($q) use ($role_id) {
              $q->where('role_id', $role_id);
          })->first();
          $member->notify(new SystemNotification($data));

        }
        elseif(!EventPurchaseList::where('task_id',$task->id)->get()->isEmpty())
        {
          //FOR TASK WITH REQUEST TO BUY (EVENT)
          $expected_budget=0;
          $eventpurchaselists = EventPurchaseList::where('task_id',$task->id)->get();
          foreach ($eventpurchaselists as $eventpurchaselist) {
            $total_price_product = $eventpurchaselist->event_orig_quan * $eventpurchaselist->event_est_price;
            $expected_budget = $expected_budget + $total_price_product;
          }
          $request_money = New MoneyRequest;
          $request_money->amount = $expected_budget;

          $data = array(
            'message' => "External is requesting money for " . $task->task_name,
            'redirect' => array('role' => 'finance', 'event_id' => $task->event_id),
            'origin' => 'external',
            );

          $role_id = Role::where('name', 'finance')->first()->id;
          $member = Member::whereHas('role', function($q) use ($role_id) {
              $q->where('role_id', $role_id);
          })->first();
          $member->notify(new SystemNotification($data));

        }
        else
        { //FOR TASK WITHOUT REQUEST TO BUY
            $this->validate($request, [
            'remarks' => 'required',
            ]);

          $request_money = New MoneyRequest;
          $request_money->amount = 0;
        }

          $request_money->remarks = $request->remarks;
          $request_money->task_id = $id;
          $request_money->member_requested_id = Auth::id();
          $request_money->save();

        return redirect()->back()->with('success', 'Money Requested!');
    }



    //TASK DETAILS VIEW //TASK DETAILS VIEW //TASK DETAILS VIEW //TASK DETAILS VIEW

    public function task_details_view($id)
    {

       if(!Gate::allows('external-only'))
        {
        return redirect('/');
        }

      //Check if the task id is invalid
      if(Task::where('id',$id)->get()->isEmpty()){
           return redirect()->back()->withErrors('Task Id is invalid');
        }
      //check if there is request-to-buy for this task
      $task = Task::with('mticspurchaselist')->with('eventpurchaselist')->where('id',$id)->first();

      if($task->mticspurchaselist->isEmpty() and $task->eventpurchaselist->isEmpty()){
          return redirect()->back()->withErrors('There is no request to buy for this task');
       }

      //Check if there is Money Request for this task
      $moneyrequest = MoneyRequest::where('task_id',$id)->first();
      if($moneyrequest !== null){
         $sum_equip_price = 0;
         $sum_price = 0;


         if($task->event_id == null)
         {
             foreach ($task->mticspurchaselist as $mticspurchaselist) {
               if(!MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->isEmpty()){
                $sum_equip_price = $sum_equip_price + MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->sum('mtics_equip_price');
               }
               else{
                $sum_price = $sum_price + $mticspurchaselist->mtics_total_price;
               }
             }
         }
         else
         {
              foreach ($task->eventpurchaselist as $eventpurchaselist) {
               if(!EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get()->isEmpty()){
                $sum_equip_price = $sum_equip_price + EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get()->sum('event_equip_price');
               }
               else{
                $sum_price = $sum_price + $eventpurchaselist->event_total_price;
               }
             }
         }


         $total_deduction = $sum_price + $sum_equip_price;
         //COMPUTING TOTAL MONEY BUCKET
         $total_money_bucket = $moneyrequest->amount - $total_deduction;
         if($moneyrequest->mon_req_status == 'cleared' or $moneyrequest->mon_req_status == 'denied' or $moneyrequest->excess_amount !== null or $moneyrequest->mon_req_status == 'for validation' ){
          $total_money_bucket = 0;
         }




      }
      else
      {
        $total_money_bucket = 0;
      }

      $purchasedItems = MticsPurchaseList::where('task_id', $task->id)->where('mtics_itemstatus', 'purchased')->where(function($q){
          $q->where('mtics_itemname', '!=', 'meal')->where('mtics_itemname', '!=', 'fare');
      })->get();

      return view('officers/external_task_view',compact('task','moneyrequest','total_money_bucket', 'purchasedItems'));
    }

    // ADD EQUIPMENT // ADD EQUIPMENT // ADD EQUIPMENT // ADD EQUIPMENT

    public function task_details_addequip(Request $request,$id,$pur_id)
    {

        //IF HAVE NOT REQUEST A MONEY
        if(MoneyRequest::where('task_id',$id)->get()->isEmpty()){
            return redirect()->back()->withErrors('You need to Request Money first');
          }

        //IF REQUESTED MONEY HAVE NOT ACCEPTED
        if(MoneyRequest::where('mon_req_status','pending')->where('task_id',$id)->first()){
            return redirect()->back()->withErrors('Money Request still not accepted');
            }

       //IF REQUEST MONEY HAS BEEN DENIED
        if(MoneyRequest::where('mon_req_status','denied')->where('task_id',$id)->first())
          {
          return redirect()->back()->withErrors('Request Money for this task has been denied');
          }


        $this->validate($request, [
        'brandname' => 'required',
        'serialno' => 'required',
        'specs' => 'required',
        'price' => 'required',
        ]);

        $task = Task::find($id);
        if($task->event_id == null)
        {
            $mticspurchaselist = MticsPurchaseList::find($pur_id);
            if($mticspurchaselist->mtics_itemstatus == 'purchased'){
              return redirect()->back()->withErrors('This Item has been purchased');
            }

            if($mticspurchaselist->mtics_itemname == 'meal' or $mticspurchaselist->mtics_itemname == 'fare')
            {
              return redirect()->back()->withErrors('cannot add equipment for this item');
            }


            $mtics_equip = New MticsPurchaseEquipList;
            $mtics_equip->mtics_purchase_list_id = $pur_id;
            $mtics_equip->mtics_brandname = $request->brandname;
            $mtics_equip->mtics_serialnum = $request->serialno;
            $mtics_equip->mtics_specs = $request->specs;
            $mtics_equip->mtics_equip_price = $request->price;
            $mtics_equip->save();

            //CHANGING INVENTORY CATEGORY TO EQUIPMENT
            $equip = InventoryCategory::where('inv_cat_name','equip')->first();
            MticsPurchaseList::where('id', $pur_id)->update(array('inventory_category_id' => $equip->id));

            //DETERMINE IF THE QUANTITY HAS BEEN MATCH UP
            $quantity = $mticspurchaselist->mtics_orig_quan;
            $count = count(MticsPurchaseEquipList::where('mtics_purchase_list_id',$pur_id)->get());

             $all_equips = MticsPurchaseEquipList::where('mtics_purchase_list_id',$pur_id)->get();
             $mtics_total_price = MticsPurchaseEquipList::where('mtics_purchase_list_id',$pur_id)->get()->sum('mtics_equip_price');

             //CHECK IF THEY HAVE SAME PRICE
             $cons_price = 0;
             $isSame = false;
             foreach ($all_equips as $all_equip) {
               $price = $all_equip->mtics_equip_price;
               if($cons_price == 0){
                $cons_price = $price;
                $fprice = $price;
               }
               elseif($cons_price == $price){
                $isSame = false;
                $fprice = $price;
               }
               else
               {
                $isSame = true;
                goto here;
               }
             }

          here:
            if($isSame == false){
                MticsPurchaseList::where('id', $pur_id)->update(array('mtics_act_price' => $fprice));
            }

            MticsPurchaseList::where('id', $pur_id)->update(array('mtics_act_quan' => $count));
            MticsPurchaseList::where('id', $pur_id)->update(array('mtics_total_price' => $mtics_total_price));

            if($quantity == $count){
            MticsPurchaseList::where('id', $pur_id)->update(array('mtics_itemstatus' => 'purchased'));
            MticsPurchaseList::where('id', $pur_id)->update(array('external_id' => Auth::id()));
            }
        }
        else
        {

            $eventpurchaselist = EventPurchaseList::find($pur_id);
            if($eventpurchaselist->event_itemstatus == 'purchased'){
              return redirect()->back()->withErrors('This Item has been purchased');
            }

            if($eventpurchaselist->event_itemname == 'meal' or $eventpurchaselist->event_itemname == 'fare')
            {
              return redirect()->back()->withErrors('cannot add equipment for this item');
            }


            $event_equip = New EventPurchaseEquipList;
            $event_equip->event_purchase_list_id = $pur_id;
            $event_equip->event_brandname = $request->brandname;
            $event_equip->event_serialnum = $request->serialno;
            $event_equip->event_specs = $request->specs;
            $event_equip->event_equip_price = $request->price;
            $event_equip->save();

            //CHANGING INVENTORY CATEGORY TO EQUIPMENT
            $equip = InventoryCategory::where('inv_cat_name','equip')->first();
            EventPurchaseList::where('id', $pur_id)->update(array('inventory_category_id' => $equip->id));

            //DETERMINE IF THE QUANTITY HAS BEEN MATCH UP
            $quantity = $eventpurchaselist->event_orig_quan;
            $count = count(EventPurchaseEquipList::where('event_purchase_list_id',$pur_id)->get());

            if($quantity == $count){
             $all_equips = EventPurchaseEquipList::where('event_purchase_list_id',$pur_id)->get();
             $event_total_price = EventPurchaseEquipList::where('event_purchase_list_id',$pur_id)->get()->sum('event_equip_price');

             //CHECK IF THEY HAVE SAME PRICE
             $cons_price = 0;
             $isSame = false;
             foreach ($all_equips as $all_equip) {
               $price = $all_equip->event_equip_price;

               if($cons_price == 0){
                $cons_price = $price;
               }
               elseif($cons_price == $price){
                $isSame = false;
                $fprice = $price;
               }
               else
               {
                $isSame = true;
                goto hereevent;
               }
             }

            hereevent:
            //dd($isSame);
            if($isSame == false){
                EventPurchaseList::where('id', $pur_id)->update(array('event_act_price' => $fprice));
            }

            EventPurchaseList::where('id', $pur_id)->update(array('event_act_quan' => $count));
            EventPurchaseList::where('id', $pur_id)->update(array('event_total_price' => $event_total_price));
            EventPurchaseList::where('id', $pur_id)->update(array('event_itemstatus' => 'purchased'));
            EventPurchaseList::where('id', $pur_id)->update(array('external_id' => Auth::id()));
            }

        }



        //Check if all item has been purchase
        $this->isTaskforvalidation($id);
        $this->isMoneyReqforvalidation($id);

        Event::fire(new SystemEvent(auth::id(), 'Added Equipment.'));

    return redirect()->back()->with('success', 'Done!');
    }

//ADD PURCHASE //ADD PURCHASE //ADD PURCHASE //ADD PURCHASE
    public function task_details_purchased(Request $request,$id,$pur_id)
    {
        //IF THERE IS NO REQUEST MONEY FOR THIS TASK
        if(MoneyRequest::where('task_id',$id)->get()->isEmpty()){
          return redirect()->back()->withErrors('You need to Request Money first');
        }

        //IF REQUEST MONEY HAVEN'T ACCEPTED YET
        if(MoneyRequest::where('mon_req_status','pending')->where('task_id',$id)->first())
          {
          return redirect()->back()->withErrors('Money Request still not accepted');
          }

        //IF REQUEST MONEY HAS BEEN DENIED
        if(MoneyRequest::where('mon_req_status','denied')->where('task_id',$id)->first())
          {
          return redirect()->back()->withErrors('Request Money for this task has been denied');
          }

        $task = Task::find($id);
        $equip = InventoryCategory::where('inv_cat_name','equip')->first();

        //UPDATING PURCHASED ITEM INFO WITH ACTUAL QUANTITY AND PRICE
        if($task->event_id == null)
        { //FOR MTICS PURCHASED ITEMS
            $mticspurchaselist = MticsPurchaseList::find($pur_id);
            if($mticspurchaselist->mtics_itemstatus == 'purchased'){
              return redirect()->back()->withErrors('This Item has been purchased');
            }
            if($mticspurchaselist->inventory_category_id == $equip->id)
            {
              return redirect()->back()->withErrors('This item is equipment, please add equip instead');
            }

            //For item fare and meal, must be quantity 1 only
            if($mticspurchaselist->mtics_itemname !== 'fare' and $mticspurchaselist->mtics_itemname !== 'meal')
            {
              $this->validate($request, [
              'quan' => 'required|integer',
              ]);
              $quan = $request->quan;
            }
            else{
              $quan = 1;
            }


            //changes the information from pending to purchased
            if($request->tprice !== null or $request->tprice < 1){
              $this->validate($request, [
              'tprice' => 'required|integer',
              ]);

              if($mticspurchaselist->mtics_act_quan !== null and $mticspurchaselist->mtics_total_price !== null)
              {
                $quan = $quan + $mticspurchaselist->mtics_act_quan;
                $totalprice = $request->tprice + $mticspurchaselist->mtics_total_price;

              }
              else
              {
                $totalprice = $request->tprice;
              }


              MticsPurchaseList::where('id', $pur_id)->update(array('mtics_total_price' => $totalprice));
              $act_price = $totalprice/$quan;
              MticsPurchaseList::where('id', $pur_id)->update(array('mtics_act_price' => $act_price));
              MticsPurchaseList::where('id', $pur_id)->update(array('mtics_act_quan' => $quan));
              if($mticspurchaselist->mtics_orig_quan <= $quan)
              {
                MticsPurchaseList::where('id', $pur_id)->update(array('mtics_itemstatus' => 'purchased'));
              }
              MticsPurchaseList::where('id', $pur_id)->update(array('external_id' => Auth::id()));
            }

          }
        else
        { //FOR EVENT PURCHASED ONLY
            //if the items already purchased
            $eventpurchaselist = EventPurchaseList::find($pur_id);
            if($eventpurchaselist->event_itemstatus == 'purchased'){
              return redirect()->back()->withErrors('This Item has been purchased');
            }

            if($eventpurchaselist->inventory_category_id == $equip->id)
            {
              return redirect()->back()->withErrors('This item is equipment, please add equip instead');
            }

            //for meal and fare item, must be quantity 1 only
            if($eventpurchaselist->event_itemname !== 'fare' and $eventpurchaselist->event_itemname !== 'meal')
            {
              $this->validate($request, [
              'quan' => 'required|integer',
              ]);
              $quan = $request->quan;
            }
            else{
              $quan = 1;
            }

            //updating information
            if($request->tprice !== null or $request->tprice < 1){
              $this->validate($request, [
              'tprice' => 'required|integer',
              ]);
              EventPurchaseList::where('id', $pur_id)->update(array('event_total_price' => $request->tprice));
              $act_price = $request->tprice/$quan;
              EventPurchaseList::where('id', $pur_id)->update(array('event_act_price' => $act_price));
              EventPurchaseList::where('id', $pur_id)->update(array('event_act_quan' => $quan));
              if($eventpurchaselist->event_orig_quan <= $quan)
              {
                EventPurchaseList::where('id', $pur_id)->update(array('event_itemstatus' => 'purchased'));
              }
              EventPurchaseList::where('id', $pur_id)->update(array('external_id' => Auth::id()));


            }

        }

              //Check if all item has been purchase
              $this->isTaskforvalidation($id);
              $this->isMoneyReqforvalidation($id);


        Event::fire(new SystemEvent(auth::id(), 'Purchased an item.'));



    return redirect()->back()->with('success', 'Purchased!');

    }

    public function reimburse($id)
    {
       $task = Task::with('mticspurchaselist')->with('eventpurchaselist')->where('id',$id)->first();
       //Check if the task is already done
       if($task->task_status !== 'done' and $task->task_status !== 'failed' and $task->task_status !== 'for validation' ){
          return redirect()->back()->withErrors('This Task is '.$task->task_status .', You cannot request Reimbursement');
       }

       //Check the total purchased item and total deduction
       $sum_equip_price = 0;
       $sum_price = 0;

       if($task->event_id == null)
       {  //FOR MTICS
           //Check if there is 'Request to buy' for this task
           if($task->mticspurchaselist->isEmpty()){
              return redirect()->back()->withErrors('There is no request to buy for this task');
           }
           foreach ($task->mticspurchaselist as $key => $mticspurchaselist) {
             if(!MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->isEmpty()){
              $sum_equip_price = $sum_equip_price + MticsPurchaseEquipList::where('mtics_purchase_list_id',$mticspurchaselist->id)->get()->sum('mtics_equip_price');
             }
             else{
              $sum_price = $sum_price + $mticspurchaselist->mtics_total_price;
             }

             if(count($task->mticspurchaselist) == ++$key) {
                  $data = array(
                        'message' => "You have a request for reimbursement",
                        'redirect' => array('role' => 'finance', 'event_id' => $task->event_id, 'task_id' => $task->id),
                        'origin' => 'external',
                        );

                  $role_id = Role::where('name', 'finance')->first()->id;
                  $member = Member::whereHas('role', function($q) use ($role_id) {
                      $q->where('role_id', $role_id);
                  })->first();

                  $member->notify(new SystemNotification($data));

              }
           }
       }
       else
       { //FOR EVENT
          //Check if there is 'Request to buy' for this task
           if($task->eventpurchaselist->isEmpty()){
              return redirect()->back()->withErrors('There is no request to buy for this task');
           }
           foreach ($task->eventpurchaselist as $key => $eventpurchaselist) {
             if(!EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get()->isEmpty()){
              $sum_equip_price = $sum_equip_price + EventPurchaseEquipList::where('event_purchase_list_id',$eventpurchaselist->id)->get()->sum('event_equip_price');
             }
             else{
              $sum_price = $sum_price + $eventpurchaselist->event_total_price;
             }

             if(count($task->eventpurchaselist) == ++$key) {
              $data = array(
                    'message' => "You have a request for reimbursement",
                    'redirect' => array('role' => 'finance', 'event_id' => $task->event_id, 'task_id' => $task->id),
                    'origin' => 'external',
                    );

              $role_id = Role::where('name', 'finance')->first()->id;
              $member = Member::whereHas('role', function($q) use ($role_id) {
                  $q->where('role_id', $role_id);
              })->first();

              $member->notify(new SystemNotification($data));

              }


           }
       }



       $total_deduction = $sum_price + $sum_equip_price;

      //GET THE TOTAL MONEY BUCKET - THE TOTAL DEDUCTION
      $moneyrequest = MoneyRequest::where('task_id',$id)->first();
      $total_money_bucket = $moneyrequest->amount - $total_deduction;
      if($moneyrequest->mon_req_status == 'cleared'){
          return redirect()->back()->withErrors('The Request Money for this task has been cleared');
      }



      //CHECK IF THE MONEY STATUS IS CAPABLE FOR REIMBURSEMENT
      if($total_money_bucket < 0){

       if(ReimburseRequest::where('money_request_id',$moneyrequest->id)->get()->isEmpty()){
       $reimburse = New ReimburseRequest;
       $reimburse->money_request_id = $moneyrequest->id;
       $reimburse->reimburse_amount = $total_money_bucket * -1;
       $reimburse->save();
       }
       else {
        return redirect()->back()->withErrors('You already requested reimbursement for this task');
       }

      }
      else {
        return redirect()->back()->withErrors('You can not reimburse; check the money bucket');

      }

      Event::fire(new SystemEvent(auth::id(), 'Sent a Reimbursement Request.'));

      return redirect()->back()->with('success', 'Request Sent!');
    }





    private function isTaskforvalidation($task_id)
    {
      $task = Task::find($task_id);
      if($task->event_id == null)
      {
          if(MticsPurchaseList::where('task_id',$task_id)->where('mtics_itemstatus','pending')->get()->isEmpty()){
            Task::where('id', $task_id)->update(array('task_status' => 'for validation'));
          $data = array(
            'message' => "There are purchased items to be validated.",
            'redirect' => 'admin/logistics/purchased-items',
            'origin' => 'purchased-item',
            );

            $role_id = Role::where('name', 'logistic')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
            })->first();
            $member->notify(new SystemNotification($data));
          }


      }
      else
      {
          if(EventPurchaseList::where('task_id',$task_id)->where('Event_itemstatus','pending')->get()->isEmpty()){
            Task::where('id', $task_id)->update(array('task_status' => 'for validation'));

            $reimbursement = $task->requestmoney->amount - $task->eventpurchaselist->sum('mtics_total_price');

            if ($reimbursement < 0) {

                $data = array(
                'message' => "You can now request for reimbursement.",
                'redirect' => 'admin/external/manage-event/'.$task->event_id.'/task/'.$task->id,
                'origin' => 'logistics-validation',
                );

                $role_id = Role::where('name', 'external')->first()->id;
                $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
                })->first();
                $member->notify(new SystemNotification($data));

            }

            $data = array(
            'message' => "There are purchased items to be validated",
            'redirect' => array('role' => 'logistics', 'event_id' => $task->event_id),
            'origin' => 'external',
            );

            $role_id = Role::where('name', 'logistic')->first()->id;
            $member = Member::whereHas('role', function($q) use ($role_id) {
                $q->where('role_id', $role_id);
            })->first();
            $member->notify(new SystemNotification($data));
          }
      }
    }


    private function isMoneyReqforvalidation($task_id)
    {
      $task = Task::find($task_id);
      $moneyrequest = MoneyRequest::where('task_id',$task_id)->first();
      if($task->event_id == null)
      {
        $totalmticspurchase = MticsPurchaseList::where('task_id',$task_id)->get()->sum('mtics_total_price');

          if($totalmticspurchase == $moneyrequest->amount)
          {
            $moneyrequest->mon_req_status = 'for validation';
          }
      }
      else
      {
        $totaleventpurchase = EventPurchaseList::where('task_id',$task_id)->get()->sum('event_total_price');
        if($totaleventpurchase == $moneyrequest->amount)
        {
          $moneyrequest->mon_req_status = 'for validation';
        }
      }

      $moneyrequest->save();
    }


}
