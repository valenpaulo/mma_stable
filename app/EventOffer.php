<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventOffer extends Model
{
     protected $table = 'event_offers';
	protected $fillable = [
       'activities_id','event_id','offer_name','offer_desc','offer_image','offer_amt','finance_id','offer_status'
    ];
    
}
