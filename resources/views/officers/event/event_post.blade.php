@extends('layout/layout')
@section('title')

{{$event->event_title}}

@endsection

@section('content')

<style type="text/css">
  @media only screen and (max-width: 1365px) {
    .main {
      margin-top: 70px;
    }
  }

  @media (min-width: 1366px) {
    .main {
      margin-top: 105px;
    }
  }
</style>


<div class="container main">
  <div class="row">
    <div class="col-sm-8"></div>
     <div class="col-sm-4 w3-right">
    @if($errors->any())
      @foreach ($errors->all() as $error)

      <div class="alert alert-danger alert-dismissable fade in col-sm-12" id="success-alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>
      {{$error}}
      </strong>
      </div>
      @endforeach
    @endif
    </div>
  </div>

  <div class="row w3-margin-top">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#post" class="w3-text-black">Event Post</a></li>

      <li><a data-toggle="tab" href="#programme" class="w3-text-black">Event Programme</a></li>

      <li><a data-toggle="tab" href="#gallery" class="w3-text-black">Event Gallery</a></li>

      <li><a data-toggle="tab" href="#report" class="w3-text-black">Event Financial Report</a></li>
    </ul>
  </div>

  <div class="tab-content">
    <div id="post" class="tab-pane fade in active">
      <div class="row w3-padding" align="center">
        <h1>{{$event->event_title}}</h1>
        <p>Start Date {{$event->start_date}} | End Date {{$event->end_date}}</p>
      </div>

      <div class="row" align="center">
        <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$event->theme_image) : asset('images/'.$event->theme_image) }}" style="width: 50%">
      </div>

      <br>

      <div class="row w3-padding" align="justify">
        <p><?php echo($event->description);?></p>
      </div>

      <hr>

      <div class="row">
        <div class="w3-card-4 w3-white w3-padding w3-margin">
          <div class="w3-container">
            <div class="row w3-padding-small">
              <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.Auth::user()->avatar) : asset('images/'.Auth::user()->avatar) }}" alt=" Avatar" class="w3-left w3-circle w3-margin-right w3-hide-small" style="width: 80px; height: 80px">

              <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.Auth::user()->avatar) : asset('images/'.Auth::user()->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-large w3-hide-medium" style="width: 50px; height: 50px">

              <span class="w3-right w3-large">
                <button class="btn btn-default w3-green" type="submit" form="reply"><i class="fa fa-paper-plane-o fa-fw w3-large"></i>Send</button>
              </span>

              <span>
                <strong>{{Auth::user()->username}}</strong>
                <br>
                {{Auth::user()->id_num}}
              </span>
            </div>
            <br>
            <div class="row">
              <form action="{{ url('member/'.$event->id.'/'.$event->slug.'/reply' ) }}" id="reply" method="POST">
                <div class="form-group">
                  <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="reply_body" required placeholder="Join the discussion" rows="4"></textarea>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </form>
            </div>
          </div>
        </div>
      </div>

      @foreach($event->reply as $reply)
      <div class="row w3-margin">
          <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$reply->member->avatar) :  asset('images/'.$reply->member->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-small" style="width: 80px; height: 80px">

          <img src="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$reply->member->avatar) : asset('images/'.$reply->member->avatar) }}" alt="Avatar" class="w3-left w3-circle w3-margin-right w3-hide-large w3-hide-medium" style="width: 50px; height: 50px">

          <span class="w3-margin w3-right">
            @if($reply->member->username == Auth::user()->username)
            <div class="w3-dropdown-hover w3-left w3-white">
              <a href="javascript:void(0)" class="w3-text-gray"><i class="fa fa-sort-down fa-fw w3-large"></i></a>

              <div class="w3-dropdown-content w3-card-4 panel panel-default w3-animate-zoom" style="right: 0">
                <a href="javascript:void(0)" onclick="deleteReply( {{$reply->id}} )" class="w3-text-gray w3-hover-teal" style="outline: 0px"><i class="fa fa-trash-o fa-fw w3-large"></i>Delete Reply</a>
              </div>

            </div>

            <!-- DELETE REPLY MODAL -->
                <form action="{{ url('member/profile/post/comment/' . $reply->id . '/delete' ) }}" id="formReply-{{$reply->id}}" method="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
            <!-- END DELETE REPLY MODAL -->
            @endif
          </span>

          <span>
            <strong>{{ $reply->member->first_name }} {{ $reply->member->last_name }}</strong>
            <br>
            {{$reply->created_at}}
            <br>
            <br>
            <i class="fa fa-comments-o fa-fw w3-large"></i>
            {{$reply->reply_body}}
          </span>
      </div>
      @if(!$loop->last)
      <hr>
      @endif
      @endforeach
      <br>

    </div>

    <div id="programme" class="tab-pane fade in">
      @if(!is_null($event->program))
      <div class="row w3-margin">
        <h4>{{$event->program->title}}</h4>
      </div>

      <div class="row w3-margin">
        <p>@php echo($event->program->body) @endphp</p>
      </div>

      @endif
    </div>

    <div id="gallery" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <p>
          <a href="#gallery1" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i> Add Photos</a>
        </p>
      </div>

      <!-- add photo modal -->

      <div class="modal fade" id="gallery1" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Gallery</h4>
            </div>
            <form action="{{ url('admin/docu/manage-event/'.$event->id.'/upload-photo') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <input type="file" name="upload[]" id="upload" multiple>
            </div>

            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green"><i class="fa fa-fw fa-save"></i>Save</button>
            </div>
          </form>
          </div>
        </div>
      </div>

      <!-- END modal -->

      <div class="row w3-margin-top">
      @foreach($event->gallery as $eventphoto)

        <div class="col-sm-3 col-md-3 col-lg-3">
          <div class="w3-card-4 w3-sand w3-margin">
            <div class="w3-container w3-padding">
            <div class="w3-center">
            <div align="right" class="w3-margin-bottom">
            @if($eventphoto->member->username == Auth::user()->username)
            <p>
              <a href="#delete-{{$eventphoto->id}}" data-toggle="modal"><i class="fa fa-trash-o w3-text-gray w3-large fa-fw w3-large w3-hover-opacity" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a>
            </p>
            @endif
            </div>

            <hr style="border-color: black">
            </div>

            <div class="w3-center w3-margin-bottom">
             <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$eventphoto->image_name) : asset('images/'.$eventphoto->image_name)}}" style="width: 200px; height: 200px; overflow: hidden">
            </div>

            </div>
          </div>
        </div>

        <div class="modal fade" id="delete-{{$eventphoto->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title w3-text-gray" id="myModalLabel">Delete</h4>
              </div>
              <form action="{{ url('admin/docu/manage-event/'.$eventphoto->event_id.'/gallery/'.$eventphoto->id.'/delete') }}" method="POST" enctype="multipart/form-data">
              <div class="modal-body">

                <b>Are you sure you want to delete this image?</b>

                <div class="form-group" align="center">
                  <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$eventphoto->image_name) : asset('images/'.$eventphoto->image_name)}}" style="width: 50%">
                </div>

              </div>

              <div class="modal-footer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default w3-green"> Yes</button>
                <button type="button" class="btn btn-default w3-red" data-dismiss="modal"> No</button>
              </div>
            </form>
            </div>
          </div>
        </div>

      @endforeach
      </div>
    </div>

    <div id="report" class="tab-pane fade in">
      <div class="row w3-margin-top">
      <form action="{{ url('admin/finance/financial-reports/event/'.$event->id.'/view-reports/print') }}" method="POST" enctype="multipart/form-data">
          <button type="submit" class="btn btn-default w3-teal w3-text-white" title="Edit"><i class="fa fa-eye"></i> View Liquidation Report</button>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
 window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);
</script>

@endsection
