<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBudgetName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
     Schema::table('mtics_budgets', function ($table) {
            $table->string('budget_name')->after('confirm_admin_role_id')->nullable();
        });   

          Schema::table('mtics_withdrawals', function ($table) {
            $table->integer('mtics_budget_id')->after('faculty_id')->nullable();
        });  

     Schema::table('mtics_budget_expenses', function ($table) {
            $table->longtext('expense_desc')->nullable()->change();
        });         
         Schema::create('mtics_budget_exp_cats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });  

         Schema::table('mtics_budget_breakdowns', function ($table) {
            $table->dropColumn('budget_bd_name');
            $table->integer('mtics_budget_exp_cat_id')->after('mtics_budget_id');
        });  

         Schema::table('mtics_budgets', function ($table) {
            $table->dropColumn('budget_excess_money');
        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
