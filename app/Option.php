<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    //
    protected $fillable = [
        'option', 'image',
    ];

    public function question(){
        return $this->belongsTo('App\Question', 'question_id');
    }

    public function answer(){
        return $this->hasMany('App\Answer');
    }
}
