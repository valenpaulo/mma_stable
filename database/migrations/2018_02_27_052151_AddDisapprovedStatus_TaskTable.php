<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDisapprovedStatusTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function($table)
        {
            $table->dropColumn('task_status');
        });

        Schema::table('tasks', function($table)
        {
            $table->enum('task_status',['pending', 'on-going','cancel','overdue','done','for validation','failed', 'disapproved'])->default('pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function($table)
        {
            $table->dropColumn('task_status');
        });

        Schema::table('tasks', function($table)
        {
            $table->enum('task_status',['pending', 'on-going','cancel','overdue','done','for validation','failed'])->default('pending');
        });

    }
}
