<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventPurchaselist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('event_purchase_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id');
            $table->string('event_itemname');
            $table->longtext('event_itemdesc')->nullable();
            $table->integer('event_orig_quan')->nullable();
            $table->integer('event_est_price')->nullable();
            $table->integer('event_act_quan')->nullable();
            $table->integer('event_act_price')->nullable();
            $table->longtext('event_remarks')->nullable();
            $table->integer('inventory_category_id')->nullable();
            $table->integer('external_id')->nullable();
            $table->enum('event_itemstatus',['pending','cancel','purchased'])->default('pending');
            $table->integer('event_total_price')->nullable();
            $table->integer('logistics_id')->nullable();
            $table->enum('event_inv_status',['pending','moved','unable to move'])->default('pending');
            $table->timestamps();
        });


         Schema::create('event_purchase_equip_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_purchase_list_id');
            $table->string('event_brandname');
            $table->string('event_serialnum');
            $table->longtext('event_specs');
            $table->integer('event_equip_price');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
