<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/w3.css') : asset('css/w3.css') }}">
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/bootstrap.min.css') : asset('css/bootstrap.min.css') }}">
    <link href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/fullcalendar.css') : asset('css/fullcalendar.css')}}" rel='stylesheet' />

    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/jquery-3.2.1.min.js') : asset('js/jquery-3.2.1.min.js')}}"></script>

    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/font-awesome.min.css') : asset('css/font-awesome.min.css') }}">
    <link rel="shortcut icon" href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.ico') : asset('images/logo/mtics.ico')}}" />

    <title>Calendar of Activities</title>
</head>
<body>

<div class="container">
    <div class="row" align="center">
        <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mticsBanner.png') : asset('images/logo/mticsBanner.png')}}"  style="width: 85%">
        <h2>Calendar of Activities</h2>
        <p>YR. {{$fromyear}} - {{$toyear}}</p>
        
    </div>
</div>

<div class="container" style="padding-left: 5em; padding-right: 5em; padding-top: 3em;">

@foreach($activities as $activity)
    <div class="row" align="center">
        <p><strong>{{$activity['event']->event_title}}</strong></p>
        <p><strong>{{$activity['date']}}</strong></p>
    </div>
    <p><i><strong>Description</strong></i></p>
    <p>{{$activity['event']->description}}</p>
    <p><i><strong>Goal</strong></i>:</p>
    <p>{{$activity['event']->goal}}</p>
    <br>
@endforeach
  
  <br>
  <p><strong>Prepared by:</strong></p>
  <br>
  @foreach($roles as $role)
  @if(!$role->member->isEmpty())
    @foreach($role->member as $member)
    @if($role->name == 'president')
      <p><strong>Approved by:</strong></p>
    @endif
  <p>{{$member->first_name}} {{$member->last_name}}</p>
  <p><i>{{$role->display_name}}</i></p>

    @endforeach 
  @endif
  @endforeach
  <p></p>





</div>

<script>
  document.addEventListener("DOMContentLoaded", function(event) { 
    window.print()
  });
</script>

</body>
</html>

