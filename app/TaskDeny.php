<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskDeny extends Model
{
    protected $table = 'task_denies';
	protected $fillable = [
        'task_id','validator_id','reason','created_at','updated_at'
    ];
}
