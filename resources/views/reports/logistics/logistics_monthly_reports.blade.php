<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/w3.css') : asset('css/w3.css') }}">
    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/bootstrap.min.css') : asset('css/bootstrap.min.css') }}">
    <link href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/fullcalendar.css') : asset('css/fullcalendar.css')}}" rel='stylesheet' />

    <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/jquery-3.2.1.min.js') : asset('js/jquery-3.2.1.min.js')}}"></script>

    <link rel="stylesheet" href="{{ url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/css/font-awesome.min.css') : asset('css/font-awesome.min.css') }}">
    <link rel="shortcut icon" href="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mtics.ico') : asset('images/logo/mtics.ico')}}" />

    <title>Inventory Report</title>
</head>
<body>

<div class="container">
    <div class="row" align="center">
        <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/logo/mticsBanner.png') : asset('images/logo/mticsBanner.png')}}"  style="width: 85%">
        <h2>Inventory Report</h2>
        <p>{{$selectedmonth}}</p>
    </div>
</div>

<div class="container" style="padding-left: 5em; padding-right: 5em;">

    @if(!$addeds->isEmpty())
      <div class="row">
        <h3>New Items</h3>
      </div>

      <hr>

      <div style="padding-left: 5em;">
      <div class="row">
        <div class="table-responsive">
          <table class="w3-table">
            <thead>
              <tr>
               <th>Item/s Name</th>
               <th>Quantity</th>
               <th>Category</th>
               <th>Added_at</th>
               <th>Updated_at</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">

              @foreach($addeds as $added)
              <tr>
                <td>{{$added->inv_name}}</td>
                <td>{{$added->inv_quantity}}</td>
                <td>{{$added->Inventory_Category->inv_cat_displayname}}</td>
                <td>{{$added->created_at}}</td>
                <td>{{$added->updated_at}}</td>

              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      </div>
      </div>
    @endif

    @if(!$archiveds->isEmpty())
    <hr>

    <div class="row w3-margin-top">
      <h4>Archived Items</h4>
    </div>

    <div style="padding-left: 5em;">

      <div class="row">
        <div class="table-responsive">
          <table class="w3-table">
            <thead>
              <tr>
               <th>Item/s Name</th>
               <th>Quantity</th>
               <th>Reason</th>
               <th>Added_at</th>
               <th>Updated_at</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">

              @foreach($archiveds as $archived)
              <tr>
                <td>{{$archived->inventory->inv_name}}</td>
                <td>{{$archived->archived_quantity}}</td>
                <td>{{$archived->archived_reason}}</td>
                <td>{{$archived->created_at}}</td>
                <td>{{$archived->updated_at}}</td>

              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      </div>

    </div>
    @endif

    @if(!$pending_borrows->isEmpty())

      <hr>

      <div class="row w3-margin-top">
        <h3> Borrowed Items </h3>
      </div>

      <div style="padding-left: 5em;">

      <div class="row">
        <div class="table-responsive">
          <table class="w3-table">
            <thead>
              <tr>
               <th>Borrower's Name</th>
               <th>Item's Name</th>
               <th>Quantity</th>
               <th>Added_at</th>
               <th>Updated_at</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">

              @foreach($pending_borrows as $pending_borrow)
              <tr>
                @foreach($pending_borrow->borrower_inv as $borrower_inv)
                <td>{{$pending_borrow->member->first_name}} {{$pending_borrow->member->last_name}}</td>
                <td>{{$borrower_inv->inventory->inv_name}}</td>
                <td>{{$borrower_inv->borrower_quantity}}</td>
                <td>{{$pending_borrow->created_at}}</td>
                <td>{{$pending_borrow->updated_at}}</td>

              </tr>
                @endforeach
              @endforeach

            </tbody>
          </table>
        </div>
      </div>

      </div>
    @endif

    @if(!$repairreports->isEmpty())

      <hr>

      <div class="row w3-margin-top">
        <h3> Damaged Items </h3>
      </div>

      <div style="padding-left: 5em;">

      <div class="row">
        <div class="table-responsive">
          <table class="w3-table">
            <thead>
              <tr>
               <th>Brand Name</th>
               <th>Serial</th>
               <th>Spec</th>
               <th>Report Reason</th>
               <th>Added_at</th>
               <th>Updated_at</th>
              </tr>
            </thead>
            <tbody class="w3-text-gray">

              @foreach($repairreports as $repairreport)
              <tr>

                <td>{{$repairreport->equip_inv->brand_name}}</td>
                <td>{{$repairreport->equip_inv->serial_num}}</td>
                <td>{{$repairreport->equip_inv->specs}}</td>
                <td>{{$repairreport->report_desc}}</td>
                <td>{{$repairreport->created_at}}</td>
                <td>{{$repairreport->updated_at}}</td>

              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      </div>

      </div>
    @endif


</div>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    window.print()
  });
</script>

</body>
</html>

