<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAuditorFinancialApproved extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
        Schema::table('auditor_financial_approves', function ($table) {
            $table->string('month')->nullable()->change();
            $table->integer('event_id')->nullable();
        });

        Schema::table('events', function ($table) {
            $table->enum('post_status',['pending','posted'])->default('pending')->after('status');
            $table->integer('post_admin_id')->nullable()->after('post_status');
            $table->integer('post_admin_role_id')->nullable()->after('post_admin_id');
        });

        Schema::table('roles', function ($table) {
            $table->enum('assistant',['true','false'])->default('false')->after('display_name')->nullable();
        });

        Schema::table('events', function ($table) {
            $table->string('theme_image')->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
