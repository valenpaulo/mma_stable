@extends('layout/layout')

@section('title')
Additional Feature
@endsection

@section('content')

<!-- 
<div class="container-fluid">

<div class="w3-container w3-text-black w3-center">
  <a href="{{route('home')}}">
  <img src="{{asset('public/images/logo/mtics.png')}}" class="w3-margin-bottom w3-margin-top" style="height: 100px; width: 100px"></a>
  <h4>Manila Technician Institute Computer Society</h4>

</div>

<div class="container" align="center">
  @can('superadmin-only')
  <a class="w3-text-teal w3-xlarge" href="{{route('admin.manage.user')}}" title="Manage Member"><i class="fa fa-user-circle fa-fw"></i></a>
  @endcan

  @if(Gate::allows('accntreq_approval-only'))
  <a class="w3-text-teal w3-xlarge" href="{{route('admin.request.account')}}" title="Manage Request Account"><i class="fa fa-thumbs-up fa-fw"></i></a>
  @endif

  @can('superadmin-only')
  <a class="w3-text-teal w3-xlarge" href="{{route('admin.manage.privilege')}}" title="Manage Privilege"><i class="fa fa-key fa-fw"></i></a>
  @endcan

  @can('superadmin-only')
  <a class="w3-text-teal w3-xlarge" href="{{route('admin.manage.section')}}" title="Manage Sections"><i class="fa fa-group fa-fw"></i></a>
  @endcan

  @if(Gate::allows('BorrowReturn-only') AND !Gate::allows('logistics-only'))
  <a class="w3-text-teal w3-xlarge" href="{{route('admin.borrow.return')}}" title="Manage Borrow/Return"><i class="fa fa-database fa-fw"></i></a>
  @endif

  @if(Gate::allows('ReceivePayment-only') AND !Gate::allows('finance-only'))
  <a class="w3-text-teal w3-xlarge" href="{{route('admin.manage.payment')}}" title="Manage Payment"><i class="fa fa-money fa-fw"></i></a>
  @endif
</div>

<hr style="border: 0.5px solid gray ">

<div class="container-fluid w3-margin-right">
  <div class="row">
    <div class="col-sm-8"></div>
    <div class="col-sm-4">
    @if($errors->any())
      @foreach ($errors->all() as $error)
      <div class="alert alert-danger alert-dismissable fade in w3-right col-sm-12" id="success-alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>
      {{$error}}
      </strong>
      </div>
      @endforeach
    @endif
    </div>
  </div>
</div>
<br> -->
<br><br><br><br><br>

@yield('middle')
</div>
@endsection
