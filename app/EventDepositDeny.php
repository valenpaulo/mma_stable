<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDepositDeny extends Model
{
    protected $table = 'event_deposit_denies';
	protected $fillable = [
      'event_deposit_id','faculty_id','denied_reason'
    ];
     public function faculty() {
        return $this->belongsTo(Member::class, 'faculty_id');
    }
}
