<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTotalAmountinMticsPurchaseList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct()
    {
    DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
        Schema::table('mtics_purchase_lists', function ($table) {
            $table->integer('mtics_total_price')->nullable();
            $table->enum('mtics_inv_status',['pending','moved'])->default('pending');
           
            
        });

         Schema::table('mtics_purchase_equip_lists', function ($table) {
            $table->integer('mtics_equip_price');
            $table->longtext('mtics_specs')->change();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
