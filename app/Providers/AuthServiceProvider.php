<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Member;
use Auth;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


        Gate::define('admin-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            foreach($memberrole as $role)
            {
            if($role->name)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        });

        Gate::define('superadmin-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='super_admin' or $role->name=='president' or $role->name=='vice_president' or $role->name=='mtics_adviser' )
                {
                    $value=true;
                }
            }
            return $value;

        });


        Gate::define('superadminrole-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='super_admin' )
                {
                    $value=true;
                }
            }
            return $value;

        });

         Gate::define('accntreq_approval-only', function()
        {
            $memberaction = Member::find(Auth::user()->id);
            //dd($memberaction->action);
            $value=false;
            foreach($memberaction->action as $action)
            {
            if($action->action_name=='acctreq_approval')
                {
                    $value=true;
                }
            }
            return $value;

        });

        Gate::define('president-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='president' or $role->name=='vice_president' )
                {
                    $value=true;
                }
            }
            return $value;
        });

         Gate::define('logistics-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='logistic' or $role->name=='asst_logistic')
                {
                    $value=true;
                }
            }
            return $value;

        });


          Gate::define('finance-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='finance' or $role->name=='asst_finance')
                {
                    $value=true;
                }
            }
            return $value;

        });

          Gate::define('docu-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='docu' or $role->name=='asst_docu')
                {
                    $value=true;
                }
            }
            return $value;

        });

           Gate::define('internal-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='internal' or $role->name=='asst_internal')
                {
                    $value=true;
                }
            }
            return $value;

        });


        Gate::define('info-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='info' or $role->name=='asst_info')
                {
                    $value=true;
                }
            }
            return $value;

        });

        Gate::define('external-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='external' or $role->name=='asst_external')
                {
                    $value=true;
                }
            }
            return $value;

        });

        Gate::define('auditor-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='auditor' or $role->name=='asst_auditor')
                {
                    $value=true;
                }
            }
            return $value;

        });

        Gate::define('faculty-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='faculty')
                {
                    $value=true;
                }
            }
            return $value;

        });

        Gate::define('activity-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='activity' or $role->name=='asst_activity')
                {
                    $value=true;
                }
            }
            return $value;

        });

         Gate::define('mticsadviser-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;

            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='mtics_adviser')
                {
                    $value=true;
                }
            }
            return $value;

        });



           Gate::define('BorrowReturn-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;
            $memberaction = Member::find(Auth::user()->id);
            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='logistic' or $role->name=='asst_logistic')
                {
                    $value=true;
                }
            }

            foreach($memberaction->action as $action)
            {

            if($action->action_name=='borrowed_return_items')
                {
                    $value=true;
                }
            }

            return $value;

        });


        Gate::define('ReceivePayment-only', function()
        {
            $memberrole = Member::find(Auth::id())->role;
            $memberaction = Member::find(Auth::user()->id);
            /*dd($memberrole);*/
            $value=false;
            foreach($memberrole as $role)
            {
            if($role->name=='finance' or $role->name=='asst_finance')
                {
                    $value=true;
                }
            }

            foreach($memberaction->action as $action)
            {

            if($action->action_name=='receive_payment')
                {
                    $value=true;
                }
            }

            return $value;

        });









    }


}
