<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDeposit extends Model
{
     protected $table = 'event_deposits';
	protected $fillable = [
        'admin_id','faculty_id','deposit_desc','deposit_amt','deposit_status','deposit_source','event_id','remarks'
    ];

    public function event()
    {
        return $this->belongsto(MticsEvent::class);
    }

    public function faculty() {
        return $this->belongsTo(Member::class, 'faculty_id');
    }

    public function receipt() {
        return $this->hasOne(EventDepositReceipt::class);
    }


     public function deny() {
        return $this->hasOne(EventDepositDeny::class,'event_deposit_id');
    }

    public function report() {
        return $this->hasOne(EventDepositReport::class,'event_deposit_id');
    }
}
