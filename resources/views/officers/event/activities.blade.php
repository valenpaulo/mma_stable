@extends('layout/control_panel')

@section('title')
{{$event->event_title}}
@endsection

@section('middle')

<style type="text/css">
  .no-border, .no-border:focus {
    border: 0;
    box-shadow: none;
    outline: none; /* You may want to include this as bootstrap applies these styles too */
  }

  .input-lg {
    font-size: 32px
  }
</style>

<div class="w3-container w3-margin">
  <div class="row">
    <h3>
      <i class="fa fa-calendar fa-fw w3-xxlarge"></i>
      <strong>Manage Event</strong>
    </h3>
  </div>

  <hr>

  <div class="row">

    <ul class="nav nav-tabs">
      <li class="active">
        <a data-toggle="tab" href="#task" class="w3-text-black">Task</a>
      </li>

      <li>
        <a data-toggle="tab" href="#programme" class="w3-text-black">Programme</a>
      </li>

      <li>
        <a data-toggle="tab" href="#offers" class="w3-text-black">Event Offers</a>
      </li>
    </ul>
  </div>

  <div class="tab-content">
    <div id="task" class="tab-pane fade in active">
      <!-- ACTIVITIES TASK -->
        <div class="row w3-margin-top">

          <h4>
              <i class="fa fa-tasks fa-fw"></i>
              <strong>Task</strong>
          </h4>

        </div>

        <div class="row">
          <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                   <th class="w3-center">Task Name</th>
                   <th class="w3-center">Description</th>
                   <th class="w3-center">Due Date</th>
                   <th class="w3-center">Status</th>
                   <th class="w3-center">Action</th>
                  </tr>
                </thead>
                <tbody class="w3-text-gray">
                @foreach($activity_tasks as $task)
                  <tr>
                    <td class="w3-center">{{$task->task_name}}</td>
                    <td class="w3-center">{{$task->task_desc}}</td>
                    <td class="w3-center">{{$task->due_date}}</td>
                    <td class="w3-center">{{$task->task_status}}</td>
                    <td class="w3-center">
                      @if ($task->task_status == 'pending')
                      <a class="w3-text-green" href="{{url('admin/finance/manage-event/start-task/' . $task->id)}}"><i class="fa fa-play fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Start Task"></i></a>
                      @elseif ($task->task_status == 'on-going' || $task->task_status == 'disapproved')

                      <a class="w3-text-green" href="{{url('admin/finance/manage-event/validate/'.$task->id)}}"><i class="fa fa-check-square fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Validate"></i></a>

                      @elseif ($task->task_status == 'for validation')
                      <i class="fa fa-check-square fa-fw w3-large" data-toggle="tooltip" data-placement="bottom" title="Validate"></i>
                      @endif
                    </td>

                  </tr>


                @endforeach
               </tbody>
              </table>
          </div>
        </div>
    </div>

    <div id="programme" class="tab-pane fade in">
      <!-- EDIT EVENT PROGRAM -->
      <script src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/js/tinymce/js/tinymce/tinymce.min.js') : asset('js/tinymce/js/tinymce/tinymce.min.js')}}"></script>

      <script>tinymce.init({selector:'.thisarea', plugins: "autoresize"});</script>

      <div class="w3-card-4 w3-white w3-container w3-padding w3-margin">
        <br>
        <form action="{{ url('admin/activities/manage-event/'.$event->id.'/add-event-program') }}" method="post" enctype="multipart/form-data">

          <div class="form-group" align="right">
             <button type="submit" class="btn btn-default w3-green"><i class="fa fa-pencil-square-o fa-fw w3-large"></i> Publish</button>
          </div>

          <hr>

          <div class="row">
            <div class="form-group">
              <div class="col-lg-8 col-md-8">
                <strong>
                  <input type="text" name="title" id="title" class="form-control no-border input-lg w3-text-black" placeholder="Title" value="{{$event->program ? $event->program->title : ''}}" required>
                </strong>
              </div>
            </div>

          </div>

          <hr>

          <div class="form-group">
            <textarea rows="2" placeholder="what's on your mind?" name="body" class="form-control thisarea">{{$event->program ? $event->program->body : ''}}</textarea>
          </div>

          <hr>

          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>

      </div>

    </div>

    <div id="offers" class="tab-pane fade in">
      <div class="row w3-margin-top">
        <p>
          <a href="#addOffer" data-toggle="modal" class="w3-text-gray" style="outline: 0"><i class="fa fa-plus-square-o fa-fw"></i> Add</a>
        </p>
      </div>

      <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="eventOffer">
              <thead>
                <tr>
                 <th class="w3-center">Item</th>
                 <th class="w3-center">Amount</th>
                 <th class="w3-center">Image</th>
                 <th class="w3-center">Description</th>
                 <th class="w3-center">Action</th>
                </tr>
              </thead>
              <tbody class="w3-text-gray">
              @foreach($event_offers as $offer)
                <tr>
                  <td class="w3-center">{{$offer->offer_name}}</td>
                  <td class="w3-center">{{$offer->offer_amt}}</td>
                  <td class="w3-center">
                    <img src="{{url('/') == 'http://mtics-ma.tuptaguig.com' ? asset('public/images/'.$offer->offer_image) : asset('images/'.$offer->offer_image)}}" style="width: 140px">
                  </td>
                  <td class="w3-center">{{$offer->offer_desc}}</td>
                  <td class="w3-center">
                    <a href="#editOffer-{{$offer->id}}" class="w3-text-orange w3-large" data-toggle="modal">
                      <i class="fa fa-pencil-square-o fa-fw" data-toggle="tooltip" data-placement="bottom" title="Edit Offer"></i>
                    </a>
                  </td>

                <!-- EDIT OFFERS -->
                <div class="modal fade" id="editOffer-{{$offer->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title w3-text-gray" id="myModalLabel">Edit Event Offer</h4>
                      </div>
                    <form action="{{ url('admin/activities/event-offers/'.$event->id.'/edit/'.$offer->id) }}" method="POST" enctype="multipart/form-data">
                      <div class="modal-body">
                        <div class="w3-container">
                          <div class="row">

                            <div class="form-group">
                              <label id="image-file-name" for="offer_image" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-text fa-fw w3-text-green w3-large" ></i> Upload Image</label>
                              <input type="file" name="offer_image" id="offer_image" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showImageFileName(event)" />
                            </div>

                            <div class="form-group">
                              <input type="text" name="offer_name" id="offer_name" tabindex="1" class="form-control" placeholder="Item Name" value="{{$offer->offer_name}}">
                            </div>

                            <div class="form-group">
                              <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="offer_desc" placeholder="Description" rows="3">{{$offer->offer_desc}}</textarea>
                            </div>

                          </div>
                        </div>



                      </div>
                      <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                      </div>
                    </form>
                    </div>
                  </div>
                </div>
                <!-- END MODAL -->

              @endforeach
              </tr>
             </tbody>
            </table>
        </div>
      </div>

      <!-- ADD OFFERS -->
      <div class="modal fade" id="addOffer" tabindex="100" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title w3-text-gray" id="myModalLabel">Add Event Offer</h4>
            </div>
          <form action="{{ url('admin/activities/event-offers/'.$event->id.'/store') }}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="w3-container">
                <div class="row">

                  <div class="form-group">
                    <label id="image-file-name" for="offer_image" style="cursor: pointer;" title="Bulk Add"><i class="fa fa-file-text fa-fw w3-text-green w3-large" ></i> Upload Image</label>
                    <input type="file" name="offer_image" id="offer_image" accept="image/*" style="opacity: 0; position: absolute; z-index: -1;" onchange="showImageFileName(event)" />
                  </div>

                  <div class="form-group">
                    <input type="text" name="offer_name" id="offer_name" tabindex="1" class="form-control" placeholder="Item Name">
                  </div>

                  <div class="form-group">
                    <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="offer_desc" placeholder="Description" rows="3"></textarea>
                  </div>

                </div>
              </div>



            </div>
            <div class="modal-footer">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
            </div>
          </form>
          </div>
        </div>
      </div>
      <!-- END MODAL -->
    </div>
  </div>

</div>

<script>
  // var input = document.getElementById( 'receipt' );

  // input.addEventListener('change', showFileName1);

  function showImageFileName( event ) {

    // the change event gives us the input it occurred in
    var infoArea = document.getElementById( 'image-file-name' );
    var input = event.srcElement;

    // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
    var fileName = input.files[0].name;

    // use fileName however fits your app best, i.e. add it into a div
    infoArea.textContent = fileName;
  };
</script>

<script type="text/javascript">
$(document).ready( function () {
  $('#eventOffer').DataTable();
} );
</script>
@endsection
