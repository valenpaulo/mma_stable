<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventWithdrawalReceipt extends Model
{
    protected $table = 'event_withdrawal_receipts';
	protected $fillable = [
   		'event_withdrawal_id','faculty_id','receipt_image'
    ];
}
