<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDepositReport extends Model
{
    protected $table = 'event_deposit_reports';
	protected $fillable = [
      'event_deposit_id','faculty_id','deposit_title','deposit_desc'
    ];

     public function faculty() {
        return $this->belongsTo(Member::class, 'faculty_id');
    }

}
