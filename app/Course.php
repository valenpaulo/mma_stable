<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
     protected $table = 'courses';
	protected $fillable = [
        'course_code','course_name','course_year_count'
    ];

     public function section()
    {
        return $this->hasMany(Section::class);
    }
}
