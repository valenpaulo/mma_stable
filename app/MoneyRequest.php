<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoneyRequest extends Model
{
    protected $table = 'money_requests';
	protected $fillable = [
        'amount','mon_req_status','remarks','created_at','updated_at','task_id','excess_amount','member_requested_id','admin_receive_id','admin_affirm_id','admin_confirm_id','denied_reason'
    ];



    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }

    public function reimburse()
    {
        return $this->hasOne(ReimburseRequest::class);
    }
}
