<?php

namespace App\Http\Controllers;

use Alert;
use App\Http\Requests;
use Artisan;
use Log;
use Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;


class BackupController extends Controller
{
    public function index()
    {
         if(!Gate::allows('superadmin-only')) {
            return redirect('/');
        }
        $disk = Storage::disk(config('laravel-backup.backup.destination.disks')[0]);

        $files = $disk->files(config('laravel-backup.backup.name'));
        
        $backups = [];
        $count = [ 'full' => 0, 'site' => 0, 'db' => 0 ];
        // make an array of backup files, with their filesize and creation date
        foreach ($files as $k => $f) {
            // only take the zip files into account
            if ((substr($f, -4) == '.zip' || substr($f, -4) == '.sql') && $disk->exists($f)) {
                $filename = str_replace(config('laravel-backup.backup.name') . '/', '', $f);
                $backups[] = [
                    'file_path' => $f,
                    'file_name' => $filename,
                    'file_size' => $this->human_filesize($disk->size($f)),
                    'last_modified' => $this->getdate($disk->lastModified($f)),
                ];
                if (strstr($filename, 'full')) $count['full'] += 1;
                if (strstr($filename, 'site')) $count['site'] += 1;
                if (strstr($filename, 'db')) $count['db'] += 1;
            }
        }
        // reverse the backups, so the newest one would be on top
        $backups = array_reverse($backups);

        return view("backups", ['count'=>$count])->with(compact('backups'));
    }

    public function create()
    {
        try {
            // start the backup process
            Artisan::call('backup:run');

            $output = Artisan::output();
           
            // return redirect()->back()->withErrors([
            //     'success'=>config()->get('constants.messages.backup_success')
        return redirect('admin/backup')->
        with('success', 'System Backup Successfully');

            // ]);
            // log the results
            // Log::info("Backpack\BackupManager -- new backup started from admin interface \r\n" . $output);
            // // return the results as a response to the ajax call
            // // Alert::success('New backup created');
            // return redirect()->back();
        }
        catch (Exception $e)
        {
            // return redirect()->back()->withErrors([
            //     'error'=>config()->get('constants.messages.backup_error')
            // ]);
        return redirect('admin/backup')->
        with('warning', 'System Not Backup');
        }
    }
    public function human_filesize($bytes,$decimals = 2)
    {
        if($bytes < 1024){
            return $bytes . ' B';
        }
        $factor = floor(log($bytes,1024));
        return sprintf("%.{$decimals}f ", $bytes / pow(1024,$factor)) . ['B','KB','MB','GB','TB','PB'][$factor];

    }

 public function getdate($date_modify) {
                return Carbon::createFromTimeStamp($date_modify)->formatLocalized(' %d %B %Y %H:%M');

    }
    

    public function createdb()
    {
        try
        {
            Artisan::call('backup:mysql-dump', ['filename' =>date('Y-m-d').'-'.time().'-db']);

            $output = Artisan::output();

            

            return redirect('admin/backup')->with('success', 'Database Backup Successfully!');
        

        }
        catch (Exception $e)
        {
            return redirect('admin/backup')->with('warning', 'Database Backup Failed!');
        }
    }
    /**
     * Downloads a backup zip file.
     *
     * TODO: make it work no matter the flysystem driver (S3 Bucket, etc).
     */
    public function download($file_name)
    {
        $file = config('laravel-backup.backup.name') . '/' . $file_name;
        $disk = Storage::disk(config('laravel-backup.backup.destination.disks')[0]);
        if ($disk->exists($file)) {
            $fs = Storage::disk(config('laravel-backup.backup.destination.disks')[0])->getDriver();
            $stream = $fs->readStream($file);

            return \Response::stream(function () use ($stream) {
                fpassthru($stream);
            }, 200, [
                "Content-Type" => $fs->getMimetype($file),
                "Content-Length" => $fs->getSize($file),
                "Content-disposition" => "attachment; filename=\"" . basename($file) . "\"",
            ]);
        } else {
            abort(404, "The backup file doesn't exist.");
        }
    }

    /**
     * Deletes a backup file.
     */
    public function delete($file_name)
    {
        $disk = Storage::disk(config('laravel-backup.backup.destination.disks')[0]);
        if ($disk->exists(config('laravel-backup.backup.name') . '/' . $file_name)) {
            $disk->delete(config('laravel-backup.backup.name') . '/' . $file_name);
            return redirect('admin/backup')->with('success', 'Deleted Successfully!');
        } else {
            abort(404, "The backup file doesn't exist.");
        }
    }

     public function restoredb($filename)
    {
            
        try
        {
            
            Artisan::call('backup:mysql-restore', ['--filename' => $filename, '--yes'=>' ']);
            $output = Artisan::output();

            return redirect('admin/backup')->with('success', 'Database Restored Successfully!');
        }
        catch (Exception $e)
        {
            return redirect('admin/backup')->with('warning', 'Database Restoration Failed!');
        }
    }

  
}
