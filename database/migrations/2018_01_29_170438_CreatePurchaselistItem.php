<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaselistItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::drop('mtics_purchase_lists');
         Schema::create('mtics_purchase_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id');
            $table->string('mtics_itemname');
            $table->longtext('mtics_itemdesc')->nullable();
            $table->integer('mtics_orig_quan')->nullable();
            $table->integer('mtics_est_price')->nullable();
            $table->integer('mtics_act_quan')->nullable();
            $table->integer('mtics_act_price')->nullable();
            $table->longtext('mtics_remarks')->nullable();
            $table->integer('inventory_category_id');
            $table->enum('mtics_itemstatus',['pending','cancel','purchased'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
