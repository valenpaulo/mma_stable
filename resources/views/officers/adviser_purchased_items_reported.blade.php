@extends('layout/control_panel')

@section('title')
Issues on Purchased Item
@endsection

@section('middle')
<div class="w3-container w3-margin">
    <div class="row">
        <h3>
          <i class="fa fa-exclamation-triangle fa-fw w3-xxlarge"></i>
          <strong>Issues on Purchased Items</strong>
        </h3>
    </div>

    <hr>

    <div class="row w3-margin-top">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#mticsTab" class="w3-text-black">MTICS @if($reportcount > 0)<span class="w3-badge w3-red">{{$reportcount}}</span>@endif</a></li>

            <li><a data-toggle="tab" href="#eventTab" class="w3-text-black">Event
              @if(count($eventreported)>0)
              <span class="w3-badge w3-red">{{count($eventreported)}}</span>
              @endif
            </a></li>
        </ul>
    </div>

    <div class="tab-content">

        <div id="mticsTab" class="tab-pane fade in active">
            <div class="row w3-margin-top">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="mticsTable">
                    <thead>
                      <tr>
                       <th class="w3-center">Item Name</th>
                       <th class="w3-center">Reported By</th>
                       <th class="w3-center">Name</th>
                       <th class="w3-center">Reason</th>
                       <th class="w3-center">Explanation</th>
                       <th class="w3-center">Category</th>
                       <th class="w3-center">Solution for Critical Issue</th>
                       <th class="w3-center">Action</th>
                      </tr>
                    </thead>
                    <tbody class="w3-text-gray">
                      @foreach($mticsreported as $reported)
                      <tr>
                        <td class="w3-center">{{$reported->purchaselist->mtics_itemname}}</td>
                        <td class="w3-center">{{$reported->reportedby}}</td>
                        <td class="w3-center">{{$reported->report_name}}</td>
                        <td class="w3-center">{{$reported->report_reason}}</td>
                        <td class="w3-center">{{$reported->external_explanation}}</td>
                        <td class="w3-center">{{$reported->report_category}}</td>
                        <td class="w3-center">{{$reported->solution_fr_critical}}</td>
                        <td class="w3-center">

                        @if(!$reported->solution_fr_critical)
                        <a class="w3-text-green w3-large" data-toggle="modal" href="#mticssolution-{{$reported->id}}" style="outline: 0"><i class="fa fa-wrench" data-toggle="tooltip" data-placement="bottom" title="Add Solution"></i></a>
                        @else
                        <i class="fa fa-check-circle-o w3-large w3-text-green fa-fw" data-toggle="tooltip" data-placement="bottom" title="Resolved"></i>
                        @endif

                        <!-- ADD SOLUTION -->

                        <div class="modal fade" id="mticssolution-{{$reported->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                        <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title w3-text-gray" id="myModalLabel">Solution</h4>
                        </div>
                        <form action="{{ url('admin/president/purchased-item-reported/'.$reported->id.'/add-mtics-solution') }}" method="POST" enctype="multipart/form-data">


                        <div class="modal-body">
                            <div class="w3-container">
                                <div class="row">

                                    <div class="form-group">
                                        <p><b>Solution: </b></p>
                                        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_solution" placeholder="Solution" rows="3"></textarea>
                                    </div>

                                    @if($reported->reportedby !== 'finance')
                                        <select name="inv_cat_id" class="form-control">
                                          @foreach($inventory_categories as $inventory_category)
                                              <option value="{{$inventory_category->id}}">{{$inventory_category->inv_cat_displayname}}</option>
                                          @endforeach
                                        </select>

                                        <div class="checkbox">
                                          <label><input type="checkbox" name="dontmove" value="yes">Do not move this to Inventory</label>
                                        </div>
                                    @else
                                         <div class="checkbox">
                                          <label><input type="checkbox" name="dontmove" value="yes" hidden checked></label>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                        </div>

                        </form>
                        </div>
                        </div>
                        </div>
                        <!--ADD SOLUTION END MODAL -->

                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="eventTab" class="tab-pane fade in">
            <div class="row w3-margin-top">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="eventTable">
                    <thead>
                      <tr>
                       <th class="w3-center">Item Name</th>
                       <th class="w3-center">Reported By</th>
                       <th class="w3-center">Name</th>
                       <th class="w3-center">Reason</th>
                       <th class="w3-center">Explanation</th>
                       <th class="w3-center">Category</th>
                       <th class="w3-center">Solution for Critical Issue</th>
                       <th class="w3-center">Action</th>
                      </tr>
                    </thead>
                    <tbody class="w3-text-gray">
                      @foreach($eventreported as $reported)
                      <tr>
                        <td class="w3-center">{{$reported->purchaselist->event_itemname}}</td>
                        <td class="w3-center">{{$reported->reportedby}}</td>
                        <td class="w3-center">{{$reported->report_name}}</td>
                        <td class="w3-center">{{$reported->report_reason}}</td>
                        <td class="w3-center">{{$reported->external_explanation}}</td>
                        <td class="w3-center">{{$reported->report_category}}</td>
                        <td class="w3-center">{{$reported->solution_fr_critical}}</td>
                        <td class="w3-center">

                        @if (!$reported->solution_fr_critical)
                        <a class="w3-text-green w3-large" data-toggle="modal" href="#eventsolution-{{$reported->id}}" style="outline: 0"><i class="fa fa-wrench" data-toggle="tooltip" data-placement="bottom" title="Add Solution"></i></a>
                        @else
                        <i class="fa fa-check-circle-o fa-fw w3-large w3-text-green" data-toggle="tooltip" data-placement="bottom" title="Resolved"></i>
                        @endif

                        <!-- ADD SOLUTION -->

                        <div class="modal fade" id="eventsolution-{{$reported->id}}" tabindex="100" role="dialog" aria-labelledby="myModalLabel" align="left">
                        <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title w3-text-gray" id="myModalLabel">Solution</h4>
                        </div>
                        <form action="{{ url('admin/president/purchased-item-reported/'.$reported->id.'/add-event-solution') }}" method="POST" enctype="multipart/form-data">


                        <div class="modal-body">
                            <div class="w3-container">
                                <div class="row">

                                    <div class="form-group">
                                        <p><b>Solution: </b></p>
                                        <textarea class="form-control richTextBox" tabindex="1" id="richtextbody" name="report_solution" placeholder="Solution" rows="3" required=""></textarea>
                                    </div>

                                    @if($reported->reportedby !== 'finance')
                                        <select name="inv_cat_id" class="form-control">
                                          @foreach($inventory_categories as $inventory_category)
                                              <option value="{{$inventory_category->id}}">{{$inventory_category->inv_cat_displayname}}</option>
                                          @endforeach
                                        </select>

                                        <div class="checkbox">
                                          <label><input type="checkbox" name="dontmove" value="yes">Do not move this to Inventory</label>
                                        </div>
                                    @else
                                         <div class="checkbox">
                                          <label><input type="checkbox" name="dontmove" value="yes" hidden checked></label>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default w3-green"><i class="fa fa-save fa-fw"></i> Save</button>
                        </div>

                        </form>
                        </div>
                        </div>
                        </div>
                        <!--ADD SOLUTION END MODAL -->

                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>

<script type="text/javascript">
$(document).ready( function () {
  var mticsTable = $('#mticsTable').DataTable();
  var eventTable = $('#eventTable').DataTable();

  $(mticsTable.table().container()).removeClass('form-inline');
  $(eventTable.table().container()).removeClass('form-inline');

});
</script>

@endsection
