<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MticsBudget extends Model
{
    protected $table = 'mtics_budgets';
	protected $fillable = [
       'finance_id','confirm_admin_id','confirm_admin_role_id','budget_month','budget_amt','budget_remarks','budget_status','receipt_id'
    ];

    public function budget_breakdown()
    {
    	return $this->hasMany(MticsBudgetBreakdown::class,'mtics_budget_id');
    }

    public function deny() {
        return $this->hasMany(MticsBudgetDeny::class);
    }

  
    public function withdraw() {
        return $this->hasMany(MticsWithdrawal::class);
    }

     public function withdraw1() {
        return $this->hasone(MticsWithdrawal::class,'mtics_budget_id');
    }

    public function receipt() {
        return $this->belongsto(Receipt::class);
    }
}
