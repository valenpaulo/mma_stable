<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = 'inventories';
	protected $fillable = [
        'inv_name','inv_quantity','inventory_category_id','created_at'
    ];

    public function Inventory_Category(){
        return $this->belongsTo('App\InventoryCategory', 'inventory_category_id');
    }

    public function equipment() {
        return $this->hasMany(Equip_Inventory::class);
    }

    public static function getId($model, $table, $value)
   {
    return $model::where($table, $value)->first()->id;
    }

}
